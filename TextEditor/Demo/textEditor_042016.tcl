#****h* CLIENT/textEditor
# NAME
#  texteditor
#                                       Client zum
#          Forschungs- und Dokumentserver FuD
#                     SFB 600
#                       und
#     Kompetenzzentrum fuer elektronische Erschliessungs-
#    und Publikationsverfahren in den Geisteswissenschaften
#              an der Universitaet Trier 
# DESCRIPTION
#    
#
# COPYRIGHT
#    (c) 2013 -2015 by
#               Xin Zhou,
#               Matthias Bremm,
#               Radoslav Petkov
#               Universitaet Trier
#       SFB 600 "Fremdheit und Armut"
#       und Kompetenzzentrum fuer elektronische Erschliessungs-
#       und Publikationsverfahren in den Geisteswissenschaften
#       an der Universitaet Trier (Fachbereich II Germanistik)
#
#       All Rights reserved.
#
# CREATION DATE 
#  01.07.2013
#
# HISTORY
#       *02.09.2014 XZ Change the selected PNG-File to binary, then create the image.
#       *01.10.2014 XZ Bug fixed: textTable::tableControl; setColor with "more colors"; changeStyleW -> focus $::searchTE::lastTextWidget
#       *07.10.2014 XZ Bug fixed: textModiMsg: independent tiny System; (#2701) More CR behind the "P" tags;
#       *15.10.2014 XZ Bug fixed: #2715 ; replace/replaceAll buttons can be correctly triggered
#       *20.10.2014 XZ Bug fixed: TE down when Copy-Paste; xmlData(hasHeadStyle) make link/anchor chaos;
#       *13.11.2014 XZ Bug fixed: Suche - Fehlermeldung bei Eingabe von Suchstring mit Leerzeichen (#2911)
#       *25.11.2014 XZ Take the p Paragraph Entities away from xml/Html file.
#       *22.05.2015 RP (#3306) invokeSearchDialog: adapt size of framelabels in "Search" window
#			
# NOTES
#
# BUGS
#       * bug 2715
#       * bug 2258
#
# TODO
# * need some detect when load the file to protect memory
# * data structure require some refactoring
# * Algorithms of Analyze HTML/XML attribute require optimizing
# * timestamp: ::textEditorMain::timestamp
# *-> table resizable!!!
# * rgb analyse
# * undo and redo
#
# SEE ALSO  
#
# NOTIZ
#  Bitte ab sofort den Quelltext im Stil von ROBODoc Format dokumentieren
#  http://www.xs4all.nl/~rfsber/Robo/
#  Anleitung: http://www.xs4all.nl/~rfsber/Robo/Manual/manual.html
#   bzw. die bereits dokumentierten Beispiel
#  Aktuelle Doku unter: http://gepc189.uni-trier.de/work/FuDdocfiles/toc_index.html 
#****

#����
# check if already loaded
if {[info exists textEditor::version]} {puts $textEditor::version;return}

package provide textEditor 0.0.5

#****** textEditor/images
# NAME
#  TEUtils
# NAMESPACE
#  namespace
# DESCRIPTION
#  Images for text editior.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  06.05.2014
# SYNOPSIS
namespace eval images {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        namespace export *
# USES
#  Variables:
#    NONE
#  Procedures:
#        NONE
#****   
        
        variable pngImags
        array set pngImags {}
        
        
        proc setPngImgs {} {
                variable ::images::pngImags
                
        set pngImags(timestampImg) {
                iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABEElEQVR42p2TP86CQBDFVw/AfwpC
                oMDGY3EB6CwsPqisSAwNBTUdHRUXsLCQC1BwAm3UfvWZDxJ1dzS+ZJNl5vcm7OwsY+9aqKq6cV13
                bxjGEQt7xJBjhGZ3aOX7/qmqKt73PR8T2COGHBiwr+a5bdu7MAwv7IPAgIVnCiqK8hdF0fkVbpqG
                i4qAhWf8XgZBcBSBZVly2Z/8e5bMsqxtXddCsCgKaQF44GWO4xyGYRCCeZ5LC8ADL8M1yaAsyzjV
                0IeXKtC2LU+ShKdp+ra6ruMPL3UEStMRqCZSmppIXSOl6RohTdOSOI4v35rBwvM0yqZp7u9jev1i
                lK9gn0Z5LKLr+trzPOljQg6MyPzzc74BiqKVZUR+yzgAAAAASUVORK5CYII=
        }
        
        
        set pngImags(paragraph) {
            iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
            AAAAbwAAAG8B8aLcQwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAB1SURB
            VDiN7c6xCQJREATQ54GF2M6ZqNG1Yg92IoiRlqN9iI6B/+BAkDt/YuDAJsvOYyWBGVY44YI7MnLM
            S3Fs4Q3YfTpIonzZDfZdv2+wUZGmptwDh1pgi/PXQJIbllgX6IrHlA/klWOSNskC+0lATf7ALwBP
            /YJM17alE+MAAAAASUVORK5CYII=
        }
        
        
        
        set pngImags(anchor) {
                iVBORw0KGgoAAAANSUhEUgAAAA8AAAAQCAYAAADJViUEAAABJklEQVR42mNgwA6EgHgGED+D0kIM
                JIBqIH4PxJ1QupoUzduA+AAQM0LpbaRojgDif0D8AUpHMJAIKoH4P5TGCziAuBmIJyDhXVDNu9DE
                m6HqwYATiHdDFRKLd0P1MfABsQYWjOxsbPJ8+LySANWcgE+RHhCzIwuUl5fbRUdHzwFp9vHxmZiQ
                kKCKpocdqo/hEBD/UVNTO7J9+/a2ixcvnkJS9H/NmjUnQYwzZ85sW7ZsWbGQkNBekHqoPoZiIH7p
                6el5FYurQJpPIAv4+vreBqmH6mOQAWIJkLiIiMij9evXX0bWvGLFCrDNe/fuvSsnJ/cUpA4qx4hu
                E8igw8HBwX9gmmESLCwsx7BpQAfMQNzCysr6F6SZj48PRLeQmjxdgPgClMYKALVwZC8xv3DFAAAA
                AElFTkSuQmCC
        }
        
        set pngImags(link) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAEbwAABG8BAuRQjAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEdSURB
        VDiNldO9SkNBEMXx34pVEIsYgtoEk05B30Gx8wF8A9uUlvbWFiL2VlZWVmlEROwFFQULKz+QFIri
        WNwtkiC514VtdvnPnrNzRkSostHGMV5xgEZEqAp3cI8zdHGDK9T/A5+ils/mcY3DMnhuFB646+Kt
        rMBOljsKT+ECR5PGr1m8oJVSeoyIfkppCidoYrNMwTI+8IXV/HIPd1iICEMKUkpNbGXve/mzEn6w
        ixl8Yy0iHmDwtRZu8ZBb9IHP/FlLueA2mkMqR+BelrmYZX9ipcTmMJwPp7GOc+yPKzCRvT1jIyL6
        2da7InUNPJV0yhu6I7JqivDcY67MwqEilvN/wJ0KUVfHpSJx3Sy9EjzYhbpiRF8VI9uuOua/Pjqm
        3T6CnIcAAAAASUVORK5CYII=
        }
        
        
        set pngImags(foreground) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAADKgAAAyoBEJdYGAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFsSURB
        VDiNpdPPi45RFAfwz3l702SIekZsbSSrqWFjocRSb5aKTGQhO7L0FygbSyVl+SZSJDZK1mNlYynK
        4n2UmsaPGR2L5z7TnafZ4Fu37rnf8+t+z72RmbZDRHscN3EU73ELh3AGuzHGZLxN4A7cwzI+4RS+
        4iEO4nBxfZzZbIwHwXvwBCexgUlm86Fw+6tgmMJo0MCDEgz3M5t3Fbe32n/HM5CZOh1mF5llWT+Y
        Hai4eWa/K/5Rz41Ke3O4U1V4mtl8qewLg26n/aY/PI+FwVV6XQI3Km5ts/0qwaXKIfG2sq/pxtfj
        eWaztpmgVFisHD5nNqul+iKu24ppbYwwj53V2UJEuyuiXcJtvKq4X3gR0R6LaK/AOLNZjWhXsFSc
        5vCx7E/jbpXgGy7rXuOk1mBLWzqhTmQ2K7pn22MfzuJcZvMTov8LEe1y0eINXvZCRbRHdGMMvM5s
        6iuJda76D8R6N7Z/xvAv/DX+AIJ9glRwdTpBAAAAAElFTkSuQmCC
        }
                                
        
        set pngImags(background) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAChAAAAoQBpLDYYgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFsSURB
        VDiNnZNNS0JBFIafq2Jm2YeJJFZiYS0CoSxq16LvRQTRJoiC/kKbtkWb/kgFUVCLNka0SSi09iGU
        +FFJm+qqpaYtRrjqVQsPDO+ZM+c8c2aYka4X8TlNeKjDnj4J6JwmPDYj+noAgEdTazUPhGV1rNh0
        1YovY7BxBaNWcLVAJAGnIZiwwdmsklexA28E5s/BYYKjKcjk4OQRPtIwbS/NVQHCMix5IZ2DvbFC
        mxpIZIU/0/UHYOsW5AwsOGDcKmIP70J7mmGgrQYgJMNBUPgrfUJTWbiIVt5dBTgMKrc81y30uHD2
        fwF8r0I7DNDeIHbfDhQSJZi0l5eXAaIJoRKik917sBlFbNgCBi3cxGsABs1C377AuQ/mBuEDGHWw
        cwducw3Amgta9aCVYLkXNt3KncRTsN4PjWVPT4qt8l38F16S8JMHe5PSTViGIYv6/M9J0qqn3Gks
        nVsMYlQzXVjGD4xUT6lukQT+X84jYkjYNiPwAAAAAElFTkSuQmCC
        }
                                
        
        
        set pngImags(weight) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAKQwAACkMBERqoOAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAE8SURB
        VDiNddM9Sx1REMbx36ooiPhCTGEVxFIQQcRCsAgpfKlsBL9BWkFLGxsrS1vJF0gTES3EzlKbgEWI
        aEpfsBLRoB4Lz8W5h70LB+Y8z8x/h5ldKSXx4DuOkcK5xzVucIZdjKeUKAEZ0l8A1tGORbxm7RnL
        rQATBWAieEdBP25T/0yF+Aqn4Z5CPNIKMBvig5RfXVVVN6aDt1fXfmceWqPNpUY9fgT9BIN1gG8h
        6RnD+IqdrJ1jE721W8BWALz4mPoLNtDRlF8DOAuAVfR538rvrF1iphaAL5rXNxq8saD/b3h1X2Ej
        6V/hVXgM/nZKSbnG+RDvF94ndIX7ED4AVVV15mm3AiwU9xOILc6H9p7QU/wbf4J/g4GmGeBnSDjM
        2mfM4W9RPBnqrOCX5uk/4g4PuMVFzllDXxzuG/U8KcHZ7T+HAAAAAElFTkSuQmCC
        }
                                
        
        set pngImags(slant) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAKUQAAClEBJ/B7ewAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAADMSURB
        VDiNndOhTsJRFMfxjxaKIxgIBIOBQvIZDGaahaaF8RBuPADZTGMGRzTwAIwnoNoIbG6MscF2LWcO
        Ffjf/z+ccs79fn93d+dKKSkq3GKIHVLUO3qF8IGkjk3AvZ9+CcEg4Ldf/Uz4BmsscV1FMIn07r9Z
        BtwJeHp0XgBf4RNbtKoIhpH+cvLMGfgOeyxQKyXAJWaRfn/2licE/YBHhY98BG7iCys0qgjGkf6c
        tSN/4G7Ac1yUEqAd65rwkAMH5wmvBz8t4QOPOYJvVMtlW2NKE5UAAAAASUVORK5CYII=            
        }
                                
        
        set pngImags(overstrike) {
        iVBORw0KGgoAAAANSUhEUgAAAA8AAAAQCAYAAADJViUEAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAVgAAAFYB3FMHnAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFESURB
        VCiRddOxSlxBFMbx36xiiIWFQQwSUioYizQBIVpuaRBJEwKCD5DGJwh2ksZOsMgrxEoQA+mjhYHY
        WWhIwAhiIyJiPCmclXH27sBl7sz/fHO+M+feFBGaRkppCst4gRt8xRdMYD4i3omIrgcLuMY22hjH
        Cv4h8DkiNAmHcJazPa/Ypyz+0EvczgGX6KvYaGavI0KrodzHxbxYgoj4ixP86GzUmZ/gNme4wGTF
        396/97iw9SwOHGK4KS5hE9N1p7KDvry+xnkVs9uPnVxHPUYwjxYGcIT9gu832i7sfyzsX2HoAS8C
        5xrE/dgrDnjTJcYj/OqRfaoQr5as0+dJPEspDdaFR8RP/M7L45J1xC/zDS/V4pTSGJ666/lWfTKs
        ZVunmK2+8w13P8T7rj5HhJTSt2ztD15lRxeYwQGWI+J77eo/AaczSSMeZowAAAAASUVORK5CYII=    
        }
                                
        
        set pngImags(superscript) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAB1QAAAdUB82zL4QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAD8SURB
        VCiRndIvS8RBEMbxz/oPrlgvaDIogk2TIiKc4dKBCHaDBs9sFsRXYLkqtnsFokEwmGzCWbVoMRgt
        rsFRFvl5xzkwYXaY78w+MynnbBhLKY3iEKvopH8AGhjHFppjQ1Uj53wVoDqm5JyHdtRwgenycRE3
        eAlvF7kWnnGLNRxjBmmkGO0uxKmH3xeTf+ABm9iJBi0sV413iYyziBvohHAHuA7vIlUBmgF4xz6O
        kP7UowKQ0AtId5CgPxr8su//r6eUaoP2WnafwCk28BpT7PVdaVE8iXMsRHwSgN5ADTDr6wbmCuB8
        ADK2+xyVNt7whKUisVsAHrFSBfgEwk8lGMomdEIAAAAASUVORK5CYII=
        }
                                
        set pngImags(subscript) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAB1QAAAdUB82zL4QAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEcSURB
        VCiRldLPKyVQFAfwz33IykYmlK2sbLBBnsWshERZ2dsrW/+C7JSSbGcxNf/BSxaKpinJBimWMlZm
        pWPhvtx5P8Sp0+187/d87/3ec0WEiIAJnOIp50axt4S/+INqHY8I/iuYQeSsFvg8jvGt5DcJZHIt
        C+znegqH6G7kthNYyQLPWMU2Kq2a2wl04DaL/GrXWM+KhoiIl2wDxlJKnY2cMpoEUkpb+I1/GMLy
        RwLl1SvYwWKu97KNoxY2R7CO/jrQgx+YK0iT3kc61YAf4BxnMI4LXKK3II4WAicYrB+W1zXcJG+f
        pyM7uo+Iq/wW0+gq3N5FxHXxVruofTiitrNnAZsRIWXg05FSmsUAfmL4qyd/x6O3j/aAvlcK4kdB
        Q9wrAAAAAABJRU5ErkJggg==
        }
                                
        
        set pngImags(LeftAlignment) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAZwAAAGcB1SjUJgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAACISURB
        VCiRrdOxCQJREITh78m1Ipgo9mADgo2Y2IhdiIkdCGZGd5EINmBmA8IaXHJq8lhuYJNlZtg/WGgR
        yekKNpjK6V4iIpntVXDCstJ/jIjdcNHggmdlwfXvgjEQ9lgk87eJHiOr9ygIW8wq/eeIOAwXDVaY
        Vxa88FUwCsJaPcKvHtDJP1P7AXCtPtjz2RLuAAAAAElFTkSuQmCC
        }
                                        
        set pngImags(RightAlignment) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAZwAAAGcB1SjUJgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAB+SURB
        VCiRtdO7CQJREIXhb8QCDG1AMNrUBgwMBU2syNzGdkEQH60I12Az3eA6ugcmGfgP84QWJRldYIeF
        nK5RSkmyvabviYg44lDJnz8M0GFWaXD7uYXACU2Sv0wMzOELPUfZwh7rSv4+VP4Km0qD+V+2sMUy
        yT/oDyf7TO0L1wA15ATDvroAAAAASUVORK5CYII=
        }
                                
        
        set pngImags(CenterAlignment) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAZwAAAGcB1SjUJgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAACFSURB
        VCiRrdMxCsJQEITh74lXEazsrOzEAwjeQG+U2lzF1iJFUongVQJrIQqve3lmYGGbf4ZlWOgRlTMk
        nLBSp2eKiEr2o+V3SSldcSjkbhFxzgxwx1ho0P2C/z0hocGmkn8s5GdM1ThrCxdsC7kuItrMADvs
        JwS3zNTCEetK/gWD+mfq359UPhVnaZJqAAAAAElFTkSuQmCC                
        }
                                
                                        
        set pngImags(tableConfig) {
        iVBORw0KGgoAAAANSUhEUgAAABEAAAAQCAYAAADwMZRfAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAACnAAAApwByS7AzQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAExSURB
        VDiNpZKxSgNBEIa/Hw5MqZ1ooY0KElQQIYXFgbZaB6s8RsrYiYWCzyBoYaO1RYoUljZCwAdQjFiJ
        ldxY3G5uXEyMODDMfPdzs7M7I+ABWA3xispyoOt4G+g5/jCz4wgW/NLMiA50Em6P4gx4Al6AJUlH
        /mRJNccNSdOO51xeVvzhpD91siap6WK0esKpPh+TeJ17YCtE/5CeU73uiwzMrC9pYGb9KEh6TTjV
        34c55RjvgEaIvpMe8AZ8AuuUU/T/GlDABA8LXFOtQupFBuyGUaYjzSV1Qn4D7DutDUwBh8MPE3Qy
        k5y+A6wAi8CCgNtw93Stc6q1Pwce+W4XZnbwn05OgKZftmVJLWAzxGgbjv3UAE6BTFIXKATMAjXG
        2xmwN0KzzMyefymAJBunfwFHY+LCcEouegAAAABJRU5ErkJggg==
        }
                                
        
        set pngImags(tableWidth) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAOCAYAAAAmL5yKAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAACxwAAAscB9ii8ZgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEFSURB
        VCiRpdK9SgNREAXg72KijZ0iWIiFCGkEtRAEizzCPlJKwUYigoWVYCs+QgofQC2ttrAIUXAV/EW5
        FrmLyxKV6MBhmLmHw+HcCbjECu6x66taeMUj3lPfRK/CmYWYkMcYlUCGLm5xgz10apxOwHXCDI5r
        Dgr0K7t2zUFbqTpCPcN+Eujj4DsHpwlZ6mWt4yPlAA/YqHGyBvJka7VmbxKLWErzRYVb1moDRYwx
        DyEUMca8fAkhDNDERFrlmKpxipAUeyMCamE6AQaYM2aIOzhP2B7B6fzm4N/feIiXhKO/OHjDU5qf
        sTaug65h+rkfTvkEV9jCWUV92fAWmmm+w0KNM/8JbMPBfqHhO4UAAAAASUVORK5CYII=
        }
                        
        set pngImags(search) {
                iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABOklEQVR42mNgQABJYWHh6UD8XExM
                7Lm4uPh9AQGBOqC4EAMRwE5CQuL1woULPyEL9vf3fxQUFHwIZCrg0ywPtO3VnTt3PmGT3L9//2cR
                EZF7QCYTVt1CQkITFi1a9BmfDTExMa+AVCBWSaCfnxLy3+HDhz8DvbgRqyTQeSDTGbZv3/7F1NT0
                DzoGyV29evWTtLT0PlwueEbIBQcPHvwsKSm5HmcYzJ079yM+AyIiIl4DKQ9c8lJA/7188OAB1rAA
                eu0TMF1cATJzgJgRlyE2QCe+XLJkyRdkwZqamneioqJ3gMzdQPwdiFfgM0QZGKCzQLECtBGE73Nz
                cxeDEhkQvwHi/1BDFjOQAQqQDPkKxPPIMaQUyRCQV2eSY0glEL+FGgJK+pPJMaQGiN8B8V8g3stA
                JmgE4j1AzAEAgSFp5NlrWBoAAAAASUVORK5CYII=
        }
        
        set pngImags(table) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAPCAYAAADtc08vAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAATAAAAEwBzjPcugAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFhSURB
        VCiRxZM/qiJBGMR/3dMjPP8EoqN0auYpvME7i0fwHObLwIabjCcQA2EiEQwEU0UGg3XGnv6zyY67
        8FiQ3YVXWfFBUV9RJcbj8dfb7fYupUQIEVqtlpVSBgDnnASIosgDeO+l914opZy1VrTb7W90Op0S
        CEDodrthu92GBlmWhSzLnjzP85Cm6ZMnSXKTQojATzweD5xzvAqllBe9Xu8+HA7frLXEccxsNmM0
        GgFwPB4BmEwmAJzPZ67XK9PpFIDlcvkdrXXRWErTNOR5/vILWutCvuz3D/hnAVWWZTyfzwE4HA70
        +32SJAHgdDoBsFqtALhcLhRFwWazAaAsy/jzM1AAVVUBUNc1xpgnN8bw+90YQ13XTw6gqqqKF4sF
        APv9nsFg8KEH6/Ua+NWD3W7XCP+HDKy1f52DtVYKrfWX+/3+rpQKzjkphAjNGr33EkBK6QFCCMJ7
        L6Io8s0afwCBPWPlpfyNaQAAAABJRU5ErkJggg==
        }
        
        set pngImags(normalscript) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAANCAYAAACgu+4kAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAACUgAAAlIBt7VFyQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAD8SURB
        VCiRfdGxK4VRGMfxz7m5izJQMpBMMopB/AFmpQwGmW/XajEY3MnMaLLYLMoms+iWwWpQFpESZbnH
        cq6O957XqWd5ft++5znPEWPUL+zgDTHVCzayfBLXKfvEvlyQoFYmuC3kB3jGYoxRSTCExyToYSbL
        1tDF1G+vKkjgbjbFYeqt4gqjf9gawRi+kuAVmzjH8ABbEiTJSTbFJZpF7h/BfCa4q+Ma6s96+lJY
        CCEsl6ABQQihEUI4Tm9vZVG7eE1l7CZOsZ196VN6xjcmaneAEVxgqyLtZLvoFAWYxQOOCstcygQf
        GK/k9vCegHusZOE0zjJBxA3m+swP0adLieF7/NwAAAAASUVORK5CYII=
        }
                                        
        set pngImags(underline) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAcAAAAHABznhikwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEnSURB
        VDiNndK/SpxREAXw313/FNuYFcRK0CqmiIKdpSxoIJ2tjT6A4DvkBfIA6VfLELERFVuxsxB8Ag2E
        ZC2UlYWx2PvBZ7ifxQ4MF87MnHOGuSIClvENV7hGD7sRoUos4jtu8APdiOC/pj/oY6aO1+pt3GO6
        wlpypJTmMYteRPSVYxVHEfFSAa1acTO/vxqGYQM/3yA1ez08o12yn3uOMVnHWtn+BLZwGRFPJemU
        Ugf/ImJYx6sV1tHByTv2t0r1iqCb39N3CL7mFYoEa/gbEbelyZTSBwwi4rGJYArDlNJUg/oBDkuF
        iuACc9gpqH/CCs6L1Pk8E0b3f8AelvAZ+0Z7N542ZYJKbRtfsIDfODP6eYOG1d4SjBMp2/045vwd
        I5sxZp6/Atuktm3LJOUTAAAAAElFTkSuQmCC    
        }
                                
        
        set pngImags(delete) {
                iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA2klEQVR42rVTwQ3DIAzMh1fXyIN3
                p+gqeYUlsgsbZACkKlKyQr5skLfLOXYLUaOqRUU6CcPdgW1omte4JAwJ94QtgQ7YZG8QbjGuCesb
                0RlW0TxPZrExhrquI+89jeNYAGvYAycz4ZsMKg4hUPNhgJOZQMt5sTuCaZqo73uKMd5UhLlzjuZ5
                Zo5wSbR7wXBFBCAittayCYA51mAMjnC1sHthkKeepoK2bRm5ITjCVZQGaqJCNcpT+q/BLykURZRC
                fVXEoo3LshA6cWwjxGdtrH5I1U+5+jNVfecHhKU5u2oeAKoAAAAASUVORK5CYII=
        }
        
        set pngImags(externalFrame) {
                iVBORw0KGgoAAAANSUhEUgAAABYAAAAQCAYAAAAS7Y8mAAAB8ElEQVR42q3UwWvaUBwH8Gidt3bM
                rWwHwYN43h/Qv8DLwv4ALx68rFQWbZNqoqg7xMMURJFQcBPxXudlJztlCqKCB5ltQRQ8jdxkuI0p
                373n1jDTzi3dfvAj4SXvk19+7yUMc/vYMzPml+S4Y2jWfD6v1Gq1y3q9ftFsNi/a7fZ5t9s97/f7
                w8Fg8EFRTiYmxowH93e/8bxweg2w2Wxv7XZ7S5+dTucznfi3yTBb7wn3WIMdDoc6Ho+hz+l06jcC
                /8BNC9KeLGHvMU6n8+NNrQBwxyj8E18S/MV/hcm0mtaO38FksSCJEsSwiGPhGEeHR+Cec/Af+LH/
                bB/sE1YDH+4+mpEpT9cAPdxoNEBWHh6PB8lkEm63G6PRCPoHn529w872XchyErPZrHStMj0syzJE
                UUShUEA8Hkc4HF4dS6XSGq6q6jZpl4meL5fL1xvhVCoFjuOQTqeRzWZRqVQgSRLoeLVaXYPJHv9K
                4g1BTxeLxcFGuNVqIRAIIBKJaEi5XEaxWITX68VkMnl1Ne5yudSNX9ivMK1OEAREo1ENTiQSCIVC
                yOVyaxUbgofD4arafD6PWCy2ykwms3oLnudvD9Og1dIFo71mWRY+nw+9Xg90X/8TfBXBYBDkZwNF
                UXDT9T/CVqv1E73JaFosli+b3O98cGw9o4/DyAAAAABJRU5ErkJggg==
        }
        
        ### images of popup menu ####
        
        set pngImags(exit) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAPCAYAAADtc08vAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAgwAAAIMBYu+7WgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFTSURB
        VCiRpZIxS0JxFMV/f5/yJnEVoiVqMpCGiGYR3dr6CklubUF7n8AhcNG1xsChwI9QvIfgZgkOgbwQ
        wlJ5nobUnk/TogNnuP97/5d7zr0mFosN0ul0L5FIDFmBbrdrt9vtC8/zruYStm2P4vG4stmsSqWS
        hsOhlqFWqymZTJ5KIkhs2x4BAmSMUSqVUrFYVKvV+lWDaHAaSTQajRnr9fqCFGNMQlJvGkcXKiaw
        LAsAx3HwvFccx6X/1t8xWLfGWHcwvpQ0mpMQZCaTkSTlc3kZIsvoAPuRVc6vwRZEDn+UMMVJoUAu
        n6fZbFKtVm4+3ge7oGehgqSntRLCWwD2glv4swRJD8H4Px4AEPV93yxLdDodv1wuz87bdd3IeDxW
        uM4AFeAA2AasQO4ROA/Vu5I6cy+zm4Yj4Bp44cvI+/DZLqOZfP4eyZgN4AzYlHS8zoNPCygSid7L
        BhEAAAAASUVORK5CYII=            
        }
        
        
        set pngImags(newFile) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAPCAYAAADtc08vAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAASAAAAEgBMc5bqAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAF0SURB
        VCiRlZM9awJBEIafC1ecGCHXXJoUKW3SiIUfzcEVili4XSCNASuDpAv+Cct0VmJlGVstjKTwJ6QI
        gWCjiIpKDuW4TSERjavEgSl2dved5x12NcuyBslk8htFdLtdYzQaPUopG6p9AIQQn/JAZLPZd+AJ
        uJNSosqzg8qA7/sa8ALcaJr2oDqjHxPI5/NXkUjkFaDRaHwBzydZ2I5EIvGmsqADVKtV+v2+ksIw
        DMrl8kFKHaBQKBxzAoDjOBf1ev0+HA7Po9Ho4reuCSE+M5nM9V8C27axbXuz9jwP0zQRQlCr1U4n
        8DyPxWLBcDjctzAej1mtVnuXDMMgEAjgui7L5XIjNJ1OAQgGg2uBTqfDYDDYEwiFQhSLRWaz2abW
        brcxTROAWCy2FhBCKLHn8zmtVovJZILv+zSbTSzLIh6PA+s5/fsduK4rAZlKpXbqeq/X03O53IeK
        wHGc81KpdHlwsoDyg2zl7Xa3dDotK5XKDsEPZRg/ohlbSHEAAAAASUVORK5CYII=        
        }
        
        set pngImags(openFile) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAMCAYAAABr5z2BAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAHEAAABxABY5kuZQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAC8SURB
        VCiR3dIxSgNQEATQNzGInZUQRUHEysI64AUsvICNtSCeysbCLhewEQT1CBYhYCNWgpVhLfIRCRpD
        7ByYYneG2WXZYBPHGKOLND7hoqoqyRH2mwfeUbgOznFXVbe+IMkeTlo5qKqbKb2HMy2pX1WmiUOc
        /qD1UB1/xD8I6P6iP+B14Q2q6hkvcwUkWUqyNsucpJNkK8nKZ8/kD4ZYxzLe8Ih7XGEDfRxgp3nG
        GGE7uJwxdBerJrf4Fh8On0c0Ipj8FwAAAABJRU5ErkJggg==        
        }
        
        set pngImags(copy) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAEowAABKMBA4EWAQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAGeSURB
        VDiNlZIxb9NQFIW/95LGxGqGIMwAydLEbosqVYWl8AMoG9CFMIUxvyDJlv6FbMBEM7VD1S4oFKlz
        xmRhqNSRgSmKyhDbit5loHLs4ETiSGe45953dM/VQ6ElnaoPWCLCKqLQAtxLiGQ+KPRYob4CuVUG
        miUQOAJmCnWmlMotm1MKLYLZAA7msubp3l6nUFgPgyAo23n75nG5fAWws/PkV7PZvF40eATsxowH
        btX76XnuuuM4kTgej+n3v3FyesLh4ds7h783uA9sxelWvd+DwUDiGA6H8nz/hTgPHsr5+YWIiGTv
        zB2glrjBvJfA9vYWnaMOtXc11PGXKEJeRPz4oOdu3n76/LHQ7XYjrV6v0zvuAWCM4fLyexThGXAR
        Z7Xi+osR4phOp6LQ0Zo/ROTN4gZhGFrtdjsRoVgs0mq1ojo1Z9TMZmk0Gv9oiRpAo3sZlTHxxkal
        kjfGMBqNEg9s26ZUKs0NBPVe0hd4LSLZyWSSEGezWXJq2R93q97t/xwxFWEY4vt+ai8IgvkN0pBb
        y5mDl69W+WNZFn8AcSIeueYThuMAAAAASUVORK5CYII=    
        }
        
        set pngImags(cut) {
        iVBORw0KGgoAAAANSUhEUgAAAA8AAAAQCAYAAADJViUEAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAASAAAAEgBMc5bqAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEnSURB
        VCiRjdIxK4VRHMfxz0MpRTevQAZkZzGyWmTyArwBm9ULUIa72W5GVjEYURbDtRilLEqx3BLH4He5
        Hvcpp55O/9/5fv+n85yjlKKUAttY7NfDPixiu1+P+BkL6FRVNW7ISN4J9zUGui7hHfsNu+5nfek7
        qwFtfGC1lq8mb//Ka1ALj7hHayC7T95qlANvoqCTupN68w/bcL6zCAeZz4ZyDfIsehF7mP23nAaX
        kS8bmQZxK+JT5q1hXBVYVVVzmMQozvGMZVxgCiu559dSyl3/nYxhF28D5yzYSeOdgawXbjeePbxi
        AyO4DjgfeT71ddY3wu/BFQ4DVrhBt/YPusn7xzyM5wgPWMcpXrBWk9ey22m4h3gmcIxbnGC64QZm
        8ni64Sc+AddfgG9voTk4AAAAAElFTkSuQmCC
        }
        
        set pngImags(paste) {
        iVBORw0KGgoAAAANSUhEUgAAAA8AAAAQCAYAAADJViUEAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAPwAAAD8BR5eJ4AAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFESURB
        VCiRrZAxSwJhHIefN4/Xu0APLBVaGkIOGu2EC85VErc+grg5t7TXF3Cpj5BfICjcXumWFttcHG4Q
        KjGRyCC8hig89S6IfuPz/p/3975/sQtdCTYxmYP/DIcvQfC0yDUJdgLkSavFdjYbluZzzppNXsfj
        PQ0sICwDCOCm3UY3jBX5bTqNfJEowLsGMkTTaWQmE55Mpa41KR8Hg8HdaDS6/GlejpHP0+31lnF1
        MplQKpU2hRBbQRCcr5UBdF1fYbPZDNM0h77v3wohTiPldUkmkxSLxWPbtqtKKbn2z0ahwH2/H3tR
        o9EYRDZ7nofneWvPKpUKELEwAMdxcBwntj1SVkqhlAqxer1OLpf7XXZdF9d1/6/5O7Va7Uv+gIcE
        HPypeQiHG7CzCPdN8wI4ijUBEQTBCrQs66pcLpfixE6nIz8BY9hhouOC2k0AAAAASUVORK5CYII=
        }
        
        set pngImags(save) {
        iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz
        AAAAawAAAGsBDl9bUQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAFISURB
        VDiNxZAxagJBFIb/mdmwMxC2MKBrN8IQLKxT2Io38A57hLRb7gVS5BDp0kgai4jkBKkE24GAbood
        diT70qi4xA1JLPLBK97/Hl/xM6XUutPpFABARJdCiAvOOSmlKuwoy9J770trbeGcuyWih/0NxhhL
        O7TWBIBGoxE55w6Tpik556jb7b4CuAdwByAkInAcIYQAAHDOIaU8TBiGAADGGBFRAuAZwBNj7Do4
        FjDG8B2bzUb3er0XrTW89zLP88cfC6SUWC6XCsDNPhsOh281wXg8hjEGURQhy7JD3m63AQBxHNek
        QoiqVuJvMcbYWol/4WxBrYP5fI7tdtv4HMcx+v3+aYG1FkmSYDKZNAqm0ykWi8VpARFhMBggTdNG
        wWw2+5L9f4lnC1gQBE5r/V5VFcvz/KrVajU+F0XxoZRa7/fVahV9Ah+Ruqu+STFcAAAAAElFTkSu
        QmCC    
        }
        }
        variable gifImags
        array set gifImags {}
        
        
        proc setGifImgs {} {
                variable ::images::gifImags
          
 
                
        set gifImags(table) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMp
        KLrc/i7ISSWrKyidad5XJV4fKHCbiJVniZ6qx6LcG8tma9q3Bf1ARwIAOw==
        }
        set gifImags(timestampImg) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMm
        KLrc/jDCQGukDDsdlvadEoqj8JWfOVZdyjVumckp+cQXK0s8nwAAOw==
        }
        set gifImags(openFile) {
        R0lGODlhEAAMAPcIAOPj44qKio2NjYmJiYiIiIeHh6Ghof7+/v///7Ozs4WFhYiIiIiIiIyMjI2N
        jc3NzZKSksfHx////////////////9XV1Xd3d3d3d8TExP///////////////+fn53x8fAAAAMfH
        x////////////////////7e3t5eXl////////////////////+fn5wAAAAAAAMfHx///////////
        /////////7e3t5eXl////////////////////+fn5wAAAAAAAMfHx////////////////////7e3
        t5eXl////////////////////+fn5wAAAAAAAMfHx////////////////////7e3t5eXl///////
        /////////////+fn5wAAAAAAAMfHx////////////////////7e3t5eXl///////////////////
        /+fn5wAAAAAAAMfHx////////////////////+rq6sTExP///////////////////+fn5wAAAAAA
        AMfHx////////////////////////9fX1////////////////////+fn5wAAAAAAAMfHx/z8/Orq
        6v///////////////9fX1/////////39/eLi4vf39+fn5wAAAAAAAB8fHwYGBgAAAAoKCi0tLWFh
        YbW1tZubm2NjYygoKAYGBgAAAAMDAyAgIAAAAFdXV1dXV1dXV1dXV1dXV1dXV1dXVzMzMyQkJFdX
        V1dXV1dXV1dXV1dXV1dXV1dXVwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAQAAwA
        AAjbAAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fvwICADs=
        }
        set gifImags(newFile) {
        R0lGODlhEAAPAPcJACoqKmdnZ2dnZ2dnZ2dnZ2dnZ2dnZyoqKqmpqf//////////////////////
        /////2dnZ////////////////////////2dnZ4mJiaSkpP///////////////////////2dnZ///
        /////////////////////2RkZNfX13d3d5SUlP///////////////////2dnZ///////////////
        /////////62trYeHh4eHh1FRUf///////////////////2dnZ///////////////////////////
        /////////2dnZ////////////////////2dnZ5ubm39/f39/f39/f39/f39/f39/f4eHh////2dn
        Z////////////////////2dnZ////////////////////////////////////5KSktzc3O7u7v//
        /////////2dnZ5ubm39/f39/f39/f39/f7+/v/////////////v7+wwMDF1dXf///////////2dn
        Z/////////////////////////////////////f39wAAAEdHR////////////2dnZ+3t7efn5+fn
        5+fn5+fn5+fn5+/v7/j4+O/v7+jo6AAAAENDQ+/v7+/v7/r6+mdnZ66urpeXl5eXl5eXl5eXl5eX
        l6SkpA4ODgAAAAAAAAAAAAAAAAAAAAAAADg4OGdnZ/////////////////////////Ly8mBgYE9P
        T01NTQAAABYWFk9PT09PT4+Pj2dnZ/////////////////////////////////////f39wAAAEdH
        R////////////zMzM2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ5ycnP////////f39wAAAEdHR///////////
        /////////////////////////////////////////////0hISI2Njf///////////wAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAQAA8A
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AThRM3jlw5c+fQpVO3jl07d+8CAgA7
        }
        set gifImags(LeftAlignment) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMa
        KLrc/i7ISSWrOMDNu1cYF35kWWbVhU5mKyQAOw==
        }
        set gifImags(slant) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMY
        KLrc/jBKEepc4aqsaecaOInj91ldqk4JADs=
        }
        set gifImags(copy) {
        R0lGODlhEAAQAPcIAAEAAgEAAgEAAgEAAgEAAgEAAklISfj4+P//////////////////////////
        /////wEAAvf39////////////5+foBIRE0tKS/n5+f///////////////////////////wEAAvf3
        9////////////5+foJGRkk9OT01MTfn5+f///////////////////////wEAAt7e3peXl5eXl5eX
        l5eXmEVERlZVVjk4OV1cXZ+foJ+foMTExP///////////wEAAubm5re3t7e3t7e3t/Dw8C8uMEtK
        S2BfYGBfYGBfYE5NTgUEBq2trv///////wEAAu3t7dfX19fX19fX19fX1y8uMMfHx///////////
        /8/Pzzk4OhUUFq+vsP///wEAAtjY2H9/f39/f39/f39/fysqLMfHx////////////8/Pz2JhYri4
        uRAPEbCwsQEAAvf39////////////////zAvMb6+vk9PT09PT09PT66urlBPUFBPUE5NTgcGCAEA
        AszMzE9PT09PT09PT09PTyopK8fHx/////////////////////////f39wEAAgEAAvf39///////
        /////////zAvMcHBwXd3d3d3d3d3d3d3d4iIiP////f39wEAAgEAAvf39////////////////zAv
        McXFxd/f39/f39/f39/f3+Pj4/////f39wEAAgEAAlZVVlhXWFhXWFhXWFhXWCcmKMPDw6+vr6+v
        r6+vr6+vr7m5uf////f39wEAAqenqKenqKenqKenqKenqKenqCsqLMLCwp+fn5+fn5+fn5+fn6ur
        q/////f39wEAAv///////////////////////zAvMcfHx/////////////////////////f39wEA
        Av///////////////////////zAvMcHBwff39/f39/f39/f39/f39/f39/Dw8AEAAv//////////
        /////////////zc2OAgHCQgHCQgHCQgHCQgHCQgHCQgHCQgHCQgHCSH5BAAAAAAALAAAAAAQABAA
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AlhRM3jlw5c+fQpVO3jl07d+/gxZM3j149e/fw
        5dO3j18/f/8EBAA7
        }
        set gifImags(delete) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMm
        KLrc/jDCQGukDDudQ8Ma6C2hJXDnmJLqmpadK36tgrLxVN1S3ycAOw==
        }
        set gifImags(anchor) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMh
        KLrc/pCF6CZdIV+R++afYn3jVj4ayJ3Y5LJSBzdeaEMJADs=
        }
        set gifImags(cut) {
        R0lGODlhDwAQAPcAAP///////42NjcvLy////////////////////////////8vLy42Njf//////
        /////////2RkZCgoKPb29v////////////////////b29igoKGRkZP///////////////8rKygIC
        Am5ubv///////////////////25ubgICAsrKyv///////////////////2pqagEBAby8vP//////
        /////7y8vAEBAWpqav////////////////////////Dw8BgYGB0dHfDw8P////Dw8B0dHRgYGPDw
        8P///////////////////////////6ampgAAAFxcXP///1xcXAAAAKampv//////////////////
        /////////////////0VFRQAAAGNjYwAAAEVFRf//////////////////////////////////////
        /9ra2gcHBwAAAAcHB9ra2v///////////////////////////////////////////zg4OAAAADg4
        OP///////////////////////////////////////////5qamgAAABMTEwAAAJqamv//////////
        /////////////////9nZ2czMzMnJyQsLCxwcHOLi4hwcHAsLC8nJyczMzNnZ2f////////n5+X19
        fQQEBAcHBwAAAAAAAIaGhv///4aGhgAAAAAAAAcHBwQEBH19ffn5+XFxcQwMDKysrP39/TQ0NAAA
        ANjY2P///9jY2AAAADQ0NP39/aysrAwMDHFxcTc3N1paWv////7+/i8vLysrK////////////ysr
        Ky8vL/7+/v///1paWjc3N1JSUhsbG66urkZGRg0NDbOzs////////////7OzswwMDEZGRq6urhsb
        G1JSUvPz81FRUSoqKkhISODg4P///////////////////97e3kVFRSsrK1FRUfPz8wAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAPABAA
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AThRM3jlw5c+fQpVO3jl07d+8CAgA7
        }
        set gifImags(paste) {
        R0lGODlhDwAQAPcLAFI6OioGBioGBioGBioGBioGBioGBioGBioGBkEjI+vq6v//////////////
        /yoGBigAAJ2Tk7GoqLGoqLGoqLGoqK+mpjQRESkAAMfDw////////////////yoGBikAAFc9PV9G
        Rl9GRl9GRl9GRl1ERCsEBCkAAMfDw////////////////yoGBikAACkAACkAACkAACkODi8gIC8g
        IC8gIC8gIGtpaX9/f5+fn////////yoGBikAACkAACkAACkAADIlJff39/f39/f39/f39/f39+jo
        6GhoaIWFhf///yoGBikAACkAACkAACkAADIlJf///////////////////+/v74eHh8rKyoWFhSoG
        BikAACkAACkAACkAADIlJf////////////////////b29nBwcHd3dz8/PyoGBikAACkAACkAACkA
        ADIlJf///////////////////////////////3d3dyoGBikAACkAACkAACkAADIlJcfHx4+Pj4+P
        j4+Pj4+Pj4+Pj4+Pj9XV1Xd3dyoGBikAACkAACkAACkAADIlJf//////////////////////////
        /////3d3dyoGBikAACkAACkAACkAADIlJcPDw4eHh4eHh4eHh4eHh+np6f///////3d3dyoGBikA
        ACkAACkAACkAADIlJf///////////////////////////////3d3dyoGBikAACkAACkAACkAADIl
        JcPDw4eHh4eHh4eHh4eHh4eHh4eHh9LS0nd3d00zMygAACgAACgAACgAADIlJf//////////////
        /////////////////3d3d/r6+uHg4OHg4OHg4OHg4G1tbf//////////////////////////////
        /3d3d////////////////////29vb3d3d3d3d3d3d3d3d3d3d3d3d3d3d3d3dzg4OAAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAPABAA
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AThRM3jlw5c+fQpVO3jl07d+8CAgA7
        }
        set gifImags(RightAlignment) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMa
        KLrc/i7ISSWrOMDNu4cYF35kuWXVhU5mKyQAOw==
        }
        set gifImags(paragraph) {
        R0lGODlhEAAQAPcAAP////7+/q+vr0lJSSAgIAICAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        AAAAAPr6+klJSQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHNzcwAA
        AAAAAAAAAAAAAAAAAAAAAAAAAP///////6enpwAAAAAAAKenp////////yEhIQAAAAAAAAAAAAAA
        AAAAAAAAAAAAAP///////6enpwAAAAAAAKenp////////yEhIQAAAAAAAAAAAAAAAAAAAAAAAAAA
        AP///////6enpwAAAAAAAKenp////////3NzcwAAAAAAAAAAAAAAAAAAAAAAAAAAAP///////6en
        pwAAAAAAAKenp/////////r6+klJSQAAAAAAAAAAAAAAAAAAAAAAAP///////6enpwAAAAAAAKen
        p/////////////7+/q+vr0lJSSAgIAICAgAAAAAAAP///////6enpwAAAAAAAKenp///////////
        /////////////////1dXVwAAAAAAAP///////6enpwAAAAAAAKenp///////////////////////
        /////1dXVwAAAAAAAP///////6enpwAAAAAAAKenp////////////////////////////1dXVwAA
        AAAAAP///////6enpwAAAAAAAKenp////////////////////////////1dXVwAAAAAAAP//////
        /6enpwAAAAAAAKenp////////////////////////////1dXVwAAAAAAAP///////6enpwAAAAAA
        AKenp////////////////////////////1dXVwAAAAAAAP///////6enpwAAAAAAAKenp///////
        /////////////////////1dXVwAAAAAAAP///////6enpwAAAAAAAKenp///////////////////
        /////////1dXVwAAAAAAAP///////6enpwAAAAAAAKenp////////yH5BAAAAAAALAAAAAAQABAA
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AlhRM3jlw5c+fQpVO3jl07d+/gxZM3j149e/fw
        5dO3j18/f/8EBAA7
        }
        set gifImags(tableConfig) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMq
        KLrc/i7ISSWrKyidad5XJV4fKHCbiJVnN7mqxaJyGNeteaJwDNm4HyMBADs=
        }
        set gifImags(save) {
        R0lGODlhEAAQAPcNABgYGDc3Nzc3NysrKyoqKi0tLTc3Nzc3Nzc3NzU1NTY2NisrK6ysrP//////
        /////zc3N////////yAgIAAAAENDQ/f39/f39/f394CAgPf39z09PWZmZmpqavj4+P///zc3N///
        /////yMjIwAAAEVFRf///////////4eHh/f39z8/P////6ampj09PdnZ2Tc3N////////yQkJAAA
        AEVFRf///////////4eHh/f39xMTE09PT09PT0NDQy4uLjc3N////////yUlJQAAAEVFRf//////
        /////4eHh////+fn5+fn5+fn5+fn5zIyMjc3N////////2pqaklJSVdXV4eHh4eHh4eHh52dnf//
        /////////////////zc3Nzc3N///////////////////////////////////////////////////
        /////zc3Nzc3N////////////////////////////////////////////////////////zc3Nzc3
        N////////////////////////////////////////////////////////zc3Nzc3N////////8TE
        xL+/v7+/v7+/v7+/v7+/v7+/v7+/v9fX1////////////zc3Nzc3N////+jo6CsrK39/f39/f39/
        f39/f39/f39/f39/fzg4OP///////////zc3Nzc3N////+fn51dXV///////////////////////
        /////z8/P////////////zc3Nzc3N////+fn51dXV////////////////////////////z8/P///
        /////////zc3Nzc3N////+fn51dXV////////////////////////////z8/P////////////zc3
        Nzc3N////+fn51dXV////////////////////////////z8/P////////////zc3NwwMDDc3NzQ0
        NBUVFScnJycnJycnJycnJycnJycnJycnJx8fHzc3Nzc3Nzc3NwwMDCH5BAAAAAAALAAAAAAQABAA
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AlhRM3jlw5c+fQpVO3jl07d+/gxZM3j149e/fw
        5dO3j18/f/8EBAA7
        }
        set gifImags(weight) {
        R0lGODlhDwAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAPABAAAgMi
        KLrc/jCuQGuIVFWY9X3dBlrf6JVNJ6hpyTLhe1pSbd9MAgA7
        }
        set gifImags(background) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMt
        KLrcXDBKJ2uhNuJ8G4dbIE5eMZpaGZ3dA41B6savvLw03d54TG8Zh3BIbCQAADs=
        }
        set gifImags(CenterAlignment) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMa
        KLrc/i7ISSWrOMDNu28YF35k6WXVhU5mKyQAOw==
        }
        set gifImags(tableWidth) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMs
        KLrc/i7ISSWjy4pwZ+ZbV1WXAm7giY5YZn7vx3puaKbirOJ3fOsqiHC4SAAAOw==
        }
        set gifImags(exit) {
        R0lGODlhEAAPAPcMAAsLC0VFRVdXV1dXV1dXV1dXV1dXV1dXV1dXV1dXVz8/P5+fn///////////
        /////wsLCw0NDUVFRZWVlfr6+v///////////////////7e3t5+fn////////////////wsLCwAA
        AAAAAAEBASsrK2tra9/f3////////////7e3t5+fn////////////////wsLCwAAAAAAAAAAAAAA
        AAAAAEBAQP///////////7e3t5+fn/Dw8P///////////wsLCwAAAAAAAAAAAAAAAAAAAENDQ///
        /9HR0b+/wImJinh3eD08PoGBgvr6+v///wsLCwAAAAAAAAAAAAAAAAAAAENDQ////0hHSAEAAgEA
        AgEAAgEAAgEAAi4tL8rKygsLCwAAAAAAAAAAAAAAAAAAAENDQ////0hHSAEAAgEAAgEAAgEAAgEA
        AgwLDZGRkgsLCwAAAAAAAAAAAAAAAAAAAENDQ////6+vsI+PkGdnaFpaWikoKkhHSN/f3////wsL
        CwAAAAAAAAAAAAAAAAAAAENDQ////////////7e3t5+fn87Ozv///////////wsLCwAAAAAAAAAA
        AAAAAAAAAENDQ////////////7e3t5+fn////////////////wsLCwAAAAAAAAAAAAAAAAAAAEND
        Q////////////7e3t5+fn////////////////wcHBwAAAAAAAAAAAAAAAAAAACsrK5+fn5+fn5+f
        n3Jycp+fn////////////////15eXicnJwMDAwAAAAAAAAAAADAwMLe3t7e3t7e3t7e3t+Tk5P//
        /////////////////////7CwsFRUVBYWFgAAAENDQ///////////////////////////////////
        /////////////////+Tk5HJyclVVVf///////////////////////////////////wAAAAAAAAAA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAAAAAAALAAAAAAQAA8A
        AAj+AAEEEDCAQAEDBxAkULCAQQMHDyBEkDCBQgULFzBk0LCBQwcPH0CEEDGCRAkTJ1CkULGCRQsX
        L2DEkDGDRg0bN3Dk0LGDRw8fP4AEETKESBEjR5AkUbKESRMnT6BEkTKFShUrV7Bk0bKFSxcvX8CE
        ETOGTBkzZ9CkUbOGTRs3b+DEkTOHTh07d/Dk0bOHTx8/fwAFEjSIUCFDhxAlUrSIUSNHjyBFkjSJ
        UiVLlzBl0rSJUydPn0CFEjWKVClTp1ClUrWKVStXr2DFkjWLVi1bt3Dl0rWLVy9fv4AFEzaMWDFj
        x5AlU7aMWTNnz6BFkzaNWjVr17Bl07aNWzdv38AThRM3jlw5c+fQpVO3jl07d+8CAgA7
        }
        set gifImags(foreground) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMa
        KLrc/jDKSas8GN+cKd8f1HUip0Xmaa1skwAAOw==
        }
        set gifImags(superscript) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMl
        KLrc/jC6QBUNFjNtu+Ae6C2aKGbNiZJXOkKgus5b+9lSru9MAgA7
        }
        set gifImags(overstrike) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMh
        KLrc/jDGQEWQ1d7JNszSQnnPaI6hQjrk2mRgqblpbUcJADs=
        }
        set gifImags(link) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMi
        KLrc/vCFENkUtOYbM9YbiHkSxUEb6Xhqw7XuWC3nbN9MAgA7
        }
        set gifImags(search) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMk
        KLrcHTC4F+FUdmXqtpZcyHjfCE5VdQlqtFLny5KrK2PxrTMJADs=
        }
        set gifImags(normalscript) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMg
        KLrc/jC+QBUNES+drfQCJ1WfJXbhJ54MaU6X6ZZ0HSUAOw==
        }
        set gifImags(underline) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMh
        KLrc/jC+QBUNES8NufCOBzaiVGbMmK6ZVplXe82qZD8JADs=
        }
        set gifImags(externalFrame) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMo
        KLrc/i7ISSULUOG12+4CWIXXRZHc85UhqJUr+8ZpbXmyalZj5v+LBAA7
        }
        set gifImags(subscript) {
        R0lGODlhEAAQAPICAMzMzAAAAP///4iIiKoiAN1VAO67AACIACH5BAEAAAIALAAAAAAQABAAAgMj
        KLrc/jC6QBUNES+drfQCJ1UM2ZghuKlry4ooCV/WZX94jicAOw==
        }
          
          
        
        }
        
        proc initImages {} {
            variable ::images::pngImags
            if {[array exists ::images::pngImags] && [array size ::images::pngImags] == 0} {
            #puts "pngImages already there? exists [array exists ::images::pngImags] - size [array size ::images::pngImags]"
                # add images data to namespace
                ::images::setPngImgs
                foreach {name} [array names pngImags] {
                        image create photo ::images::$name -format png -data $pngImags($name)
                }
                
            }
        }
}
        


#****** textEditor/TEUtils
# NAME
#  TEUtils
# NAMESPACE
#  namespace
# DESCRIPTION
#  Utilities for text editior.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  06.05.2014
# SYNOPSIS
namespace eval TEUtils {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        
        namespace export *
# USES
#  Variables:
#    NONE
#  Procedures:
#    NONE
#****
        
                # get widget class name
                proc getWidgetClass {curW} {
                        if {![winfo exists $curW]} {return "";}
                        if {[catch {winfo class $curW}]} {return ""}
                        return [winfo class $curW];
                }
        
        # swap elements places in the list with selected positions
        proc swap {list pos1 pos2} {
                set temp [lindex $list $pos1]
                set list [lreplace $list $pos1 $pos1 [lindex $list $pos2]]
                set list [lreplace $list $pos2 $pos2 $temp]
                return $list
        }
        
        #puts [swap $aList 2 3]
        
        # ratingMap:
        # firstPos:             first position of selected list range
        # lastPos:                      last position of selected list range
        proc quickSortA {ratingMap targetList firstPos lastPos} {
                upvar 1 $ratingMap arrM
                if {![array exists arrM]} {
                   error "\"$ratingMap\" isn't an array"
                }
                set lowerPos [expr $firstPos + 1]
                set upperPos $lastPos
                
                set targetList [swap $targetList $firstPos [expr ($firstPos + $lastPos) / 2]] 
                #puts "targetList $targetList"
                set bound [getTagName [lindex $targetList $firstPos]]
                # #2908
                if {[catch {array exists $arrM($bound)} res]} {
                        putsstderr $res
                        return;
                                } else {
                set boundValue $arrM($bound)
                                }
                while {$lowerPos <= $upperPos} {
                        set lowerKey [getTagName [lindex $targetList $lowerPos]]
                        set upperKey [getTagName [lindex $targetList $upperPos]]
                        #puts "lowerPos $lowerPos upperPos $upperPos lowerKey $lowerKey upperKey $upperKey"
                        while {[info exists arrM($lowerKey)] && $arrM($lowerKey) < $boundValue } {
                                #puts "lowerPos $lowerPos upperPos $upperPos lowerKey $lowerKey upperKey $upperKey"
                                incr lowerPos
                                if {[lindex $targetList $lowerPos] == ""} {break;}
                                    set lowerKey [getTagName [lindex $targetList $lowerPos]]
                        }
                        while { [info exists arrM($upperKey)] && $boundValue < $arrM($upperKey)} {
                            incr upperPos -1
                                                        if {[lindex $targetList $upperPos] == ""} {break;}
                            set upperKey [getTagName [lindex $targetList $upperPos]]
                                                }
                        
                        if {$lowerPos < $upperPos} {
                                set targetList [swap $targetList $lowerPos $upperPos]
                                incr lowerPos
                                incr upperPos -1
                        } else {
                                incr lowerPos
                        }
                }
                
                set targetList [swap $targetList $upperPos $firstPos]
                
                if {$firstPos < [expr $upperPos -1]} {
                        set targetList [quickSortA arrM $targetList $firstPos [expr $upperPos - 1] ]
                } 
                if {[expr $upperPos  + 1 ] < $lastPos} {
                        set targetList [quickSortA arrM $targetList [expr $upperPos + 1] $lastPos] 
                }
                return $targetList
        }

        proc getTagName {inputAttr} {
                if {[string first ":" $inputAttr] <=0} {
                   putsstderr "Warning: $inputAttr no valid attribute!"
                   return $inputAttr;
                }
                foreach {name value} [split $inputAttr ":"] break;
                return $name
        }
        
        
        # width: the selected width number (integer)
        # fName: function name
        # sName: shortcut name
        proc setMenuStrAlign {width fName sName} {
                if {![string is integer $width] } {return;}
                set fName [::msgcat::mc $fName]
                set sName [::msgcat::mc $sName]
                return [format "%-*s %s" $width $fName $sName]
        }
        
        #****f* textEditor/TEUtils::initTextEditor
        # NAME
        #  initTextEditor
        # DESCRIPTION
        #  Module "textEditor" initialization
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc initTextEditor { { initdefaultSize 12 } { initdefaultConfigure {} } { initdefaultFont "Helvetica" } } {
                # PARAMETERS
                #       NONE
                # RETURN VALUE
                #       NONE
                # NOTES
                global tcl_platform
                
                package require BWidget
                package require msgcat
                package require Tk
                package require htmlparse
                package require fileutil
                package require tablelist
                package require base64
          
                #set defaultfont "Courier"
                #font configure TkTextFont -family Helvetica -size 12
                
                set ::textEditorMain::defaultFont $initdefaultFont
                set ::textEditorMain::defaultSize $initdefaultSize

                
                if {[::WordProcess::checkfontexits "TextEditorFont"] == 0} {
                                font create "TextEditorFont" -family $::textEditorMain::defaultFont -size $::textEditorMain::defaultSize
                }
          
                set ::textEditorMain::defaultConfigure $initdefaultConfigure


                # if platform is windows, take the package twapi to use window's 
                # clipboard for copy/paste function.
                if {[string tolower $tcl_platform(platform)] == "windows"} {
                   package require twapi
                }
                
                # icons
                ::images::initImages
                
        }
        # USES
        #  Variables:
        #       * tcl_platform -- System variable
        #       * msgcat
        #       * Tk
        #       * htmlparse
        #       * fileutil
        #       * tablelist
        #       * twapi
        #****
        
        
        proc initialServer {} {
                        global tcl_platform
                        
                        package require msgcat
                        package require htmlparse
                        package require fileutil
                        package require base64
          
                                
        }
        
        #****f* textEditor/TEUtils::getCurrentTime
        # NAME
        #  getCurrentTime
        # DESCRIPTION
        #  Get the current locale date and time with list form.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  10.09.2014
        # SYNOPSIS
        proc getCurTime {{locale "de"}} {
                # PARAMETERS
                #       locale:         a localized time and date string will appropriate to the inputed locale.
                # RETURN VALUE
                #       NONE
                # NOTES
                ;#"{date}  {time}"
                return [split [clock format [clock seconds] -format "%T;%a, %d %b %Y" -locale $locale] ";"]
        }
        # USES
        #  Variables:
        #       None
        #****
        
        
        #****** textEditor/TEUtils::fileForm
        # NAME
        #  fileForm
        # NAMESPACE
        #  namespace
        # DESCRIPTION
        #  all the file types for tk_getOpenFile dialog.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        namespace eval fileForm {
                # PARAMETERS
                #       None
                # RETURN VALUE 
                #       None
                # NOTES
                variable defaultextension
                # default form is txt
                set defaultextension ".txt"
                
                variable filetypes
                set filetypes {
                        {{XML Files}        {.xml}        }
                        {{HTML Files}       {.html}       }
                        {{Text Files}       {.txt}        }
                        {{TCL Scripts}      {.tcl}        }
                        {{C Source Files}   {.c}      TEXT}
                        {{GIF Files}        {}        GIFF}
                        {{All Files}        *             }
                }
        }
        # USES
        #  Variables:
        #       * defaultextension
        #****
        
        #****f* textEditor/TEUtils::wordSplit
        # NAME
        #  wordSplit
        # DESCRIPTION
        #  String as Separator to split the selected input string.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  02.09.2014
        # SYNOPSIS
        proc wordSplit {inputStr sep} {
                # PARAMETERS
                #       * inputStr              input data to be splited
                #       * sep                   string's separator
                # RETURN VALUE
                #       NONE
                # NOTES
                
                #\1 
                return [split [string map [list $sep \1] $inputStr] \1]
        }
        # USES
        #  Variables:
        #       NONE
        #****
        
        #****f* textEditor/TEUtils::png2Data
        # NAME
        #  wordSplit obsolete
        # DESCRIPTION
        #  Sets png file to the binary string data.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  02.09.2014
        # SYNOPSIS
        # 
        proc png2Data {fName} {
                # PARAMETERS
                #       * fName         the png file name (and Position)
                # RETURN VALUE
                #       NONE
                # NOTES
                if {[file isfile $fName] && [file extension $fName] == ".png"} {
                        set data {}
                        set fh [open $fName r]
                        fconfigure $fh -translation binary
                        set data  [base64::encode [read $fh]]
                        close $fh
                        return $data
                } else {return;}
        }
        # USES
        #  Variables:
        #       NONE
        #****      
  
  
        #****f* textEditor/TEUtils::png2Data
        # NAME
        #  wordSplit
        # DESCRIPTION
        #  Sets png file to the binary string data.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  02.09.2014
        # SYNOPSIS
        # 
        proc img2Data {fName} {
                # PARAMETERS
                #       * fName         the png file name (and Position)
                # RETURN VALUE
                #       NONE
                # NOTES
                if {[file isfile $fName]} {
                        set data {}
                        set fh [open $fName r]
                        fconfigure $fh -translation binary
                        set data  [base64::encode [read $fh]]
                        close $fh
                        return $data
                } else {return;}
        }
        # USES
        #  Variables:
        #       NONE
        #****
  
        #****f* textEditor/TEUtils::Data2img
        # NAME
        #  Data2img
        # DESCRIPTION
        #  Sets png file to the binary string data.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  02.09.2014
        # SYNOPSIS
        # 
        proc Data2img {fName fExt fData} {
                # PARAMETERS
                #       * fName         the png file name (and Position)
                # RETURN VALUE
                #       NONE
                # NOTES            
                set fh [open $fName.$fExt w]
                fconfigure $fh -translation binary
                set data  [base64::decode $fData]
                puts $fh $data
                close $fh
        }
        # USES
        #  Variables:
        #       NONE
        #****
        
        #****f* textEditor/::TEUtils::getValidTexSpacing
        # NAME
        #  compareTextIndex
        # DESCRIPTION
    #  Change the input spacing of Tcl/Tk to appropriate tex size
        #  for package setspace.
    # AUTHOR
    #  Xin Zhou
    # CREATION DATE 
    #  06.08.2014
    # SYNOPSIS
        proc getValidTexSpacing {dist} {
                # PARAMETERS
                #  * dist:                      input Tcl/Tk spacing parameter
                # RETURN VALUE
                #       Returns 
                # NOTES
                if {![string is double $dist]} {
                        putsstderr "Warning: the input spacing distance parameter \"$dist\" is not valid!"; 
                        return;
                }
                # default size: 10 px/pt <-> 1.25 (reference: setspace.sty)
                set deffactor 0.125
                
                return [format "%.3f" [expr $dist * $deffactor ]]
                                                                 #Yvonne +1 ? 
        }
        # USES
        #  Variables:
        #        NONE
        #  Procedures:
        #    NONE
        # USED BY
        # * 
        #****
        
        
        # clean the input list
        # Attention: key - value pair!
        proc getValidList {frameInfos inputList} {
                set temp ""
                set pattern {(\s*(\{|\})\s*)|(sel)} 
                regsub -all $pattern $inputList " " temp
                set result ""
                set getValue 0
                foreach item $temp {
                        # user preset tag name
                        #if {[lsearch -exact $TextStyle::userPreset $item] >= 0} {
                        #        set getValue 0
                        #        set temp [list "preset" $item]
                        #        set result [::TEUtils::appendValue $result $temp " "]
                        #}
                        if {$getValue == 1} {
                                set result [::TEUtils::appendValue $result $item " "]
                                set getValue 0
                        } else {
                                if {[lsearch -exact $frameInfos $item] >= 0} {
                                        set result [::TEUtils::appendValue $result $item " "]
                                        set getValue 1
                                }
                        }
                }
                return $result
        }
        
        
        # fontMap --
        #    Change the undefined font style to the specified font style.
        #
        # Arguments:
        #    inputFont: the given font style (e.g. Arial, Calibri...)
        #
        # Result:
        #    Returns the specified font style
        #
        proc fontMap {inputFont} {
                if {$inputFont == ""} {return "";}
                set resultFont ""
                set fMap {
                        "Arial"                         ""
                        "Tahoma"                        ""
                        "Verdana"                       ""
                        "Calibri"                       ""
                        "Times New Roman"       "Times"
                        "Roman"                         "Times"
                        "Times"                         "Times"
                        "Courier New"           "Courier"
                        "Letter Gothic"         "Courier"
                        "Courier"                       "Courier"
                }
                # the valid font style
                if { [lsearch -exact [dict values $fMap] $inputFont]>= 0 } {
                        return $inputFont
                } else {
                        set families ""
                        if {[string first "," $inputFont] >= 0} {
                                # more styles
                                set families [split $inputFont ","]
                        } else {
                                # single style
                                set families $inputFont
                        }
                        # compare the input font styles
                        if {$families != ""} {
                                
                                foreach item $families {
                                        set item [string trim [string map {\" {} ' {}} $item]] ;#"eclipse
                                        if {[dict exists $fMap $item]} {
                                                lappend resultFont [dict get $fMap $item]
                                                # get the first valid one
                                                break;
                                        }
                                }
                        }
                        # if no valid style
                        if {[lsearch -exact $fMap $inputFont] < 0} {
                                lappend resultFont ""
                        } 
                        return [lindex $resultFont 0]
                }
        }
        
        proc mapXMLToHTML {value} {
                set xmlMap {
                        "\x26" "\&amp;"
                        "<" "\&lt;"
                        ">" "\&gt;"
                        "<lb\/>" "<br\/>"
                }
                return [string map $xmlMap $value]
        }
        
        
        
        proc mapXML {value} {
                set xmlMap {
                        "\x26" "\&amp;"
                        "<" "\&lt;"
                        ">" "\&gt;"
                        "\n" "<lb\/>" 
                     
                }
                return [string map $xmlMap $value]
        }
        
        proc mapXMLAndSpace {value} {
          set value [mapXML $value ]
          # removed, not required in TEI
          #regsub -all {[ ]{2,}} $value "<space>\\0</space>" value ;
          #set spaceMap {
          #  " <lb\/>" "<space> </space><lb\/>" 
          #  "<lb\/> " "<lb\/><space> </space>" 
          #}
          #set value [string map $spaceMap $value]
          return $value
        }
        
        proc mapHTMLspace {value} {
          regsub -all {[ ]{2,}} $value " " value ;
          return $value
        }
        
        proc mapLinkTextToXml {value} {
                set map {
                        " " "0xA1A1"
                }
                return [string map $map $value]
        }
        
        proc mapLinkXmltoText {value} {
                set map {
                        "0xA1A1" " "
                }
                return [string map $map $value]
        }
        
        proc mapHTMLToText {value} {
                set xmlMap {
                        "\&lt;" "<" 
                        "\&gt;" ">" 
                        "\&amp;" "\x26"
                }
                        ;#"&#123;" "\{"
                        ;#"&#125;" "\}"
                return [string map $xmlMap $value]
        }
        
        proc changeBracket {value} {
                set scMap {
                        "\{" "&#123;" 
                        "\}" "&#125;"
                }
                return [string map $scMap $value]
        }
        
        proc backToBracket {value} {
                set scMap {
                        "&#123;" "\{"  
                        "&#125;" "\}" 
                }
                return [string map $scMap $value]
        }
        
        # cleanWhitespace -- 
        #       Elimination of whitespace in the given string
        #
        # Arguments:
        #        inputStr: the given string to analyze
        #
        # Return:
        #        Returns the string without whitespaces
        #
        proc cleanWhitespace {inputStr} {
                if {$inputStr == ""} {return ""}
                set wsMap {
                        "\r" " "
                        "\v" " "
                        "\n" " "
                        "\f" " "
                }
        #       "\t" " "
        #               "><" "> <"
                set result [string map $wsMap $inputStr]
        #       puts $result
                return $result
        }
                
                proc cleanWhitespaceW {inputStr} {
                                if {$inputStr == ""} {return ""}
                                regsub -all  "(\n|\r|\v|\f)+" $inputStr " " str
                                return $str
                }
                
        
        proc cleanWhitespaceInText {inputStr} {
                set wsMap {
                        "\r" ""
                        "\v" ""
                        "\n" ""
                        "\f" ""
                }
                set result [string map $wsMap $inputStr]
                return $result
        }
        
        
        proc checkBodyTag {inputStr} {
                if {$inputStr == ""} {return 0;}
                # get start position of <body>
                set startIndex [string first "<body" $inputStr]
                
                # get end position of <body>
                set endIndex [string last "</body>" $inputStr]
                if {$startIndex >=0 & $endIndex >=0} {
                        return 1;
                } else {
                        return 0;
                }
        }
        
        
        #****f* textEditor/::TEUtils::compareTextIndex
        # NAME
        #  compareTextIndex
        # DESCRIPTION
        #  Compare the given firstIdx and secondIdx in the selected text widget,
        #  returns -1 if firstIdx less than secondIdx, 1 if firstIdx greater than secondIdx
        #  and 0 if they are equal.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc compareTextIndex { text firstIdx secondIdx {compType dict}} {
                # PARAMETERS
                #  * text:                      the selected text widget
                #  * firstIdx:          the first index
                #  * secondIdx:         the second index
                #  * compType:          compare type: dict, ascii, integer...
                # RETURN VALUE
                #       Returns -1, 0 or 1.
                # NOTES
                if {$firstIdx == "" || $secondIdx == ""} {return;}
                
                # make sure to the line.char form
                if {[catch {set firstIdx [$text index $firstIdx]}]} {
                        putsstderr "Error: There is index Error in the $text with index $firstIdx"
                        return "";
                }
                if {[catch {set secondIdx [$text index $secondIdx]}]} {
                        putsstderr "Error: There is index Error in the $text with index $secondIdx"
                        return "";
                }
                
                # if they equal, returns 0
                if { $firstIdx == $secondIdx } {return 0;}
                
                # set a new list to sort
                set tempList [concat $firstIdx $secondIdx]
                
                # get compare form
                # default: dictionary
                switch -nocase -glob -- $compType {
                        d* { set compT "-dict" }
                        a* { set compT "-ascii" }
                        r* { set compT "-real" }
                        default { set compT "-dict" }
                }
                
                # sorting
                set tempList [eval [list lsort $compT $tempList]]
                
                # get first item of sorted list
                set firstPlace [lindex $tempList 0]
                
                if {$firstPlace eq $firstIdx} {
                        set result -1
                } else {
                        set result 1
                }       
                return $result
        }
        # USES
        #  Variables:
        #        NONE
        #  Procedures:
        #    NONE
        # USED BY
        # * ::searchTE::getMainTextInfos
        # * ::searchTE::searchText
        #****
        
        
        # refreshStatus --
        #    refresh and back to the default status. (e.g. Buttons, Arrays, Lists)
        # Arguments:
        #    None
        # Result:
        #    None
        # Side effect:
        #    None
        #
        proc refreshStatus {} {
                variable ::textEditorMain::tagPositions
                variable ::textEditorMain::xmlData
                ::xins::resetXinsArray

                set ::textEditorMain::savePath ""
                set ::textEditorMain::entityNum 0
                
                set ::textEditorMain::curWName ""
                
                #table number
                set ::textEditorMain::tableNum 0
                set ::textEditorMain::tableRowNum 0
                set ::textEditorMain::tableColNum 0
                
                #::textEditorMain::resetCells
                
                
                # clear tagPositions
                foreach key [array names tagPositions] {
                        unset tagPositions($key)
                }
                # clear xmlTagArray
                foreach key [array names ::textEditorMain::xmlTagArray] {
                        unset ::textEditorMain::xmlTagArray($key)
                }       
        
                
                # clear xmlData
                foreach key [array names ::textEditorMain::xmlData] {
                        unset ::textEditorMain::xmlData($key)
                }
                
                
                set xmlData(justify) {}
        
                set xmlData(inBody) 0
                set xmlData(inTable) 0
                set xmlData(inHeader) 0
                set xmlData(inRend) 0
                set xmlData(hasHeadStyle) 0     
                set xmlData(paragraphTag) ""
                set xmlData(main,inBody) 0
                set xmlData(main,inRend) 0
                set xmlData(main,bodyLevel) 0
                set xmlData(main,currentAttrs) ""
                set xmlData(main,tagName) ""
                set xmlData(main,xmlArrName) "ct"
                set xmlData(main,headText) {}
                set xmlData(main,bodyText) {}              
                set xmlData(main,inTag) 0
                set xmlData(main,text-align) ""
                set xmlData(main,padding-top) ""
                set xmlData(main,padding-bottom) ""
                
                set xmlData(tableCell,inBody) 0
                set xmlData(tableCell,inRend) 0
                set xmlData(tableCell,bodyLevel) 0
                set xmlData(tableCell,currentAttrs) ""
                set xmlData(tableCell,tagName) ""
                set xmlData(tableCell,xmlArrName) "tableCt"
                set xmlData(tableCell,bodyText) {}
                set xmlData(tableCell,embeddedTable) 0
                set xmlData(tableCell,currentTableName) ""
                set xmlData(tableCell,curPosition) ""
                set xmlData(tableCell,curCellAttrs) ""
                set xmlData(tableCell,text-align) ""
                set xmlData(tableCell,padding-top) ""
                set xmlData(tableCell,padding-bottom) ""
        }
        
        # only ; and digit
        proc validEditTableInput {val} {
                #puts "val: $val"
                return [regexp {^(\d+\;?\d*)+$} $val]
        }
        
        
        # only following form: 34;(0)7;(0)55
        proc validTableCellWidths {input} {
                #puts "input: $input"
                return [regexp {^[1-9][\d]*([;][1-9]+)*$} $input]
        }
        
        
        # checkTableContents --
        #       Check the row/column number and the text widget number.
        #
        # Arguments:
        #   rowNum:     row number  
        #   colNum:     column number 
        #   textlist:   the given text widget list  
        #
        # Returns:
        #       Returns 1 if they are conformed otherweise returns 0.
        #
        proc checkTableContents {rowNum colNum textlist} {
                if {![string is integer $rowNum] | ![string is integer $colNum]} {
                        putsstderr "$rowNum or $colNum is not a Integer"
                        return 0;
                }
                if {$textlist == ""} {
                        putsstderr "text list is empty!"
                        return;
                }
                set total [expr $rowNum * $colNum]
                set listLen [llength $textlist]
                # they must be equal!
                return [expr $total == $listLen]
        }
        
        
        # winChildrenList --
        #       Close the selected window.
        #
        # Arguments:
        #   w:  the given window  
        #
        # Returns:
        #       none.
        #
        proc winChildrenList {{W .}} {
          set childrenlist [list $W]
          foreach w [winfo children $W] {
                set childrenlist [concat $childrenlist [winChildrenList $w]]
          }
          return $childrenlist
        }
        
        
        # Attention: While-Loop!
        #(a1) .ef.tedit.text.tableWindow0table                                          Tablelist
        #(a2) .ef.tedit.text.tableWindow0table.body                             Text
        #(a3) .ef.tedit.text.tableWindow0table.body.frm_k0,0            TablelistWindow
        #(a4) .ef.tedit.text.tableWindow0table.body.frm_k0,0.w          Frame
        #(a5) .                                                                                                         Applaunch
        proc getWidgetParent {curW parClass} {
                if {![winfo exists $curW] || $parClass == ""} {return "";}
        
                set tempW $curW
                while {[winfo exists $tempW] != ""} {
                        set tempW [winfo parent $tempW]
                        if {[catch {winfo class $tempW} ] } {return ""}
                        if {[string equal -nocase [winfo class $tempW] $parClass]} {break;}
                }
                return $tempW
        }
        
                proc getWidgetClass {curW} {
                        if {![winfo exists $curW]} {return "";}
                        if {[catch {winfo class $curW}]} {return ""}
                        return [winfo class $curW];
                }
                
        
        # get mouse position: x, y
        proc getMousePosition {} {
                set curWindow [lindex [wm stackorder .] end]
                set x [expr {[winfo pointerx .] - [winfo rootx $curWindow]}]
                set y [expr {[winfo pointery .] - [winfo rooty $curWindow]}]
                return "$x $y"
        }
        
        #
        proc textIndexCompare {string1 string2} {
        #analog zu string match: gibt 1 zur�ck, wenn string1 gr��er ist als string2, 0 bei Gleichheit;
        # -1, wenn string1 kleiner ist als string2 (dabei stehen die strings f�r Indexe der Form 2.89)
        
                 set string1 [textIndex2array $string1]
                 set string2 [textIndex2array $string2]
        
                 set string1line [lindex $string1 0]
                 set string1char [lindex $string1 1]
                 set string2line [lindex $string2 0]
                 set string2char [lindex $string2 1]
        
                 if {$string1line < $string2line} {
                          return -1
                 } elseif {$string1line == $string2line && $string1char < $string2char} {
                          return -1
                 } elseif {$string1line == $string2line && $string1char ==  $string2char} {
                          return 0
                 } elseif {$string1line == $string2line && $string1char > $string2char} {
                          return 1
                 } elseif {$string1line > $string2line} {
                          return 1
                 }
        }
        #
        proc textIndex2array {index} {
                 set trenner [string first . $index]
                 set indexline [string range $index 0 [expr $trenner -1]]
                 set indexchar [string range $index [expr $trenner +1] end]
        
                 return "$indexline $indexchar"
        }
        
        # UTF8FullCodes -- 
        #        Change the given sequences of given buffer convert to utf-8. 
        #
        # Arguments:
        #        pBuffer: the given sequences
        #
        # Return:
        #        Returns the converted utf8 sequences 
        #
        proc UTF8FullCodes {pBuffer {form 0}} {
                if {$form == 0} {
                   upvar $pBuffer Buffer
                } else {
                        set Buffer $pBuffer
                }
                # > Get last position index
                set LastPos [string length $Buffer]
                incr LastPos -1
                # > Current bytecount of a multi byte character includes the start character
                set nBytes 1
                # Loop over bytes from the end
                for {set Pos $LastPos} {$Pos >= 0} {incr Pos -1} {
                        set Code [scan [string index $Buffer $Pos] %c]
                        if { $Code < 0x80 } {
                                # > ASCII
                                break
                        }
                        if { $Code < 0xbf } {
                                # > Component of a multi-byte sequence
                                incr nBytes
                        } else {
                                # > First byte of a multi-byte sequence
                                # Find number of required bytes
                                for {set Bytes 2} {$Bytes <= 6} {incr Bytes} {
                                        # > Check for zero at Position (7 - Bytes)
                                        if {0 == (( 1 << (7 - $Bytes)) & $Code)} {
                                                break
                                        }
                                }
        #                       puts "Bytes=$Bytes"
                                if { $Bytes == $nBytes } {
                                        # > Complete code
                                        set Pos $LastPos
                                        break
                                } else {
                                        # > Incomplete code
                                        incr Pos -1
                                        break
                                }
                        }
           }
           # Convert complete sequence until Pos
           set Res [encoding convertfrom utf-8 [string range $Buffer 0 $Pos]]
           # Put incomplete character in Buffer
           incr Pos
           set Buffer [string range $Buffer $Pos end]
           return $Res
        }
        
        
        # posDetermine --
        #    Get the last position index of input string with different types
        # Arguments:
        #    inputStr      the given input string
        #        type: start or end form (start 0 / end 1)
        # Returns:
        #    Returns the index
        #
        proc posDetermine {inputStr type} {
                if {$inputStr == ""} {return -1;}
                if {$type == 0} {
                        #start
                        return [string length $inputStr]
                } else {
                        return [expr [string length $inputStr] -1]
                }
        }
        
        # lengthPrediction --
        #    Analyze the input string and double the characters number 
        #        then change the number as form as "00..00". 
        #        This is for whole text length's prediction.
        # Arguments:
        #    inputStr      the given input string
        # Returns:
        #    Returns the predicted Number as form as "00...00"
        #
        proc lengthPrediction {inputStr} {
                if {$inputStr == ""} {return -1;}
                set charNum [string length $inputStr]
                set charNumLen [string length $charNum]
                set doubleNumLen [expr $charNumLen * 2]
                set result ""
                while {$doubleNumLen > 0} {
                        append result 0
                        incr doubleNumLen -1
                }
                return $result
        }
        
        # changeIndexForm --
        #       Change the real index form to the reference value form.
        # Arguments:
        #    realIdx            the real index
        #    defaultNumber  the reference values of index with form "00..00"
        # Returns:
        #    Returns the index with "00..xyz" (e.g. 0012, 000878)
        #
        proc changeIndexForm {realIdx defaultNumber} {
                if {$realIdx == "" | $defaultNumber == ""} {return -1;}
                set realIdxLen [string length $realIdx]
                set defNumLen [string length $defaultNumber]
                if {$realIdxLen > $defNumLen} {return -1;}
                
                set complemZeroNum [expr $defNumLen - $realIdxLen]
                set result ""
                while {$complemZeroNum > 0} {
                        append result 0 
                        incr complemZeroNum -1
                }
                append result $realIdx
                return $result
        }
        
        proc valueExists {old val} {
                #puts "old: $old - val: $val"
                set result [expr [string first $val $old] >=0]
                #puts $result
                return $result
        }
        
        # delListElement -- 
        #        Delete the selected Element in the given List.
        #
        # Arguments:
        #        inputList: the given list
        #        ele: the selected Element
        #
        # Returns:
        #        Returns the new result List.
        #
        proc delListElement {inputList ele} {
                if {[llength $inputList] == 0 || $ele == ""} {return}
                # get element's index
                set index [lsearch -exact $inputList $ele]
                if {$index >= 0} {
                        # if list has the element, delete it.
                        set inputList [lreplace $inputList $index $index]
                }
                return $inputList
        }
        
        # listSearchPart --
        #       Returns the found list from the given input list 
        #       with the selected part string and type(prefix, suffix, contain).
        #
        # Arguments:
        #   inputList:  the given input list
        #       part:           the selected part to search
        #       type:           0:prefix; 1:suffix; 2:contain;
        #
        # Returns:
        #       Returns the new found list.
        #
        proc listSearchPart {inputList part type} {
                if {$inputList == "" | $part == ""} {
                        putsstderr "inputlist or part has some probleme!"
                        return $inputList;
                }
                
                set result ""
                set pattern ""
                if {$type == 0} {
                        set pattern "$part*"
                } elseif {$type==1} {
                        set pattern "*$part"
                } elseif {$type==2} {
                        set pattern "*$part*"
                } 
                if {$pattern == ""} {
                        putsstderr "$type is not valid type!"
                        return $inputList;
                }
                return [lsearch -inline -all $inputList $pattern]
        }
        
        
        
        # appendValue -- 
        #        Append the new value to the old values with separator
        #
        # Arguments:
        #        oldValues: the existed values 
        #        newValue:      the given new value
        #        sep:           the given separator(e.g. ; \s)
        #
        # Returns:
        #        Returns the new value list.
        #
        proc appendValue {oldValues newValue sep} {
                if {$newValue == ""} {
                        return $oldValues;
                } else {
                        if {$oldValues == ""} {
                                return $newValue;
                        } else {
                                append oldValues "$sep" $newValue
                                return $oldValues;
                        }
                }
        }
        
        
        
        # copyArr -- 
        #        Copy the source array to the new target array
        #
        # Arguments:
        #        sourceArr: the copy source array
        #        targetArr: the target array
        #
        # Returns:
        #        none
        #
        proc copyArr {sourceArr targetArr} {
                upvar  $sourceArr sArr
                upvar  $targetArr tArr
                
                if {![array exists sArr]} {
                        puts "There is empty or not valid source am $sourceArr"
                        return
                }
                array set tArr [array get sArr]
        }
        
        # setMapValue -- 
        #       Set new array with given key and value
        #
        # Arguments:
        #        map: the given key value mapping array
        #        key: the new key 
        #        value: the new value
        #
        # Return:
        #        Returns the new array.
        #
        proc setMapValue {map key value} {
                upvar $map arrM
                if {![array exist arrM]} {
                        array set arrM {}       
                } 
                set arrM($key) $value
        }
        
        
        # getNumber -- 
        #       Returns the integer of the given input string with regexp
        #
        # Arguments:
        #        input:         the selected input string
        #        
        # Return:
        #        Returns the found integer(or number, decimals).
        #
        proc getNumber {input} {
                if {$input == ""} {return "";}
                set all ""
                set integer ""
                set decimal ""
                if {[catch {regexp {(-?\d+)(\.\d+)?} $input all integer decimal} result]} {
                        putsstderr "Error: There is no Number in $input!"
                        putsstderr $result
                        return $input;
                }
                return $integer
        }
        
        # getFileSuffix -- 
        #       Returns the file valid suffix of the given input file name or NULL.
        #
        # Arguments:
        #        fileName:      the selected input file name
        #        
        # Return:
        #        Returns the found valid suffix or Null.
        # Attention:
        #
        # Now only tei,xml,html,xhtml are valid.
        proc getFileSuffix {fileName} {
                set result ""
                # valid suffix (..|..|....|..)
                if {[catch {regexp -nocase {^.*?\.(tei|xml|html|xhtml|tex|rtf)$} $fileName match suffix} res]} {
                        putsstderr "Error: There is something wrong in $fileName!"
                        putsstderr $res
                        return "";
                }
                if {![info exists suffix]} {
                        putsstderr "sry, no suffix is xml|html|xhtml|tex!"
                        return "";
                } else {
                        set result [string tolower $suffix]
                }
                return $result
        }
        
        
        # checkBracketPairs --
        #    Returns 1 when there are the same number of left bracket
        #        and right bracket in the given input string. 
        # Arguments:
        #    inputStr           the given input string to analyze
        #    leftBrac           the selected bracket(e.g. \{, \[, \(...)
        #        rightBrac      the selected right bracket(e.g. \}, \], \)...)
        # Result:
        #    Returns true 1 or false 0
        #
        proc checkBracketPairs {inputStr leftBrac rightBrac} {
                if {$inputStr == "" | $leftBrac=="" | $rightBrac == ""} {return 0;}
                if {[string first $leftBrac $inputStr] <0 | [string first $rightBrac $inputStr] <0} {return 0;}
                # left bracket count
                set lbCount 0
                # right bracket count
                set rbCount 0
                
                # left bracket index
                set lbIdx 0
                # right bracket index
                set rbIdx 0
                
                while {$lbIdx >= 0} {
                        # from 0 to start
                        set lbIdx [string first $leftBrac $inputStr $lbIdx]
                        if {$lbIdx >= 0} {
                                incr lbCount
                                incr lbIdx
                        }
                }
                
                while {$rbIdx >= 0} {
                        # from 0 to start
                        set rbIdx [string first $rightBrac $inputStr $rbIdx]
                        if {$rbIdx >= 0} {
                                incr rbCount
                                incr rbIdx
                        }
                        if {$rbCount > $lbCount} {return 0;}
                }
                return [expr $rbCount==$lbCount]
        }
        
        
        #getLineend --
        #       Get the index of selected line end in a text.
        #
        #Arguments:
        #       text:           the given text widget
        #       lineNum:        the selected line
        #
        #Result:
        #       None
        #Effect:
        #       None
        #
        proc getLineend {text lineNum} {
                return [$text index "$lineNum.0 lineend"]
        }
        
        #TextDump --
        #       Dump the contents of the given text widget
        #
        #Arguments:
        #       start: (optional) the start position of the specified contents
        #       end : (optional) the end position of the specified contents
        #
        #Returns:
        #       Nothing
        #
        #Side effects
        #       Dumping the text widget in a console.
        #
        proc TextDump {t {startIdx 1.0} {endIdx end}} {
                
                if {$startIdx == "sel.first"} {
                        #::WordProcess::getSelOldTags $t
                        puts "startIdx: [$t index $startIdx]"
                        puts "endIdx: [$t index $endIdx]"
                        puts "start tags: [$t tag names $startIdx]" 
                        puts "end tags: [$t tag names $endIdx]"
                }
                puts "$startIdx $endIdx"
                foreach {key value index} [$t dump $startIdx $endIdx] {
                        puts "$index $key $value"
                }
                puts "------"
                #puts [::textEditorMain::getTableIndices $t end 1.0]
        }
}





#NTODO: REPLACE PROBLEM
#****** textEditor/searchTE
# NAME
#  searchTE
# NAMESPACE
#  namespace
# DESCRIPTION
#  Searching the key word in the text widgets. 
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  04.04.2014
# SYNOPSIS
namespace eval searchTE {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        
        namespace export *
        
        # all options and parameters 
        variable searchOptions
        array set searchOptions {}
        set searchOptions(curText) ""
        # for main text
        set searchOptions(mainText) ""
        set searchOptions(hitWord) 0
        
        variable lastTextWidget {}
#Important: end for namespace description!!
#****   
        #curText, cursorIndex,
        #direction, searchKeyWord, replaceKeyWord
        #wrap, newStart, caseSensitiv
        
        #default situations
        #set ::searchTE::searchOptions(searchKeyWord) ""
        #set ::searchTE::searchOptions(replaceKeyWord) ""
        #set ::searchTE::searchOptions(direction) "searchForward"
        #set ::searchTE::searchOptions(wrap) 1
        #set ::searchTE::searchOptions(newStart) 0
        #set ::searchTE::searchOptions(caseSensitiv) 0
        #set ::searchTE::searchOptions(curText) "" 
        #set ::searchTE::searchOptions(cursorIndex) "" 
        
        proc initSearchFunc {targetText} {
                        variable ::searchTE::searchOptions
                        variable ::textEditorMain::editorText
                        if {[winfo exists $targetText]} {
                                set searchOptions(mainText) $targetText 
                        } else {
                                set searchOptions(mainText) $editorText
                        }
                        return 1;
        }
        
        #****f* textEditor/searchTE::replaceText
        # NAME
        #  replaceText
        # DESCRIPTION
        #  Replace main function: Replace the found words with new word 
        #  in text widgets(e.g. main text, cell text in table)
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc replaceText {{type 0}} {
                # PARAMETERS
                #       type: 0 - single word to replace / 1 - all same words to replace
                # RETURN VALUE
                #       NONE
                # NOTES
                variable ::searchTE::searchOptions
        
                if {$type == 0} {
                        # invoke single replace
                        replaceSingle
                } elseif {$type == 1} {
                        # all replace
                        set searchOptions(curText) $searchOptions(mainText)
                        # search direction
                        if {$searchOptions(direction) == "searchForward"} {
                                set startIdx 1.0
                        } else {
                                set startIdx end
                        }
                        # from the start of main text widget
                        set searchOptions(cursorIndex) $startIdx
                        $searchOptions(mainText) mark set insert $startIdx
                        # invoke search function 
                        searchTextShell
                        # initialized the search flag.
                        set ja 1
                        # Attention: Recursive method!!
                        while {$ja} {
                                # invoking the procedure replaceSingle and take the result.
                                set ja [replaceSingle]
                        }
                }
        }
        # USES
        #  Variables:
        #  * searchOptions: all options and parameters of search functions
        #  Procedures:
        #  * (searchTE::)replaceSingle
        #  * (searchTE::)searchTextShell
        # USED BY
        # * ::TEGUI::invokeSearchDialog
        #****
        
        #****f* textEditor/searchTE::replaceSingle
        # NAME
        #  replaceSingle
        # DESCRIPTION
        #  Replace the found word with new word 
        #  in text widget(e.g. main text or cell text in table)
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc replaceSingle {} {
                # PARAMETERS
                #       NONE
                # RETURN VALUE
                #       1 - ok ; 0 - no hit.
                # NOTES
                variable ::searchTE::searchOptions
                # locate the selection position and found word.
                if { [$searchOptions(curText) tag ranges sel] != "" } {
                        # remove old word
                        $searchOptions(curText) delete sel.first sel.last
                        # insert a new replacing word
                        
                        set tagList [::WordProcess::getPTag $searchOptions(curText) {}]
                        $searchOptions(curText) insert insert $searchTE::searchOptions(replaceKeyWord) $tagList
                        
                        #$searchOptions(curText) insert insert $searchTE::searchOptions(replaceKeyWord)
                        # invoke for searching new target
                        searchTextShell
                        return 1;
                } else {
                        return 0;
                }
        }
        # USES
        #  Variables:
        #  * searchOptions: all options and parameters of search functions
        #  Procedures:
        #  * (searchTE::)searchTextShell
        # USED BY
        # * (searchTE::)replaceText
        #****
        
        
        #****f* textEditor/searchTE::invokeReplaceButtons
        # NAME
        #  invokeReplaceButtons
        # DESCRIPTION
        #  Replace/Replace buttons are usually disabled to use,
        #  then check some conditions to decide the buttons states (normal or disabled).
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc invokeReplaceButtons {replaceEntry} {
                # PARAMETERS
                #  * replaceEntry: text entry for replacing word. 
                # RETURN VALUE
                #       NONE
                # NOTES
                variable ::searchTE::searchOptions
                
                if {![winfo exists $replaceEntry] } {return;}
                # get replacing word
                set content [$replaceEntry get]
                #puts "invokeReplaceButtons.content: $content"
                # buttons exists and with form "replaceButton replaceAllButton"
                if {[info exists searchTE::searchOptions(replaceButton)] 
                        && [llength $searchTE::searchOptions(replaceButton)] == 2 
                } {
                        #puts "searchTE::searchOptions(replaceButton) $searchTE::searchOptions(replaceButton)"
                                set replaceButton [lindex  $searchTE::searchOptions(replaceButton) 0]
                        set replaceAllButton [lindex  $searchTE::searchOptions(replaceButton) 1]
                        # if there is some searched word in the text widget and replace entry not empty
                        if {$searchOptions(hitWord) && [string length $content] > 0} {
                                $replaceButton  configure -state normal
                                $replaceAllButton  configure -state normal
                        } else {
                                $replaceButton  configure -state disabled
                                $replaceAllButton  configure -state disabled
                        }
                } 
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  Procedures:
        #       default
        # USED BY
        # * ::TEGUI::invokeSearchDialog
        #****
        

        #****f* textEditor/searchTE::searchTextShell
        # NAME
        #  searchTextShell
        # DESCRIPTION
        #  search main function
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc searchTextShell {} {
                # PARAMETERS
                #  NONE 
                # RETURN VALUE
                #       NONE
                # NOTES
                variable ::searchTE::searchOptions
                variable ::searchTE::lastTextWidget
                
                # hit a word or not
                set searchOptions(hitWord) 0
                
                # refresh the cursor informations 
                set infoValue [getCursorInfo]           ;# 0.
                if {!$infoValue} {return;}
                
                # clean the selection tag
                catch {$searchOptions(curText) tag remove sel sel.first sel.last}
                
                # if current text widget is not main text, in other words the cell text of embedded table.
                if { $searchOptions(curText) != $searchOptions(mainText) } {
                        # search in the table cell texts
                        set tableRes [ searchTableCells $searchOptions(curText) ]       ;# 1.
                        if {$tableRes} {
                                # Return and exit, if found some word. 
                                return;
                        } else {
                                # found nothing in the table cell texts 
                                
                                # get the current table index
                                set tbl [::TEUtils::getWidgetParent $searchOptions(curText) "tablelist"]
                                set tableIdx [::textEditorMain::getTableIndexByName $searchOptions(mainText) $tbl]
                                
                                #2850
                                # set the new insert tag in the main text depending the search direction
                                if {$searchOptions(direction) == "searchForward"} {
                                                                        if {$tableIdx == ""} {set tableIdx 1.0}
                                    set insertPos [$searchOptions(mainText) index "$tableIdx + 1 chars"]
                                } else {
                                                                        if {$tableIdx == ""} {set tableIdx end}
                                    set insertPos [$searchOptions(mainText) index $tableIdx]
                                }
                                
                                $searchOptions(mainText) mark set insert $insertPos
                                set searchOptions(cursorIndex) $insertPos
                                
                                # set main text as next searching text widget 
                                set searchOptions(curText) $searchOptions(mainText)
                                set lastTextWidget $searchOptions(mainText)
                        }
                }
                # set end index
                set endIdx [getEndIndex $searchOptions(curText)]
                # get the main text informations, where are pure text contents and tables
                set areaList [getMainTextInfos $searchOptions(cursorIndex) $endIdx]             ;# 3.
                
                # main text have some text Contents and tables
                if {[llength $areaList] != 0} {
                        # get all index items
                        foreach item $areaList {
                                # table index
                                if {[llength $item] == 1} {
                                        # get the table name according to the index
                                        set tableName [$searchOptions(mainText) window cget $item -window] 
                                        if {$tableName == ""} {
                                                putsstderr "Warning: There is no valid table in the position $item!"
                                                 continue;
                                        }
                                        
                                        # get the first/last cell of table according to the search direction
                                        if {$searchOptions(direction) == "searchForward"} {
                                                set firstCellW [$tableName windowpath 0,0]
                                                set firstCellText "$firstCellW.text"
                                                # get next widget name
                                                set searchOptions(curText) $firstCellText
                                                set lastTextWidget $firstCellText
                                                set insertPos "1.0"
                                                
                                        } else {
                                                set lastCellW [$tableName windowpath end]
                                                set lastCellText "$lastCellW.text"
                                                # get next widget name
                                                set searchOptions(curText) $lastCellText
                                                set lastTextWidget $lastCellText
                                                #ATTENTION: bug in embedded window only allowed forwards 
                                                set insertPos "end"
                                        }
                                        # set cursor Index in the new text widget
                                        set searchOptions(cursorIndex) $insertPos
                                        
                                        # search in the given table
                                        set tableRes [searchTableCells $searchOptions(curText)]         ;# 1.
                                        if {$tableRes} {
                                                # success and return
                                                return;
                                        } else {
                                                # not hit and continue
                                                continue;
                                        }
                                        
                                } elseif {[llength $item] == 2} {
                                        # pure text indices (startIdx, endIdx)

                                        foreach {startIdx endIdx} $item break;
                                        # searching the text content in the main text widget with start/end indices.
                                        set textRes [searchText $searchOptions(mainText) $startIdx $endIdx]          ;#5.
                                        
                                        if {$textRes} {
                                                return;
                                        } else {
                                                continue;
                                        }
                                }
                        }
                }
                # nothing to do more and bell for the user
                bell
                # wrap search
                if {$searchOptions(wrap)} {
                        set searchOptions(newWrapStart) 1
                }
                return;
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  * (searchTE::)lastTextWidget: the last text widget to configure
        #  Procedures:
        #  * (searchTE::)searchTableCells
        #  * (searchTE::)getMainTextInfos
        #  * (searchTE::)searchText
        #  * (searchTE::)getEndIndex
        # USED BY
        # * ::TEGUI::invokeSearchDialog
        # * (searchTE::)replaceSingle
        # * (searchTE::)replaceText
        #****
        
        
        #****f* textEditor/searchTE::getCursorInfo
        # NAME
        #  getCursorInfo (No.0)
        # DESCRIPTION
        #  Initialization for search function, 
        #  take the user cursor's informations: index and in which text widget.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc getCursorInfo {} {
                # PARAMETERS
                #  NONE 
                # RETURN VALUE
                #       Returns 0, if there are something wrong, else means successful running.
                # NOTES
                variable ::searchTE::searchOptions
                variable ::searchTE::lastTextWidget
                
                # setting text target
                # if selected CheckBox newStart in Dialog or wrap started
                if {$searchOptions(newStart) != 0 
                        || ([info exists searchOptions(newWrapStart)] && $searchOptions(newWrapStart) == 1)
                        || ([info exists searchOptions(replaceAll)] && $searchOptions(replaceAll) == 1)
                } {
                        # new start
                        set searchOptions(curText) $searchOptions(mainText)
                        set lastTextWidget $searchOptions(mainText)
                        if {$searchOptions(direction) == "searchForward"} {
                                set startIdx 1.0
                        } else {
                                set startIdx end
                        }
                        set searchOptions(cursorIndex) $startIdx
                        $searchOptions(mainText) mark set insert $startIdx
                        
                        set searchOptions(newStart) 0
                        set searchOptions(newWrapStart) 0
                        set searchOptions(replaceAll) 0
                } else {
                        # normal situation
                        
                        # set focus text
                        set focusText ""
                        # check variable lastTextWidget
                        if {[info exists lastTextWidget] && $lastTextWidget != ""} {
                                set focusText $lastTextWidget
                        }
                        # check focus text
                        if {$focusText != ""} {
                                set searchOptions(curText) $focusText   
                                set lastTextWidget $focusText   
                        } elseif {$searchOptions(mainText) != ""} {
                                # main text
                                set searchOptions(curText) $searchOptions(mainText)
                                set lastTextWidget $searchOptions(mainText)
                        } else {
                                # no focus or main text
                                putsstderr "Warning: There is no valid target Text!!"
                                return 0;
                        }
                        #puts "searchOptions(curText) $searchOptions(curText)"
                        # set cursor index in the target text
                                                if {[catch {set searchOptions(cursorIndex) [$searchOptions(curText) index insert]} ] } {
                                                        set searchOptions(cursorIndex) "1.0" 
                                                }
                }
                # #2850 bad text
                if {$searchOptions(curText) == "" 
                                        || ![winfo exists $searchOptions(curText)] 
                                        || [::TEUtils::getWidgetClass $searchOptions(curText)] != "Text"} {
                                        return 0;
                } else {
                    # successful running and set searchOptions(cursorIndex) and searchOptions(curText).
                    return 1;
                }
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  * (searchTE::)lastTextWidget: the last text widget to configure
        #  Procedures:
        #    NONE
        # USED BY
        # * (searchTE::)searchTextShell
        #****
        
        #****f* textEditor/searchTE::searchTableCells
        # NAME
        #  searchTableCells (No.1)
        # DESCRIPTION
        #  Searching in the given cell text widget of table.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc searchTableCells {cellText} {
                # PARAMETERS
                #  * cellText: the selected cell text widget. 
                # RETURN VALUE
                #       Returns 0, if there are something wrong or nothing to find, else means find some word.
                # NOTES
                variable ::searchTE::searchOptions
                variable ::searchTE::lastTextWidget
                
                # get the end index of cell text widget
                set endIdx [getEndIndex $cellText]                                                                              ;# 7.
                # search in the text widget(Attention: There is no table in the cell text!)
                set result [searchText $cellText $searchOptions(cursorIndex) $endIdx]   ;# 5. 
                set cellTextList ""
                if {$result == 1} {
                        return 1;
                } else {
                        # get the rest cell text list
                        set tbl [::TEUtils::getWidgetParent $cellText "tablelist"]
                        if {$tbl == ""} {
                                putsstderr "Error: $cellText has no valid table parent!"
                                return 0;
                        }
                        # get next cell text widget to searching
                        set cellTextList [getNextCellText $tbl]                         ;# 6.
                }
                # if there is no more cell text widget in the rest
                if {$cellTextList != ""} {
                        
                        # get text sequence according to search direction
                        if {$searchOptions(direction) != "searchForward"} {
                                set cellTextList [lreverse $cellTextList]
                        }
                        
                        #Attention: While Loop! set end condition!
                        while { [llength $cellTextList] != 0 } {
                                # set next text widget to search
                                set nextText [lindex $cellTextList 0]   
                                set searchOptions(curText) $nextText
                                set lastTextWidget $nextText
                                
                                # only forwards in the cell text
                                set startIdx "1.0"
                                set endIdx "end"
                                
                                # get result of search text
                                set result [searchText $nextText $startIdx $endIdx]             ;# 5. 
                                
                                # hit some word!
                                if {$result == 1} { return 1; } 
                                
                                # Important: remove the first one!
                                set cellTextList [lreplace $cellTextList 0 0]
                        }
                }
                # Nothing to hit.
                return 0;
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  * (searchTE::)lastTextWidget: the last text widget to configure
        #  Procedures:
        #  * (searchTE::)getEndIndex
        #  * (searchTE::)searchText
        #  * ::TEUtils::getWidgetParent
        #  * (searchTE::)getNextCellText
        # USED BY
        # * (searchTE::)searchTextShell
        #****
        
        #****f* textEditor/searchTE::setCursorIndex
        # NAME
        #  setCursorIndex (No.2)
        # DESCRIPTION
        #  Set cursor index into searchOptions(cursorIndex)
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc setCursorIndex {inputIndex} {
                # PARAMETERS
                #  * inputIndex: the given index of cursor. 
                # RETURN VALUE
                #       NONE    
                # NOTES
                variable ::searchTE::searchOptions
                set searchOptions(cursorIndex) $inputIndex
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  Procedures:
        #       NONE    
        # USED BY
        # * (searchTE::)searchText
        #****
        
        #****f* textEditor/searchTE::getMainTextInfos
        # NAME
        #  getMainTextInfos (No.3)
        # DESCRIPTION
        #  Get informations of main text in the index range(pure text contents and table positions).
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc getMainTextInfos {startIdx endIdx} {
                # PARAMETERS
                #  * cellText: the selected cell text widget. 
                # RETURN VALUE
                #       Returns the positions tables and ranges of text contents. 
                # NOTES
                variable ::searchTE::searchOptions
                
                # sort the start/endIdx, make sure startIdx less than endIdx.
                # Attention: Although Tcl/Tk have 2 standard(forward/backward) search direction, but
                # sometimes backwards with greater startIdx will cause cache Bug and SYSTEM DOWN!
                switch -exact [ ::TEUtils::compareTextIndex $searchOptions(mainText) $startIdx $endIdx ] {
                        -1 {
                                set tempStartIdx $startIdx
                                set tempEndIdx $endIdx
                        }
                        0 { return "" }
                        1 {
                                set tempStartIdx $endIdx
                                set tempEndIdx $startIdx
                        }
                        default { return "" }
                }
                # get all table indices in this range of main text widget
                set tableIndices [::textEditorMain::getTableIndices $searchOptions(mainText) $tempStartIdx $tempEndIdx]
                
                # get the order according the search direction.
                if {$searchOptions(direction) != "searchForward"} {
                        # backward
                        set tableIndices [lreverse $tableIndices]
                } 
                # set index to the line,char form
                set startIdx [$searchOptions(mainText) index $startIdx]
                set endIdx [$searchOptions(mainText) index $endIdx]
                
                set areaList ""
                set tempIdx $startIdx
                
                # cut the text range with table positions, need avoid boundary problem.
                foreach tableIdx $tableIndices {
                        # start
                        if {$tableIdx == $startIdx} {
                                lappend areaList [list $tableIdx]
                                set tempIdx [$searchOptions(mainText) index $startIdx]
                        } elseif {$tableIdx == $endIdx} {
                                # end
                                lappend areaList [list $tempIdx [$searchOptions(mainText) index $tableIdx]]
                                lappend areaList [list $tableIdx]
                                set tempIdx $endIdx
                        } else {
                                # in the middle
                                lappend areaList [list $tempIdx [$searchOptions(mainText) index $tableIdx]]
                                lappend areaList [list $tableIdx]
                                set tempIdx [$searchOptions(mainText) index $tableIdx]
                        }
                }                       
                
                # may be there is last range
                if {$tempIdx != $endIdx} {
                        lappend areaList [list $tempIdx $endIdx]
                }
                
                return $areaList
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  Procedures:
        #  * ::TEUtils::compareTextIndex
        #  * ::textEditorMain::getTableIndices
        # USED BY
        # * (searchTE::)searchTextShell
        #****
        
        # 4.
        proc setNextText {inputText} {
                variable ::searchTE::searchOptions
                variable ::searchTE::lastTextWidget
                set searchOptions(curText) $inputText
                set lastTextWidget $inputText
        }
        
        #****f* textEditor/searchTE::searchText
        # NAME
        #  searchText (No.5)
        # DESCRIPTION
        #  search the selected text with start/end index
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc searchText {text startIdx endIdx} {
                # PARAMETERS
                #  * text:              the selected text widget. 
                #  * startIdx:  the given start index. 
                #  * endIdx:    the given end index. 
                # RETURN VALUE
                #       Returns 0, if there are something wrong or nothing to find, else means find some word.
                # NOTES
                variable ::searchTE::searchOptions
                variable ::searchTE::lastTextWidget
                
                # set search parameter string
                set params " -all -exact"
                # paramter has only forwars direction
                append params " -forwards"
                # nocase
                if {!$searchOptions(caseSensitiv)} {append params " -nocase"}

                # get the line,char form of start/end index
                set startIdx [$text index $startIdx]
                set endIdx [$text index $endIdx]
                
                # if no contents then return 0;
                if {$startIdx == $endIdx} {return 0;}
                
                # if startIdx greater than endIdx, then change them together
                if {[::TEUtils::compareTextIndex $text $startIdx $endIdx] > 0} {
                        set tempIdx $startIdx
                        set startIdx $endIdx
                        set endIdx $tempIdx
                }
                #puts "startIdx $startIdx - endIdx $endIdx"
                
                # search in the text content
                # ATTENTION: SYSTEM BUG HERE WITH EMBEDDED WINDOWS, so endIdx must be greater as startIdx
                # #2911
                set startList [eval $text search $params -count countList -exact [list $searchOptions(searchKeyWord)] $startIdx $endIdx]
                
                # if found something
                if {$startList != ""} {
                        # set new insert index according to search direction
                        if {$searchOptions(direction) == "searchForward"} {
                                set firstHitIdx [lindex $startList 0]
                                set count [lindex $countList 0]
                                set insertPos [$text index "$firstHitIdx + $count chars"]
                        } else {
                                set firstHitIdx [lindex $startList end]
                                set count [lindex $countList end]
                                set insertPos $firstHitIdx
                        }
                        
                        $text mark set insert $insertPos
                        # set new insert position
                        setCursorIndex $insertPos                       ;# 2.
                        # select the found word
                        $text tag add sel $firstHitIdx "$firstHitIdx + $count chars"
                        # see this word
                        $text see $firstHitIdx
                        
                        set searchOptions(hitWord) 1
                        set replaceButton [lindex  $searchTE::searchOptions(replaceButton) 0]
                                                set replaceAllButton [lindex  $searchTE::searchOptions(replaceButton) 1]
                        if {[winfo exists $replaceButton]} {
                            event generate $replaceButton <<TriggerReplace>>
                            event generate $replaceAllButton <<TriggerReplace>>
                        }
                        # set last text widget
                        set lastTextWidget $text 
                        focus -force $text
                        # found word
                        return 1;
                } 
                # nothing to find
                return 0;
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  * (searchTE::)lastTextWidget: the last text widget to configure
        #  Procedures:
        #  * ::TEUtils::compareTextIndex
        #  * (searchTE::)setCursorIndex
        # USED BY
        # * (searchTE::)searchTextShell
        #****
        
        #****f* textEditor/searchTE::getNextCellText
        # NAME
        #  getNextCellText (No.6)
        # DESCRIPTION
        #  check all the table and get the rest cell texts(with current text).
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc getNextCellText {tbl} {
                # PARAMETERS
                #  * tbl:               the selected table
                # RETURN VALUE
                #       Returns the cell text list.
                # NOTES
                variable ::searchTE::searchOptions
                
                # get all cell text list of given table
                set childrenList [::TEUtils::winChildrenList $tbl.body]
                set textList [::TEUtils::listSearchPart $childrenList ".w.text" 1]
                
                # get current text index in the cell text list
                set textIndex [lsearch -exact $textList $searchOptions(curText)]
                # something wrong, return NULL.
                if {$textIndex < 0} { return ""; }
                
                # depending the search direction, take the rest texts of text list.
                if {$searchOptions(direction) == "searchForward"} {
                        # forwards
                        incr textIndex
                        set cellList [lrange $textList $textIndex end]
                } else {
                        # backwards
                        incr textIndex -1
                        set cellList [lrange $textList 0 $textIndex]
                }
                
                return $cellList;
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  Procedures:
        #  * ::TEUtils::winChildrenList
        #  * ::TEUtils::listSearchPart
        # USED BY
        # * (searchTE::)searchTableCells
        #****
        
        #TODO: get the end index of splitted text!
        #****f* textEditor/searchTE::getEndIndex
        # NAME
        #  getEndIndex (No.7)
        # DESCRIPTION
        #  Get end index according to the search direction.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc getEndIndex {inputText} {
                # PARAMETERS
                #  * inputText:         the selected text widget
                # RETURN VALUE
                #       Returns end index of text widget.
                # NOTES
                variable ::searchTE::searchOptions
                if {$searchOptions(direction) == "searchForward"} {
                        # forwards
                        set endIdx "end"
                } else {
                        set endIdx "1.0"
                }
                
                return $endIdx
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  Procedures:
        #    NONE
        # USED BY
        # * (searchTE::)searchTextShell
        # * (searchTE::)searchTableCells
        #****
        

        #****f* textEditor/searchTE::getLastText
        # NAME
        #  getLastText (No.8)
        # DESCRIPTION
        #  Get cursor's last text widget.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc getLastText {inputText textType} {
                # PARAMETERS
                #  * inputText:         the selected text widget
                #  * textType:          "cell" - cell text widget / "main" - main text widget
                # RETURN VALUE
                #       Returns end index of text widget.
                # NOTES
                variable ::searchTE::searchOptions
                variable ::searchTE::lastTextWidget
                # set the main text
                                if {![::searchTE::initSearchFunc $inputText]} {return;}
                                if {![winfo exists $inputText] || [winfo class $inputText] != "Text"} {return;}
                # set new last widget
                if {$inputText != "" } {
                        set lastTextWidget $inputText
                        set ::searchTE::oldTextWidget $inputText
                        if {$textType =="main"} {
                            set ::textEditorMain::editorText $inputText
                        }
                } elseif {$inputText == ""} {
                        # leave the old text widget
                        if {$textType == "cell"} {
                                set ::searchTE::oldTextWidget $lastTextWidget
                                set lastTextWidget $searchOptions(mainText)
                        } elseif {$textType == "main"} {
                                if {![info exists searchTE::oldTextWidget] || $searchTE::oldTextWidget == $searchOptions(mainText)} {
                                        set lastTextWidget $searchOptions(mainText)
                                } else {
                                        set lastTextWidget $searchTE::oldTextWidget
                                }
                        }
                }
        }
        # USES
        #  Variables:
        #  * (searchTE::)searchOptions: all options and parameters of search functions
        #  Procedures:
        #    NONE
        # USED BY
        # * ::TEGUI::showEditorWindow
        # * ::TEGUI::createFrameText
        # * (::TextStyle::keyEvent: searchTE::oldTextWidget)
        #****
}


#****** textEditor/combo
# NAME
#  combo
# NAMESPACE
#  namespace
# DESCRIPTION
#  Control ComboBoxes in the text editor GUI.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  04.04.2014
# SYNOPSIS
namespace eval combo {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        
#****
        namespace export *
        #****f* textEditor/combo::comboBoxAdd
        # NAME
        #  comboBoxAdd
        # DESCRIPTION
        #  Add the new value into ComboBox value-list.
        # AUTHOR
        #  Xin Zhou
        #  Matthias Bremm
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc comboBoxAdd {w item} {
                # PARAMETERS
                #  * w:                 the selected entry widget
                #  * item:              the new item to be added
                # RETURN VALUE
                #       Returns a new value list in the combobox
                # NOTES
                
                # get the combobox name
                set w [winfo parent $w] ;# binding comes from entry
                # value-list
                set values [$w cget -values] 
                # value-list has not this new item and item not empty
                if {[lsearch -exact $values $item] < 0 && $item != ""} {
                        # add new item into value-list
                        set values [linsert $values 0 $item] 
                        # sorting and configure
                        $w configure -values [lsort -dictionary $values]
                }
        }
        # USES
        #  Variables:
        #    NONE
        #  Procedures:
        #    NONE
        # USED BY
        # * (combo::)getComboBox
        #****
        
        
        #****f* textEditor/combo::comboBoxEnable
        # NAME
        #  comboBoxEnable
        # DESCRIPTION
        #  Bind the key event(e.g. Return and FocusOut) and comboboxes
        # AUTHOR
        #  Xin Zhou
        #  Matthias Bremm
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc comboBoxEnable {w {type 0} {windowtype "standard"} } {
                # PARAMETERS
                #  * w:         the selected combobox
                #  * type:      0 normal comboboxes / 1: search or replace combobox
                # RETURN VALUE
                #       NONE    
                # NOTES
                foreach evt {<Return> } {
                        $w bind $evt "combo::getComboBox $w.e $type $windowtype ;break;"
                }
        }
        # USES
        #  Variables:
        #    NONE
        #  Procedures:
        #    * (combo::)getComboBox
        # USED BY
        # * ::TEGUI::showEditorWindow
        # * ::TEGUI::invokeSearchDialog
        # * ::TEGUI::extraFontsWindow
        #****
        
        
        #****f* textEditor/combo::getComboBox
        # NAME
        #  getComboBox
        # DESCRIPTION
        #  Control the combobox' entry and invoke the new function.
        # AUTHOR
        #  Xin Zhou
        #  Matthias Bremm
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc getComboBox {w {type 0} {windowtype "standard"} } {
                # PARAMETERS
                #  * w:         the selected entry widget
                #  * type:  0 normal comboboxes / 1: search or replace combobox
                # RETURN VALUE
                #       NONE    
                # NOTES
                set item [$w cget -text]
                combo::comboBoxAdd $w $item
                set packInfoList [pack info $w] 
                # comboBox 
                set parentCombo [lindex $packInfoList 1]
                set parentComboList [split $parentCombo "."]
                #option
                set option [lindex $parentComboList end]
                # if w is normal combobox, invoke the proc ::combo::getComboCurrent
                if {$type == 0} {
                        ::combo::getComboCurrent $parentCombo $textEditorMain::editorText $option $windowtype
                } elseif {$type == 1} {
                        # search/replace combobox
                }
        }
        # USES
        #  Variables:
        #    NONE
        #  Procedures:
        #  * (combo::)comboBoxAdd
        #  * getComboCurrent
        # USED BY
        # * (combo::)comboBoxEnable
        #****
        
        
        
        
        #getComboCurrent --
        #       get the selected value of ComboBox and change the selected text style
        #
        #Arguments:
        #       combobox: the given combobox
        #       text : the text editor
        #       option: the value of given combobox
        #
        #Returns:
        #       Nothing
        #
        #Side effects
        #       None
        #
        proc getComboCurrent {combobox text option {windowtype "standard"}} {
                #puts "getComboCurrent"
                if {[catch {$combobox get} ] } {
                        return
                }
                set comboValue [$combobox get]
                
                if {$option == "preset"} {
                        
                        #puts "text: $text - comboValue: $comboValue"
                        if { $comboValue != "(standard)" && $::TextStyle::presetType($comboValue) == "p" } {
                                ::WordProcess::setParagraphStyles $text $comboValue
                                
                                #::WordProcess::configureParagraph $text $name
                        } else {
                          ::WordProcess::ChangeTagW $text $comboValue
                        }


                } else {
                        ::WordProcess::ChangeStyleW $text $option $comboValue 0 $windowtype
                }
                if {$option == "bgstipple"} {   
                        # set bg color black
                        ::WordProcess::ChangeStyleW $text background #000000 0 $windowtype
                }
        }
}


#TODO: REFACTORING TO THE DICT:
#****** textEditor/xins
# NAME
#  xins
# NAMESPACE
#  namespace
# DESCRIPTION
#  Control and save xml attributes when parse xml file.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  04.04.2014
# SYNOPSIS
namespace eval xins {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        ###############
        #       <xins_1>-
        #           <xins_2>-
        #                       <xins_3>-
        #                               <xins_4>-
        #                       <xins_3>-
        #               <xins_2>-
        #       <xins_1>-
        ###############
        
#****
        
        #****f* textEditor/xins::setNewArray
        # NAME
        #  setNewArray
        # DESCRIPTION
        #  Set a new array in the xins-namespace.
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSISy
        proc setNewArray {name} {
                # PARAMETERS
                #       None
                # RETURN VALUE 
                #       None
                # NOTES
                
                # if already same array exists, remove it and set a new one.
                if {[array exists ::xins::$name]} {array unset ::xins::$name}
                variable $name
                array set $name {}
                return ::xins::$name
        }
        # USES
        #    NONE
        # USED BY
        # * ::WordProcess::addParamToArrList
        #****
        
        
        
        #****f* textEditor/xins::resetXinsArray
        # NAME
        #  reset
        # DESCRIPTION
        #       Reset all arrays in namespace xins 
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  04.04.2014
        # SYNOPSIS
        proc resetXinsArray {} {
                # PARAMETERS
                #       None
                # RETURN VALUE 
                #       None
                # NOTES
                foreach  index [array names ::xins] {
                        unset ::xins($index)
                }
        }
        # USES
        #    NONE
        # USED BY
        # * ::TEUtils::refreshStatus
        #****
        
        
        #TODO:
        #namespace export
}

# for dynamically generated new auxiliary window(s)
# e.g. extra fonts control window, table configuration window

#****** textEditor/newWindows
# NAME
#  newWindows
# NAMESPACE
#  namespace
# DESCRIPTION
#  Control all the extra generated Windows.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  04.04.2014
# SYNOPSIS
namespace eval newWindows {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        
        variable win
        
        # extra fonts dialog
        set win(W) .newFontsWin
        
        # table configuration's dialog
        variable tableConfig
        set win(TC) .newTableConfig
        
        # link/anchor dialog
        variable linkConfig
        set win(LC) .newLinkConfig
        
        # table editor dialog
        variable tableEditorConfig
        set win(TE) .newTableEditorConfig
        
        # color editor dialog
        variable colorEditorConfig
        set win(CE) .newColorEditorConfig
        
        # search main dialog
        variable searchTextDialog
        set win(ST) .newSearchTextDialog
# USES
#  Variables:
#    * win:     a windows array
#  Procedures:
#        NONE
# USED BY
# * ::TEGUI::insertLinkDialog
# * ::TEGUI::invokeTableEditDialog
# * ::TEGUI::invokeSearchDialog
# * ::TEGUI::invokeColorDialog
# * ::TEGUI::insertTable
# * ::TEGUI::extraFontsWindow
#****
}

#TODO: Transfer table functions from namespace textEditor into textTable.
#****** textEditor/textTable
# NAME
#  textTable
# NAMESPACE
#  namespace
# DESCRIPTION
#  Maniplate and save the table variables, parameters.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  04.04.2014
# SYNOPSIS
namespace eval textTable {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        variable tableControl
        array set tableControl {}
        set tableControl(status) {}
        
# USES
#  Variables:
#    * tableControl:    the centr
#  Procedures:
#        NONE
# USED BY
# * ::TEGUI::insertLinkDialog
# * ::TEGUI::invokeTableEditDialog
# * ::TEGUI::invokeSearchDialog
# * ::TEGUI::invokeColorDialog
# * ::TEGUI::insertTable
# * ::TEGUI::extraFontsWindow
#****
}

#TODO: undo, redo
namespace eval SpecialButton {
        namespace export *
        
        variable last_x
        variable last_y
        
        proc textUndoEvent {text} {
                if {![string equal -nocase [winfo class $text] "text"]} {return;}
                catch {$text edit undo}
                return;
        }
        
        proc textRedoEvent {text} {
                if {![string equal -nocase [winfo class $text] "text"]} {return;}
                catch {$text edit redo}
                return;
        }
}

#****** textEditor/TEDB
# NAME
#  TEDB
# NAMESPACE
#  namespace
# DESCRIPTION
#  Maniplate and Controlling xml, html, inner database of text editor.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  04.05.2014
# SYNOPSIS
namespace eval TEDB {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        namespace export *
        
        # presetOption: for user pre-setting options. 
        #       -- value pair, with flexible structure (XML, HTML, TKTEXT)              
        # key:                  preset name (global, repeatable, bijection)
        # value:                level 1: type (xml, html, tclText) --- level 2: style value (local, subjection)
        # key form:     preset1, preset2, "hallo xin"(incl. inner whitespace), "nonoWord" , (andere Headers-class: div, div > p)
        # value form:   {underline:1;background:#00ff00}
        variable presetOption
        array set presetOption {}
        # e.g.
        ########################
        # presetOption(options) {
        #   preset1 {
        #               XML     {xxxx;xxxx} 
        #               HTML    {xxxx;xxxx} 
        #               TKTEXT  {xxxx;xxxx}
        #               status  0/1
        #   }
        # }
        #########################
        
        # save the fonts options
        set presetOption(options) {}
        # prefix for fud project (for valid check-function)
        set presetOption(prefix) "PRS"
        # Saved the pre-settting parameters as given data forms.
        set presetOption(types) {TKTEXT XML HTML}
        # check this flag, wether presetting in the text editor or not. Default: 0 (None).
        set presetOption(hasHead) 0
        
        # use for checking whether in the Header file of xml/html 
        set presetOption(inHead) 0
        
        set presetOption(htmlHValue) ""
        set presetOption(xmlHValue) ""
        set presetOption(combobox) ""
        
        # htmlFilter: Filter from html
        # key: tag or style name
        # value: coresponding value
        variable htmlFilter
        array set htmlFilter {}
        
        # noParagraph: no creation of p tags
        variable noParagraph
        set noParagraph 0
  
        # outputFilter: Filter for output
        # key: tag or style name
        # value: coresponding value
        variable outputFilter
        array set outputFilter {}
        
# USES
#  Variables:
#    * presetOption:    for user pre setting
#  Procedures:
#        NONE
# USED BY
# * xxx
#****
        
        # type: 0: text->xml / 1: xml->html
        # Return:
        #       Returns a dict form string with 2 key word, rend / rendition, 
        #       to get the rend and rendition value.
        proc getRenditionValue {attrValue type} {
                variable presetOption
                variable ::TextStyle::presetStyles
                
                set rendResult ""
                set rendition ""
                set result ""
                if { $type == 0 } {
                        # text->xml
                        set valueList [split $attrValue ";"]
                        foreach item $valueList {
                                foreach {key value} [split $item ":"] break;
                                if {$key eq "preset"} {
                                        set rendition $presetOption(prefix)$value
                                } else {
                                        set rendResult [::TEUtils::appendValue $rendResult $item ";"] 
                                }
                        }
                        set result "rendition [list $rendition] rend [list $rendResult]"
                } elseif {$type == 1} {
                        # xml -> html
                }
                return $result
        }
        
        
        proc removePresetPrefix {value} {
                variable presetOption
                set value [::TEUtils::backToBracket $value]
                set map "# {} $presetOption(prefix) {}"
                return [string map $map $value]
        }
        
        
        proc checkPresetStatus {presetKey presetValue type} {
                variable presetOption
                variable ::TextStyle::presetStyles
                
                #TODO: any more condition?
                if {![dict exists  $presetOption(options) $presetKey status] 
                        || [dict get $presetOption(options) $presetKey $type] != $presetValue} {
                        dict set presetOption(options) $presetKey status 0
                } elseif {[dict get $presetOption(options) $presetKey $type] == $presetValue} {
                        dict set presetOption(options) $presetKey status 1 
                }
        }
        
        # inputValue: single, list?
        # type: TKTEXT / XML / HTML
        # e.g. TKTEXT: Preset2 {{slant italic} {weight bold}} 
        #                          Paragraph {...}, Headline1 {...}
        proc addToPreset {presetKey type {presetValue ""}} {
                variable presetOption
                variable ::TextStyle::presetStyles
                
                if {$type == "TKTEXT"} {                        
                        checkPresetStatus $presetKey $presetStyles($presetKey) $type
                } else {
                        checkPresetStatus $presetKey $presetValue $type
                }
                
                # add to dict only once
                if { [dict get $presetOption(options) $presetKey status] == 0} {
                        
                        if {$presetKey == "" 
                                || [lsearch -exact $presetOption(types) $type] < 0} {
                                putsstderr "Waring: There is no valid input in the procedure addToPreset!  inputValue: $presetKey - type : $type";
                                return;
                        }
                        if {![info exists presetStyles($presetKey) ] } {
                                if {$presetValue != ""} {
                                        set presetValue [cleanStyleValue $presetValue]
                                        set presetStyles($presetKey) $presetValue
                                } else {
                                        putsstderr "Warning: Not found $presetKey in the TextStyle::presetStyles!"
                                        return;
                                }
                        }
                        if { $type == "TKTEXT" } {
                                dict set presetOption(options) $presetKey $type $presetStyles($presetKey)
                        } elseif { $type == "HTML" || $type == "XML" } {
                                if {$presetValue == ""} {
                                        putsstderr "Warning: presetValue in html/xml type is empty!"
                                        return;
                                }
                                dict set presetOption(options) $presetKey $type $presetValue
                        }
                        
                        # convert current style value to other 2 form of ::TEDB::presetOption(types)
                        convertForm $presetKey $type
                        # status 1
                        dict set presetOption(options) $presetKey status 1 
                }
                # add to every combobox
                #puts $presetOption(combobox)
                if {[winfo exists $presetOption(combobox).e]} {
                        ::combo::comboBoxAdd $presetOption(combobox).e $presetKey
                }
        }
        
        proc cleanStyleValue {value} {
                set map {\{ "" \} "" <  "" >  "" \n "" "\x26" "" "\&amp;" "" " " "" "0xA1A1" "" "\&lt;" "" "\&gt;" "" "<lb\/>" "" "<br\/>" ""}
                return [string map $map $value]
        }
        
        # type: TKTEXT XML HTML
        proc getValueFromPreset {type} {
                variable presetOption
                variable ::TextStyle::presetStyles
                
                set result ""
                dict for {name styles} $::TEDB::presetOption(options) {
                        #puts "presetName: $name"
                        dict with styles {
                                # $XML $HTML $TKTEXT
                                set itemValue [eval list $$type " "]
                                set item "$name $itemValue"
                                #puts "getValueFromPreset_item: $item"
                                set result [::TEUtils::appendValue $result $item " "]
                        }
                }
                
                # returns: "presetName presetValue" (repeat)....
                return $result
        }
        
        
        # convert current style value to other 2 form of ::TEDB::presetOption(types)
        # presetKey: 
        # type: TKTEXT XML HTML 
        proc convertForm {presetKey type} {
                variable presetOption
                variable ::TextStyle::presetStyles
                if {$type == "TKTEXT"} {
                        # convert text's style value to xml and html form
                        set xmlStyle [::WordProcess::getRendValue [dict get $presetOption(options) $presetKey $type]]
                        set xmlRendForm "rend=\"$xmlStyle\""  
                        set htmlStyle [::WordProcess::getAttrValues [::WordProcess::xmlParamToHtml $xmlRendForm rend ::TextStyle::xmlHtmlMap] style]
                        
                        dict set presetOption(options) $presetKey XML $xmlStyle
                        dict set presetOption(options) $presetKey HTML $htmlStyle 
                } elseif {$type == "XML"} {
                        # convert xml's style value to text and html form
                        # text form saved in the TextStyle::presetStyles
                        set xmlStyle [join [dict get  $presetOption(options) $presetKey $type]] 
                        set xmlRendForm "rend=\"$xmlStyle\"" 
                        set htmlStyle [::WordProcess::getAttrValues [::WordProcess::xmlParamToHtml $xmlRendForm rend ::TextStyle::xmlHtmlMap] style]
                        set textStyle [xmlRendToTextForm $xmlStyle]
                        
                        dict set presetOption(options) $presetKey TKTEXT $textStyle
                        dict set presetOption(options) $presetKey HTML $htmlStyle 
                        
                        ::TextStyle::addToPresetStyles $presetKey  $textStyle
                        
                        
                } elseif {$type == "HTML"} {
                        # translate html to xml and text
                        set htmlStyle [dict get  $presetOption(options) $presetKey $type] 
                        set htmlStyleForm "style='$htmlStyle'"
                        set xmlStyle [::WordProcess::getValidStyleValue $htmlStyleForm style ::TextStyle::htmlXmlMap]
                        set textStyle [xmlRendToTextForm $xmlStyle]
                        
                        dict set presetOption(options) $presetKey XML $xmlStyle
                        dict set presetOption(options) $presetKey TKTEXT $textStyle
                        
                        ::TextStyle::addToPresetStyles $presetKey [list $textStyle]
                }
                #parray ::TextStyle::presetStyles
        }
        
        proc xmlRendToTextForm {rendValue} {
                if {$rendValue == "" || [string first ":" $rendValue] <0 } {return;}
                
                set result ""
                foreach item [split $rendValue ";"] {
                        foreach {key value} [split $item ":"] { break; }
                        lappend result [list $key $value]
                }
                #puts "xmlRendToTextForm.result: $result"
                return $result
        } 
        
        
        
        # 
        proc removeFromPreset {presetKey} {
                variable presetOption

                dict unset presetOption(options) $presetKey             
        }
        
        # reset and initialize
        proc resetPreset {} {
                variable presetOption
                # reset options
                set presetOption(Options) {}
                set presetOption(hasHead) 0
                set presetOption(inHead) 0
                set presetOption(htmlHValue) ""
                set presetOption(xmlHValue) ""
        }
        
        proc setHTMLFilter {htmlFilterList} {
                variable htmlFilter
                foreach {key value} $htmlFilterList {
                        set htmlFilter($key) $value
                }
        }
        
        proc setNoParagraph {v} {
                variable noParagraph
                set noParagraph $v
        }
  
        proc setOutputFilter {outputFilterList} {
                variable outputFilter
                foreach {key value} $outputFilterList {
                        set outputFilter($key) $value
                }
        }
  
        proc isOutputFilter {key value} {
          variable outputFilter
          set filterKeyValue 0
          if {[info exists outputFilter($key)]} {
                  if {$outputFilter($key) == {}} {
                          set filterKeyValue 1
                  } else {
                          if {$value == $outputFilter($key) }  {
                                  set filterKeyValue 1
                          }
                  }
          }
          #puts "OUTPUT $key $value"
          #if {$filterKeyValue} {
            #puts "OUTPUTFILTER"
          #}
          return $filterKeyValue
        }
        

}

namespace eval texParser {
    variable tex
    variable current
    set current ""
    variable inPTag 
        set inPTag 0
        proc resetFlags {} {
                variable ::texParser::inPTag
                set inPTag 0
        }
        proc setinPTag {tagValue} {
                variable ::texParser::inPTag
                set inPTag $tagValue
        }
        proc getinPTag {} {
                variable ::texParser::inPTag
                return $inPTag
        }
        
}

namespace eval rtfParser {
    variable rtf
    variable rtfColorTable
    variable current
    set current ""
        variable inPTag
        set inPTag 0
        proc resetFlags {} {
                variable ::rtfParser::inPTag
                set inPTag 0
        }
        proc setinPTag {tagValue} {
            variable ::rtfParser::inPTag
            set inPTag $tagValue
    }
        proc getinPTag {} {
                variable ::rtfParser::inPTag
                return $inPTag
        }
}



# main perferences
namespace eval textEditorMain {
        
        namespace export *
        
        variable defaultFont Helvetica
        variable defaultSize 12
        variable defaultSelectBG        #000000
        variable defaultSelectVG        #ffffff
        variable defaultConfigure {}
                
        variable mainFrame ""

        variable editorText
        variable editable
        #variable textModiMsg    ;# 0 (no change) / 1 (changed)
        #set  textModiMsg 0      ;# as default
        variable textIndexInsert
        set textIndexInsert 1.0
        variable textIndexInsertPress
        set textIndexInsertPress 0
        
        variable coefficients
        array set coefficients {}
        set coefficients(defaultCellWidth) 24
        
        # text url link and anchor
        variable urlData
        array set urlData {}
        
        variable searchTextValue
        set searchTextValue ""
        variable replaceTextValue
        set replaceTextValue ""
        variable searchDirection 
        set searchDirection "searchForward"
        variable searchOption
        array set searchOption {}
        
        # Array: name, defaultname, currentDate, currentTime
        variable timestamp
        array set timestamp {}
        set timestamp(defaultName) "noname"
        
        
        variable time_now                       ;# current time on the status bar 
        # save the file
        variable savePath
        set savePath ""
        
        variable tagPositions
        array set tagPositions {}
        
        # get the current window's name: main, tableCell ...
        variable curWName ""
        
        # for loading/saving xml and html data 
        variable xmlData

        set xmlData(inBody) 0
        set xmlData(inRend) 0
        set xmlData(inTable) 0
        set xmlData(inHeader) 0
        set xmlData(hasHeadStyle) 0                     ;# check, whether valid styles in the head file
        set xmlData(paragraphTag) ""
        
        
        # in the main widget
        set xmlData(main,inBody) 0
        set xmlData(main,inRend) 0
        set xmlData(main,bodyLevel) 0
        set xmlData(main,currentAttrs) ""
        set xmlData(main,tagName) ""
        set xmlData(main,xmlArrName) "ct"
        set xmlData(main,headText) {}
        set xmlData(main,bodyText) {}
        set xmlData(main,inTag) 0
        set xmlData(main,justify) ""
        set xmlData(main,spacing1) ""
        set xmlData(main,spacing3) ""
        # in the table cell widget
        set xmlData(tableCell,inBody) 0
        set xmlData(tableCell,inRend) 0
        set xmlData(tableCell,bodyLevel) 0
        set xmlData(tableCell,currentAttrs) ""
        set xmlData(tableCell,tagName) ""
        set xmlData(tableCell,xmlArrName) "tableCt"
        set xmlData(tableCell,bodyText) {}
        set xmlData(tableCell,embeddedTable) 0
        set xmlData(tableCell,currentTableName) ""
        set xmlData(tableCell,curPosition) ""
        set xmlData(tableCell,curCellAttrs) ""
        set xmlData(tableCell,justify) ""
        set xmlData(tableCell,spacing1) ""
        set xmlData(tableCell,spacing3) ""
        
        #TODO: JUSTIFY
        set xmlData(justify) {}
        
        
        #CONSIDER: undo/redo
        variable undo
        variable redo
        variable undoStorage
        variable redoStorage
        
        #xml entities
        variable entityNum
        
        # table informations
        # table id numbers
        variable tableNum 0
        # count of row in a table
        variable tableRowNum 0
        # count of column 
        variable tableColNum 0
        
        variable tableEditRowNum 0
        variable tableEditColNum 0
        
        # insert or delete
        variable tableEditValue
        
        variable tableEditWidth
        array set tableEditWidth {}
        
        # row 0, col 1
        variable tableEditForm 0
        
        # td number for html to calculate the row/column number
        variable tableTdNum 0
        # yPos,xPos for table
        variable tablePoint 0,0
        
        # table width, means the maximal count of th/tds in a row 
        # ??map(tableName:x,y) row,col
        # ??map(tableName:row,col) x1,y1 x2,y2 .......
        variable tableMap
        array set tableMap {}
        set tableMap(maxWidth) 0
        
        variable ignoreListGlobal
        set ignoreListGlobal {}
        
        
        # save cell data with xml form 
        variable tableId 0
        variable tableCellId 0
        variable tableCellData
        array set tableCellData {}      
        variable tableCellIndex 
        array set tableCellIndex {}
        
        
        # id, name, rowNum, colNum, textWidgetList
        variable tableInfos
        array set tableInfos {}
        
        
        # update the username, current date and time in the variable timestamp
        proc updateTimestamp {} {
                variable timestamp
                
                if {![info exists timestamp(name)] || $timestamp(name) == ""} {
                        set timestamp(name) $timestamp(defaultName)
                }
                set time1 [::TEUtils::getCurTime de]
                set timestamp(currentDate) [lindex $time1 0]
                set timestamp(currentTime) [lindex $time1 1]
        }
        
        
        #resetTableEditWidth --
        #       reset the table widths storage to the default status(Null).
        #Arguments:
        #       tableEditWidth(ns): the namespace to save the table widths
        #Returns:
        #       Nothing
        proc resetTableEditWidth {} {
                variable tableEditWidth
                foreach key [array names tableEditWidth] {
                        unset tableEditWidth($key)
                }
        }
        
        # input
        # position: rowNum,colNum
        # attrInput: cols="4" rows="3"
        #type: html/xml
        
        #setTableMap --
        #       change the oringal table cell positions to the virtual y,x coordinate system.
        #        0
        #       -----------------> x
        #        |
        #        |
        #        |
        #        y
        #
        #Arguments: 
        #       tableName:      the given table name 
        #       rowCol:         the original table cell position
        #       attrInput:  the selected attribute of cell element
        #       type:           input file type (xml or html...) 
        #Returns:
        #       Nothing
        proc setTableMap {tableName rowCol attrInput {type "xml"}} {
                variable tableMap 
                variable tablePoint
                variable tableCellId
                variable xmlData
                
                if {$tableName == ""} {puts "There is no table!"; return;}
                if {$rowCol == "" || [string first "," $rowCol] <=0} {
                        putsstderr "Error: There is no valid row,Col: $rowCol"
                        return;
                }
                
                if {[string equal -nocase $type "xml"] || [string equal -nocase $type "tei"]} {
                        set columnKey "cols"
                        set rowKey "rows"
                } elseif {[string equal -nocase $type "html"] || [string equal -nocase $type "xhtml"]} {
                        set columnKey "colspan"
                        set rowKey "rowspan"
                } else {
                        putsstderr "Error: no correct input type! please check it!"
                        return;
                }
                
                #default cell width and height is 1 unit
                set rowNum 1
                set colNum 1
                set styles ""
                
                # there are some attribute in this cell element
                if {$attrInput != ""} {
                        if {[ string first "style" $attrInput ] >= 0} {
                                #set xmlData(tableCell,curCellAttrs) [::WordProcess::getAttrValuesW $attrInput style]
                                set xmlData(tableCell,curCellAttrs) [::WordProcess::getAttrValues $attrInput style]
                        }
                        set temp [getHtmlAttrs $attrInput]
                        if {$temp != ""} {
                                # whether more colspan?
                                foreach item $temp {
                                        set key [lindex $item 0]
                                        set value [lindex $item 1]
                                        # empty value is 1
                                        if {$value == ""} {set value 1}
                                        if {![string is integer $value]} {
                                                #putsstderr "Warning: $value has no valid integer number for table cols/rows!"
                                                continue;
                                        }
                                        if {$key== $columnKey} {
                                                set colNum $value
                                        } elseif {$key == $rowKey} {
                                                set rowNum $value
                                        }
                                }
                        }
                }
                
                set point [split $tablePoint ","]
                set yPos [lindex $point 0]
                set xPos [lindex $point 1]
                
                set newXPos [expr $xPos + $colNum]
                set newYPos [expr $yPos + $rowNum]
                
                # maxWidth
                if {$newXPos > $tableMap(maxWidth)} {
                        set tableMap(maxWidth) $newXPos
                }
                # set point back to the correct position
                set tablePoint $yPos,$newXPos
                
                set count 0
                #tableCellId
                set dataYX ""
                # y coordinate
                for {set y $yPos} {$y < $newYPos} {incr y} {
                        # x coordinate
                        for {set x $xPos} {$x < $newXPos} {incr x} {
                                
                                if {[info exists tableMap($tableName:$y,$x)]} {
                                        puts "Something wrong in the puzzle tableMap($tableName:$y,$x)!"
                                        continue;
                                }
                                if {$count == 0} {
                                        # only the first puzzle will be inputed data
                                        set tableMap($tableName:$y,$x) data:$rowCol
                                        set dataYX $y,$x
                                } else {
                                        set tableMap($tableName:$y,$x) NULL:$rowCol
                                }
                                incr count
                        }
                }
                
                # data cell
                if {$dataYX == ""} {
                        puts "Warning: $rowCol has no data cell!!"
                }
                set tableMap($tableName:data:$rowCol) $dataYX
                set tableMap($tableName:style:$rowCol) $styles
        }
        #void
        #problem: is there must be a free position in the next row?
        proc newRow {} {
                variable tableMap 
                variable tablePoint
                variable tableRowNum
                
                # y++
                incr tableRowNum                
                # set x back to 0
                set xPos 0
                
                set tablePoint $tableRowNum,$xPos
        }
        #return
        proc searchFree {tableName} {
                variable tableMap 
                variable tablePoint
                
                set point [split $tablePoint ","]
                set yPos [lindex $point 0]
                set xPos [lindex $point 1]
                
                while {[info exists tableMap($tableName:$yPos,$xPos)]} {
                        # this table position has been taken.
                        incr xPos
                        # !!Attention: recursive method!!
                } 
                # free position was found
                set tablePoint $yPos,$xPos
                
                return $yPos,$xPos
        }
        

        #getHtmlAttrs --
        #       get the formated attribute form from the given html element's attributes 
        #Arguments:
        #       htmlAttrs: the given html attributes
        #Returns:
        #       Returs the formated attribute list.
        #Example: 
        #       rowspan="3" colspan="1" --> {rowspan 3} {colspan 1}
        proc getHtmlAttrs {htmlAttrs} {
                if {$htmlAttrs == "" } {return "";}     
                set result {}
                set pattern {([^=|\s]*)=('|\")?([^'\"\s]*)('|\")?}
                foreach {all key k1 value k2} [regexp -all -inline $pattern $htmlAttrs] {
                        # no key: error
                        if {$key == ""} {continue}
                        lappend result [list $key $value]
                }
                return $result
        }
        
        
        # increase the td numbers
        proc incrTdNum {} {
                variable tableTdNum
                incr tableTdNum
        }
        # increase the row numbers
        proc incrRowNum {} {
                variable tableRowNum
                incr tableRowNum
        }
        # increase the column numbers
        proc incrColNum {} {
                variable tableColNum
                incr tableColNum
        }
        
        # reset all the count of table's row,column and td.
        proc resetTableNum {} {
                variable tableRowNum
                variable tableColNum
                variable tableTdNum
                variable tablePoint
                
                set tableRowNum 0
                set tableColNum 0
                set tableTdNum 0
                set tablePoint 0,0
        }
        # increase table's number
        proc incrTable {} {
                variable tableNum
                incr tableNum 
        }

        
        

        #POSITION: row,col
        proc cellInsert {tableName rowCol datablob cellAttrs} {
                variable tableCellData
                variable tableCellId
                variable tableCellIndex
                variable tableMap 
                #puts "cellAttrs: $cellAttrs"
                
                set dataYX $tableMap($tableName:data:$rowCol)
                set tableCellData($tableName:data:$rowCol) $datablob
                set tableCellData($tableName:style:$rowCol) $cellAttrs
                if {$dataYX == ""} {return;}
                set tableCellIndex($tableName:index:$dataYX) $rowCol
        }
        #indexName -> tableName:dataYX
        # type: data:0 / style:1
        proc cellGet {tableName dataYX type} {
                variable tableCellData
                variable tableCellIndex
                set result ""
                set indexName $tableName:index:$dataYX
                if {![info exists tableCellIndex($indexName)]} {
                        #putsstderr "Error: There is no Content in the $indexName!"
                        return "";
                }               
                if {$type == 0} {
                        set result $tableCellData($tableName:data:$tableCellIndex($indexName))
                } elseif {$type == 1} {
                        set result $tableCellData($tableName:style:$tableCellIndex($indexName))
                }
                return $result;
        }
        
        proc resetCells {} {
                variable tableId 
                variable tableCellId 
                variable tableCellData
                variable tableCellIndex 
                variable tableMap 
                
                set tableId 0
                set tableCellId 0
                foreach index [array names tableCellData] {
                        unset tableCellData($index)
                }
                foreach index [array names tableCellIndex] {
                        unset tableCellIndex($index)
                }
                
                unset tableMap
                array set tableMap {}
                set tableMap(maxWidth) 0
        }
        
        proc resetTableCells {tableName} {
                variable tableId 
                variable tableCellId 
                variable tableCellData
                variable tableCellIndex 
                variable tableMap 
                
                resetTableNum
                
                #set id [getTableIDByName $tableName]
                #if {$id eq ""} {return;}
                
                foreach {key} [array names tableCellIndex] {
                        if {[string first $tableName $key]>=0} {
                                unset tableCellIndex($key)
                        }
                }
                #parray tableCellIndex
                foreach {key} [array names tableCellData] {
                        if {[string first $tableName $key]>=0} {
                                unset tableCellData($key)
                        }
                }
                #parray tableCellData
                foreach {key} [array names tableMap] {
                        if {[string first $tableName $key]>=0} {
                                unset tableMap($key)
                        }
                }
                #parray tableMap
                set tableMap(maxWidth) 0
                
        }
        
        # Hinzufügen, entfernen, korrigieren, aktualisieren.
        # 4. rowNum/colNum don't match the length of textlist.
        # 5. textlist name rules
        # 6. string match - key, then update or add keys and values
        
        
        
        # add a new table in the tableInfos with parameters
        proc tableAddRecord {id name {rowNum 0} {colNum 0} {textlist ""}} {
                variable tableInfos
                if {$id=="" |$name == "" } {return;}
                if {[hasTable $id]} {
                        #removeTable $tableInfos(name,$id)
                        resetTableCells $name
                }
                set tableInfos(id,$name) $id
                set tableInfos(name,$id) $name
                set tableInfos(rowNum,$name) $rowNum
                set tableInfos(colNum,$name) $colNum
                set tableInfos(textlist,$name) $textlist
                # add the tableColNum
                #FIXME:
                textEditorMain::incrTable
        }
        # 
        proc setTableTextState {mainText statetype} {
                variable tableInfos
                if {![winfo exists $mainText]} {return;}
                
                foreach {type wName index} [$mainText dump -window 1.0 end] {
                        #tk_messageBox -message $wName
                        set textList $tableInfos(textlist,$wName)
                        foreach text $textList {                        
                                $text configure -state $statetype
#                               set parent [winfo parent $text]
#                               $parent.popupMenu delete 1 2
                        }
                }
        }
        
        # get all table's name in the given text widget
        proc getTableNames {text {startIdx 1.0} {endIdx end}} {
                variable tableInfos
                #reset tableNames
                set tableInfos(tableNames) ""
                # traverse the whole text widget
                foreach {key value index} [$text dump -window $startIdx $endIdx] {
                        lappend tableInfos(tableNames) $value
                }
                return tableInfos(tableNames)
        }
        
        # get all indices of tables in the selected text 
        proc getTableIndices {text {startIdx 1.0} {endIdx end}} {
                if {![winfo exists $text]} {return;}
                set indices ""
                foreach {key value index} [$text dump -window $startIdx $endIdx] {
                        set indices [::TEUtils::appendValue $indices $index " "]
                }
                return $indices
        }
        
        proc getTableIndexByName {text name {startIdx 1.0} {endIdx end}} {
                if {![winfo exists $text]} {return "";}
                set index ""
                foreach {key value index} [$text dump -window $startIdx $endIdx] {
                        if {$value eq $name} { return $index; }
                }
                return "";
        }
        
        proc getTableIDByName {tableName} {
                variable tableInfos
                if {![info exists tableInfos(id,$tableName)]} {return "";}
                return $tableInfos(id,$tableName)
        }
        
        proc getTableNameByID {tableNum} {
                variable tableInfos
                if {![info exists tableInfos(name,$tableNum)]} {return "";}
                return $tableInfos(name,$tableNum)
        }
        proc getTableTextlistByID {tableNum} {
                variable tableInfos
                if {![info exists tableInfos(name,$tableNum)]} {return "";}
                return $tableInfos(textlist,$tableInfos(name,$tableNum))
        }
        proc getTableRowNumByName {tableName} {
                variable tableInfos
                if {![info exists tableInfos(rowNum,$tableName)]} {return "";}
                return $tableInfos(rowNum,$tableName)
        }
        proc getTableColNumByName {tableName} {
                variable tableInfos
                if {![info exists tableInfos(colNum,$tableName)]} {return "";}
                return $tableInfos(colNum,$tableName)
        }
        proc getTableTextlistByName {tableName} {
                variable tableInfos
                if {![info exists tableInfos(textlist,$tableName)]} {return "";}
                return $tableInfos(textlist,$tableName)
        }
        # remove the table from tableInfos
        proc removeTable {name} {
                variable tableInfos
                if {$name == ""} {return;}
                if {[info exists tableInfos(id,$name)]} {
                        set id $tableInfos(id,$name) 
                        unset tableInfos(id,$name) 
                        unset tableInfos(name,$id) 
                        unset tableInfos(rowNum,$name) 
                        unset tableInfos(colNum,$name) 
                        unset tableInfos(textlist,$name)
                } else {
                        puts "Attention: There is no table named $name!"
                }
        }
        # check the id-name pair whether there is table named input string.
        proc hasTableWithName {name} {
                variable tableInfos
                set result 0
                if {$name != ""} {
                        set result [info exists tableInfos(id,$name)]
                }
                return $result
        }
        
        # check the id-name pair whether there is table named input string.
        proc hasTable {id} {
                variable tableInfos
                set result 0
                set tableName [textEditorMain::getTableNameByID $id]
                return [expr {$tableName eq "" ? 0 : 1}]
        }
        # There is a table with id in the text widget or not.
        proc hasTableInText {id text} {
                variable tableInfos
                set tableName [textEditorMain::getTableNameByID $id]
                if {$tableName == ""} {return 0;}
                # sometimes remove some tables from text widget, but cannot remove the id in the meantime. 
                textEditorMain::getTableNames $text
                return [expr [lsearch -exact $tableInfos(tableNames) $tableName] >= 0 ]
        }
        
        # reset tableInfos to null
        proc resetTableInfos {} {
                variable tableInfos
                # clear tagPositions
                foreach key [array names tableInfos] {
                        unset tableInfos($key)
                }
        }
        
        
        # show me the table
        proc getTableInfo {name} {
                variable tableInfos
                return "$tableInfos(id,$name) $tableInfos(rowNum,$name) $tableInfos(colNum,$name) $tableInfos(textlist,$name)"
        }
        # update text widget paths in the table
        proc setTableTextlist {name textlist} {
                variable tableInfos
                set tableInfos(textlist,$name) $textlist
        }
        proc setTableColNum {name colNum} {
                variable tableInfos
                set tableInfos(colNum,$name) $colNum
        }
        proc setTableRowNum {name rowNum} {
                variable tableInfos
                set tableInfos(rowNum,$name) $rowNum
        }

}



#TODO: SET A TAGRATING TO DISPLAY
namespace eval TextStyle {
        namespace export *
        #style names
        variable inserttagList
        set inserttagList {}

        # for saving all the button type and name
        # key: background       <--> value: $ef.toolbar.buttonFrame.firstLine.colorBar.background
        variable allButtons 
        array set allButtons {}
        
        variable popupLabels
        array set popupLabels {
                dump            dump
                dumpSel         dumpSel
                tagNames        tagNames
                new                     "New"
                open            "Open"
                copy            "Copy"
                cut                     "Cut"
                paste           "Paste"
                save            "Save"
                saveAs          "Save as ..."
                saveAsHtml      "Save as HTML"
                saveAsXml       "Save as XML"
                saveAsTex       "Save as TEX"
                saveAsRtf       "Save as RTF"
                exit            "Exit"
        }
        
        
        #tag names
        variable tagNames
        array set tagNames {
                overstrike {{} 1}
                underline {{} 1}
                slant {roman italic}
                weight {normal bold}
                justify {left center right}
                relief {flat groove raised ridge solid sunken}
                borderwidth {1 2 3 4}
                size {8 10 12 14}
                family {Times Courier}
                offset {-4 0 4}
                background {}
                foreground {}
                bgstipple {error gray75 gray50 gray25 gray12 hourglass info questhead question warning}
                fgstipple {error gray75 gray50 gray25 gray12 hourglass info questhead question warning}         
                lmargin1 {2 4 6 8}
                lmargin2 {2 4 6 8}
                rmargin {2 4 6 8}
                spacing1 {2 4 6 8}
                spacing2 {2 4 6 8}
                spacing3 {2 4 6 8}      
                tabs {1 {1 3} {1 3 5} {3 5}}
                preset {Preset1 Preset2 Preset3 Paragraph Headline1 Headline2 Headline3 Headline4 Headline5 Headline6}
        }
        
        proc getNewTag {tagFontList} {
                variable ::TextStyle::tagorfont
                if {$tagFontList == ""} {return;}
                foreach item $tagFontList {
                        # bsp. timestamp {{foreground green} {underline 1}}
                        set name [lindex $item 0]               ;# name of current tag or font
                        set value [lindex $item 1]              ;# value of current tag or font
                        if {![info exists tagorfont($name)]} {putsstderr "Warning: $item not belong to Array ::TextStyle::tagorfont";continue;}
                        set tagList ""
                        set fontList ""
                        if {$tagorfont($name) == "tag"} {
                                lappend tagList "-$name $value" 
                        } elseif {$tagorfont($name) == "font"} {
                                lappend fontList "$name $value"
                        }
                }
        }
        
                
        variable presetStyles
        array set presetStyles {
                Preset1 {{weight bold} {background #FF88FF}} 
                Preset2 {{foreground #008800}}
                Preset3 {{slant italic} {foreground #008800}}
                timestamp {{foreground #008800} {underline 1}}
                Paragraph {{spacing3 10}}
                Headline1  {{spacing3 10} {size 18} {weight bold}}
                Headline2  {{spacing3 10} {size 16} {weight bold}}
                Headline3  {{spacing3 10} {size 14} {weight bold}}
                Headline4  {{spacing3 10} {size 12} {weight bold}}
                Headline5  {{spacing3 10} {size 10} {weight bold}}
                Headline6  {{spacing3 10} {size 8} {weight bold}}
        }
        
        variable presetStylesPara "spacing3:10"
        
        array set presetType {
                Preset1 hi
                Preset2 hi
                Preset3 hi
                timestamp hi
                Paragraph p
                Headline1 p
                Headline2 p
                Headline3 p
                Headline4 p
                Headline5 p
                Headline6 p
        }
        
        
;#              lmargin1 {1 2 3}

        #tag and font types
        variable tagorfont
        array set tagorfont {
                overstrike              tag
                underline               tag
                justify                 tag
                relief                  tag
                lmargin1                tag
                borderwidth     tag
                offset                  tag
                background              tag
                foreground              tag
                bgstipple               tag
                fgstipple               tag
                lmargin1                tag
                lmargin2                tag
                rmargin                 tag
                spacing1                tag
                spacing2                tag
                spacing3                tag
                tabs                    tag
                slant                   font
                weight                  font
                size                    font
                family                  font
                window                  window
                link                    link
                anchor          anchor
                preset                  preset
                p                               p
        }
        # smaller number with higher rating (valid: rational number)
        variable tagRating 
                array set tagRating {
                        head                            1
                        body                            1
                        teiheader                       1
                        p                                       2
                        div                                     2
                        table                           2
                        window                          2
                        h1                      2
                        h2                      2
                        h3                      2
                        h4                      2
                        h5                      2
                        h6                      2
                        tr                                      3
                        td                                      3
                        th                                      3
                        style                           3
                        cell                            3
                        span                            4
                        hi                                      4       
                        a                                       4
                        ref                                     4
                        anchor                          4
                        href                            4
                        link                            4
                        anchor                          4
                        name                            4
                        br                                      4
                        lb                                      4
                        caption                 4
                        width               4
                        overstrike                      5
                        underline                       5
                        slant                           5
                        weight                          5       
                        justify                         5
                        relief                          5
                        borderwidth             5
                        size                            5
                        family                          5
                        offset                          5
                        background                      5
                        foreground                      5
                        bgstipple                       5
                        fgstipple               5         
                        lmargin1                        5       
                        lmargin2                        5
                        rmargin                         5
                        spacing1                        5
                        spacing2                        5
                        spacing3                        5      
                        tabs                            5
                        preset                          5
                        b                       5
                        "font-weight"       5
                        i                       5
                        "font-style"        5
                        s                       5
                        u                       5
                        sub                 5
                        "vertical-align"    5
                        sup                 5
                        "font-size"         5
                        "font-family"       5
                        "text-align"        5
                        "padding-top"        5
                        "line-height"        5
                        "padding-bottom"        5
                        background          5
                        "background-color"  5
                        color               5
                        "border-width"      5
                        "border-style"      5
                        colspan             5
                        rowspan             5
                }
                
        
        
        
        # tag with the matching attribute's name and some others
        variable tagAttrMap
        array set tagAttrMap {
                teiQuot         "\""
                htmlQuot        "\'"
                hi              "rend"
                span            "style"
                table           "rowsCols"
                tr                      ""
                td                      ""
                th                      ""
                a                       "href"
                cell            "colrowspan"
                p                       "p"
                h1                      "p"
                h2                      "p"
                h3                      "p"
                h4                      "p"
                h5                      "p"
                h6                      "p"
        }
        
        # HTML --->     TEI
        # Attention: text-decoration
        variable htmlXmlMap
        array set htmlXmlMap {
                "text-decoration"               "underline:overstrike"
                "p"                                             "p"
                "h1"                                    "p"
                "h2"                                    "p"
                "h3"                                    "p"
                "h4"                                    "p"
                "h5"                                    "p"
                "h6"                                    "p"
                "div"                                   "div"
                "span"                                  "hi"
                "br"                                    "lb"
                "v:"                                    "img"
                "![if !supportLists]"   ""
                "![if !supportFields]"  ""
                "mso-"                                  ""
                "b"                                     "weight:bold"
                "font-weight"                   "weight:bold"
                "i"                                     "slant:italic"
                "font-style"                    "slant:italic"
                "s"                                             "overstrike:1"
                "u"                                             "underline:1"
                "sub"                                   "offset:-4"
                "vertical-align"                "offset:-4;offset:4"
                "sup"                                   "offset:4"
                "font-size"                     "size"
                "font-family"                   "family"
                "text-align"                    "justify"
                "padding-top"                    "spacing1"
                "line-height"                    "spacing2"
                "padding-bottom"                    "spacing3"
                "background"                    "background"
                "background-color"              "background"
                "color"                                 "foreground"
                "border-width"                  "borderwidth"
                "border-style"                  "relief"
                "table"                                 "table"
                "tr"                                    "row"
                "td"                                    "cell"
                "th"                                    "cell"
                "a"                                             "ref"
                "href"                                  "target"
                "name"                                  "n"
                "colspan"                               "cols"
                "rowspan"                               "rows"
                "caption"                               "head"
                "width"                                 "width"
                "style"                                 "style"
                "kbd"                                   "space"                
        }
        
        # TCL/TK xml  --->      HTML
        variable xmlHtmlMap
        array set xmlHtmlMap {
                overstrike              text-decoration                 
                underline               text-decoration                  
                slant                   font-style                              
                weight                  font-weight                             
                justify                 text-align                              
                relief                  border-style                    
                borderwidth     border-width            
                size                    font-size                                       
                family                  font-family                     
                offset                  vertical-align                  
                background              background-color                
                foreground              color
                bgstipple               bgstipple
                fgstipple               fgstipple
                lmargin1                lmargin1
                lmargin2                lmargin2
                rmargin                 rmargin
                spacing1                padding-top
                spacing2                lineheigth
                spacing3                padding-bottom
                tabs                    tabs
                table                   table
                row                             tr
                cell                    td
                hi                              span
                p                               p
                ref                             a
                anchor                  a
                target                  href
                ana                     href
                n                               name
                rend                    style
                rendition               class
                rows                    rowspan
                cols                    colspan
                head                    caption
                width                   width
                style                   style
        }
        
        
        ;#overstrike text-decoration            ;# line-through
        ;#underline text-decoration                     ;# underline     
        ;#slant font-style                                      ;# italic
        ;#weight font-weight                            ;# bold
        ;#justify text-align                            ;#CONSIDER: left, right, center
        ;#relief border-style                           ;# flat groove raised ridge solid sunken
        ;#borderwidth border-width                      ;# with pixel (px)
        ;#size font-size                                        ;# with pixel (px)
        ;#familiy font-family                           ;# Times Courier
        ;#offset vertical-align                         ;# sub (-4), super (4), baseline (0)
        ;#background background-color           ;# color in form #ff00ff
        ;#foreground color                                      ;# color in form #ff00ff
        ;##bgstipple background-image           ;#CONSIDER: url(accept.png)
        ;#CONSIDER: fgstipple img Element
        
        variable types
        array set types {
                overstrike boolean
                underline boolean
                justify list
                relief list
                wrap list
                slant boolean
                weight boolean
                borderwidth numList
                size numList
                family list
                offset list
                background color
                foreground color
                bgstipple list
                fgstipple list
                lmargin1 numList
                lmargin2 numList
                rmargin numList
                spacing1 numList
                spacing2 numList
                spacing3 numList
                tabs numList
                link link
                anchor anchor
                preset preset
                timestamp timestamp
                p       p
        }
                
        array set frameposition {
                overstrike firstline.basicbar
                underline firstline.basicbar
                justify firstline.justifybar
                relief extend
                wrap extend
                slant firstline.basicbar
                weight firstline.basicbar
                borderwidth extend
                size secondline.fontbar
                family secondline.fontbar
                offset firstline.offsetbar
                background firstline.colorbar
                foreground firstline.colorbar
                bgstipple extend
                fgstipple extend
                lmargin1 extend
                lmargin2 extend
                rmargin extend
                spacing1 extend
                spacing2 extend
                spacing3 extend
                tabs extend
                preset secondline.presetbar
                timestamp secondline.presetbar
        }

        
        # layout form for internal and external Windows
        variable internalFonts {p overstrike underline justify slant weight size family offset background foreground preset timestamp}
        # layout form for internal and external Windows
        variable externalFonts {borderwidth relief lmargin1 lmargin2 rmargin spacing1 spacing2 spacing3 tabs bgstipple fgstipple}
        
        variable validCellStyles {width}
        
        
        variable frameInfos {p preset timestamp overstrike underline slant weight size family background foreground 
                {justify left} {justify center} {justify right}  {offset -4} {offset 4} {offset 0}
                borderwidth relief lmargin1 lmargin2 rmargin spacing1 spacing2 spacing3 tabs bgstipple fgstipple}
        
        variable userPreset 
        set userPreset {}
        
        
        # for saving all the button type and name
        # key: background       <--> value: $ef.toolbar.buttonFrame.firstLine.colorBar.background
        variable allButtons 
        array set allButtons {}
        
        
        
        proc addToPresetStyles {key value} {
                variable presetStyles
                variable presetType
                if {![info exists presetStyles($key)]} {
                set presetStyles($key) $value
                  set presetType($key) hi
        }
        }
        
        
        # type: the button function name
        # path: the button frame position
        proc addButton {ef type path} {
                variable frameInfos
                variable allButtons
                
                #puts "type: $type - path: $path"
                # type has this type and path is not null
                if { [ lsearch -exact $frameInfos $type ] >= 0 && $path != "" } {
                        set allButtons($ef,$type)   $path
                }
                #parray allButtons
        }
        
        # type: the button function name
        proc hasButton {ef type} {
                variable frameInfos
                variable allButtons
                # types has this function name and database has record
                if { [ lsearch -exact $frameInfos $type ] >= 0 && [info exists allButtons($ef,$type)] } {
                        # in real exists
                        if {[winfo exists $allButtons($ef,$type)]} {
                                return 1;
                        }
                } 
                return 0;
        }
        
        # type: the button function name
        proc getButtonPath {ef type} {
                variable frameInfos
                variable allButtons
                
                if { ![ info exists allButtons($ef,$type) ] } { return; }
                return $allButtons($ef,$type)
        }
        
        # type: the button function name
        proc removeButton {ef type} {
                variable frameInfos
                variable allButtons
                
                if { ![ info exists allButtons($ef,$type) ] } { return; }
                unset allButtons($ef,$type)
        }
        # typeList: the function's list 
        proc removeButtons {ef typeList} {
                variable frameInfos
                variable allButtons
                #puts "typeList: $typeList"
                #parray allButtons
                foreach type $typeList {
                        removeButton $ef $type
                }
        }
        
        proc keyEvent {ef event} {
                variable frameInfos
                variable allButtons
          
                #puts "keyEvent $ef $event"
                
                set buttonList ""
                
                foreach {key value} [array get allButtons] {
                        lappend buttonList $value
                }
                
                # initialization
                foreach buttonName $buttonList {
                        # button
                        catch {$buttonName configure -state normal}
                        # Bwidget combobox
                        catch {$buttonName clearvalue}
                }
                
                set tagList [$ef.tedit.text tag names insert]
          
                #puts "keyEvent_tagList: $tagList"
                        
                set tagList [::TEUtils::getValidList $frameInfos $tagList]
                
                #puts "keyEvent_tagList: $tagList"

                
                if {$tagList != ""} {
                        
                        foreach {tagName tagValue} $tagList {
                                
                                set tag [list $tagName $tagValue]
                          
                                if {$tagName == "p"} {
                                  set tagName preset
                                }
                          
                                set hitButtonName ""
                                # tag: {justify left} {justify center} {justify right}  {offset -4} {offset 4} {offset 0}
                                if {[info exists allButtons($ef,$tag)] && $allButtons($ef,$tag) != ""} {
                                        set hitButtonName $allButtons($ef,$tag)
                                } else {
                                    # tag: {weight bold}
                                    if {[llength $tag] == 2} {
                                            #
                                            if { ([lsearch -exact $frameInfos $tagName] >= 0 && [info exists allButtons($ef,$tagName)] ) } {
                                                    set hitButtonName $allButtons($ef,$tagName)
                                            } 
                                    }
                                }
                                
                                if {$hitButtonName != ""} {                             
                                        set buttonClass [winfo class $hitButtonName]
                                        
                                        if {[string equal -nocase $buttonClass "TButton"]} {
                                        # button 
                                                $hitButtonName configure -state active
                                        } elseif {[string equal -nocase $buttonClass "ComboBox"]} {
                                        # combobox
                                                set values [$allButtons($ef,$tagName) cget -values]
                                                $allButtons($ef,$tagName) setvalue "@[lsearch $values $tagValue]"
                                        }
                                }
                                
                        }
                }
                
                # for search function and table formatting
                if {[string first "ButtonRelease-1" $event] >= 0} {
                        set ::searchTE::oldTextWidget $ef.tedit.text 
                        set ::searchTE::lastTextWidget $ef.tedit.text 
                        set ::textEditorMain::tableeditwinpath {}                   
                }
        }
        
        
}



#****** textEditor/TEGUI
# NAME
#  TEGUI
# NAMESPACE
#  namespace
# DESCRIPTION
#  GUI and GUI's events for text editior.
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  24.06.2014
# SYNOPSIS
namespace eval TEGUI {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        namespace export *
        variable popupmenuTE 
        array set popupmenuTE {}
# USES
#  Variables:
#    * popupmenuTE      for popup menu in the textEditor
#  Procedures:
#        NONE
#****
        
        # menu:                 target popupmenu widget
        # validList :   the label list of valid items
        proc setValidPopupMenuItems {menu validList} {
                variable ::TEGUI::popupmenuTE
                # count of items in the menu
                set count [$menu index end] 
                set validIndexList ""
                for {set i 0} {$i <= $count} {incr i} {
                        set label ""
                        catch {set label [$menu entrycget $i -label]} 
                        catch {$menu entryconfigure $i -state disabled}
                        foreach name $validList {
                                #translate to find
                                set name [::msgcat::mc $name]
                                if {[string match -nocase $name* $label]} {
                                        lappend validIndexList $i
                                }
                        }
                }
                foreach index $validIndexList {
                        catch {$menu entryconfigure $index -state normal}
                }
                
        }
        
       
        # status: disabled / normal
        proc setButtonStatus {widget status} {
                        if {![winfo exists $widget] || $widget == "" || $status == ""} {return;}
                        $widget configure state $status
        }
        
            # status: disabled / normal
            # menu:         target popupmenu widget
            # validList :   the label list of invalid items
            proc setMenuItemStatus {menu itemList status} {
                    if {![winfo exists $menu] || $menu == "" || $invalidList == "" || $status == ""} {return;}
                    set count [$menu index end] 
                    set indexList ""
                    for {set i 0} {$i <= $count} {incr i} {
                            set label ""
                            catch {set label [$menu entrycget $i -label]} 
                            # set other items status 
                            if {$status == "disabled"} {
                                    catch {$menu entryconfigure $i -state normal}
                            } else  {
                                    catch {$menu entryconfigure $i -state disabled}
                            }
                            foreach name $invalidList {
                                    # exact name
                                    if {[string match -nocase $name $label]} {
                                                    lappend indexList $i
                                    }
                            }
                    }
                    foreach index $invalidIndexList {
                            if {$status == "disabled"} {
                                    catch {$menu entryconfigure $index -state disabled}
                            } else  {
                                    catch {$menu entryconfigure $index -state normal}
                            }
                    }
            } 
        
        #****f* textEditor/TEGUI::checkPopMenuItems
        # NAME
        #   initTextEditor
        # DESCRIPTION
        #   Control the popup menu, show the valid menu items in different situation. 
        # AUTHOR
        #   Xin Zhou
        # CREATION DATE 
        #   23.09.2014
        # SYNOPSIS
        proc checkPopupMenuItems {type menu targetText} {
                # PARAMETERS
                #       * type: the popup menu type (main/cell)
                # RETURN VALUE 
                #       None
                # NOTES
                #variable ::textEditorMain::textModiMsg
                variable ::searchTE::lastTextWidget
                variable ::TEGUI::popupmenuTE
                variable ::textEditorMain::editorText
                variable ::TextStyle::popupLabels
                
                        #puts "modi: [::textEditor::getModi]"
                        
                # 1. always show the  "new", "open", "Exit" items
                #       1.1. if lastTextWidget is null, take the main Text
                # 2. if lastTextWidget is valid, show "dump/dump sel", "Save as..", "Save as HTML/XML/Latex" items
                #       2.1 if text modified, show more "Save" item.
                # 4. if lastTextWidget and sel tag are valid, show more "Copy", "Cut" items
                # 6. if textModiMsg ist 1, show "Save" item
                if {$type == "main"} {
                        #if {[catch {set targetText $lastTextWidget}] || $lastTextWidget == ""} {
                        #        setValidPopupMenuItems $menu "new open exit"
                        #        return;
                        #}
                        # get ancestors of target text widget
                        #if {$targetText != $editorText} {
                        #                                #1
                        #        setValidPopupMenuItems $menu "new open exit"
                        #        return;
                        #}
                        if { [catch {$targetText index sel.first} ] } {
                        #4. if lastTextWidget and sel tag are valid, show "Copy", "Cut" items
                                if {[::textEditor::getModi]} {
                                        set vlist [list new open exit save dump paste]
                                } else {
                                        set vlist [list new open exit "save " dump paste]
                                }
                                setValidPopupMenuItems $menu $vlist
                        } else {
                                if {[::textEditor::getModi]} {
                                        set vlist [list new open exit save dump paste copy cut]
                                } else {
                                        set vlist [list new open exit "save " dump paste]
                                }
                                setValidPopupMenuItems $menu $vlist
                        }
                        
                } elseif {$type == "cell"} {
                        # no target text, there ist no valid menu items
                        #if {[catch {set targetText $lastTextWidget}] || $lastTextWidget == "" } {
                        #        setValidPopupMenuItems $menu ""
                        #}
                        if {[catch {$targetText index sel.first} ]} {
                                # no select, then only paste
                                setValidPopupMenuItems $menu "Paste"
                        } else {
                                # with select
                                setValidPopupMenuItems $menu "Copy Cut Paste"
                        }
                }
                
                
        }
        # USES
        #  Variables:
        #    *
        #****
        
        
        # showMain --
        #    Show the main window
        # Arguments:
        #    None
        # Result:
        #    None
        # Side effect:
        #    The main window is filled
        #
        proc showMain {} {
                variable ::textEditorMain::mainFrame
                variable ::TEGUI::popupmenuTE
                variable ::TextStyle::popupLabels
                # singleton
                catch {uplevel destroy $mainFrame}
                
                #Set up the (simple) menu bar
                set mw .menu
        
                menu $mw
                menu $mw.file -tearoff false
                menu $mw.edit -tearoff false
                menu $mw.view -tearoff false
                menu $mw.help -tearoff false
        
        
                #$mw add cascade -label File -menu $mw.file
                #$mw add cascade -label Edit -menu $mw.edit
                #$mw add cascade -label View -menu $mw.view
                #$mw add cascade -label Help -menu $mw.help
        
                #
                # Add the menu into the MainFrame
                #
                . configure -menu $mw
        
                # generate Frame
                
                set ef [ttk::frame .ef]
                
                set mainFrame $ef
        
                showEditorWindow $ef 1
                
                # various commands for text editor
                set newFileList [list ::TEGUI::NewFile .ef.tedit .ef]
                set openFileList [list ::TEGUI::OpenFile .ef.tedit 1 .ef]
                set saveFileList0 [list ::TEGUI::SaveFile .ef.tedit 0]
                set saveFileList1 [list ::TEGUI::SaveFile .ef.tedit 1]
                set saveFileList2 [list ::TEGUI::SaveFile .ef.tedit 2]
                set saveFileList3 [list ::TEGUI::SaveFile .ef.tedit 3]
                set saveFileList5 [list ::TEGUI::SaveFile .ef.tedit 5]
                set exitList [list ::TEGUI::closeTextWidget .ef.tedit.text .ef]
                
                #
                #Set up the "File" menu
                #
                $mw.file add command -label [::TEUtils::setMenuStrAlign 14 "New" "Ctrl-n"] -underline 0 -command $newFileList
                $mw.file add command -label [::TEUtils::setMenuStrAlign 14 "Open" "Ctrl-o"] -underline 0 -command $openFileList
                $mw.file add separator 
                $mw.file add command -label [::TEUtils::setMenuStrAlign 14 "Save" "Ctrl-s"]  -underline 0 -command $saveFileList0
                $mw.file add command -label "Save as .." -underline 1 -command $saveFileList1
                $mw.file add command -label "Save as XML" -underline 1 -command $saveFileList2
                $mw.file add command -label "Save as HTML" -underline 1 -command $saveFileList3
                $mw.file add command -label "Save as TEX" -underline 1 -command $saveFileList5
                $mw.file add separator 
                $mw.file add command -label "Exit" -underline 1 -command $exitList
                
                #
                #Set up the "Help" menu
                # 
                $mw.help add command -label Overview -underline 0 \
                        -command {tk_messageBox -message "Sry no help ^^"}
                $mw.help add command -label About -underline 0 \
                        -command {tk_messageBox -message "TextEditor v 0"}
        
                
                # text widget bind the mouse button 3
                # right clicked 
                
                bind .ef.tedit.text "<Button3-ButtonRelease>" "::TEGUI::showPopupMenu $::TEGUI::popupmenuTE(main) %X %Y;break;"
                
                #bind .ef.tedit.text <Control-Key-a> {
                #        %W tag add sel 1.0 end;
                #        break;
                #}
                #
                #bind .ef.tedit.text <Control-Key-U> {
                #        ::SpecialButton::textUndoEvent %W
                #        break;
                #}
                #bind .ef.tedit.text <Control-Key-R> {
                #        ::SpecialButton::textRedoEvent %W
                #        break;
                #}
                
                
                        # new file
                bind .ef.tedit.text <Control-Key-n> {
                        #puts "control-n"
                        ::TEGUI::NewFile .ef.tedit .ef
                        break;
                }
                # open file
                bind .ef.tedit.text <Control-Key-o> {
                        #puts "control-o"
                        ::TEGUI::OpenFile .ef.tedit 1 .ef
                        break;
                }
                
                # save file
                bind .ef.tedit.text <Control-Key-s> {
                        #puts "control-s"
                        ::TEGUI::SaveFile .ef.tedit 0
                        break;
                }
                
                # copy funktion
                bind .ef.tedit.text <<Copy>> {
                        ::TEGUI::copyEditor .ef.tedit
                        break;
                }
                
                # paste funktion
                bind .ef.tedit.text <<Paste>> {
                        ::TEGUI::pasteEditor .ef.tedit
                        break;
                }
                
                # text modified 
                #bind .ef.tedit.text <<Modified>> { 
                #        puts "modifiedold"
                #        #set ::textEditorMain::textModiMsg 1
                #        ::textEditor::setModi 1
                #        %W edit modified false
                #}
                
                # bottom toolbar
                #frame .statusbar
                
                
                #pack [ttk::label .statusbar.labelmodify -text "modified"] -side right
                #pack [ttk::separator .statusbar.sep0 -orient vertical] -side right -padx 3 -fill y
                #pack [ttk::checkbutton .statusbar.readonly -text "read only"] -side right
                #pack [ttk::separator .statusbar.sep1 -orient vertical] -side right -padx 3 -fill y
                #pack [ttk::label .statusbar.labeltextsize -text "size: 0c"] -side right
                #pack [ttk::separator .statusbar.sep2 -orient vertical] -side right -padx 3 -fill y
                #pack [ttk::label .statusbar.labelcols -text "cols: 0"] -side right
                #pack [ttk::separator .statusbar.sep3 -orient vertical] -side right -padx 3 -fill y
                #pack [ttk::label .statusbar.labelrows -text "rows: 0"] -side right
                #pack [ttk::separator .statusbar.sep4 -orient vertical] -side right -padx 3 -fill y
                #pack [ttk::label .statusbar.labelclock -textvariable ::textEditorMain::time_now] -side left
                
                
                #
                # set the Layout of the mainFrame
                #
                grid .ef -sticky news
                #grid .statusbar -sticky news
        
                #expand
                grid columnconfigure . 0 -weight 1
                grid rowconfigure . 1 -weight 1
                
                # save the file when wm closing
                wm protocol . WM_DELETE_WINDOW {::TEGUI::closeTextWidget .ef.tedit.text .ef}
        }
        
        

        
        
        
        # getCopy --
        #       Select the content from the given text widget and then copy to the clipboard. 
        #
        #   name:       the given text widget
        #       
        # Attention:
        #       1. package twapi
        #     2. proc savefile (4)
        # Returns:
        #       None
        #
        proc getCopy {name} {
        #       ::TEGUI::SaveFile .ef.tedit 0
                set text $name.text
                
                if { [catch {$text index sel.first} ] } {
                        #puts "!insert"
                        set newInsert  [$text index insert]
                } else {
                        #puts "!sel"
                        set selrange [$text tag nextrange sel sel.first sel.last]
                        set startIdx [$text index sel.first]
                        set endIdx [$text index sel.last]
                        ::TEGUI::SaveFile $name 4 $startIdx $endIdx
                }
        }
        
        proc copyEditor {name} {

                getCopy $name

        }
        
        proc cutEditor {name} {

                getCopy $name
                catch {eval $name.text delete sel.first sel.last}

        }
        
        # need test
        proc pasteEditor {name} {

                catch {$name.text delete sel.first sel.last}
                getPaste $name
        }

        
        # getPaste --
        #       get the HTML contents from the clipboard and then
        #       paste it to the tcl/tk text editor (now only for windows) 
        #
        # Arguments:
        #   name:       the given text widget  
        #       
        # Attention:
        #       1. package twapi
        #
        # Returns:
        #       none
        #
        # serial Nr:    Effects:  
        # 1:                    CF_TEXT             
        # 2:                    CF_BITMAP           
        # 3:                    CF_METAFILEPICT     
        # 4:                    CF_SYLK             
        # 5:                    CF_DIF              
        # 6:                    CF_TIFF             
        # 7:                    CF_OEMTEXT          
        # 8:                    CF_DIB              
        # 9:                    CF_PALETTE          
        # 10:                   CF_PENDATA          
        # 11:                   CF_RIFF             
        # 12:                   CF_WAVE             
        # 13:                   CF_UNICODETEXT      
        # 14:                   CF_ENHMETAFILE      
        # 15:                   CF_HDROP                         
        # 16:                   CF_LOCALE           
        # 17:                   CF_MAX    
        # 49154:                        ObjectLink
        # 49155:                OwnerLink
        # 49156:                Native
        # 49158:                FileName
        # 49159:                FileNameW
        # 49161:                DataObject              
        # 49163:                Embed Source
        # 49165:                Link Source
        # 49166:                Object Descriptor
        # 49167:                Link Source Descriptor
        # 49171:                Ole Private Data                
        # 49321:                Shell IDList Array
        # 49384:                OLE2UIresimage
        # 49423:                Rich Text Format
        # 49454:                Preferred DropEffect
        # 49477:                Shell Object Offsets
        # 49485:                HTML Format     
        # 49501:                AsyncFlag
        # 49528:                DataObjectAttributes
        # 49529:                DataObjectAttributesRequiringElevation
        # 49605:                HyperLink
        # 49640:                HyperLink
        # 49685:                text/html       
        # 50047:                text/_moz_htmlcontext
        # 50048:                text/_moz_htmlinfo
        # 50049:                text/x-moz-url-priv
        # 50137:                GIF
        # 50139:                PNG
        # 50141:                JFIF
        # 50243:                Art::GVML ClipFormat
        proc getPaste {name} {
                set isHtml 0
                set inputFmt 1
                ::textEditor::setIndexInsert $name.text; ::textEditor::setIndexInsertPress 0
                
                global tcl_platform
                if {[string tolower $tcl_platform(platform)] == "windows"} {
                                catch {twapi::empty_clipboard; twapi::close_clipboard}
                                catch {twapi::open_clipboard}
                                #puts "-------clipboard opened-------"
                                #puts [twapi::get_registered_clipboard_format_name 49640]
                        
                                set htmlFmt [twapi::register_clipboard_format "HTML Format"]
                                
                                if {[hasClipFormat $htmlFmt]} {
                                        set inputFmt $htmlFmt
                                        set isHtml 1
                                } 
                                
                                set contents [getClipContents $inputFmt]
                                #puts "clipboard.Contents: $contents"
                                
                                # HTML format number
                                #       clipFormatList
                                twapi::close_clipboard
                                #puts "-------clipboard closed-------"			
                } elseif {[string tolower $tcl_platform(platform)] == "unix" && [string tolower $tcl_platform(os)] == "darwin" } {
                  			set cscriptfile [::TEGUI::setTempFile "osascript.sh" ]
                  			set cscript [open $cscriptfile "w"]
                  			puts $cscript "osascript -e 'the clipboard as \u00ABclass HTML\u00BB' | perl -ne 'print chr foreach unpack(\"C*\",pack(\"H*\",substr(\$_,11,-3)))' "
                  			close $cscript
                  			if {[catch {set contents [exec sh $cscriptfile ]}]} {	
                  			  set contents [clipboard get]
                  			} else {
                  			  set isHtml 1
                  			}
                  			#puts "MAC Clipboard $contents"
                } else {
                        
                        if {[catch {set contents [clipboard get -type text/html ]}]} {	
                          set contents [clipboard get]
                        } else {  
                          set contentstmp ""   
                          foreach c $contents {

                            #if {$c == "0xff"} 
                            #if {$c == "0xfe"} 
                            if {$c == "0x0"} continue;
                            set cf $c
                            if {[string is integer $c]}  {
                              set cf [format %c $c]
                            }
                            #puts "$c - $cf"
                            append contentstmp $cf
                          }
                          
                          set contents $contentstmp
                          if {![::TEUtils::checkBodyTag $contents]} {
                            set contents "<body>$contentstmp</body>"
                          }
                          
                          set isHtml 1
                        }                       
                }
          
                #puts "Targets [clipboard get -type TARGETS]"
                #puts "Clipboard is HTML $isHtml with $contents"
                
                set insertPos [$name.text index insert]
        
                if {$isHtml} {
                        ::TEUtils::refreshStatus
        
                        # some problem in html contents
                        if {![::TEUtils::checkBodyTag $contents]} {
                                set tagList [::WordProcess::getPTag $name.text {} ]
                                $name.text insert  $insertPos $contents $tagList
                                return;
                        }
        
                        # parse html content            
                        set contents [::WordProcess::parseHtmlToXmlContents $contents]
                        # parse xml to text
                        #puts "getPaste contents: $contents"
                        # update tags and fonts to the textEditor
                        set insertPos [$name.text index insert]
                        #::TEGUI::addText $name.text $insertPos 
                        if {[string tolower $tcl_platform(platform)] == "windows"} {
                                set contents [::TEUtils::UTF8FullCodes $contents 1]
                        }
                        
                        ::WordProcess::parseXmlContents $name $contents $insertPos
                        
                } else {
                        #$name.text insert  $insertPos $contents
                        #set tagList [::WordProcess::getPTag $name.text {} ]
                        tk_textPaste $name.text
                }
                        # Paste nonempty content  generate text modified.
                        ::textEditor::setIndexInsertPress 1
                        if {$contents != ""} {::textEditor::setModi 1; event generate $name.text <<Modified>>}
                # Finally remains the clipboard status and don't clean it.
        }
        
        # getClipContents --
        #       Returns the clipboard's contents with clipboard format number.
        #
        # Arguments:
        #       clipFormat:     the given clipboard format number
        #       
        # Returns:
        #       Returns the selected clipboard content
        #
        proc getClipContents {clipFormat} {
                set result ""
                if {[hasClipFormat $clipFormat]} {
                        catch {twapi::read_clipboard $clipFormat} result
                        #puts $result
                } else {
                        catch {twapi::read_clipboard_text} result
                        #puts $result
                }
                return $result
        }
        
        
        # hasClipFormat --
        #       Check clipboard that contains the given format number or not. 
        #
        # Arguments:
        #       format: the given format number
        #       
        # Returns:
        #       Returns true/1 if contains the given format number
        #
        proc hasClipFormat {format} {
                set available [twapi::clipboard_format_available $format]
                #puts "$format available? $available"
                return $available
        }
        
        # clipFormatList --
        #       Returns all the format numbers of Clipboard 
        #
        # Arguments:
        #       none.
        #       
        # Returns:
        #       Returns list of all clipboard formats
        #
        proc clipFormatList {} {
                twapi::open_clipboard
                #puts "-------clipboard opened-------"
                set list [twapi::get_clipboard_formats] 
                twapi::close_clipboard
                #puts "-------clipboard closed-------"
                #puts "formatList: $list"
                return $list
        }
        
        proc setIgnoreList {newIgnoreList} {
                variable ::textEditorMain::ignoreListGlobal
                set textEditorMain::ignoreListGlobal $newIgnoreList
        }
        
        # right click popmenu
        proc showPopupMenu {menu x y} {
                tk_popup $menu $x $y
        }
        
        # preview widget
        proc showEditorPreviewWindow {ef format} {
                
                # Set up the rest of the user interface
                frame $ef.tedit
                text $ef.tedit.text -wrap word -font TextEditorFont -height 2
                
                pack $ef.tedit.text -side left -expand true -fill both
                
                # information of hover text 
                interp alias {} hoverInfo {} DynamicHelp::register
                
                set exampletext [::msgcat::mc "The quick brown fox jumps over the lazy dog"]
                
                $ef.tedit.text insert end $exampletext
                
                foreach optionValueString [split $format ";"]  {
                        set optionValue [split $optionValueString :]
                        set option [lindex $optionValue 0 ]
                        set value [lindex $optionValue 1 ]
                        ::WordProcess::ChangeStyleW $ef.tedit.text $option $value 0 preview
                }
                
                # toolbar
                frame $ef.toolbar
        
                showToolBar $ef preview
                set ::textEditorMain::editorText $ef.tedit.text
        
                pack $ef.tedit -side bottom -expand 1 -fill both
                pack $ef.toolbar -side top
                
                return "$ef.tedit.text"
        }
        
        proc getFormatEditorPreviewWindow {ef} {
                #set tagnames [$ef.tedit.text tag names 1.0]
                set rend ""
                set tags [$ef.tedit.text dump -tag 1.0 end]
                foreach {key value index} $tags {
                  puts "getFormatEditorPreviewWindow $key $value $index"
                  if {$key == "tagon"} {
                        set rendvalue [WordProcess::getRendValue $value] 
                        set rend [::TEUtils::appendValue $rend $rendvalue ";"]
                  }
                }
                return $rend
                
        }
        
        # main text widget
        proc showEditorWindow {ef editable} {
                variable ::TEGUI::popupmenuTE
                variable ::TextStyle::popupLabels
                
                # Set up the rest of the user interface
                frame $ef.tedit
                text $ef.tedit.text -yscrollcommand "$ef.tedit.scroll set" -xscrollcommand "$ef.tedit.scrollx set"\
                                -wrap word -font TextEditorFont -selectbackground $::textEditorMain::defaultSelectBG -selectforeground $::textEditorMain::defaultSelectVG
                
                if {$::textEditorMain::defaultConfigure != {}} {
                  foreach {configureoption configurevalue} $::textEditorMain::defaultConfigure {
                    $ef.tedit.text configure $configureoption $configurevalue
                  }
                }
                ttk::scrollbar $ef.tedit.scroll -command "$ef.tedit.text yview" 
                ttk::scrollbar $ef.tedit.scrollx -orient horizontal  -command "$ef.tedit.text xview"
                pack $ef.tedit.scroll -side right -fill y
                pack $ef.tedit.scrollx -side bottom -fill x
                pack $ef.tedit.text -side left -expand true -fill both
        
                
                # information of hover text 
                interp alias {} hoverInfo {} DynamicHelp::register 
                
                bind $ef.tedit.text <FocusIn> {
                        #puts "focusIn maintext tableeditwinpath $textEditorMain::tableeditwinpath "  
                        # another in proc ::TEGUI::createFrameText
                        set ::textEditorMain::tableeditwinpath {}
                        #set ::searchTE::searchOptions(curText) {}                        
                        ::searchTE::getLastText %W main
                                             
                }
                
                bind $ef.tedit.text <FocusOut> { 
                        ::searchTE::getLastText "" main
                        #puts "focus out main Text"
                        break;
                }
                
                # toolbar
                frame $ef.toolbar
        
                if {$editable} {
                        showToolBar $ef standard
                        set ::textEditorMain::editorText $ef.tedit.text
                        bindKeyEvent $ef
                        # text modified 
                  
                        bind $ef.tedit.text <<Paste>> {
                          puts "Paste"


                                  
                        }
                        bind $ef.tedit.text <<Modified>> {
                         ::TEGUI::modifiedEvent %W                         
                        }
                }
                
                set ::textEditorMain::editable $editable
                
                #grid $ef.toolbar  -sticky news
                #grid $ef.tedit -sticky news
                
                #expand
                #grid columnconfigure . 0 -weight 1
                #grid rowconfigure . 1 -weight 1
                
                pack $ef.tedit -side bottom -expand 1 -fill both
                pack $ef.toolbar -side top
                
                # POPUPMENU
                set popupmenuTE(main) $ef.tedit.popupMenuMain
                menu $popupmenuTE(main) -tearoff 0 -postcommand "::TEGUI::checkPopupMenuItems main $popupmenuTE(main) $ef.tedit.text"


                $popupmenuTE(main) add command -label $popupLabels(dump) -command [list ::TEUtils::TextDump $ef.tedit.text ]
                $popupmenuTE(main) add command -label $popupLabels(dumpSel) -command "::TEUtils::TextDump $ef.tedit.text  sel.first sel.last"   
                $popupmenuTE(main) add command -label $popupLabels(tagNames) -command "puts \[$ef.tedit.text tag names insert\]"   
                $popupmenuTE(main) add separator
                $popupmenuTE(main) add command -label $popupLabels(new)  -command [list ::TEGUI::NewFile $ef.tedit $ef]
                $popupmenuTE(main) add command -label $popupLabels(open)  -command [list ::TEGUI::OpenFile $ef.tedit 1 $ef]
                $popupmenuTE(main) add separator
                $popupmenuTE(main) add command -label $popupLabels(copy) -command [list ::TEGUI::copyEditor $ef.tedit]
                $popupmenuTE(main) add command -label $popupLabels(cut) -command [list ::TEGUI::cutEditor $ef.tedit]
                $popupmenuTE(main) add command -label $popupLabels(paste) -command [list ::TEGUI::pasteEditor $ef.tedit]
                $popupmenuTE(main) add separator
                $popupmenuTE(main) add command -label $popupLabels(save) -command [list ::TEGUI::SaveFile $ef.tedit 0]
                $popupmenuTE(main) add command -label $popupLabels(saveAs) -command [list ::TEGUI::SaveFile $ef.tedit 1]
                $popupmenuTE(main) add command -label $popupLabels(saveAsXml) -command [list ::TEGUI::SaveFile $ef.tedit 2]
                $popupmenuTE(main) add command -label $popupLabels(saveAsHtml) -command [list ::TEGUI::SaveFile $ef.tedit 3]
                $popupmenuTE(main) add command -label $popupLabels(saveAsTex) -command [list ::TEGUI::SaveFile $ef.tedit 5]
                $popupmenuTE(main) add command -label $popupLabels(saveAsRtf) -command [list ::TEGUI::SaveFile $ef.tedit 6]
                $popupmenuTE(main) add separator
                $popupmenuTE(main) add command -label $popupLabels(exit) -command [list ::TEGUI::closeTextWidget $ef.tedit $ef]
                
          
                bind $ef.tedit.text "<Shift-Button3-ButtonRelease>" "::TEGUI::showPopupMenu $::TEGUI::popupmenuTE(main) %X %Y;break;"
      
                $ef.tedit.text edit modified false
                return "$ef.tedit.text"
        }
        
        
        
        
        proc showToolBar {ef {windowtype "standard"}} {
                set buttonlist [array names ::TextStyle::tagNames]
                
                #creates the buttons on the bottom of frame
                ttk::frame $ef.toolbar.buttonFrame -borderwidth 1 -relief groove -height 20
                #ttk::frame $ef.toolbar.comboboxFrame  -borderwidth 1 -relief groove -height 20
                ttk::frame $ef.toolbar.buttonFrame.firstLine -borderwidth 1
                ttk::frame $ef.toolbar.buttonFrame.secondLine -borderwidth 1 
                
                # Button frame
                ttk::frame $ef.toolbar.buttonFrame.firstLine.basicBar -borderwidth 1  ;# i, b, elide, u, over...
                ttk::frame $ef.toolbar.buttonFrame.firstLine.functionBar -borderwidth 1  ;# i, b, elide, u, over...
                ttk::frame $ef.toolbar.buttonFrame.firstLine.colorBar -borderwidth 1  ;# bg/fg color
                ttk::frame $ef.toolbar.buttonFrame.firstLine.offsetBar -borderwidth 1         ;# sup & sub
                ttk::frame $ef.toolbar.buttonFrame.firstLine.justifyBar -borderwidth 1  ;# left & center & right
                ttk::frame $ef.toolbar.buttonFrame.secondLine.tableBar -borderwidth 1 ;# table, config, width
                
                # ComboBox frame
                ttk::frame $ef.toolbar.buttonFrame.secondLine.fontBar -borderwidth 1  ;# fontfamily & size
                ttk::frame $ef.toolbar.buttonFrame.secondLine.presetBar -borderwidth 1  ;# fontfamily & size
                
                
                foreach buttonname $buttonlist {
        #               puts "$buttonname length:"
        #               puts [llength ::TextStyle::tagNames($buttonname)]
                        if {[llength $TextStyle::tagNames($buttonname)] <= 2 && $buttonname != "family"} {
                                set newbuttonname ""
                                set command ""
                                
                                # background/foreground color
                                if {$buttonname == "background" | $buttonname == "foreground"} {
                                        set newbuttonname $ef.toolbar.buttonFrame.firstLine.colorBar.$buttonname
                                        set command "::TEGUI::invokeColorDialog $ef.tedit.text $buttonname $windowtype"
                                } else {
                                        set newbuttonname $ef.toolbar.buttonFrame.firstLine.basicBar.$buttonname
                                        set command "::WordProcess::ChangeStyleW $ef.tedit.text $buttonname [lindex [lindex [array get ::TextStyle::tagNames $buttonname] 1 ] 1] 1 $windowtype"
                                }
                                
                                ttk::button $newbuttonname -text $buttonname    \
                                -image ::images::$buttonname -width 16          \
                                -command $command       
                                
                                hoverInfo $newbuttonname balloon [::msgcat::mc $buttonname] 
                                
                                # packing
                                pack $newbuttonname -side left -padx 1 -ipadx 3
                                
                                ::TextStyle::addButton $ef $buttonname $newbuttonname
                        } else {
                                # take those items to the pop dialog.
                                if {[lsearch -exact $TextStyle::internalFonts $buttonname] < 0} { continue; }
                                
                                if {$buttonname == "offset" | $buttonname == "justify"} {
                                        
                                        foreach value $TextStyle::tagNames($buttonname) {
                                                set img ""
                                                set newbuttonname ""
                                                
                                                if {$buttonname == "offset"} {
                                                        if {$value == "-4"} {
                                                                set img "subscript"  
                                                        } elseif {$value == "4"} {
                                                                set img "superscript"
                                                        } else {
                                                                set img "normalscript"
                                                        }
                                                        set newbuttonname $ef.toolbar.buttonFrame.firstLine.offsetBar.$buttonname$img
                                                        ::TextStyle::addButton $ef [list $buttonname $value] $newbuttonname
                                                        
                                                } elseif {$buttonname == "justify"} {
                                                        if {$value == "left"} {
                                                                set img "LeftAlignment"
                                                                
                                                        } elseif {$value == "center"} {
                                                                set img "CenterAlignment"
                                                        } else {
                                                                set img "RightAlignment"
                                                        }
                                                        set newbuttonname $ef.toolbar.buttonFrame.firstLine.justifyBar.$buttonname$img
                                                        ::TextStyle::addButton $ef [list $buttonname $value] $newbuttonname
                                                }
                                                                                 
                                                set buttoncommand  "::WordProcess::ChangeStyleW $ef.tedit.text $buttonname $value 1 $windowtype"
                                                
                                                ttk::button $newbuttonname                                                              \
                                                -image ::images::$img                                                                   \
                                                -width 16p                                                                                              \
                                                -command $buttoncommand
                                                
                                                hoverInfo $newbuttonname balloon [::msgcat::mc $img] 
                                                
                                                pack $newbuttonname -side left -padx 1 -ipadx 3
                                                
                                        }
                                        
                                } else {
                                        # normal combobox
                                
                                        set type [lindex [array get ::TextStyle::types $buttonname] 1]
                                        set lblname "lbl_$buttonname"
                                        
                                        set values $TextStyle::tagNames($buttonname)
                                        set width 10
                                        set comboboxname $ef.toolbar.buttonFrame.$buttonname 
                                        set labelname $ef.toolbar.buttonFrame.$lblname
                                        set comboeditable false
                                        set exportselection false
                                        
                                        if { $type == "numList"} {
                                                set width  4
                                                set comboeditable true
                                                set exportselection true
                                                if {$buttonname == "size"} {
                                                        set comboboxname $ef.toolbar.buttonFrame.secondLine.fontBar.$buttonname 
                                                        set labelname $ef.toolbar.buttonFrame.secondLine.fontBar.$lblname
                                                }                                                                               
                                        } 
                                        if {$buttonname == "family"} {
                                                #set values [lsort -dictionary [font families]]
                                                set width 10
                                                set comboboxname $ef.toolbar.buttonFrame.secondLine.fontBar.$buttonname 
                                                set labelname $ef.toolbar.buttonFrame.secondLine.fontBar.$lblname
                                        }
                                        if {$buttonname == "preset"} {
                                                set width 10
                                                set comboeditable true
                                                set exportselection true
                                                set comboboxname $ef.toolbar.buttonFrame.secondLine.presetBar.$buttonname 
                                                set labelname $ef.toolbar.buttonFrame.secondLine.presetBar.$lblname
                                        }        
                                        
                                                                        
                                        
                                        set values [concat "(standard)" $values]
                                        
                                        ComboBox $comboboxname                                                                  \
                                        -editable $comboeditable                                                                        \
                                        -exportselection $exportselection                              \
                                        -bd 2                                                                                           \
                                        -width $width                                                                           \
                                        -values $values                                                                         \
                                        -textvariable   ""                                                                  \
                                        -modifycmd "::combo::getComboCurrent $comboboxname $ef.tedit.text $buttonname $windowtype"      \
                                        -text {}
                                        
                                        hoverInfo $comboboxname balloon [::msgcat::mc $buttonname]
                                        
                                        # combobox editable
                                        if {$type == "numList" || $type == "preset" } {
                                                combo::comboBoxEnable $comboboxname $windowtype
                                                if {$type == "preset"} {
                                                        set ::TEDB::presetOption(combobox) $comboboxname
                                                }
                                        }
                                        
                                        ttk::label $labelname -text [::msgcat::mc $buttonname] ;# combobox label
                                                
                                        pack $labelname -side left 
                                        pack $comboboxname -side left
                                        ::TextStyle::addButton $ef $buttonname $comboboxname
                                }
        
                        }
        
                }
          
                
                # so right click for popup menu will be considered!
                set tableButton $ef.toolbar.buttonFrame.secondLine.tableBar.tableButton
                ttk::button $tableButton -image ::images::table -command "::TEGUI::insertTable $ef.tedit.text"
                hoverInfo $tableButton balloon [::msgcat::mc "Insert Table"] 
                pack $tableButton -side left -padx 1 -ipadx 3
                
                set tableConfigButton $ef.toolbar.buttonFrame.secondLine.tableBar.tableConfigButton
                ttk::button $tableConfigButton -image ::images::tableConfig -command "::TEGUI::invokeTableEditDialog $ef.tedit.text 0"
                hoverInfo $tableConfigButton balloon [::msgcat::mc "Table Configure"] 
                pack $tableConfigButton -side left -padx 1 -ipadx 3
                
                set tableWidthButton $ef.toolbar.buttonFrame.secondLine.tableBar.tableWidthButton
                ttk::button $tableWidthButton -image ::images::tableWidth -command "::TEGUI::invokeTableEditDialog $ef.tedit.text 1"
                hoverInfo $tableWidthButton balloon [::msgcat::mc "Table Width"] 
                pack $tableWidthButton -side left -padx 1 -ipadx 3
              
                ### begin of change by Li: 2014.11.19
                ## link button
                set linkButton $ef.toolbar.buttonFrame.firstLine.functionBar.link
              #         # TODO: Li linkButton command
                ttk::button $linkButton -image ::images::link -command "::TEGUI::insertLinkDialog link $ef.tedit.text"
                hoverInfo $linkButton balloon  [::msgcat::mc "Insert Link"] 
                pack $linkButton -side left -padx 1 -ipadx 3
                ### end of change by Li: 2014.11.19
               
                ## link button
                set anchorButton $ef.toolbar.buttonFrame.firstLine.functionBar.anchor
              #         # TODO: Li linkButton command
                ttk::button $anchorButton -image ::images::anchor -command "::TEGUI::insertLinkDialog anchor $ef.tedit.text"
                hoverInfo $anchorButton balloon  [::msgcat::mc "Insert Anchor"] 
                pack $anchorButton -side left -padx 1 -ipadx 3
                
                ## delete button
                set deleteButton $ef.toolbar.buttonFrame.firstLine.functionBar.deleteButton
                ttk::button $deleteButton -image ::images::delete -command "::WordProcess::ChangeTagW $ef.tedit.text null"
                hoverInfo $deleteButton balloon [::msgcat::mc "Delete Tags"] 
                pack $deleteButton -side left -padx 1 -ipadx 3
                
                set searchButton $ef.toolbar.buttonFrame.firstLine.functionBar.searchButton
                ttk::button $searchButton -image ::images::search -command "::TEGUI::invokeSearchDialog $ef.tedit.text 0"
                hoverInfo $searchButton balloon  [::msgcat::mc "Search"] 
                pack $searchButton -side left -padx 1 -ipadx 3
                
                set paraButton $ef.toolbar.buttonFrame.firstLine.functionBar.paraButton
                ttk::button $paraButton -image ::images::paragraph -command "::WordProcess::setParagraphStyles $ef.tedit.text Paragraph"
                hoverInfo $paraButton balloon  [::msgcat::mc "Paragraph"] 
               #pack $paraButton -side left -padx 1 -ipadx 3
                
                # timestamp
                set tsButton $ef.toolbar.buttonFrame.secondLine.presetBar.timestampBtn
                ttk::button $tsButton -image ::images::timestampImg -command "::WordProcess::insertTimestamp $ef.tedit.text"
                hoverInfo $tsButton balloon  [::msgcat::mc "Timestamp"] 
                pack $tsButton -side left -padx 1 -ipadx 3

                        
                # pack blocks

                pack $ef.toolbar.buttonFrame.firstLine.basicBar -in $ef.toolbar.buttonFrame.firstLine -side left
                pack [ttk::separator $ef.toolbar.buttonFrame.firstLine.sep0 -orient vertical] -side left -padx 3 -fill y
                if {$windowtype != "preview"} { 
                        pack $ef.toolbar.buttonFrame.firstLine.functionBar -in $ef.toolbar.buttonFrame.firstLine -side left
                }
                pack [ttk::separator $ef.toolbar.buttonFrame.firstLine.sep0a -orient vertical] -side left -padx 3 -fill y
                pack $ef.toolbar.buttonFrame.firstLine.colorBar -in $ef.toolbar.buttonFrame.firstLine -side left
                pack [ttk::separator $ef.toolbar.buttonFrame.firstLine.sep2 -orient vertical] -side left -padx 3 -fill y
                pack $ef.toolbar.buttonFrame.firstLine.offsetBar -in $ef.toolbar.buttonFrame.firstLine -side left
                pack [ttk::separator $ef.toolbar.buttonFrame.firstLine.sep3 -orient vertical] -side left -padx 3 -fill y
                pack $ef.toolbar.buttonFrame.firstLine.justifyBar -in $ef.toolbar.buttonFrame.firstLine -side left
                #pack [ttk::separator $ef.toolbar.buttonFrame.firstLine.sep4 -orient vertical] -side left -padx 3 -fill y
                
                if {$windowtype != "preview"} { 
                  pack $ef.toolbar.buttonFrame.secondLine.tableBar -in $ef.toolbar.buttonFrame.secondLine -side left
                }
                pack [ttk::separator $ef.toolbar.buttonFrame.secondLine.sep1 -orient vertical] -side left -padx 3 -fill y

                pack $ef.toolbar.buttonFrame.secondLine.fontBar -in $ef.toolbar.buttonFrame.secondLine -side left       

                if {$windowtype != "preview"} { 
                  pack $ef.toolbar.buttonFrame.secondLine.presetBar -in $ef.toolbar.buttonFrame.secondLine -side left 
                }
                pack [ttk::separator $ef.toolbar.buttonFrame.secondLine.sep2 -orient vertical] -side left -padx 3 -fill y
                ttk::button $ef.toolbar.buttonFrame.secondLine.getinsert -text ">>>>>" -image ::images::externalFrame\
                        -command "::TEGUI::extraFontsWindow $ef $windowtype" 
                hoverInfo $ef.toolbar.buttonFrame.secondLine.getinsert balloon [::msgcat::mc "More..."] 
                pack $ef.toolbar.buttonFrame.secondLine.getinsert -side left ;#-expand 1 -fill x
                
                pack $ef.toolbar.buttonFrame.firstLine -side top -fill x
                pack $ef.toolbar.buttonFrame.secondLine -side top -anchor center
                pack $ef.toolbar.buttonFrame -fill x
        }
        
        
        proc insertStampDialog {type text} {
                variable ::textEditorMain::urlData
                
                
                
                
        }
        
        
        # insert link dialog
        #
        # Li : 2013.11.19
        proc insertLinkDialog {type text} {
                variable ::textEditorMain::urlData
                
                catch {uplevel destroy $newWindows::win(LC)}
                toplevel $newWindows::win(LC) 
                #  title
                wm title $newWindows::win(LC) [::msgcat::mc $type]
                
                set f [ttk::frame $newWindows::win(LC).linkf]
                ttl::label  $f.u -text [::msgcat::mc $type]
                ttk::entry  $f.link -textvar ::textEditorMain::urlData(textEditorLink)
                ttk::button $f.ok -text [::msgcat::mc OK] -command {catch {set ::textEditorMain::urlData(configLink) $::textEditorMain::urlData(textEditorLink)}}
                ttk::button $f.cancel -text [::msgcat::mc Cancel] -command {set ::textEditorMain::urlData(configLink) ""}
                
                pack $f.u $f.link $f.ok $f.cancel -side left -padx 1 -ipadx 3
                pack $f
        
                #bind
                bind $newWindows::win(LC) <Return> {
                        catch {$newWindows::win(LC).linkf.ok invoke}
                        break;
                }
                bind $newWindows::win(LC) <Escape> { 
                        catch {$newWindows::win(LC).linkf.cancel invoke}
                        break;
                }
                #parray ::textEditorMain::urlData
                
                
                #CONSIDER: VWAIT?
                #raise $f
                vwait ::textEditorMain::urlData(configLink)
                
                #
                if {$textEditorMain::urlData(configLink) != ""} {
                        ::WordProcess::ChangeStyleW $text $type $textEditorMain::urlData(configLink) 0 standard
                }
                destroy $newWindows::win(LC)
                return $textEditorMain::urlData(configLink)
        }
        
        
        #type: editDialog:0;  widthDialog 1
        proc invokeTableEditDialog {text type} {
                variable ::textTable::tableControl
                variable ::textEditorMain::tableeditwinpath
                #puts "invokeTableEditDialog_tableeditwinpath: $tableeditwinpath"
                if {![info exists tableeditwinpath] || $tableeditwinpath == ""} {
                        putsstderr "Warning: No Table cell was selected! Please Check it!"
                        return;
                }
                
                set curTable [::TEUtils::getWidgetParent $textEditorMain::tableeditwinpath Tablelist]
                if {[catch {winfo class $curTable}]} {return;}
                
                set cellIndex [getCellIndex $textEditorMain::tableeditwinpath]
                if {$cellIndex == ""} {return;}
                
                # in such form: row,col
                if {$type == 0} {
                        createEditTableDialog $text $curTable $newWindows::win(TE) $cellIndex
                } elseif {$type == 1} {
                        createEditWidthDialog $text $curTable $newWindows::win(TE) $cellIndex
                }
        }

        
        #type: 0 - with replace / 1 - without replace
        #*22.05.2015 RP (#3306) adapt size of framelabels in "Search" window
        proc invokeSearchDialog {text {type 0}} {
                variable ::searchTE::searchOptions
                # set the main text 
                                ::searchTE::initSearchFunc $text
                catch {uplevel destroy $newWindows::win(ST)}
                # build a new table window
                toplevel $newWindows::win(ST) -padx 10 -pady 10
                # table title
                wm title $newWindows::win(ST) [::msgcat::mc "Search Text"]
                
                set ::searchTE::searchOptions(searchKeyWord) ""
                set ::searchTE::searchOptions(replaceKeyWord) ""
                set ::searchTE::searchOptions(direction) "searchForward"
                set ::searchTE::searchOptions(wrap) 1
                set ::searchTE::searchOptions(newStart) 0
                set ::searchTE::searchOptions(caseSensitiv) 0
                set buttonArea1 [ttk::frame $newWindows::win(ST).buttonArea1]
                set buttonArea2 [ttk::frame $newWindows::win(ST).buttonArea2]
                
                set seachButton  $buttonArea1.searchButton
                set replaceButton $buttonArea2.replaceButton
                set replaceAllButton $buttonArea2.replaceAllButton
                set cancelButton $buttonArea1.cancelButton
                
                set searchArea [ttk::frame $newWindows::win(ST).searchArea]
                set replaceArea [ttk::frame $newWindows::win(ST).replaceArea]
                
                set controlArea [ttk::frame $newWindows::win(ST).controlArea]
                
                set directionArea [ttk::labelframe $controlArea.directionArea -width 100 -height 100 -text [::msgcat::mc "Direction"]]
                set optionArea [ttk::labelframe $controlArea.optionArea -width 200 -height 100 -text [::msgcat::mc "Options"]]
                
                # set master window
                pack propagate $directionArea 0
                pack propagate $optionArea 0
                
                set searchLabel $searchArea.label
                set replaceLabel $replaceArea.label
                
                set searchBox $searchArea.searchBox
                set replaceBox $replaceArea.replaceBox
                
                set forwardButton $directionArea.forwardButton
                set backwardButton $directionArea.backwardButton
                
                set caseSensitiveButton $optionArea.caseSensitivButton
        #       set wholeWordButton $optionArea.wholeWordButton
        #       set regexpButton $optionArea.regexpButton
                set wrapButton $optionArea.wrapButton
                set startPosButton $optionArea.startPosButton
                
                ttk::radiobutton $forwardButton -value "searchForward" -text [::msgcat::mc "Forward"] -variable ::searchTE::searchOptions(direction)
                ttk::radiobutton $backwardButton -value "searchBackward" -text [::msgcat::mc "Backward"] -variable ::searchTE::searchOptions(direction)
                
                ttk::checkbutton $caseSensitiveButton -text [::msgcat::mc "Case sensitive"] -variable ::searchTE::searchOptions(caseSensitiv) 
        #       ttk::checkbutton $wholeWordButton -text [::msgcat::mc "Whole word"] -variable textEditorMain::searchOption(wholeWord) 
        #       ttk::checkbutton $regexpButton -text [::msgcat::mc "RegExp search"] -variable textEditorMain::searchOption(regexp)
                ttk::checkbutton $wrapButton -text [::msgcat::mc "Wrap search"] -variable ::searchTE::searchOptions(wrap)  
                ttk::checkbutton $startPosButton -text [::msgcat::mc "From the start"] -variable ::searchTE::searchOptions(newStart)
        
                
                ttk::button $seachButton -text [::msgcat::mc "Search"] -state disabled -command "::searchTE::searchTextShell"
                #"::TEGUI::closeWindow $newDialog;::TEGUI::editTable $text $tableName $cellIndex"
                ttk::button $replaceButton -text [::msgcat::mc "Replace"] -state disabled -command "::searchTE::replaceText 0" 
                ttk::button $replaceAllButton -text [::msgcat::mc "Replace all"] -state disabled -command "::searchTE::replaceText 1" 
                #"::TEGUI::closeWindow $newDialog;::TEGUI::editTable $text $tableName $cellIndex"
                ttk::button $cancelButton -text [::msgcat::mc "Cancel"] -command "::TEGUI::closeWindow $newWindows::win(ST)"
                #"::TEGUI::closeWindow $newDialog;::TEGUI::editTable $text $tableName $cellIndex"
                
                
                ttk::label $searchLabel -text [::msgcat::mc "Search: "] -width 10 -anchor w
                ttk::label $replaceLabel -text [::msgcat::mc "Replace: "] -width 10 -anchor w
                
                ComboBox $searchBox -editable true -exportselection true -bd 2 -width 20 \
                                                        -values "" -textvariable ::searchTE::searchOptions(searchKeyWord) \
                                                        -modifycmd "" 
                ComboBox $replaceBox -editable true -exportselection true -bd 2 -width 20 \
                                                        -values "" -textvariable ::searchTE::searchOptions(replaceKeyWord) \
                                                        -modifycmd ""
                
                combo::comboBoxEnable $searchBox 1
                
                ############################
                # packen
                pack $searchLabel -side left
                pack $searchBox -side right
                pack $searchArea -side top -pady 5 -fill x
                bind $searchBox.e <KeyRelease> "::TEGUI::setButtonState $searchBox $seachButton;break;"
                bind $searchBox.e <Return> "::searchTE::searchTextShell; focus $searchBox"
                
                
                if {$type == 0} {
                    ::combo::comboBoxEnable $replaceBox 1
                    pack $replaceLabel -side left
                    pack $replaceBox -side right
                    pack $replaceArea -side top -pady 5 -fill x
                        
                    bind $replaceBox.e <KeyRelease> "::searchTE::invokeReplaceButtons $replaceBox.e;break;"
                                        bind $replaceButton <<TriggerReplace>> "::searchTE::invokeReplaceButtons $replaceBox.e;break;"
                                        bind $replaceAllButton <<TriggerReplace>> "::searchTE::invokeReplaceButtons $replaceBox.e;break;"
                }
                
                pack $forwardButton $backwardButton -side top -fill x
                pack $directionArea -side left
                
        #       pack $caseSensitiveButton $wholeWordButton $regexpButton -side top -fill x
                pack $caseSensitiveButton $wrapButton $startPosButton -side top -fill x
                pack $optionArea -side right
                
                pack $controlArea -side top -padx 5 -pady 5 -fill x
                
                
                set ::searchTE::searchOptions(replaceButton) "$replaceButton $replaceAllButton"
                if {$type == 0} {
                        pack $seachButton $replaceButton $replaceAllButton $cancelButton -side left -pady 2 -padx 2 -ipadx 2 -ipady 2
                        pack $buttonArea1 $buttonArea2 -side top -pady 2 -padx 2 -ipadx 2 -ipady 2
                } elseif {$type == 1} {
                        pack $seachButton $cancelButton -side left -pady 5 -padx 10 -ipadx 10 -ipady 10 -fill x
                        pack $buttonArea1 -side top -pady 2 -padx 2 -ipadx 2 -ipady 2
                }
                
                ##############
                # can't resize
                wm resizable $newWindows::win(ST) 0 0
                
                #Attention: focus here! Dangerous!
                focus $newWindows::win(ST)
                focus $searchBox
                bind $newWindows::win(ST) <Escape> {
                        #puts "escape"
                        ::TEGUI::closeWindow %W
                        break;
                }
                # alpha games
                bind $newWindows::win(ST) <FocusIn> {
                        wm attributes $newWindows::win(ST) -alpha 1
                        break;
                }
                # alpha games
                bind $newWindows::win(ST) <FocusOut> {
                        wm attributes $newWindows::win(ST) -alpha 0.4
                        raise $newWindows::win(ST)
                        break;
                }
                
        }
        
        
        # check entry to set button state 
        proc setButtonState {entry button} {
                if {![winfo exists $entry] || ![winfo exists $button]} {return;}
                set content [$entry get]
                if {[string length $content] > 0} {
                        $button configure -state normal
                } else {
                        $button configure -state disabled
                }
        }
        
        
        proc createEditTableDialog {text tableName newDialog cellIndex} {
                
                # destroy old window, make sure only one new extra window
                catch {uplevel destroy $newDialog}
                
                # build a new table window
                toplevel $newDialog -padx 10 -pady 10
                # table title
                wm title $newDialog [::msgcat::mc "Table Edit Configuration"]
                #refresh the row/col number for new table
                set ::textEditorMain::tableEditValue ""
                
                set deleteCurRowArea [ttk::frame $newDialog.delCurRow]
                set deleteCurColArea [ttk::frame $newDialog.delCurCol]
                set insertLeftColArea [ttk::frame $newDialog.insertLeftCol]
                set insertRightColArea [ttk::frame $newDialog.insertRightCol]
                set insertAboveRowArea [ttk::frame $newDialog.insertAboveRow]
                set insertBelowRowArea [ttk::frame $newDialog.insertBelowRow]
                
                set deleteCurRowButton $deleteCurRowArea.button
                set deleteCurColButton $deleteCurColArea.button
                set insertLeftColButton $insertLeftColArea.button
                set insertRightColButton $insertRightColArea.button
                set insertAboveRowButton $insertAboveRowArea.button
                set insertBelowRowButton $insertBelowRowArea.button
                
                set okButton $newDialog.ok
                set cancelButton $newDialog.cancel
                
                ttk::radiobutton $insertLeftColButton -value "insert col left" -text [::msgcat::mc "Insert Column to the Left"] -variable ::textEditorMain::tableEditValue 
                ttk::radiobutton $insertRightColButton -value "insert col right" -text [::msgcat::mc "Insert Column to the Right"] -variable ::textEditorMain::tableEditValue 
                ttk::radiobutton $insertAboveRowButton -value "insert row above" -text [::msgcat::mc "Insert Row Above"] -variable ::textEditorMain::tableEditValue 
                ttk::radiobutton $insertBelowRowButton -value "insert row below" -text [::msgcat::mc "Insert Row Below"] -variable ::textEditorMain::tableEditValue 
                                
                ttk::radiobutton $deleteCurRowButton -value "delete row cur" -text [::msgcat::mc "Delete Row"] -variable ::textEditorMain::tableEditValue 
                ttk::radiobutton $deleteCurColButton -value "delete col cur" -text [::msgcat::mc "Delete Column"] -variable ::textEditorMain::tableEditValue 
                
                ttk::button $okButton -text [::msgcat::mc OK] -command "::TEGUI::closeWindow $newDialog;::TEGUI::editTable $text $tableName $cellIndex"
                ttk::button $cancelButton -text [::msgcat::mc Cancel] -command "::TEGUI::closeWindow $newDialog" 
                
                pack $deleteCurRowButton -side left
                pack $deleteCurRowArea -side top -pady 5 -fill x
                pack $deleteCurColButton -side left
                pack $deleteCurColArea -side top -pady 5 -fill x
                pack [ttk::separator $newDialog.sep0 -orient horizontal ] -side top -pady 3 -fill x
                
                pack $insertLeftColButton -side left
                pack $insertLeftColArea -side top -pady 5 -fill x
                pack $insertRightColButton -side left
                pack $insertRightColArea -side top -pady 5 -fill x
                pack $insertAboveRowButton -side left
                pack $insertAboveRowArea -side top -pady 5 -fill x
                pack $insertBelowRowButton -side left
                pack $insertBelowRowArea -side top -pady 5 -fill x
                
                pack $okButton -side left -pady 5 -padx 10
                pack $cancelButton -side right -pady 5 -padx 10
                
                # can't resize
                wm resizable $newDialog 0 0
                
                #Attention: focus here! Dangerous!
                focus $newDialog
                bind $newDialog <Escape> {
                        #puts "escape"
                        ::TEGUI::closeWindow %W
                        break;
                }
        }
        
        
        
        #cellIndex: row,col
        proc createEditWidthDialog {text tableName newDialog cellIndex} {
                # destroy old window, make sure only one new extra window
                catch {uplevel destroy $newDialog}
                 
                # build a new table window
                toplevel $newDialog -padx 10 -pady 10
                # table title
                wm title $newDialog [::msgcat::mc "Table width Configuration"]
                #refresh the row/col number for new table
                set ::textEditorMain::tableEditValue ""
                ::textEditorMain::resetTableEditWidth
                set ::textEditorMain::tableEditColumnType "selCol"
                
                set okButton $newDialog.ok
                set cancelButton $newDialog.cancel
                 
                set widthArea [ttk::frame $newDialog.widthArea]
                set widthLabel $widthArea.label
                set widthEntry $widthArea.entry
                
                set allArea [ttk::frame $newDialog.allArea]
                set colArea [ttk::frame $newDialog.colArea]
                 
                set allButton $allArea.radioButton
                set colButton $colArea.radioButton
                
                ttk::radiobutton $allButton -value "allCols" -text [::msgcat::mc "All Columns"] -variable ::textEditorMain::tableEditColumnType
                ttk::radiobutton $colButton -value "selCol" -text [::msgcat::mc "The selected Column"] -variable ::textEditorMain::tableEditColumnType
                 
                ttk::label $widthLabel -text [::msgcat::mc "Width: "] -width 20 -anchor w
                ttk::entry $widthEntry -textvariable textEditorMain::tableEditWidth(dialogInput) \
                                         -justify center -width 20 -validate all -validatecommand {::TEUtils::validEditTableInput %P}
                 
                 
                ttk::button $okButton -text [::msgcat::mc OK] -command "::TEGUI::setTableWidthW $text $tableName 0 $cellIndex;::TEGUI::closeWindow $newDialog"
                ttk::button $cancelButton -text [::msgcat::mc Cancel] -command "::TEGUI::closeWindow $newDialog" 
                 
                pack $widthLabel -side left 
                pack $widthEntry -side right
                pack $widthArea -side top -pady 5 -fill x
                
                pack $allButton -side left
                pack $allArea -side top -padx 10 -fill x
                
                
                pack $colButton -side left
                pack $colArea -side top -padx 10 -fill x
        
                
                
                pack $okButton -side left -pady 5 -padx 10
                pack $cancelButton -side right -pady 5 -padx 10
        
                # can't resize
                wm resizable $newDialog 0 0
                 
                #Attention: focus here! Dangerous!
                focus $newDialog
                bind $newDialog <Escape> {
                        #puts "escape"
                        ::TEGUI::closeWindow %W
                        break;
                }
        }
        
        
        #cellIndex: row,col
        #source: inputDialog: 0; xml: 1;
        #Attention: before set the value of tableEditWidth need to reset whole array
        proc setTableWidthW {text tbl source {cellIndex ""}} {
                variable ::textEditorMain::coefficients                 ;# the default coefficient for initialization
                variable ::textEditorMain::tableEditColumnType  ;# the input column type, single column or all of table
                variable ::textEditorMain::tableEditWidth                       ;# the input width value, Array
                if {$text== "" || $tbl == "" || $source == "" 
                                || ![string equal -nocase [winfo class $tbl] tablelist]} {return;}
                
                set tableDiffValue 0
                set oldTableWdith [$tbl cget -width]
                
                if {$source == 0 && [info exists tableEditWidth(dialogInput)]} {
                        if {![::TEUtils::validTableCellWidths $tableEditWidth(dialogInput)]} {
                                putsstderr "Warning: width value of new table: $widthValue is invalid!"
                                return;
                        }
                        set widthList [split $tableEditWidth(dialogInput) ";"]
                        
                        if {$tableEditColumnType eq "selCol"} {
                                if {$cellIndex == "" || [string first "," $cellIndex] <= 0} {return;}
                                set col [lindex [split $cellIndex ","] 1 ]
                                set newWidth [lindex $widthList 0]
                                #set oldColWidth [expr { round ( [$tbl columnwidth $col] / 7) + 2 } ]
                                set oldColWidth [$tbl columncget $col -width ]
                                # difference
                                incr tableDiffValue [expr $newWidth - $oldColWidth + 2]
                                $tbl columnconfigure $col -width $newWidth
                        } elseif {$tableEditColumnType eq "allCols"} {
                                # oldvalue make sure all column's width
                                set oldValue 0
                                if {$widthList == ""} {return;}
                                set colCount [$tbl columncount]
                                for  {set col 0} {$col < $colCount} {incr col} {
                                        #set oldColWidth [expr { round ( [$tbl columnwidth $col] / 7) + 2 } ]
                                        set oldColWidth [$tbl columncget $col -width ]
                                        # get new value
                                        set newValue [lindex $widthList $col]
                                        set newValue [string map {em {}} $newValue]
                                        if {$newValue == ""} {set newValue -1}
                                        # old or new value
                                        #puts "newValue: $newValue"
                                        set newValue [expr  $newValue == -1 ? $oldValue : $newValue]
                                        # no valid value, oldvalue is empty too # unusually
                                        set newValue [expr $newValue == 0 ? $coefficients(defaultCellWidth) : $newValue]
                                        # min 2
                                        set newValue [expr { $newValue < 2 ? 2 : $newValue}]
                                        # difference
                                        incr tableDiffValue [expr $newValue - $oldColWidth + 2]
                                        # set width value
                                        $tbl columnconfigure $col -width $newValue
                                        # get old value
                                        set oldValue $newValue
                                }
                        }
                        #set ::textEditorMain::textModiMsg 1
                                                ::textEditor::setModi 1
                        set parentText [::TEUtils::getWidgetParent $tbl text]
                        event generate $parentText <<Modified>>
                } elseif {$source == 1} {
                        # xml
                        set oldValue 0
                        set colCount [$tbl columncount]
                        for  {set col 0} {$col < $colCount} {incr col} {
                                #set oldColWidth [expr { round ( [$tbl columnwidth $col] / 7) + 2 } ]
                                set oldColWidth [$tbl columncget $col -width ]
                                if {[info exists tableEditWidth(curTable,$col)]} {
                                        set newValue $tableEditWidth(curTable,$col)
                                        #puts "setTableWidthW_source_1_newValue: $newValue"
                                } else {set newValue -1}
                                # must reserve the oringal form to here 
                                set newValue [string map {em {}} $newValue]
                                if {$newValue == ""} {set newValue -1}
                                set newValue [expr  $newValue == -1 ? $oldValue : $newValue]
                                # no valid value, oldvalue is empty too # unusually
                                set newValue [expr $newValue == 0 ? $coefficients(defaultCellWidth) : $newValue]
                                # min 2
                                set newValue [expr { $newValue < 2 ? 2 : $newValue}]
                                # difference
                                incr tableDiffValue [expr $newValue - $oldColWidth + 2]
                                $tbl columnconfigure $col -width $newValue
                                set oldValue $newValue
                        }
                }
                
                set tWidth [expr $oldTableWdith + $tableDiffValue ]
                
                $tbl configure -width $tWidth
                
                # set width value to NULL
                ::textEditorMain::resetTableEditWidth
        }
        
        
        
        proc editTable {text tbl cellIndex} {
                #variable ::textTable::tableControl
                if {$tbl == "" || ![string equal -nocase [winfo class $tbl] tablelist]} {return;}
                if {$cellIndex == "" || [string first "," $cellIndex] <0} {return;}
                #puts "table: $tbl - cellIndex: $cellIndex"
        
        #       set tableRowNum [$tbl index end]
        #       set tableColNum [$tbl columncount]
                set command $textEditorMain::tableEditValue
                
                foreach {rowNum colNum} [split $cellIndex ","] break;
                #puts "rowNum: $rowNum - colNum: $colNum"
                
                
                set rowCount [$tbl size]                ;# get number of rows
                
                if {$command == ""} {
                        #TODO: WARNING DIALOG
                        putsstderr "Warning: No insert or delete command!"
                        return;
                } else {
                        foreach {cmd rc pos} $command break;
                        #puts "$cmd $rc $pos"
                        
                        #(1) change old contents, (2) save to xml, string or array 
                        #(3) new table (4) insert xml content to the new table
        
                        set index ""
                        if {$cmd == "delete"} {
                                if {$rc == "row"} {
                                        set index $rowNum
                                } elseif {$rc == "col"} {
                                        set index $colNum
                                }
                        } elseif {$cmd == "insert"} {
                                if { $rc == "row" } {
                                        if { $pos == "above"} {
                                                set index $rowNum
                                        } elseif { $pos == "below" } {
                                                set index [expr $rowNum + 1 ]
                                        }
                                } elseif { $rc == "col" } {
                                        if { $pos == "left"} {
                                                set index $colNum                                       
                                        } elseif { $pos == "right" } {
                                                set index [expr $colNum +1 ]
                                        }
                                }                       
                        }
                        
                        set tableIndex [getWinIndex $text window $tbl]
                        if {$tableIndex == ""} {
                                putsstderr "Warning: There is no table $tbl in the $text!"
                        }
                        set newTableContent [::WordProcess::changeTableContents $tbl $cmd $rc $index]
                        #puts "newTableContent: $newTableContent"
                        
                        $text delete $tableIndex
                        
                        ::TEUtils::refreshStatus
                        
                        set ::textEditorMain::editorText $text
                        
                        # parse html content 
                        # key proc: parseCommend
                        htmlparse::parse -cmd ::WordProcess::parseXmlToText $newTableContent
                        
                        ::textEditor::setIndexInsert $text; ::textEditor::setIndexInsertPress 1
                  
                        #puts "main,bodyText: $textEditorMain::xmlData(main,bodyText)"
                        # update tags and fonts to the textEditor       
                        ::TEGUI::addText $text $textEditorMain::xmlData(main,bodyText) $tableIndex
                        
                         event generate $text <<Modified>>
                  
                        ::TEUtils::refreshStatus
                }
                
        }
        
        # _type: window
        proc getWinIndex {text _type tableName} {
                if { ![winfo exists $text] || ![winfo exists $tableName] || $_type == ""} {
                        putsstderr "Error: There is no text or table!"
                        return "";
                }
                set result ""
                #puts [$t dump -window 1.0 end ]
                #window .ef.tedit.text.tableWindow0table 1.6 window .ef.tedit.text.tableWindow1table 1.13
                foreach {type wName index} [$text dump -$_type 1.0 end] {
                        if {$tableName == $wName} {
                                set result $index
                                break;
                        }
                }
                return $result;
        }
        
        

        # createTableConfig --
        #       create a dialog to configure the new table. (now only row, column number)
        #
        # Arguments:
        #   _text:              the given text widget to insert 
        #       tableName:      name of table configuration dialog
        #
        # Returns:
        #       None
        #
        proc createTableConfig {_text tableName} {
                # destroy old table window, make sure only one new extra table window
                catch {uplevel destroy $tableName}
                
                # build a new table window
                toplevel $tableName -padx 10 -pady 10
                # table title
                wm title $tableName [::msgcat::mc "Table Configuration"]
                #refresh the row/col number for new table
                set ::textEditorMain::tableRowNum 2
                set ::textEditorMain::tableColNum 2
                
                set rowNumArea [ttk::frame $tableName.row]
                set colNumArea [ttk::frame $tableName.col]
                
                set rowNumName $rowNumArea.rowLabel
                set colNumName $colNumArea.colLabel
                
                set rowNumBox $rowNumArea.rowSBox
                set colNumBox $colNumArea.colSBox
                
                set okButton $tableName.ok
                set cancelButton $tableName.cancel
                
                ttk::label $rowNumName -text [::msgcat::mc "rows: "] -width 20 -anchor w             
                ttk::label $colNumName -text [::msgcat::mc "columns: "] -width 20 -anchor w
                
                # from 0 to 100, textvariable
                ttk::spinbox $rowNumBox -from 0 -to 100 -wrap 0 -width 10 -font fixed -textvariable ::textEditorMain::tableRowNum
                ttk::spinbox $colNumBox -from 0 -to 100 -wrap 0 -width 10 -font fixed -textvariable ::textEditorMain::tableColNum
                
                ttk::button $okButton -text [::msgcat::mc OK] -command "::TEGUI::addTable $_text;::TEGUI::closeWindow $tableName"
                ttk::button $cancelButton -text [::msgcat::mc Cancel] -command "::TEGUI::closeWindow $tableName" 
                
                pack $rowNumName -side left 
                pack $rowNumBox -side right
                pack $rowNumArea -side top -pady 5 -fill x
                
                pack $colNumName -side left 
                pack $colNumBox -side right
                pack $colNumArea -side top -pady 5 -fill x
                
                pack $okButton -side left -pady 5 -padx 10
                pack $cancelButton -side right -pady 5 -padx 10
                
                # can't resize
                wm resizable $tableName 0 0
                
                #Attention: focus here! Dangerous!
                focus $tableName
                bind $tableName <Escape> {
                        #puts "escape"
                        ::TEGUI::closeWindow %W
                        break;
                }
        }
        
        #TODO: prepare for a new proc to configure the exists table
        # insertTable --
        #       Insert a new table windows into text widget.
        #
        # Arguments:
        #   text:       the given text widget  
        #
        # Returns:
        #       None
        #
        proc insertTable {text} {
                set insertIdx ""
                if { [catch {$text index insert} ] } {
                        set insertIdx [$text index end]
                } else {
                        set insertIdx  [$text index insert]
                }
                # get a new extra table windows
                # ?? need it ??
                
                ;# insert Table into Cell
                ;# AutoFit or Fixed width/height
                ;# Merge Cell (Merge ids) and split Cell (split ids)
                ;# resize
                
                createTableConfig $text $newWindows::win(TC)
        }
        
        # addTable --
        #       Insert a new table to the given text widget with table row/col numbers and new table name.
        #
        # Arguments:
        #   text:               the given text widget to insert  
        #
        # Returns:
        #       None
        #
        proc addTable {text} {
                if {$textEditorMain::tableRowNum == 0 && $textEditorMain::tableColNum == 0} {return;}
                set rowNum $textEditorMain::tableRowNum
                set colNum $textEditorMain::tableColNum
                #puts "rowNum: $rowNum - colNum: $colNum"
                ::textEditor::setIndexInsert $text; ::textEditor::setIndexInsertPress 1
                set newTableName [setNewTableName $text]
                ::textEditorMain::tableAddRecord $textEditorMain::tableNum $newTableName $rowNum $colNum
                setNewTable $text $newTableName $rowNum $colNum
                event generate $text <<Modified>>
        }
        
        
        # setNewTableName --
        #       Check and generate the new table name to insert into the given text widget.
        #
        # Arguments:
        #   text:               the given text widget to insert  
        #
        # Returns:
        #       None
        #
        proc setNewTableName {text} {
                set newTableName ""
                set newTableWindowName $text.tableWindow$textEditorMain::tableNum
                append newTableName $newTableWindowName "table"
                # check it whether already exists or not.
                while {[winfo exists $newTableName]} {
                        set newTableName ""
                        incr ::textEditorMain::tableNum
                        set newTableWindowName $text.tableWindow$textEditorMain::tableNum
                        append newTableName $newTableWindowName "table"
                }
                #puts "setNewTableName.newtableName: $newTableName"
                return $newTableName
        }
        
        
        
        # setNewTable --
        #       Generate the new table with package tablelist 
        #
        # Arguments:
        #   text:                       the given text widget to insert  
        #       newTableName:
        #       rowNum:
        #       colNum:
        #
        # Returns:
        #       None
        #
        proc setNewTable {text newTableName rowNum colNum {insertIdx insert}} {
                if {$rowNum == 0 && $colNum == 0} {return;}
                # modified the main text widget
                #set ::textEditorMain::textModiMsg 1
                                ::textEditor::setModi 1
                #event generate $text <<Modified>>
                if { ![string is integer $rowNum] || ![string is integer $colNum]} {
                        putsstderr "Warning: $rowNum or $colNum is not integer! "
                        return;
                }
                #puts "textEditorMain::coefficients(defaultCellWidth) - $textEditorMain::coefficients(defaultCellWidth)"
                #puts "textEditorMain::coefficients(width) $textEditorMain::coefficients(width)"
                set tblWidth [expr $colNum * ($textEditorMain::coefficients(defaultCellWidth) + 2)]
                set tblHeight [expr $rowNum * 4]
                
                #puts "rowNum: $textEditorMain::tableRowNum - colNum: $textEditorMain::tableColNum"
                #puts "tblWidth: $tblWidth - tblHeight: $tblHeight"
                #puts "newTableName: $newTableName"
                tablelist::tablelist $newTableName -width $tblWidth -height $tblHeight -cursor hand2 \
                                                                        -showlabel 0 -labelpady 0       -stretch all    \
                                                                        -showseparators 1 -resizablecolumns 1 -resizecursor hand2
                
                # column configure
                for {set col 0} {$col < $colNum} {incr col} {
                        $newTableName insertcolumns end 0 "col$col"
                        $newTableName columnconfigure $col -maxwidth 0
                        $newTableName columnconfigure $col -resizable 1
                        $newTableName columnconfigure $col -showlinenumbers 0
                        # stretch
                        $newTableName columnconfigure $col -stretchable 1
                        $newTableName columnconfigure $col -width $textEditorMain::coefficients(defaultCellWidth)
                        #set colWidthPx [$newTableName columnwidth $col]
                        #puts "setNewTable_colWidthPx: $colWidthPx"
                }
                
                # generate the cells 
                for {set row 0} {$row < $rowNum} {incr row} {
                        $newTableName insert end {}
                        for {set col 0} {$col < $colNum} {incr col} {
                                # edit cell windows
                                $newTableName cellconfigure $row,$col -window ::TEGUI::createFrameText -stretchwindow 1 
                         }
                }
                
                set childrenList [::TEUtils::winChildrenList $newTableName.body]
                set textList [::TEUtils::listSearchPart $childrenList ".w.text" 1]
                
                #puts $textList
                #set newCellIndex 0,0
                #puts [$newTableName windowpath end].text
                
                if {![::TEUtils::checkTableContents $rowNum $colNum $textList]} {
                        putsstderr "Attention: In $newTableName row/column number is not conformed with text widget number!"
                }
                # set the child text widget list into the table informations
                ::textEditorMain::setTableTextlist $newTableName $textList
                
                bind [$newTableName bodytag] <FocusIn> {
                        #puts "FocusIn %W"
                        dict set ::textTable::tableControl(status) currentSelectTable %W
                        break;
                }
                
                bind $newTableName <<TablelistSelect>> {
                        ::TEGUI::selectionChanged %W
                        break;
                }
                
        #       bind $newTableName <FocusIn> {
        #               #puts "FocusIn %W"
        #       }
                #TODO: POPUPMENU
                #bind
                $text window create $insertIdx -stretch 1 -window $newTableName
        }
        
        # insert the exists tables and tablecell contents(text widget) into the given text widget.
        proc insertTableTexts {textW tableName {insertIdx end}} {
                
                if {![::textEditorMain::hasTableWithName $tableName]} {putsstderr "Error: no valid Table with name $tableName!";return;}
                set rowNum [::textEditorMain::getTableRowNumByName $tableName]
                set colNum [::textEditorMain::getTableColNumByName $tableName]
        
                # test for number
                if {$rowNum == 0 | $colNum == 0} {putsstderr "Error: $tableName has no rows/cols!";return;}
                if { ![string is integer $rowNum] || ![string is integer $colNum]} {
                        putsstderr "Warning: $rowNum or $colNum is not integer! "
                        return;
                }
                
                setNewTable $textW $tableName $rowNum $colNum $insertIdx
                
                #TODO: INSERT TEXT CONTENTS TO THE CELL
                
                set cellCount [expr $rowNum * $colNum] 
                
                #???
                set textlist [::textEditorMain::getTableTextlistByName $tableName]
                
                array set widthList {}
                ::textEditorMain::resetTableEditWidth
                set count 0
                #row
                for {set y 0} {$y < $rowNum} {incr y} {
                        #col
                        for {set x 0} {$x < $colNum} {incr x} {
                                set tempCellBodyText [::textEditorMain::cellGet $tableName $y,$x 0]
                                set tempCellStyle [::textEditorMain::cellGet $tableName $y,$x 1]
                                set curCellText [lindex $textlist $count]
                                set cellWidth [::WordProcess::getAttr $tempCellStyle width]
                                #puts "insertTableTexts_cellWidth: $cellWidth"
                                if {$cellWidth != ""} {
                                        if {![info exists textEditorMain::tableEditWidth(curTable,$x)]} {
                                                set textEditorMain::tableEditWidth(curTable,$x) $cellWidth
                                                #set widthList($x) $cellWidth
                                        }
                                }
                                ::TEGUI::addText $curCellText $tempCellBodyText           
                                incr count
                                #update idletasks
                                ::TEGUI::autoModiHeight $curCellText
                        }
                }
                #update idletasks
                #resizeTable $tableName
                #update idletasks
                setTableWidthW $textW $tableName 1
                update idletasks
        }
        
        # addText --
        #    Add the fonts and tags with xmlTagArray into the text widget.
        #       bsp. text1 optionvalue1 text2 optionvalue2 ... textN optionvalueN
        # Arguments:
        #    text        the text widget to insert
        #        bodyText        the given contents
        #        insertIdx
        #
        # Result:
        #    None and no Return.
        #
        proc addText {text bodyText {insertIdx end}} {
                # option as "slant:italic", "underline:1"...
                # split :
                set pattern {([^:]*):(.*?)}
                #puts "addText.bodyText: $bodyText"
                # this tempOpValues is for saveing option values of tag in a same index.        
                set tempOpValues {}
                foreach {chars optionValues} $bodyText {
                        #puts "text: $text"
                        #puts "chars: $chars - optionValue: $optionValues"
                        set newfont {}
                        set tagList {}
        #FIXME:         element 1 in array TextStyle::tagorfont!
                        foreach {optionValue} $optionValues {
                                #puts "addText.optionValue: $optionValue"
                                set match ""
                                set option ""
                                set value ""
                                regexp $pattern $optionValue match option value
                                set value [::TEUtils::mapLinkXmltoText $value]
                                if {[info exists match] && $match != ""} {
                                        set optionValue [list $option $value]
                                } else {
                                        #CONSIDER: BETTER WAY!!!!
                                        set option $optionValue
                                }
                                
                                if {![info exists option]||$option == ""} {
                                        putsstderr "There is a empty option with $chars"
                                        continue;
                                }
                                if {![info exists TextStyle::tagorfont($option)]} {
                                        putsstderr "Warning: There is no such element $option in array TextStyle::tagorfont!"
                                        continue;
                                }
                                if {$TextStyle::tagorfont($option) == "font"} {
                                        #font
                                        set fontOption [list $option $value]
        #                               puts "fontOption: $fontOption"
                                        
                                        # add the fonts to the font list
                                        lappend newfont $fontOption
                                        
                                } elseif {$TextStyle::tagorfont($option) == "tag"} {
                                        #tag
                                        if {[catch {$text tag configure $optionValue -$option $value} result]} {
                                                        putsstderr "Error: $result"
                                                        continue;
                                        }
                                        lappend tagList $optionValue
                                } elseif {$TextStyle::tagorfont($option) == "window"} {
                                        lappend tagList "window"
                                } elseif {$TextStyle::tagorfont($option) == "link"} {
                                        if {$value == ""} {continue;}
                                        $text tag configure $optionValue -foreground blue
                                        $text tag configure $optionValue -underline 1
                                        ::TEGUI::bindURL $text $optionValue link
                                        lappend tagList $optionValue
                                } elseif {$TextStyle::tagorfont($option) == "anchor"} {
                                        if {$value == ""} {continue;}
                                        ::TEGUI::bindURL $text $optionValue anchor
                                        lappend tagList $optionValue
                } elseif {$TextStyle::tagorfont($option) == "p"} {
                                        if {$value == ""} {continue;}
                                        ::WordProcess::configureParagraph $text $value
                   lappend tagList $optionValue
                                } elseif {$TextStyle::tagorfont($option) == "preset"} {
                                        if {$value == ""} {continue;}
                                        ::WordProcess::configurePreset $text $value
                                        lappend tagList $optionValue
                                }
                                
                        }
                        
                        if {$newfont != {}} {
                                if {[::WordProcess::checkfontexits $newfont] == 0} {
                                        font create $newfont -family $::textEditorMain::defaultFont -size $::textEditorMain::defaultSize
                                        foreach newoptionvalue $newfont {
                                                set newoption [lindex $newoptionvalue 0]
                                                set newvalue [lindex $newoptionvalue 1]
        #                                       puts "newoptionvalue $newoptionvalue - newoption $newoption - newvalue $newvalue"
                                                if {[catch {font configure $newfont -$newoption $newvalue} result]} {
                                                        putsstderr "Error: font configure- $result"
                                                        continue;
                                                }
                                        }
                                }
                                $text tag configure $newfont -font $newfont
                                lappend tagList $newfont
                        }
                        
                        #puts "chars: $chars -- tagList: $tagList"
        #               puts "insertIdx: $insertIdx"
                        if {$tagList == "window"} {
        #                       insertTableTexts $text $chars insert
                                insertTableTexts $text $chars $insertIdx
                        } else {
                                set chars [::TEUtils::backToBracket $chars]
                                set tagList [::WordProcess::getPTag $text $tagList ]
                                #puts "addText: $text insert $insertIdx $chars $tagList"
                                $text insert $insertIdx $chars $tagList
                        }
                        
                        set insertIdx [$text index "insert"]
                        
                }
        #       puts [font names]
        }
        
        proc selectionChanged tbl { 
                 set rowList [$tbl curcellselection] 
                 if {[llength $rowList] == 0} { 
                         return 
                 } 
        
                 set row [lindex $rowList 0]
                #puts "rowList $rowList"
                #puts "The currently selected row is $row." 
        } 
        
        
        proc printClickedCell {w x y} {
                foreach {tbl x y} [tablelist::convEventFields $w $x $y] {}
                #puts "clicked on cell [$tbl containingcell $x $y]"
        }
        
        #------------------------------------------------------------------------------
        # createFrameText
        #
        # Creates a frame widget w to be embedded into the specified cell of the
        # tablelist widget tbl, as well as a child frame representing the size of the
        # file whose name is diplayed in the first column of the cell's row.
        #------------------------------------------------------------------------------
        proc createFrameText {tbl row col w} {
                variable ::TEGUI::popupmenuTE
                set frameWidth [expr $textEditorMain::coefficients(defaultCellWidth)]
                #puts "createFrameText_frameWith: $frameWidth"
                frame $w -height 69 -width $frameWidth -highlightthickness 1 -highlightbackground white -highlightcolor red
                text $w.text -wrap word -height 4 -width 12 -font TextEditorFont 
          
                pack $w.text -expand 1 -fill both
                pack $w -expand 1 -fill both
                
                # set master window
                pack propagate $w 0
        
                set popupmenuTE($w) $w.popupMenu
                menu $popupmenuTE($w) -tearoff 0 -postcommand "::TEGUI::checkPopupMenuItems cell $popupmenuTE($w) $w.text"
                $popupmenuTE($w) add command -label [::msgcat::mc Copy] -command "::TEGUI::copyEditor $w"
                $popupmenuTE($w) add command -label [::msgcat::mc Cut] -command "::TEGUI::cutEditor $w"
                $popupmenuTE($w) add command -label [::msgcat::mc Paste] -command "::TEGUI::pasteEditor $w"
                #$m add command -label "$tbl $row $col"
                
                bind $w.text <Button3-ButtonRelease> {
                        #puts "Button3-ButtonRelease %W"
                        ::TEGUI::showCellPopup %W %X %Y
                        break;
                }
                
                bindtags $w [lreplace [bindtags $w] 1 1 TablelistCellBody]
        
                
                bind $w <FocusIn> {
                        set ::textTable::tableControl(curCell) %W
                        #puts "FocusIn %W"
                
                }
                
                bind $w.text <FocusIn> {
                        # another in proc showEditorWindow
                        set ::textEditorMain::tableeditwinpath %W
                        ::searchTE::getLastText %W cell
                        #puts "FocusIn Cell tableeditwinpath $textEditorMain::tableeditwinpath"
                }
                
                bind $w.text <FocusOut> {
                        ::searchTE::getLastText "" cell
                }
                
                # copy funktion
                bind $w.text <<Copy>> {
                        set parent [winfo parent %W]
                        ::TEGUI::copyEditor $parent
                        break;
                }
                
                # paste funktion
                bind $w.text <<Paste>> {
                        set parent [winfo parent %W]
                        ::TEGUI::pasteEditor $parent
                        break;
                }
                
        
                bind $w.text <Control-Key-a> {
                        %W tag add sel 1.0 end;
                        break;
                }
                
                bind $w.text <KeyRelease> {
                        #set ::textEditorMain::textModiMsg 1
                        ::textEditor::setModi 1
                        #puts "::textEditorMain::textModiMsg: $textEditorMain::textModiMsg"
                        set parentText [::TEUtils::getWidgetParent [::TEUtils::getWidgetParent %W text] text]
                        #puts "parentText of %W is $parentText"
                        event generate $parentText <<Modified>>
                        ::TEGUI::autoModiHeight %W
                        break;
                }
        }
        
        proc showCellPopup {text x y} {
                #upvar $text name 
                #puts $text
                set parent [winfo parent $text]
                #puts $parent
                tk_popup $parent.popupMenu $x $y
        }
        
        
        #
        proc autoModiHeight {text} {
                variable ::textTable::tableControl
                #puts $tableControl(curCell)
        
        #       set curTableNum [$tbl curselection]                     ;# selection for focus, not append cursor/insert
        #       set curCellNum [$tbl curcellselection]          ;# 0,0 0,1
        #       puts [$tbl bodypath]                                            ;# .ef.tedit.text.tableWindow0table.body
        #       puts [$tbl bodytag]                                                     ;# body.ef.tedit.text.tableWindow0table
        #       puts [$tbl cornerlabelpath]                                     ;# .ef.tedit.text.tableWindow0table-corner.l
        #       puts [$tbl cornerpath]                                          ;#.ef.tedit.text.tableWindow0table-corner
        #       puts [$tbl windowpath 0,0] ;#.ef.tedit.text.tableWindow0table.body.frm_k0,0.w
                
                set tbl [::TEUtils::getWidgetParent $text "tablelist"]
        
                set cellIndex [getCellIndex $text]
                if {$cellIndex != ""} {
                        set index [split $cellIndex ","]
                        set row [lindex $index 0]
                }
                
                if {[info exists row]} {
                        # text real height
                        set textYPixels [$text count -ypixels 1.0 end]
                        #puts "autoModiHeight_textYPixels: $textYPixels"
                        set heightChange 1
                        if {[dict exists $tableControl(status) $tbl row $row oldHeight]} {
                                set oldHeight [dict get $tableControl(status) $tbl row $row oldHeight]
                                #puts "autoModiHeight_oldHeight: $oldHeight"
                                if {$textYPixels == $oldHeight} {
                                        set heightChange 0
                                }
                        } 
                        
                        ;#if {$heightChange && $textYPixels >= 64} {}
                        if {$textYPixels >= 64} {
                                #puts "heightChange"
                                cleanRowStatus $tbl $row
                                maxRowHeight $tbl $row
                                setRowHeight $tbl $row
        #                       update idletasks
                                resizeTable $tbl
                                 #update idletasks
                        }
                        update
                }
                
        }
        
        proc resizeTable {tableName} {
                variable ::textTable::tableControl
                if {$tableName == "" || ![winfo exists $tableName]} {return;}
        #       puts "geometry:"
        #       puts [winfo geometry $tableName]
        #       puts [$tableName index end]             ;# Returns the integer row index
        #       puts [winfo reqheight $tableName]
                
                set rowNum [$tableName index end]
                set count 0
                
        #       set curCell [$tableName windowpath $row,$col]
        #       set curText $curCell.text
        #       set curTableW [::TEUtils::getWidgetParent $curText TablelistWindow]
                
                for {set row 0} {$row < $rowNum} {incr row} {
                        if {[catch {set firstCell [$tableName windowpath $row,0]} ]} {
                                putsstderr "Error by maxRowHeight: There is no row $row in the table $tableName!"
                                continue;
                        }
                        set curText $firstCell.text
                        set curTableW [::TEUtils::getWidgetParent $curText TablelistWindow]
                        
                        if {![dict exists $tableControl(status) $tableName row $row maxVertical]} {
                                set curHeight [winfo height $curText]
                                #tk_messageBox -message H$curHeight
                        } else {
                                set curHeight [dict get $tableControl(status) $tableName row $row maxVertical]
                                #tk_messageBox -message H$curHeight
                        }
                        set curHeight [expr {$curHeight >= 67 ? $curHeight : 67}]
                        
        #               set curHeight [$curText cget -height]           
                        #puts "row $row: height $curHeight"
        #               set curHeight 0
                        
                        dict set tableControl(status) $tableName row $row oldHeight $curHeight
                        incr count $curHeight
                }
                
                #puts "resizeTable_Table_count: $count"
                set defaultLs [font metrics default -linespace]
                #tk_messageBox -message DEFAULTLS$defaultLs
                set tableHeight [expr {round( $count / $defaultLs)}]
                if {![dict exists $tableControl(status) $tableName fitHeight]} {
                        #puts "fitHeight"
                        dict set tableControl(status) $tableName fitHeight $count
                        $tableName configure -height $tableHeight 
                } else {
                        set oldHeight [dict get $tableControl(status) $tableName fitHeight]
        #               if {$oldHeight != $count} {
        #                       dict set tableControl(status) $tableName fitHeight $count
        #                       $tableName configure -height $tableHeight
        #               }
                        dict set tableControl(status) $tableName fitHeight $count
                        $tableName configure -height $tableHeight
                }
                #puts "tableHeight: $tableHeight"
        }
        
        
        
        
        proc cleanRowStatus {tableName row} {
                variable ::textTable::tableControl
                if {![dict exists $tableControl(status) $tableName row $row]} {return;}
                dict unset tableControl(status) $tableName row $row 
        }
        
        
        #               puts $text                      ;#.ef.tedit.text.tableWindow0table.body.frm_k0,0.w.text
        proc setRowHeight {tableName row} {
                variable ::textTable::tableControl
                if {[catch {$tableName windowpath $row,0} ]} {
                        putsstderr "Error by setRowHeight: There is no row $row in the table $tableName!"
                        return;
                }
                set colCount [$tableName columncount]
                set hasMax 0
                set height 64
                for {set col 0} {$col < $colCount} {incr col} {
                        set curCell [$tableName windowpath $row,$col]
                        set curText $curCell.text
                        set curTableW [::TEUtils::getWidgetParent $curText TablelistWindow]
                        if {!$hasMax && [dict exists $tableControl(status) $tableName row $row maxVertical]} {
                                set height [dict get $tableControl(status) $tableName row $row maxVertical]
                                set hasMax 1
                        }
                        set curTextHeight [dict get $tableControl(status) $tableName row $row textHeight]
                        set curCellHeight [expr $height + 5]
                        set curTableWHeight [expr $height + 5]
                        #puts "setRowHeight_curText_height: $curTextHeight - curCellHeight: $curCellHeight - curTableWHeight: $curTableWHeight"
                        $curText configure -height $curTextHeight
                        $curCell configure -height $curCellHeight
                        $curTableW configure -height $curTableWHeight
                        #update
                }
                set hasMax 0
        }
        
        # returns colNum:textLineHeight:textReqHeight
        proc maxRowHeight {tableName row} {
                variable ::textTable::tableControl
                
                if {[catch {$tableName windowpath $row,0} ]} {
                        putsstderr "Error by maxRowHeight: There is no row $row in the table $tableName!"
                        return 0;
                }
                set result ""
                set colCount [$tableName columncount]
                
                for {set col 0} {$col < $colCount} {incr col} {
                        set curCell [$tableName windowpath $row,$col]
                        set curText $curCell.text
                        # winfo reqheight
        #               set textReqHeight [winfo reqheight $curText]
        #               set textReqHeight [expr {$textReqHeight > $needHeight ? $textReqHeight : $needHeight}]
                        set textDisplaylines [$curText count -displaylines 1.0 end]
        #               set textHeight [$curText cget -height]
        #               set textRHeight [winfo height $curText]
                        # new Key word
                        set textYPx [$curText count -ypixels 1.0 end]
                        
                        #puts "maxRowHeight_textYPx: $curCell - $textYPx"
                        
                        if {$textYPx >=64} {
                                if {[dict exists $tableControl(status) $tableName row $row maxVertical]} {
                                        set oldMax [dict get $tableControl(status) $tableName row $row maxVertical]
                                        set textYPx [expr {$oldMax > $textYPx ? $oldMax : $textYPx}]
                                }
                                dict set tableControl(status) $tableName row $row maxVertical $textYPx
                                
                                if {[dict exists $tableControl(status) $tableName row $row textHeight]} {
                                        set oldHeight [dict get $tableControl(status) $tableName row $row textHeight]
                                        set textDisplaylines [expr {$oldHeight >= $textDisplaylines ? $oldHeight : $textDisplaylines}]
                                } else {
                                        set textDisplaylines [expr {$textDisplaylines >= 4 ? $textDisplaylines : 4}]
                                }
                                dict set tableControl(status) $tableName row $row textHeight $textDisplaylines
                        } 
                }
        }
        
        # return y(row),x(col) otherweise ""
        proc getCellIndex {inputName} {
                set linkPat {(\d+,\d+)}
                set match ""
                regexp $linkPat $inputName match 
                return $match
        }
        
        # type: table / row / column
        proc getTableInfos {cellName type} {
                if {$cellName == "" || $type == ""} {return;}
                set p1 {tableWindow(\d+)table(.*?)(\d+),(\d+)}
                set match ""
                if {[catch {regexp $p1 $cellName match k1 k2 k3 k4 } result ]} {
                        putsstderr $result
                        return;
                }
                #puts "$match $k1 $k2 $k3 $k4"
                if {$match == ""} {
                        putsstderr "Warning: There is no matching in the proc getTableInfos with parameters: $cellName"
                        return; 
                }
                switch -nocase -glob -- $type {
                        ta* {return $k1;}
                        r* {return $k3;}
                        col* {return $k4;}
                }
        }
        
        
        # closeWindow --
        #       Close the selected window.
        #
        # Arguments:
        #   w:  the given window  
        #
        # Returns:
        #       none.
        #
        proc closeWindow {w} {
                #puts "close $w"
                catch {
                        wm withdraw $w
                }
        }
        
        
        # extraFontsWindow --
        #       set a new windows for the specified functions.
        #
        # Arguments:
        #   text:       the given text widget  
        #       inputs: input informations for new windows
        #
        # Returns:
        #       outputs: output informations for main windows
        #
        proc extraFontsWindow {ef {windowtype "standard"}} {
                
                # destroy old window, make sure only one new extra window
                catch {uplevel destroy  $newWindows::win(W)}
                
                # build a new window
                toplevel $newWindows::win(W) -padx 10 -pady 10
                # title
                wm title $newWindows::win(W) [::msgcat::mc "Text Editor Options"]
                
                
                # row number for items in the new Window
                set rowNum 0
                
                # external fonts
                foreach item $TextStyle::externalFonts {
                        #puts $item
                        # area for a label and correspond comboBox
                        set area [ttk::frame $newWindows::win(W).$rowNum]
                        
                        set lblname "lbl_$item"
                        
                        # list/numList
                        set type [lindex [array get ::TextStyle::types $item] 1]
                        set values $TextStyle::tagNames($item)
                        set width 10
                        set comboboxname $area.$item
                        set labelname $area.$lblname
                        set comboeditable true
                        set exportselection false
                        
                        set values [concat "(standard)" $values]
                        
                        ComboBox $comboboxname                                                                  \
                        -editable $comboeditable                                                                                \
                        -exportselection $exportselection                       \
                        -bd 2                                                                                                   \
                        -width $width                                                                                   \
                        -values $values                                                                                 \
                        -textvariable   ""                                                                      \
                        -modifycmd "::combo::getComboCurrent $comboboxname $ef.tedit.text $item $windowtype"     
                        
                        DynamicHelp::register $comboboxname balloon [::msgcat::mc $item]
                        
                        # combobox editable
                        if {$type == "numList"} {
                                combo::comboBoxEnable $comboboxname 0 $windowtype
                        }
                        
                        ttk::label $labelname -text [::msgcat::mc $item] ;# combobox label
                        
                        #layout
                        pack $labelname -side left 
                        pack $comboboxname -side right
                        pack $area -side top -pady 5 -fill x
                        
                        ::TextStyle::addButton $ef $item $comboboxname
                        # row number increment for next Items
                        incr rowNum
                }
                # can't resize
                wm resizable $newWindows::win(W) 0 0
        
                wm protocol $newWindows::win(W) WM_DELETE_WINDOW "::TEGUI::extraFontsWindowdelete $ef"

        }
        
        # extraFontsWindowdelete --
        #       delete windows for the specified functions.
        #
        proc extraFontsWindowdelete {ef} {
          #puts "externalFonts $TextStyle::externalFonts "
          ::TextStyle::removeButtons $ef $TextStyle::externalFonts 
          #parray ::TextStyle::allButtons; 
          catch { destroy  $newWindows::win(W) } 
        }
        
        # resetModifiedFlag --
        #       reset modified flag of the given widget
        #
        #Arguments:
        #       w: the given widget 
        #       
        #Returns:
        #       Nothing
        #
        #Side effects
        #       set ::textEditorMain::textModiMsg 0
        #
        proc resetModifiedFlag {w} {
                $w edit modified false
                ::textEditor::setModi 0
        }
        
        
        # closeTextWidget --
        #       if the given text widget have not been yet saved, 
        #       give the user a option to save it.
        #
        #Arguments:
        #       text: the given text widget 
        #
        #Returns:
        #       Nothing
        #
        #Side effects
        #       None
        #
        proc closeTextWidget {text ef} {
        #       puts "textEditorMain::textFeddbackMsg $textEditorMain::textModiMsg"
                if {[::textEditor::getModi]} {
                        set selected [tk_dialog .savePop "Save Changes" \
                                        "Do you want to save modified text?" \
                                        questhead 1 "   Yes   " "   No   "]
        #               puts "reply: $selected"
                        if {!$selected} {       
                                ::TEGUI::SaveFile $ef.tedit 1
                        }
                } 
                exit
        }
        
        
        # bindKeyEvent --
        #       bind Event with Mouse or Keyboard click 
        proc bindKeyEvent {ef} {
                #set evtList {<ButtonRelease-1> <Key-Left> <Key-Right> <Key-Down> <Key-Up>}
                # xin: 6.04.2014
                set evtList {<ButtonRelease-1> <ButtonPress-1> <Key-Left> <Key-Right> <Key-Down> <Key-Up>}
                foreach evt $evtList {
                        # proc as transit station
                        #bind $ef.tedit.text $evt " keyEvent $ef $evt"
                        bind $ef.tedit.text $evt [list ::TextStyle::keyEvent $ef $evt]                        
                }  
                set evtList {<ButtonPress> <KeyPress>}
                foreach evt $evtList {
                        bind $ef.tedit.text $evt "::textEditor::setIndexInsert $ef.tedit.text; ::textEditor::setIndexInsertPress 1"                       
                }  
                ::textEditor::setIndexInsert $ef.tedit.text; ::textEditor::setIndexInsertPress 0
        }      
        
        # modifiedEvent --
        #       main modified functions for texteditor, call this function if <<Modified>> is to be overwritten
        proc modifiedEvent {t} {
          #puts "Modified"
          ::textEditor::setModi 1         
          checkPInsert $t         
          $t edit modified false                       
        }
        
        # checkPInsert --
        #       checks completenes of ptag after insert
        proc checkPInsert {t} {
          set indexinsert [$t index insert]
          set indexinsertbefore [$t index insert-1c]
          #puts "indexinsert $indexinsert indexinsertbefore $indexinsertbefore"
          if {![::textEditor::getIndexInsertPress]} return;
          set indexinsertpress [::textEditor::getIndexInsert]
          set indexinsertlinestart [$t index "insert linestart"]
          set indexinsertlineend [$t index "insert lineend"]
          #puts "indexinsertpress $indexinsertpress indexinsertlinestart $indexinsertlinestart indexinsertlineend $indexinsertlineend"
          
          set do 0
          if {$indexinsert == $indexinsertlineend} {
             set do 1
             set indextagnames [$t index insert-2c]
             set indextagnameadd [$t index "insert lineend -1c"]
          }
          
          if {$indexinsertbefore == $indexinsertlinestart} {
            set do 1
            set indextagnames $indexinsert
            set indextagnameadd $indexinsertlinestart
          }         
          
          #puts "DO $do "
          # normal insert
          if {$do} {
            set tagnames [$t tag names $indextagnames];  
            #puts "tagnames $tagnames index $indextagnames"
            foreach tagname $tagnames {
              if {[lindex $tagname 0 ] == "p"} {
                $t tag add $tagname $indextagnameadd
              }
            }
          }
          
          
          # paste insert
          if {$indexinsertpress != $indexinsertbefore} {
            set tagnames {}
            if  {$indexinsert != $indexinsertlinestart} {
              set tagnames [concat $tagnames [$t tag names "$indexinsert -1c" ] ]
            }
            set tagnames [concat $tagnames [$t tag names $indexinsert ] [$t tag names "$indexinsertpress -1c"] [$t tag names "$indexinsert +1c"] ];  
                    
            #puts "tagnames $tagnames [$t tag names "$indexinsert -1c" ] - [$t tag names $indexinsert ] - [$t tag names "$indexinsertpress -1c"] - [$t tag names "$indexinsert +1c"]"
            set action add
            set tagnameadd {}
            foreach tagname $tagnames {
              if {[lindex $tagname 0 ] == "p"} {
                #puts "$action $tagname was $indexinsertpress $indexinsert to $indexinsertlinestart $indexinsertlineend"
                                                     
                # prevent doubled action
                if {$tagnameadd != $tagname} {
                 $t tag $action $tagname $indexinsertlinestart $indexinsertlineend 
                 $t tag $action $tagname "$indexinsertpress linestart" $indexinsertlineend 
                 #$t tag $action $tagname "$indexinsertpress linestart" "$indexinsertpress lineend" 
                                                            #not only indexinsert, if you replace a selection you can get additional text after insertmark
                }

                if {$action == "add"} {
                  set tagnameadd $tagname
                }
                set action remove; # only one tag in line
              }
            }
          }
          
          ::textEditor::setIndexInsert $t; ::textEditor::setIndexInsertPress 0
          $t edit modified false
        
        }
        
        # checkP --
        #       checks completenes of ptag not used
        proc checkP {t} {
          set tagnames [$t tag names];          
          foreach tagname $tagnames {
             if {[lindex $tagname 0 ] == "p"} {
              
              set tagprevrange [$t tag prevrange $tagname insert]
              set tagnextrange [$t tag prevrange $tagname insert]
              puts "tagranges $tagname ranges $tagprevrange $tagnextrange"
              set startrange [lindex $tagprevrange 0]
              set endrange [lindex $tagprevrange 0]
              set startline [$t index "insert linestart"]
              set endline [$t index "insert lineend"]
              if {$startrange != $startline || $endrange != $endline } {
                $t tag add $tagname $startline $endline  
              }
              
            }
            
            
          }
        }
        
        
        #setColor --
        #       set the color of the given Text
        #Arguments:
        #       text : the text editor
        #       option: the selected option (e.g. background, foreground..)
        #       value: the value of color with RGB format (e.g. #000000)
        #Returns:
        #       Nothing
        
        proc setColor {text option {color ""} {windowtype standard}} {
                #puts "setColor: $text $option $color $windowtype"
                if {$color== "" || $color == "morecolor"} {
                        set color [tk_chooseColor -parent $text]
                        if {$color == ""} {return;}
                        ::WordProcess::ChangeStyleW $text $option $color 0 $windowtype
                } elseif {$color == "null"} {
                        ::WordProcess::ChangeStyleW $text $option "null" 0 $windowtype
                } elseif {$color != ""} {
                        ::WordProcess::ChangeStyleW $text $option $color 0 $windowtype
                }
        }
        
        
        proc invokeColorDialog {text option {windowtype "standard"}} {
                catch {uplevel destroy $newWindows::win(CE)}
                # build a new table window
                toplevel $newWindows::win(CE) -padx 10 -pady 10
                # table title
                wm title $newWindows::win(CE) [::msgcat::mc "Color Edit Configuration"]
                
                #set colorList {red green blue black cyan magenta yellow grey orange turquoise violet pink}
                set colorList {#FF0000 #00FF00 #0000FF #000000 #00FFFF #FF00FF #FFFF00 #808080 #FF8000 #0080FF #8000FF #FF0080}
                
                set cellNum 0
                set rowNum -1
                set colorSelectArea [canvas $newWindows::win(CE).colorArea -width 210 -height 150]
                set colorMargin 10      ;#px
                set colorWidth 40       ;#px
                set colorHeight 40      ;#px
                foreach buttonColor $colorList {
                        # 3 colorbuttons in a row
                        if {[expr $cellNum % 4] == 0} {
                                incr rowNum 
                                set cellNum 0
                        }
                        set x1 [expr $colorMargin + $cellNum * ( $colorWidth + $colorMargin )]
                        set y1 [expr $colorMargin + $rowNum * ( $colorHeight + $colorMargin )]
                        set x2 [expr $colorMargin + $colorWidth + $cellNum * ( $colorMargin + $colorWidth )]
                        set y2 [expr $colorMargin + $colorHeight + $rowNum * ( $colorMargin + $colorHeight )]
                        #puts "x1: $x1 - y1: $y1 --- x2: $x2 - y2: $y2"
                        set colorArea [$colorSelectArea create rect $x1 $y1 $x2 $y2 -fill $buttonColor \
                                                 -activeoutline white]
                        $colorSelectArea bind $colorArea <ButtonRelease-1> "::TEGUI::setColor $text $option $buttonColor $windowtype; ::TEGUI::closeWindow $newWindows::win(CE)"
                        
                        incr cellNum
                }
                set morecolor "morecolor"
                set removeColorButton $newWindows::win(CE).removeColorbutton
                set moreColorButton $newWindows::win(CE).moreColorButton
                ttk::button $removeColorButton -text [::msgcat::mc "Remove color"] -command "::WordProcess::ChangeStyleW $text $option null 0 $windowtype; ::TEGUI::closeWindow $newWindows::win(CE)"
                ttk::button $moreColorButton -text [::msgcat::mc "More colors"] -command "::TEGUI::closeWindow $newWindows::win(CE); ::TEGUI::setColor $text $option $morecolor $windowtype;"
        
                pack $colorSelectArea -side top -pady 5 -fill x
                pack $removeColorButton -side top -pady 5 -fill x
                pack $moreColorButton -side top -pady 5 -fill x
                
                
                # can't resize
                wm resizable $newWindows::win(CE) 0 0
                
                #Attention: focus here! Dangerous!
                focus $newWindows::win(CE)
                bind $newWindows::win(CE) <Escape> {
                        #puts "escape"
                        ::TEGUI::closeWindow %W
                        break;
                }
        }
        
        proc bindURL {text tagName tagtype} {
                $text tag bind $tagName <Enter> [list ::TEGUI::tagEvent $text [escapeBind $tagName ] $tagtype enter ::TEGUI::urlEnter]
                $text tag bind $tagName <Leave> [list ::TEGUI::tagEvent $text [escapeBind $tagName ] $tagtype leave ::TEGUI::urlLeave]
                $text tag bind $tagName <Motion> [list ::TEGUI::moveURL $text [escapeBind $tagName ] $tagtype ]
                if {$tagtype == "link"} {
                $text tag bind $tagName <Control-ButtonPress-1> { ::TEGUI::URLTrigger %W; break; }
        }
        }
        
        #  Escape % for bind https://www.tcl.tk/man/tcl8.4/TkCmd/bind.htm#M25
        proc escapeBind {substring} {
          return [string map {% %%} $substring]
        }
        
                
        #TODO
        proc URLTrigger {text} {
                variable ::textEditorMain::urlData
                if {![info exists urlData($text:link)] || $urlData($text:link) == ""} { return; }
                set link $urlData($text:link)
                if {$link == ""} {return}
                set type [lindex $link 0]
                set linkValue [lindex $link 1]
                if {[info exists urlData($text:url)] && $urlData($text:url) != ""} {
                        # avoid whitespace
                        set url [list $urlData($text:url)]
                } elseif {$linkValue != ""} {
                        # avoid whitespace
                        set url [list [concat $linkValue]]
                } else {
                        putsstderr "Warning: There is no value in the $type!"
                        return;
                }
                
                #puts "url: $url"
                if {$url != ""} {
                        uplevel 1 ::TEGUI::urlCmd $url $text
                }
        }
        
        
        proc moveURL {text link tagtype} {
                variable ::textEditorMain::urlData
                if {[info exists urlData($text:range)]} {
                        set oldRange $urlData($text:range)
                } else {
                        set oldRange ""
                }
                set url [::TEGUI::getCurURL $text $link]
                if {[info exists urlData($text:range)]} {
                        set newRange $urlData($text:range)
                } else {
                        set newRange ""
                }
                if {$newRange != $oldRange && $newRange != ""} {
                        set urlData($text:range) $oldRange
                        ::TEGUI::tagEvent $text $link $tagtype leave ::TEGUI::urlLeave
                        set urlData($text:range) $newRange
                        ::TEGUI::tagEvent $text $link $tagtype enter ::TEGUI::urlEnter
                }
        }
        
        
        proc urlEnter {text} {
                global notextEditorstandalone
                variable ::textEditorMain::urlData
                catch {uplevel destroy $urlData($text:label)}
        #       $text tag configure URL -underline 1
                set urlData($text:label) .textURLLabel
                set labelText $urlData($text:link)
                if {[info exists notextEditorstandalone]} {
                   set labelText [openWebLinkLabelText $urlData($text:link)]
                }
                destroy $urlData($text:label)
                ttk::label $urlData($text:label) -text $labelText -relief raised
                lassign [winfo pointerxy .] x y
                set win [winfo containing $x $y]
                foreach {x y} [::TEUtils::getMousePosition] break
                place $urlData($text:label) -x [expr $x + 10] -y [expr $y + 10]
        }
        

        
        
        proc urlLeave {text} {
                variable ::textEditorMain::urlData
        #       $text tag configure URL -underline 0 
        }
        
        proc urlCmd {url text} {
                global notextEditorstandalone
                variable ::textEditorMain::urlData
                #CONSIDER: URL FORM FRAGEN:
                
                #
                if {[string first "#" $url ] == 0} {
                   set anchorname [string trimleft $url "#" ]
                   set tagname "anchor $anchorname"
                   $text see [lindex [$text tag nextrange $tagname insert ] 0]
                } else {
                  if {[info exists notextEditorstandalone]} {
                     openWebLink $url
                     catch {uplevel destroy $urlData($text:label)}
                  } else {
                     tk_messageBox -message $url
                  }
                }
        }
        
        proc tagEvent {text link tagtype event cmd} {
                variable ::textEditorMain::urlData
                set cursor ""
                #linkKey: link/anchor, target: e.g. www.uni-trier.de
                #foreach {linkKey linkTarget} $link break;
                
                switch $event {
                        enter {
                                set cursor hand2
                                set urlData($text:url) [::TEGUI::getCurURL $text $link]
                                set urlData($text:link) $link
                                #URL TAG
                                if {[info exists urlData($text:range)] && $urlData($text:range) != ""} {
                                        eval $text tag add URL $urlData($text:range)
                                }
                        }
                        leave {
                                set url $urlData($text:url)
                                set cursor ""
                                set urlData($text:url) ""
                                set urlData($text:link) ""
                                catch {uplevel destroy $urlData($text:label)}
                                if {[info exists urlData($text:range)] && $urlData($text:range) != ""} {
                                        eval $text tag remove URL $urlData($text:range)
                                }
                        }
                }
                
                #set/reset cursor
                if {$tagtype == "link"} {
                $text config -cursor $cursor
                }
                #invoke corresponding callback
                set urlData($text:$event) $cmd
                if {$cmd != ""} {
                        uplevel 1 $cmd $text
                }
        }
        
        # link: link/anchor
        proc getCurURL {text link} {
                variable ::textEditorMain::urlData
                #get current index
                set current [$text index current]
                foreach {linkKey linkTarget} $link break;
                
                foreach {start end} [$text tag nextrange $link $current] {break;}
                
                if {![info exists start] || $current < $start} {
                        foreach {start end} [$text tag prevrange $link $current] {break;}
                        if {![info exists start] || $current >= $end} {return}
                }
                
                set urlData($text:range) [list $start $end]
                set urlData($text:link) $link
                # get text content in the range
                return $linkTarget
        }
        
        
        # NewFile --
        #    Clean the input window (start with a new file).
        #
        # Arguments:
        #    name      Name of the input window
        # Result:
        #    None
        #
        proc NewFile {name ef} {
                # if text is modified, ask user whether to store it or not.
                if {[::textEditor::getModi]} {
                        set selected [tk_dialog .savePop "Save Changes" \
                                        "Do you want to save modified text?" \
                                        questhead 1 "   Yes   " "   No   "]
        #               puts "reply: $selected"
                        if {!$selected} {       
                                ::TEGUI::SaveFile $ef.tedit 0
                        }
                }
        
                $name.text delete 1.0 end
                # no modify at the beginning
                #set ::textEditorMain::textModiMsg 0
                ::textEditor::setModi 1
        }
        
        # OpenFile --
        #    Clean the input window and load in an existing file
        # Arguments:
        #    name      Name of the input window
        #    askname   Ask for a file name or not
        # Result:
        #    None
        #
        proc OpenFile {name askname ef} {
                # clear the text widget
                ::TEGUI::NewFile $name $ef
                
                if {$askname} {
                        set newfile \
                                [tk_getOpenFile -defaultextension $TEUtils::fileForm::defaultextension \
                                -filetypes $TEUtils::fileForm::filetypes \
                                -title "Select an existing file" ]
                } else {
                        set newfile ""
                }
                
                if {$newfile != "" } {
                        set srcfile $newfile
                        $name.text delete 1.0 end
                        
                        # refresh the textEditor status
                        ::TEUtils::refreshStatus
                        ::WordProcess::getContents $name $srcfile
                }
        }
        
        
        
        # SaveFile --
        #    Save the current file. There a 3 features: 
        #        1. parameters (0, 1, 2)
        #        2. save path compare (::textEditorMain::savePath)
        #        3. check the modify in the text widget. (::textEditorMain::textModiMsg)
        #       
        # Arguments:
        #    name      Name of the input window
        #    askname   Ask for a new file name or not (0:Save/1:Save as .../2:Save as xml)
        # Result:
        #    None
        #
        proc SaveFile {name askname {startIdx 1.0} {endIdx end}} {
        #       global filetypes
                set newfile ""
        #       puts "name $name"
                #change flag if no change in the textwidget and only "Save"-function
                if {$askname==0 && [::textEditor::getModi] == 0} {return}
                
        #       puts "@savePath: $textEditorMain::savePath"
                if {$textEditorMain::savePath == "" ||  ![file exists $textEditorMain::savePath]} {
                        if {$askname == "0"} {
                                set askname 1
                        }
                }
                
                if {$askname == "1" 
                        || $askname == "2" 
                        || $askname == "3" 
                        || $askname == "4"
                        || $askname == "5"
                        || $askname == "6"
                } {
                        
                        if {$askname == "2"} {
                                set ftypes {{{XML Files}        {.xml}        }}
                        } elseif {$askname == "3"} {
                                set ftypes {{{HTML Files}        {.html}        }}
                        } elseif {$askname == "5"} {
                                set ftypes {{{TEX Files}        {.tex}        }}
                        } elseif {$askname == "6"} {
                        set ftypes {{{RTF Files}        {.rtf}        }}
                        } else {
                                set ftypes $TEUtils::fileForm::filetypes
                        }
                        
                        if {$askname == "4"} {
                                set ::textEditorMain::savePath [::TEGUI::setTempFile "TECopyToClipboard.html"]
                        } else {
                                set newfile \
                                        [tk_getSaveFile -defaultextension $TEUtils::fileForm::defaultextension \
                                        -filetypes $ftypes \
                                        -title "Type a new name" ]
                                set ::textEditorMain::savePath $newfile
                        }
                } 
                
                if {$textEditorMain::savePath != ""} {
                        set srcfile $textEditorMain::savePath
                        #puts $srcfile
                        set fileSuffix [::TEUtils::getFileSuffix $textEditorMain::savePath]
                        if {$fileSuffix == "xml"} {
                                ::WordProcess::SaveContentsToXMLW $name $srcfile xml
                        } elseif { $fileSuffix == "html"  } {
                                if {$askname == "3"} {
                                        ::WordProcess::SaveContentsToXMLW $name $srcfile html
                                
                                } elseif {$askname == "4"} {
                                        #clipboard
                                        ::WordProcess::saveSelToFile $name $srcfile $startIdx $endIdx
                                }
                        } elseif {$fileSuffix == "tex"} {
                                ::WordProcess::SaveContentsToXMLW $name $srcfile tex
                        } elseif {$fileSuffix == "rtf"} {
                                ::WordProcess::SaveContentsToXMLW $name $srcfile rtf
                        } else {
                                ::WordProcess::SaveContents $name $srcfile
                        }
                        
                        # after saved, reset modified flag
                        ::TEGUI::resetModifiedFlag $name.text
                }
        }
        
        
        # setTempFile --
        #    Set a new file with the given name in the temp folder.
        # Arguments:
        #    filename      the given file name
        #
        # Attention:
        #       Require package fileutil (fud inclusiv)
        # Returns:
        #    Returns the file name
        #
        proc setTempFile {filename} {
                set tempDir [fileutil::tempdir]
        #       puts "tempDir: $tempDir"
                if {$filename==""} {set filename "copyTemp.xml"}
                set fileName [file join $tempDir $filename]
                set fexist [file exists $fileName]
                # set new file
                if {$fexist} {file delete -force $fileName}
                # add the file to the TEMP folder
                fileutil::touch $fileName
                return $fileName
        }
        
}


#****** textEditor/WordProcess
# NAME
#  TEGUI
# NAMESPACE
#  namespace
# DESCRIPTION
#  word and text processes
# AUTHOR
#  Xin Zhou
# CREATION DATE 
#  24.06.2014
# SYNOPSIS
namespace eval WordProcess {
        # PARAMETERS
        #       None
        # RETURN VALUE 
        #       None
        # NOTES
        namespace export *
# USES
#  Variables:
#    NONE
#  Procedures:
#        NONE
#****
        # getHtmlBody --
        #       Returns the <body> contents from the whole html contents.
        #
        # Arguments:
        #       htmlContents:   the given HTML contents
        #       
        # Returns:
        #       Returns body contents of the given HTML contents.
        #
        # type : body / head
        proc getHtml {htmlContents type} {
                # set temp contents
                set tmpContents $htmlContents
                
                # make sure all temp contents is with low case
                set tmpContents [string tolower $tmpContents]
                
                # get start position of <body>
                set startIndex [string first "\<$type" $tmpContents]
                
                # get end position of <body>
                set endIndex [string last "<\/$type\>" $tmpContents]
        
                # set temp contents empty that save the cache
                set tempContents ""
                
                
                if {$startIndex >= 0} {
                        set htmlContents [ string range $htmlContents $startIndex $endIndex+6 ]
                        set htmlContents [ ::TEUtils::changeBracket $htmlContents ]
                        #puts "getHTMLBody.htmlContent: $htmlContents"
                } else {
                        set tempContents ""
                        #putsstderr "There is no \<$type\> Element in this input contents!"
                }
                return $htmlContents
        }
        
        
        
        # parseHtmlToXmlContents --
        #    Parse the given htmlcontents and change it to the xml form
        # Arguments:
        #    htmlContents        
        #
        # Result:
        #    None
        #
        proc parseHtmlToXmlContents {htmlContents} {
                variable ::textEditorMain::xmlData
                if {$htmlContents == ""} {return;}
                ::TEUtils::refreshStatus
                set xmlBody ""
                
                ::TEDB::resetPreset
                
                set htmlHeadValue [getHtml $htmlContents head]
                #puts "parseHtmlToXmlContents.htmlHeadValue: $htmlHeadValue"
                htmlparse::parse -cmd ::WordProcess::parseHtmltoXml $htmlHeadValue
                
                
                set htmlBodyValue [getHtml $htmlContents body]
                # clean the html content
                #puts "parseHtmlToXmlContents.htmlContents: $htmlContents"
                set htmlBodyValue [::WordProcess::cleanFragment $htmlBodyValue]
                        
                #puts "parseHtmlToXmlContents.htmlHeadValue: $htmlHeadValue"
                #puts "parseHtmlToXmlContents.htmlBodyValue: $htmlBodyValue"
                
                #NTODO: copy bug
                htmlparse::parse -cmd ::WordProcess::parseHtmltoXml $htmlBodyValue
                
                #puts "parseHtmlToXmlContents.mainBodyText: $xmlData(main,bodyText)"
                set innerBody [::WordProcess::changeBodyTextToTei $xmlData(main,bodyText) TextStyle::tagAttrMap]
                
                append xmlBody "<body><list><item n='main'>" $innerBody "</item></list></body>"
                
                append xmlContent $xmlData(main,headText) $xmlBody
                #puts "parseHtmlToXmlContents.xmlBody: $xmlBody"
                ::TEUtils::refreshStatus
                return $xmlContent
        }
        
        
        # getContents --
        #    Get the contents of the given file and add it with proper tags
        #    to the edit window
        # Arguments:
        #    name        Name of the edit window
        #    filename    Name of the file to read
        # Result:
        #    None
        #
        proc getContents {name filename} {
                
                # get input file
                set infile [open $filename "r"]
                
                fconfigure $infile -encoding binary
                # read buffer
                set inBuffer ""
                # text Contents
                set contents ""
                
                if {$filename == "" | ![file exists $filename]} {return "";}
                # get the type of input file (e.g. "txt", "xml", "png", "html")
                set fileType [::TEUtils::getFileSuffix $filename]
        
                while {![eof $infile]} {
                        # CONSIDER: there are no ram limit?
                        set contents [read $infile]
                }
                
                #check is empty or not
                if {$contents != ""} {
                        set contents [::TEUtils::UTF8FullCodes $contents 1]
                        # determine whether this given file is an .xml file
                        if {$fileType=="xml" | $fileType=="tei"} {
                                #puts "xml file"
                                ::WordProcess::parseXmlContents $name $contents
                        } elseif {$fileType=="html" |$fileType=="xhtml"} {
                                #TODO: change html to xml
                                set contents [::WordProcess::parseHtmlToXmlContents $contents]
                                # parse xml to text
                                ::WordProcess::parseXmlContents $name $contents
                        }
                } else {
                        puts "There is no contents in file $filename!"
                }
                close $infile
        }
        
        # name: table name
        # type: insert, delete
        # rowCol: row col
        # index: 
        proc changeTableContents {name type rowCol index} {
                if {$name == "" || $type == "" || $rowCol == "" || $index == ""} {return;}
                
                set contents ""
                set temp ""
                # check tableInfos
                
                #parray ::textEditorMain::tableInfos
                
                if {![info exists ::textEditorMain::tableInfos(textlist,$name)]} {
                        putsstderr "Attention: there is no $name in the tableInfos!";
                        return;
                }
                
                set textlist $textEditorMain::tableInfos(textlist,$name)
                set rowNum $textEditorMain::tableInfos(rowNum,$name)
                set colNum $textEditorMain::tableInfos(colNum,$name)
                
                if {[expr $rowNum*$colNum] != [llength $textlist]} {
                        putsstderr "row/column is not conform with textlist length!
                                                        \nrow: $rowNum - col: $colNum \ntextlist: $textlist"
                }
                
                
        #       set topBoundary 0
        #       set leftBoundary 0
                set rightBoundary 0
                set bottomBoundary 0
                
                if {$rowCol == "col" && $index == $colNum} {
                        set rightBoundary 1
                }
                if {$rowCol == "row" && $index == $rowNum} {
                        set bottomBoundary 1
                }
                
                set pos 0
                set addEmptyRow 0
                # row,col -> rows : cells
                for {set row 0} {$row < $rowNum} {incr row} {
                        # row to process
                        if {$rowCol == "row" && $row == $index && !$addEmptyRow} {
                                if {$type == "delete" } {
                                        incr pos $colNum
        
                                } elseif {$type == "insert"} {
                                        #CONSIDER:
                                        append temp "<row>"
                                        for {set col 0} {$col < $colNum} {incr col} {
                                                append temp "<cell> </cell>"
                                        }
                                        append temp "</row>"
                                        incr row -1
                                        set addEmptyRow 1
                                        
                                }
                        } else {
                                #normal
                                append temp "<row>"
                                for {set col 0} {$col < $colNum} {incr col} {
                                        if {$rowCol == "col" && $col == $index} {
                                                if {$type == "delete"} {
                                                        incr pos
                                                } elseif {$type == "insert"} {
                                                        append temp "<cell> </cell>"
                                                        append temp "<cell>"
                                                        set cellText [lindex $textlist $pos]
                                                        append temp [::WordProcess::getXMLBody $cellText ""]
                                                        append temp "</cell>"
                                                        incr pos
                                                }                                       
                                        } else {
                                        append temp "<cell>"
                                        set cellText [lindex $textlist $pos]
                                        append temp [::WordProcess::getXMLBody $cellText ""]
                                        append temp "</cell>"
                                        incr pos
                                        }
                                }
                                if {$rightBoundary} {
                                        append temp "<cell> </cell>"
                                }
                                append temp "</row>"
                        }
                }
                
                if {$bottomBoundary} {
                        append temp "<row>"
                        for {set col 0} {$col < $colNum} {incr col} {
                                append temp "<cell> </cell>"
                        }
                        append temp "</row>"
                }
                
                
                # control the row/column number
                if {$rowCol == "row" && $type == "delete"} {
                        incr rowNum -1
                } elseif {$rowCol == "row" && $type == "insert"} {
                        incr rowNum 1
                }
                if {$rowCol == "col" && $type == "delete"} {
                        incr colNum -1
                } elseif {$rowCol == "col" && $type == "insert"} {
                        incr colNum 1
                }
                
                append contents "<body><list><item n='main'>"
                append contents "<table rows=\"$rowNum\" cols=\"$colNum\">"
                append contents $temp
                append contents "<\/table>"
                append contents "</item></list></body>"
                
                return $contents
        }
        
        proc insertTimestamp {text} {
                variable ::textEditorMain::timestamp
                variable ::textEditorMain::editorText
                variable ::searchTE::lastTextWidget
                ::textEditorMain::updateTimestamp
          
                catch {$text tag remove sel sel.first sel.last}
                
                # stamp content
                set insertTStamp "$timestamp(name),$timestamp(currentDate),$timestamp(currentTime)"
                set insertLen [string length $insertTStamp]               
                insertText $text insert $insertTStamp [list "sel"]
                ChangeTagW $text timestamp
        
          
        }

        proc insertText {textw mark textstring newTagList} {
          #Attention: winfo and info have not same function.
          if {[info exists textEditorMain::tableeditwinpath ] && $textEditorMain::tableeditwinpath != {}} {
                  #puts "ChangeTagW-tableeditwinpath: $text"
                  set textw $textEditorMain::tableeditwinpath              
          }
          set tagList [getPTag $textw {} ]
          if {$newTagList != {}} {
            set tagList [concat $tagList $newTagList ]
          }
          $textw insert $mark $textstring $tagList
        }
  
        proc getPTag {text tagList} {
          set tagListNew $tagList
          set tagNames [$text tag names insert]
          foreach tagName $tagNames {
            set tagtype [lindex [lindex $tagName 0 ] 0 ]
            if {[info exists TextStyle::tagorfont($tagtype)]} {
            set tagorfont $TextStyle::tagorfont($tagtype)
            if {$tagorfont == "p"} {
                          set tagListNew {}
                          foreach tagNameinList $tagList {
                                  set tagtypeinList [lindex [lindex $tagNameinList 0 ] 0 ]
                                  set tagorfontinList $TextStyle::tagorfont($tagtypeinList)
                                  if {$tagorfontinList != "p"} {
                                          lappend tagListNew $tagNameinList 
                                  }
                          }
              lappend tagListNew $tagName
              break
            }
            
          }
          }
          return $tagListNew
        }
        
        
        # ChangeTagW--
        #       change the style of selected Text
        #
        #Attention:
        #       tag add, font change
        #
        # Arguments:
        #   text:
        #               
        #       option:
        #
        #       value: now all the input value is second one
        #
        # Returns:
        #     Nothing
        #
        # Side effects:
        #       Add the Tags on the selected Text and change the selected text fonts
        #
        proc ChangeTagW {text name} {
                variable ::TextStyle::userPreset
        #       puts "text: $text "
                
                if {$name == ""} {return;}
                if {$name == "(standard)"} {set name "null"}    
                
                if {$text == "" && ![winfo exists textEditorMain::tableeditwinpath ]} {return} 
                #Attention: winfo and info have not same function.
                if {[info exists textEditorMain::tableeditwinpath ] && $textEditorMain::tableeditwinpath != {}} {
                        #puts "ChangeTagW-tableeditwinpath: $text"
                        set text $textEditorMain::tableeditwinpath              
                }
                
                #puts "ChangeTagW-text: $text"
                if { [catch {$text index sel.first} ] } {
                        #puts "ChangeTagW: There is nothing selected!"
                        set area "insert"
                        set oldtags  [$text tag names insert]
                } else {
                        # the tags in current selection area (with cursor)
                        set area "sel"
                        #set oldtags [$text tag names sel.first]
                        #set oldtags [concat $oldtags [$text tag names sel.last-1c]]
                        
                        set selrange [$text tag nextrange sel sel.first sel.last]
                        set selfirst [lindex $selrange 0]
                        set sellast [lindex $selrange 1]
                        
                        set selfirstarray [split $selfirst "."]
                        set sellastarray [split $sellast "."]
                
                        set selfirstline [lindex $selfirstarray 0]
                        set selfirstchar [lindex $selfirstarray 1]
                        
                        set oldtags [::WordProcess::getSelOldTags $text]
                }
                
                set inserttagList $oldtags

                
                foreach oldtag $oldtags {
                        if { $oldtag == "sel" } {       ;#for "sel" tag
                                continue
                        }
                        set isIgnored 0
                        foreach ignore $textEditorMain::ignoreListGlobal {
                          if {[string match $ignore $oldtag]} {set isIgnored 1}         
                        }
                        if {$isIgnored} {continue}
                  
                        set oldtagtype [lindex [lindex $oldtag 0 ] 0 ]
                        set oldtagorfont $TextStyle::tagorfont($oldtagtype)

                        if {$oldtagorfont == "p"} {
                            if {$area == "sel"} { 
                              $text tag remove $oldtag "sel.first linestart-1c" "sel.last lineend+1c"
                            } else {
                              $text tag remove $oldtag "insert linestart-1c" "insert lineend+1c"
                            }                             
                        } else {
                          if {$area == "sel"} { 
                            $text tag remove $oldtag sel.first sel.last
                          } else {
                            $text tag remove $oldtag insert
                          }
                        }
                        
                  

                        #puts "REMOVE $oldtag "
                }
                
                if {$name != "null"} {
                                set tagType preset
                                ::WordProcess::configurePreset $text $name
                                lappend userPreset $name
                                
                                if {$area == "sel"} {
                                        $text tag add [list $tagType $name] sel.first sel.last
                                        #puts "@optionvalue: $optionvalue"
                                } else {
                                        $text tag add [list $tagType $name] insert
                                        set ::TextStyle::inserttagList [concat $inserttagList [list [list preset $name ] ] ] 
                                        #puts "inserttagList $TextStyle::inserttagList"
                                }

                        
                } 
                # text modified
                #set ::textEditorMain::textModiMsg 1
                ::textEditor::setModi 1
                event generate $text <<Modified>>
                #focus $text
        }
        
                   
        # ChangeStyleW--
        #       change the style of selected Text
        #l
        #Attention:
        #       tag add, font change
        #
        # Arguments:
        #   text:
        #               
        #       option:
        #
        #       value: now all the input value is second one
        #
        # Returns:
        #     Nothing
        #
        # Side effects:
        #       Add the Tags on the selected Text and change the selected text fonts
        #
        proc ChangeStyleW {text option value doSwitch {windowtype "standard"}} {                
                #overrideswitch
                set doSwitch 1;                
                   
                variable ::textEditorMain::tableeditwinpath     
                #puts "::textEditorMain::textModiMsg $::textEditorMain::textModiMsg"
        #       puts "text: $text - option: $option - value: $value"
                if {$option == "" || $value == ""} {return;}
          
                if {$option == "justify" || $option == "p"} {
                  #1 get new text range
                  set selRang [$text tag ranges sel]
                  if {$selRang == ""} {
                          #puts "no sel"
                          set startParaPos [$text index "insert linestart"] 
                          set endParaPos [$text index "insert lineend"]
                  } else {
                          #puts "sel Rang: $selRang"
                          set startParaPos [$text index "sel.first linestart"] 
                          set endParaPosLinestart [$text index "sel.last linestart"]
                          set endSel [$text index "sel.last"]
                          if  {$endParaPosLinestart == $endSel } {
                            # empty last line
                            set endParaPos [$text index "sel.last -1c"]                            
                          } else {
                            set endParaPos [$text index "sel.last lineend"]
                          }
                          
                  }
                  if  {$startParaPos == $endParaPos } {
                    # empty line
                    return;
                  }
                  #puts "startParaPos: $startParaPos - endParaPos: $endParaPos"
                  #2 change old seleted tag to the new one
                  $text tag remove sel 1.0 end
                  $text tag add sel $startParaPos $endParaPos
                      
                  if {[$text tag ranges sel] == ""} {return;}
                }
          
                
                if {$option == "justify" && $value == "left"} {set value "null"} 
                if {$option == "offset" && $value == "0"} {set value "null"}    
                if {$value == "(standard)"} {set value "null"}  
                
                #puts "::WordProcess::ChangeStyleW tableeditwinpath: $tableeditwinpath "
                #puts [winfo exists tableeditwinpath]
                if {[info exists tableeditwinpath] && $tableeditwinpath != {}} {
                        #puts "::WordProcess::ChangeStyleW-tableeditwinpath: $tableeditwinpath"
                        set text $tableeditwinpath              
                } else {
                        if {$text == ""} {return;}
                }
                #puts "::WordProcess::ChangeStyleW-text: $text"
                
                #the input Option with its Value, e.g. weight bold, overstrike 1<<<<<
                set optionvalue [list $option $value]
                #puts "optionvalue: $optionvalue"
                
                if {$windowtype == "preview"} {
                        $text tag add sel 1.0 end;
                }
                
                if { [catch {$text index sel.first} ] } {
                        #puts "ChangeStyleW: There is nothing selected!"
                        set area "insert"
                        set oldtags  [$text tag names insert]
                
                } else {
                        set area "sel"
                        # the tags in current selection area (with cursor)
                        #set oldtags [$text tag names sel.first]
                        #set oldtags [concat $oldtags [$text tag names sel.last-1c]]
                        
                        set selrange [$text tag nextrange sel sel.first sel.last]
                        set selfirst [lindex $selrange 0]
                        set sellast [lindex $selrange 1]
                        
                        set selfirstarray [split $selfirst "."]
                        set sellastarray [split $sellast "."]
                
                        set selfirstline [lindex $selfirstarray 0]
                        set selfirstchar [lindex $selfirstarray 1]
                        
                        set oldtags [::WordProcess::getSelOldTags $text]
                }
        
                
                #check the tag state (0/1)
                set linkoranchorexists 0
                set tagexists 0
                
                set fontoptionexists 0
                
                #check the input option type (tag / font /or others?) 
                set tagorfont $TextStyle::tagorfont($option)
                #puts "@tagorfont: $tagorfont"
                
                set inserttagList $oldtags
                
                # saving the old tags with fonts from the selected text
                set oldfonttags {}
        
                #traversing all the old tags
                foreach oldtag $oldtags {
                        # doubled, because if it is a font tag it has multiple font option e.g. {{weight bold} {slant italic}}
                        set oldtagtype [lindex [lindex $oldtag 0 ] 0 ]
                        if { $oldtag == "sel" || ![info exists TextStyle::tagorfont($oldtagtype)] } {   ;#for "sel" tag
                                continue
                        }
                                        
                        #the input option not "font"
                        if {$tagorfont == "link" || $tagorfont == "anchor" || $tagorfont == "p"} {
        
                                
                                set oldtagorfont $TextStyle::tagorfont($oldtagtype)
                                set oldtagvalue [lindex $oldtag 1] 
                                #puts "$oldtagorfont - oldtag: $oldtag -oldtagvalue: $oldtagvalue "
                                # no overlapping of multiple link and anchor or p tags, but p and link/anchor can overlap
                                if {$oldtagvalue != "" && ( ( ($oldtagorfont == "link" || $oldtagorfont == "anchor") && ($tagorfont == "link" || $tagorfont == "anchor") )  || $oldtagorfont == "p" && $tagorfont == "p") } {
                                        #$text tag remove "${option}_$oldtagvalue" sel.first sel.last
                                        if {$area == "sel"} { 
                                          $text tag remove $oldtag sel.first sel.last
                                          # p remove remainder of old p tag in line
                                          if {$oldtagorfont == "p" && $tagorfont == "p"} {
                                            $text tag remove $oldtag "sel.first linestart-1c" "sel.last lineend+1c"
                                          }
                                        } else {
                                          $text tag remove $oldtag insert
                                        }
                                        #puts "REMOVE $oldtag ? $oldtagvalue == $value"
                                        if {$oldtagtype == $tagorfont && ($oldtagvalue == $value || ( $oldtagvalue != {} && $value == "null") )} {
                                                set linkoranchorexists 1
                                                #lremove
                                        }
                                }
                        } 
                        
                        if {$tagorfont == "tag"} {
        
                                #set oldtagvalue [$text tag configure $oldtag -$option ]
                                #set oldtagvalue [lindex $oldtagvalue 4]
                                
                                set oldtagorfont $TextStyle::tagorfont($oldtagtype)
                                set oldtagvalue [$text tag cget $oldtag -$option]
                                #puts "oldtag: $oldtag -oldtagvalue: $oldtagvalue"
        
                                if {$oldtagvalue != "" && !( $oldtagorfont == "link" || $oldtagorfont == "anchor" || $oldtagorfont == "p") } {
                                        #$text tag remove "${option}_$oldtagvalue" sel.first sel.last
                                        if {$area == "sel"} { 
                                          $text tag remove $oldtag sel.first sel.last
                                        } else {
                                          $text tag remove $oldtag insert
                                        }
                                        if {$oldtagtype == "preset"} {
                                                ::WordProcess::removePresetValue $text $option $oldtag $area
                                        }
                                        #puts "REMOVE $oldtag ? $oldtagvalue == $value"
                                        if {$oldtagvalue == $value || ( $oldtagvalue != {} && $value == "null") } {
                                                set tagexists 1
                                                #lremove
                                        }
                                } else {
                                        if {$oldtagtype == "preset"} {
                                                ::WordProcess::removePresetValue $text {} $oldtag $area
                                        }
                                }
        
                        }
                        if {$tagorfont == "font"} {
                        #the input option is "font"
                                
                                set oldfont [$text tag cget $oldtag -font ]
                                #puts "@oldfont: $oldfont"
                                #puts "@oldtag: $oldtag"
                                if {$oldfont != ""} {
                                        # save oldfonts for remove
                                                                            if {$oldtagtype != "preset" } {
                                          lappend oldfonttags $oldtag
                                                                            }
                                        set oldfontvalue [font configure $oldfont -$option ]
                                        #puts "?? $oldfont $oldfontvalue == $value"
                                        if {$oldfontvalue == $value || ( $oldfontvalue != 0 && $value == "null") } {
                                                set fontoptionexists 1
                                                #lremove
                                                #puts "SET"
                                                if {$oldtagtype == "preset"} {
                                                        ::WordProcess::removePresetValue $text $option $oldtag $area
                                                }
                                        } else {
                                                if {$oldtagtype == "preset"} {
                                                        ::WordProcess::removePresetValue $text {} $oldtag $area
                                                }
                                        }
                                }
                        }
                }
                
        #       puts "TAG $optionvalue"
                if {$tagorfont == "font"} {
                                if {$fontoptionexists == 0 && $value == "null" } {ifPreviewRemoveTextSelection $text $windowtype ;return;}
                                if {$fontoptionexists == 0 } {
                                  # not set, add font tag
                                  set singlefont [list $optionvalue ]
                                  if {[::WordProcess::checkfontexits $singlefont] == 0} {
                                           font create $singlefont -family $::textEditorMain::defaultFont -size $::textEditorMain::defaultSize
                                           font configure $singlefont -$option $value
                                  }
                                  $text tag configure $singlefont -font $singlefont     
                                  if {$area == "sel"} { 
                                        $text tag add $singlefont sel.first sel.last
        #                               puts "ADDTAGSEl$selfirst $sellast $singlefont"  
                                  } else {
                                        $text tag add $singlefont insert
                                        set ::TextStyle::inserttagList [concat $inserttagList [list $singlefont]]  
        #                               puts "ADDTAGSElinsert $singlefont"      
                                  }
                                 
                                }
                                foreach oldfonttag $oldfonttags {
        #                               puts "oldfont $oldfont ? $fontoptionexists : $optionvalue"
                                                                                set oldfont [$text tag cget $oldfonttag -font ]
                                        set newfont {}
                                        foreach oldoptionvalue $oldfont {
                                                set oldoption [lindex $oldoptionvalue 0]
                                                set oldvalue  [lindex $oldoptionvalue 1]
                                                if {$oldoption != $option} {
                                                        lappend newfont $oldoptionvalue
                                                }
                                        }
                                        if {$fontoptionexists == 0} {
                                          lappend newfont $optionvalue
                                          set newfont [lsort $newfont]
        #                                 puts "$fontoptionexists add@ $optionvalue fontoption $newfont;"
                                        }
                                        #puts "NEWFONT $newfont"
                                        # Font allready added
                                        if {$newfont != {}} {           
                                                if {[::WordProcess::checkfontexits $newfont] == 0} {
                                                        font create $newfont -family $::textEditorMain::defaultFont -size $::textEditorMain::defaultSize
                                                        foreach newoptionvalue $newfont {
                                                                set newoption [lindex $newoptionvalue 0]
                                                                set newvalue [lindex $newoptionvalue 1]
        #                                                       puts "newoptionvalue $newoptionvalue - newoption $newoption - newvalue $newvalue"
                                                                font configure $newfont -$newoption $newvalue
                                                        }
                                                }
                                                $text tag configure $newfont -font $newfont
                                        }
                                        #puts "@newfont $newfont"
                                        if {$area == "sel"} {
                                                set tagranges [$text tag ranges $oldfonttag]
                                                #puts "TAGRANGES $oldfonttag : $tagranges"
                                                foreach {tagfirst taglast} $tagranges {
                                                        
                                                        # tag in selection
                                                        set addfirst $tagfirst
                                                        set addlast $taglast
                                                        
                                                        # tag out of selection
                                                        # if tag.last < sel.first 
                                                        if {[::TEUtils::textIndexCompare $taglast $selfirst ] == -1} {continue}
                                                        # if sel.last < tag.first
                                                        if {[::TEUtils::textIndexCompare $sellast $tagfirst ] == -1} {continue}
                                                        
                                                        
                                                        # tag overlaps selection left 
                                                        # if tag.first < sel.first && sel.first < tag.last  
                                                        if {[::TEUtils::textIndexCompare $tagfirst $selfirst ] == -1 && [::TEUtils::textIndexCompare $selfirst $taglast ] == -1} {
                                                                  set addfirst $selfirst
                                                        }                               
                                                        # right
                                                        # if tag.first < sel.last && sel.last < tag.last 
                                                        if {[::TEUtils::textIndexCompare $tagfirst $sellast ] == -1 && [::TEUtils::textIndexCompare $sellast $taglast ] == -1} {
                                                                  set addlast $sellast
                                                        } 
                                                        
                                                        #selection in tag
                                                        # if tag.first < sel.first && sel.last < tag.last 
                                                        if {[::TEUtils::textIndexCompare $tagfirst $selfirst  ] == -1 && [::TEUtils::textIndexCompare $sellast $taglast ] == -1} {
                                                                  set addfirst $selfirst
                                                                  set addlast $sellast
                                                        } 
                                                        
                                                        # add font tag in range addfirst addlast
                                                        if {$newfont != {}} {                                   
                                                          $text tag add $newfont $addfirst $addlast
        #                                                 puts "ADDTAG$tagfirst $taglast $newfont"              
                                                          #$text tag remove $oldfont $addfirst $addlast ; REMOVE below
                                                        }
                                                        # remove font tag with single option
                                                        if {$fontoptionexists == 0} {
                                                          if {$newfont != $singlefont} {
                                                                $text tag remove $singlefont $addfirst $addlast
        #                                                   puts "REMOVE S $singlefont $addfirst $addlast"
                                                          }
                                                        }
                                                }                                                               
                                                                                                #set oldfonttagtype [lindex [lindex $oldfonttag 0 ] 0 ]
                                                                                                #set oldtagorfont $TextStyle::tagorfont($oldtagtype)
                                                                                                #if {$oldtagorfont == "font"} {
                                                $text tag remove $oldfont sel.first sel.last
                                                                                                #}
                                                #puts "REMOVE O $oldfont $selfirst $sellast"
                                        } else {
                                                if {$newfont != {}} {                                   
                                                  $text tag add $newfont insert
        #                                         puts "ADDTAGinsert $newfont"
                                                  set ::TextStyle::inserttagList [concat $inserttagList [list $newfont]]  
                                                }
                                                # remove font tag with single option
                                                if {$fontoptionexists == 0} {
                                                  if {$newfont != $singlefont} {
                                                        $text tag remove $singlefont insert
        #                                           puts "REMOVE S $singlefont insert"
                                                  }
                                                }
                                                
                                                $text tag remove $oldfont insert
        #                                       puts "REMOVE O $oldfont insert" 
                                        }
                                }
                }
                if {$tagorfont == "tag"} {
                        if {$tagexists == 0 && $value == "null" } {ifPreviewRemoveTextSelection $text $windowtype ;return;}
                        if {$tagexists == 0 && $value != "null" } {
                                
        #                       set imageInfo [$value cget -data]
        #                       puts "bildInfo: $imageInfo"
                                #puts $value
                                # xin: 08.04.2014
                                #$text tag configure $optionvalue -$option $value
                                #puts "optionvalue: $optionvalue - option: $option - value: $value"
                                if {[catch [list $text tag configure $optionvalue -$option $value] result]} {
                                        putsstderr "Error: $result"
                                        return;
                                }
                                #TODO:xin test for doubleLine
                                #$text tag configure $optionvalue -$option ::images::doubleLine
                                if {$area == "sel"} { 
                                  $text tag add $optionvalue sel.first sel.last
                                  #puts "@optionvalue: $optionvalue"
                                } else {
                                  $text tag add $optionvalue insert
                                  set ::TextStyle::inserttagList [concat $inserttagList [list $optionvalue]] 
                                }
                        }
                }
                if {$tagorfont == "link" || $tagorfont == "anchor" || $tagorfont == "p" } {
                        if {$linkoranchorexists == 0 && $value == "null" } {ifPreviewRemoveTextSelection $text $windowtype ;return;}
                        if {$linkoranchorexists == 0 && $value != "null" } {
                                
                                #puts $value
        #                       if {$tagorfont == "link"} {
        #                               set linkanchortag [list link $value]
        #                               $text tag configure $linkanchortag -foreground blue
        #                               $text tag configure $linkanchortag -underline 1
        #                       }
        #                       if {$tagorfont == "anchor"} {
        #                               set linkanchortag [list anchor $value]
        #                               $text tag configure $linkanchortag -background gray
        #                               $text tag configure $linkanchortag -bgstipple gray50
        #                       }
                                # xin:15.02.2014
                                if {$tagorfont == "link" || $tagorfont == "anchor"} {
                                        set linkAnchorTag [list $tagorfont $value]
                                        if {$tagorfont == "link"} {
                                                $text tag configure $linkAnchorTag -foreground blue
                                                $text tag configure $linkAnchorTag -underline 1
                                        }
                                        if {$tagorfont == "anchor"} {
                                                #$text tag configure $linkAnchorTag -background gray
                                                #$text tag configure $linkAnchorTag -bgstipple gray50
                                        }
                                        ::TEGUI::bindURL $text $linkAnchorTag $tagorfont
                                } elseif {$tagorfont == "p"} {
                                        set linkAnchorTag [list $tagorfont $value]
                                        configureParagraph $text $value
                                }
                                #TODO:xin test for doubleLine
                                #$text tag configure $optionvalue -$option ::images::doubleLine
                                if {$area == "sel"} { 
                                  $text tag add $linkAnchorTag sel.first sel.last
                                  #puts "@optionvalue: $optionvalue"
                                } else {
                                  $text tag add $linkAnchorTag insert
                                  set ::TextStyle::inserttagList [concat $inserttagList [list $linkAnchorTag]] 
                                }
                        }
                }
        
                # text modified
                #set ::textEditorMain::textModiMsg 1
                ::textEditor::setModi 1
                event generate $text <<Modified>>
                
                if {$doSwitch == 0} {   
                  if {$fontoptionexists || $tagexists || $linkoranchorexists } {
                    ChangeStyleW $text $option $value 1 $windowtype
                  }
                }
                #focus $text
                ifPreviewRemoveTextSelection $text $windowtype
                # return the last text widget to show the selected text segment.
                catch {focus $::searchTE::lastTextWidget}
                                
        }
        
                
        
        proc ifPreviewRemoveTextSelection {text {windowtype "standard"}} {
                if {$windowtype == "preview"} {
                        $text tag remove sel 1.0 end;
                }
        }
        
        
        #insert tags for preset without given option
        proc removePresetValue {text option preset area} {
                variable ::TextStyle::presetStyles
          
                set presetname [lindex $preset 1] 
                set presetoptionValues $presetStyles($presetname)
          
                set presetranges {}  
           
                if {$area == "sel"} { 
                  set selrange [$text tag nextrange sel sel.first sel.last]
                  set rangestart sel.first
                  #repeat
                  while {$rangestart != {}} {
                    set presetrange [$text tag nextrange $preset $rangestart sel.last]
                    #until
                    if {$presetrange == {}} {break}
                    set rangestart [lindex $presetrange 1]
                    lappend presetranges $presetrange
                  }
                  $text tag remove $preset sel.first sel.last
                  
                  foreach presetrange $presetranges { 
                    $text tag remove sel 1.0 end
                    $text tag add sel [lindex $presetrange 0] [lindex $presetrange 1]
                    foreach {presetoptionValue} $presetoptionValues {
                      set presetoption [lindex $presetoptionValue 0]
                      set presetvalue [lindex $presetoptionValue 1] 
                      if {$presetoption != $option} {
                              ::WordProcess::ChangeStyleW $text $presetoption $presetvalue 0 standard
                      }
                    }
                  }
                  
                  $text tag remove sel 1.0 end
                  $text tag add sel [lindex $selrange 0] [lindex $selrange 1]
                  
                } else {

                  $text tag remove $preset insert
                  
                  foreach {presetoptionValue} $presetoptionValues {
                    set presetoption [lindex $presetoptionValue 0]
                    set presetvalue [lindex $presetoptionValue 1] 
                    if {$presetoption != $option} {
                            ::WordProcess::ChangeStyleW $text $presetoption $presetvalue 0 standard
                    }
                  }
                }
                

        } 
        
        # getSelOldTags --
        #       Get all tags from the given text.
        # Arguments:
        #       text    the given text widget
        # Result:
        #       None
        # Effect:
        #       None
        #
        proc getSelOldTags {text} {
                #check sel tag
                if { [catch {$text index sel.first} ] } {
                                puts "getSelOldTags: There is nothing selected!"
                                return
                } 
                #the old tags
                set oldtags {} 
                #sel range
                set selrange [$text tag nextrange sel sel.first sel.last]
                
                #tag Positions
                array set tagPositions {}
        
                # selfirst is greater than sellast
                set selfirst [lindex $selrange 0]
                set sellast [lindex $selrange 1]
                
        
                set selfirstarray [split $selfirst "."]
                set sellastarray [split $sellast "."]
        #       puts $selfirstarray
        #       puts $sellastarray
        
                set selfirstline [lindex $selfirstarray 0]
                set selfirstchar [lindex $selfirstarray 1]
                set sellastline [lindex $sellastarray 0]
                set sellastchar [lindex $sellastarray 1]
                
                #selfirst and sellast in the same row
                if {$selfirstline == $sellastline} {
                        set temp $selfirstchar
                        while {$temp < $sellastchar} {
                                foreach name [$text tag names $selfirstline.$temp] {
                                        ::WordProcess::tagPositionSet tagPositions $name $selfirstline.$temp
                                }
                                incr temp
                        }
                        
                } else {
                        #selfirst and sellast not in the same row
                        #lines
                        for {set line $selfirstline} { $line <= $sellastline} {incr line} {
                                
                                #the first line
                                if {$line == $selfirstline} {
                                        set chartemp $selfirstchar
                                        set firstLineend [lindex [split [::TEUtils::getLineend $text $selfirstline] "." ] 1]
                                        while {$chartemp < $firstLineend} {
        #                                       lappend oldtags [$text tag names $selfirstline.$chartemp]
        #                                       puts "oldtags: $oldtags"
                                                
                                                foreach name [$text tag names $selfirstline.$chartemp] {
                                                        ::WordProcess::tagPositionSet tagPositions $name $selfirstline.$chartemp
                                                }
                                                
                                                incr chartemp
                                        }
                                        
                                        
                                } elseif {$line == $sellastline} {
                                        #the last line
                                        set chartemp 0
                                        while {$chartemp < $sellastchar} {
        #                                       lappend oldtags [$text tag names $sellastline.$chartemp]
        #                                       puts "oldtags: $oldtags"                                        
                                                
                                                foreach name [$text tag names $sellastline.$chartemp] {
                                                        ::WordProcess::tagPositionSet tagPositions $name $sellastline.$chartemp
                                                }
                                                incr chartemp
                                        }
                                } else {
                                        # middle lines
                                           set lineend [lindex [split [::TEUtils::getLineend $text $line] "." ] 1]
                                        for {set char 0} {$char <= $lineend} {incr char} {
        #                                       lappend oldtags [$text tag names $line.$char]
        #                                       puts "oldtags: $oldtags"
                                                
                                                foreach name [$text tag names  $line.$char] {
                                                        ::WordProcess::tagPositionSet tagPositions $name  $line.$char
                                                }
                                        }
                                }       
                        }       
                }
                #puts "-----------tagPositions begin-----------"
                #puts "startIdx: $selfirst - endIdx: $sellast"
                foreach {key value} [array get tagPositions] {
                #        puts "$key : $value"
                }
                #puts "-----------tagPositions end-------------"
                return [array names tagPositions]
        }
        
        
        # tagPositionSet--
        #       the new tag positions to set
        #
        #Attention:
        #       position is only one form, a position not a interval
        #
        # Arguments:
        #   _tagPosition: the old tags positions
        #               
        #       tagName: tag name
        #
        #       newPosition: the input Position of new tag or old tag
        #       
        # Returns:
        #       Nothing
        #
        # Side effects:
        #       None
        #
        proc tagPositionSet {_tagPosition tagName newPosition} {
                upvar $_tagPosition tagPosition
                
                set tempPosition {}
                
                # new array tagName
                if { [array names tagPosition $tagName] == "" } {
                        #puts "no name"
                        set tempPosition $newPosition
                } else {
                        #tagName exists
                        
                        # position list of the tagName
                        set tempPosition [lindex [array get tagPosition $tagName] 1 ] 
                        
                        # new position is not exists in the PositionList
                        if {[lsearch $tempPosition $newPosition] == -1} {
                                lappend tempPosition $newPosition
                        } else {
                                #new position is exists in the PositionList
                                set tempPosition [lremove $tempPosition $newPosition]
                        }
                }
                # check the tagName has the Postions or not.
                if {[llength $tempPosition] == 0} {
                        # there is no Position, delete the whole entry
                        unset tagPosition($tagName)
                } else {
                        set tagPosition($tagName) $tempPosition
                }
                
        #       puts "FontName: $name - Position: $tempPosition"
        }
        
        
        # changeBodyTextToTei --
        #    Change the content of body text to the xml form.
        # Arguments:
        #    mainBodyText        
        #        tagAttrMap:    for tag-attributte key pair
        #
        # Result:
        #    None
        #
        proc changeBodyTextToTei {mainBodyText tagAttrMap} {
                        variable ::TEDB::presetOption
                if {$mainBodyText == ""} {return "";}
                #puts "changeBodyTextToTei.mainBodyText: $mainBodyText"
                upvar 1 $tagAttrMap arrM
                if {![array exists arrM]} {
                   error "\"$mapArr\" isn't an array"
                }
                set result ""
                
                set endTagPat {/*}
                #puts "changeBodyTextToTei.mainBodyText \n $mainBodyText"
                # group: text - attributes - tag
                foreach group $mainBodyText {
                        if {[llength $group] != 3
                                || [catch {set text [lindex $group 0]} result1] 
                                || [catch {set attrs [lindex $group 1]} result2]
                                || [catch {set tag [lindex $group 2]} result3]
                        } {
                                putsstderr "Error: Bugs in the text-attrs-tag group: $group"
                                continue;
                        }
                        #puts "text: $text - attrs: $attrs - tag: $tag"
                        # tag <table> has only one form attribute {rows="xx" cols="xx"}
                        if {[llength $attrs] > 1 && [string first "rows" $attrs] < 0} {
                                #FIXME:
                                set attrs [join $attrs ";"]
                        }
                        
                        set tag [string trim $tag]
                        # empty tag
                        if {$tag == ""} {
                                #putsstderr "Warning: There is a empty tag in the ::WordProcess::changeBodyTextToTei!\ntext: $text - attrs: $attrs"
                                append result $text
                                continue;
                        }
                        #FIXME: trim problem
                        #set text [string trim $text]
                        set attrs [string trim $attrs]
                
                        set isEndTag [string match $endTagPat $tag]
                        
                        if {!$isEndTag} {
                                # start tag
                                # lb, br will be at their end tag to handle
                                if {$tag == "lb" } {
                                        #lb will be converted later
                                        append result "<lb/>$text"
                                        continue;
                                }
                                set curAttrKeyValue ""
                                
                                if {$attrs != ""} {
                                        
                                        if {$tag == "table"} {
                                                # table system
                                                set curAttrKeyValue $attrs
                                        } elseif {$tag == "ref" || $tag == "anchor"} {
                                                #link system
                                                set linkPat {([^:]*):(.*?)}
                                                regexp $linkPat $attrs match attrKey attrValue          
                                                if {$attrKey == "" || $attrValue == ""} {
                                                        putsstderr "Error: There is no link attribute name or value at $group!"
                                                        continue;
                                                }
                                                append curAttrKeyValue $attrKey "=\"" $attrValue "\""
                                        } elseif {$tag == "p" || [string match -nocase {h[0-9]} $tag]} {
                                                if {$tag == "p"} {
                                                        set paragraphValue "Paragraph"
                                                } elseif {[string match -nocase {h[0-9]} $tag]} {
                                                        set paragraphValue [string map -nocase {h Headline} $tag ]
                                                }
                                                set tag "p"
                                                append curAttrKeyValue "rendition=\"#$paragraphValue\""
                                                
                                        } elseif {$tag == "cell"} {
                                                #puts $attrs                    ;# colspan:1 rowspan:3
                                                append curAttrKeyValue [::WordProcess::changeCellAttrs $attrs TextStyle::htmlXmlMap]
                                        } elseif {[info exists arrM($tag)] && $arrM($tag) != ""} {
                                                # not tr td, only hi, span
                                                set validAttrs ""
                                                set presetValue ""
                                                foreach item [split $attrs ";"] {
                                                        if {$tag!= "cell" && ([string first "width" $item ] >= 0 
                                                         || [string first "col" $item ] >= 0 || [string first "span" $item ] >= 0)} {
                                                                continue;
                                                        } elseif {[string first [append tmpFlag "preset:" $presetOption(prefix)] $item]>=0} {
                                                                foreach {key value} [split $item ":"] {break;}
                                                                set presetValue $value
                                                                continue;
                                                        }
                                                        
                                                        # append validAttrs $item
                                                        set validAttrs [::TEUtils::appendValue $validAttrs $item ";"]
                                                }
                                                if {$validAttrs!=""} {
                                                        append curAttrKeyValue $arrM($tag) "=\"" $validAttrs  "\"" 
                                                }
                                                if {$presetValue != ""} {
                                                        append curAttrKeyValue "rendition=\"#$presetValue\""
                                                }
                                        }
                                } elseif {$tag == "p" || [string match -nocase {h[0-9]}  $tag]} {
                                        if {$tag == "p"} {
                                                set paragraphValue "Paragraph"
                                        } elseif {[string match -nocase {h[0-9]}  $tag]} {
                                                set paragraphValue [string map -nocase {h Headline} $tag ]
                                        }
                                        set tag "p"
                                        append curAttrKeyValue "rendition=\"#$paragraphValue\""
                                } 
                                
                                if {$curAttrKeyValue == ""} {
                                        append result "<$tag>$text"
                                } else {
                                        append result "<$tag $curAttrKeyValue>$text"
                                }
                        } else {
                                # end tag
                                if {$tag == "lb"} {
                                        #append result "<lb/>$text"
                                } else {
                                        append result "<$tag>$text"
                                }
                        }
                }
                
                # remove empty hi elemets
                regsub -all {<hi></hi>} $result "" result
                #puts "changeBodyTextToTei.result: $result"
                return $result
        }
        
        # lut: LOOK UP TABLE
        proc changeCellAttrs {attrs lut} {
                #attrs -> colspan:1 rowspan:3
                if {$attrs == ""} {return "";}
                upvar 1 $lut arrM
                if {![array exists arrM]} {
                   error "\"$lut\" isn't an array"
                }
                set result ""
                foreach ele [split $attrs] {
                        if {[string first ":" $ele] <=0} {
                                putsstderr "Warning: cell contains no valid attribute!"
                                continue;
                        }
                        set temp ""
                        foreach {key value} [split $ele ":"] break;
                        if {$key=="width" || $key == "height"} {
                                append temp "style=\"" $arrM($key):$value "\""
                        } else {
                                append temp $arrM($key) "=\"" $value  "\""
                        }
                        set result [::TEUtils::appendValue $result $temp " "]
                }
                return $result
        }
        
        
        
        # parseXmlContents --
        #    Parse the given xml contents with package htmlparse and insert into the selected text widget.
        # Arguments:
        #    name:              the selected text widget            
        #        contents:      the input xml contents
        #        insertIdx: index
        #
        # Result:
        #    None
        #
        proc parseXmlContents {name contents {insertIdx end}} {
                ::TEUtils::refreshStatus
                set ::textEditorMain::editorText $name.text
                # clean the html content
                #set contents [::TEUtils::cleanWhitespace $contents]
                #puts "parseXmlContents.contents: $contents"
                
                # parse html content 
                # key proc: parseCommend
                htmlparse::parse -cmd ::WordProcess::parseXmlToText $contents
                
                #puts "main,bodyText: $textEditorMain::xmlData(main,bodyText)"
                # update tags and fonts to the textEditor       
                ::TEGUI::addText $name.text $textEditorMain::xmlData(main,bodyText) $insertIdx
                
                ::TEUtils::refreshStatus
        }
        
        #Bug: #2715
        # parseXmlToText --
        #    save the xml contents to the namespace
        # Arguments:
        #    args: tag slash param text
        #
        # Result:
        #    None
        #
        proc parseXmlToText {args} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                # useful tags
                
                foreach {tag slash param text} $args {
                        #puts "=> tag= $tag - slash= $slash - param= $param - text= $text"
                        # make sure all tag name is low case
                        set tag [string tolower $tag]
                        # end with "/" in the slash-parameter
                        
                        set text [htmlparse::mapEscapes $text]
                        
                        set isTagEnd [string equal $slash "/"]
                        
                        
                        if {[string equal -nocase $tag "teiHeader"]} {
                                set xmlData(inHeader) [expr {$isTagEnd == 1 ? 0 : 1}]
                        # new teibody
                        } elseif {[string equal -nocase $tag "item"] && !$isTagEnd && [string first "n='main'" $param] >=0 } {
                                set xmlData(main,inBody) 1
                                set xmlData(main,bodyLevel) 0
                                set xmlData(main,currentAttrs) ""
                                set xmlData(main,bodyText) {}
                                set curWName "main"
                        }
                        
                        # determine current text widget (mainText or cell text)
                        ::WordProcess::checkTextName
                        
                        if {$curWName == "" && !$xmlData(inHeader)} {continue}
                        if {$isTagEnd} {
                                ::WordProcess::analyzeEndTag $tag
                        } else {
                                ::WordProcess::analyzeStartTag $tag $param $text
                        }
                        if {$curWName !="" } {
                         if {$isTagEnd && ($tag == "p" || $tag =="div"  || $tag =="lb") } {
                                # this
                                set text [string trimleft $text \n]
                                #

                                set text [::TEUtils::cleanWhitespace $text ] 
                           
                                # or
                                #set text [string trimleft $text]
                                #
                         }
                                getXmlText $text $tag $isTagEnd
                        }
                }
        #       if {$curWName == ""} {return}
        #       puts "$curWName,bodyText: $xmlData($curWName,bodyText)"
        }
        
        # add the text and current tag to the ::textEditorMain::xmlData(curWName,bodyText)
        proc getXmlText {text tag isTagEnd} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                
                #TODO: TEST
                #if {[string trim $text] == "" && $tag != "ref" && $tag != "anchor"} {return;}
                
                #TODO: TEST
                #set text [string trim $text]
                set curAttr [::WordProcess::getStyleMaps [lindex $xmlData($curWName,currentAttrs) end] 0]
                #Bug 2715
                set curAttr [lreverse [sortTextAttrs $curAttr ::TextStyle::tagRating]]
                #puts "getXmlText.curAttr: $curAttr"
                
                #puts "getXmlText.text $text - curAttr $curAttr - tag $tag"
                if {$text == "" &&  ( $tag != "ref" && $tag != "anchor" && $tag != "p")} {return;}
                # &lt;&gt; -> <,>
                set text [::TEUtils::mapHTMLToText $text]
                set text [::TEUtils::cleanWhitespace $text]
                # get the last style values
                if {$xmlData($curWName,inBody)} {
                    lappend xmlData($curWName,bodyText) $text $curAttr
                }
        }
                
                #qsort
        proc sortTextAttrs {attrList ratingArray} {
                variable ::TextStyle::tagRating
                upvar 1 $ratingArray arrM
                if {![array exists arrM]} {
                   error "\"$ratingArray\" isn't an array"
                }
                set listLen [llength $attrList]
                
                if {$listLen > 1} {
                        set attrList [::TEUtils::quickSortA arrM $attrList 0 [expr $listLen - 1]]
                }
                return $attrList
        }
        
        
        # start tag of xml
        proc analyzeStartTag {tag param text} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::tableColNum
                variable ::textEditorMain::tableRowNum
                variable ::textEditorMain::curWName
                variable ::textEditorMain::tableMap 
                variable ::textEditorMain::tablePoint
                if {$curWName == "" && !$xmlData(inHeader)} {return;}
                if {$curWName != ""} {
                        # add the xml level
                        if {$xmlData($curWName,inBody)} {
                                incr xmlData($curWName,bodyLevel)
                        }
                        #if {$tag=="p" | $tag=="div" | $tag=="lb"} {}
                        if {$tag=="lb"} {
                                lappend xmlData($curWName,bodyText) "\n" [getStyleMaps [lindex $xmlData($curWName,currentAttrs) end] 0]
                        } elseif {$tag=="hi"} {
                                
                                set rendParam [::WordProcess::getAttrValues $param rend]
                                #puts "rendParam: $rendParam"
                                set renditionParam  [::TEDB::removePresetPrefix  [::WordProcess::getAttrValues $param rendition] ]
                                if {$renditionParam != ""} {
                                        set renditionParam "preset:$renditionParam"
                                }
                                #puts "renditionParam: $renditionParam"
                                set param [::TEUtils::appendValue $rendParam $renditionParam ";"] 
                                set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                                $xmlData($curWName,currentAttrs) $param $xmlData($curWName,xmlArrName) $xmlData($curWName,bodyLevel)]
                        } elseif {$tag == "ref" || $tag == "anchor" } {
                                set pattern ""
                                if {[string first "target=" $param]>=0} {
                                        set pattern "target"
                                        set newKey "link:"
                                } elseif {[string first "ana=" $param]>=0} {
                                        set pattern "ana"
                                        set newKey "link:"
                                } elseif {[string first "n=" $param]>=0} {
                                        set pattern "n"
                                        set newKey "anchor:"
                                }
                                if {$pattern == ""} {return;}
                                set urlValue [::TEUtils::mapLinkTextToXml [::WordProcess::getAttrValues $param $pattern] ]
                                
                                append newParam $newKey $urlValue
                                set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                                                $xmlData($curWName,currentAttrs) $newParam $xmlData($curWName,xmlArrName) $xmlData($curWName,bodyLevel)]
                                
                        } elseif {$tag == "p"} {
                                set renditionParam  [::TEDB::removePresetPrefix  [::WordProcess::getAttrValues $param rendition] ]
                                set pKey "p:"
                                if {$renditionParam == ""} {set renditionParam "Paragraph"}
                                append newParam $pKey $renditionParam
                                set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                                                $xmlData($curWName,currentAttrs) $newParam $xmlData($curWName,xmlArrName) $xmlData($curWName,bodyLevel)]
                                
                                
                        } elseif {$tag=="table"} {
                                set xmlData(inTable) 1
                                set tableName [::TEGUI::setNewTableName $textEditorMain::editorText]
                                if {$tableName == ""} {return;}
                                ::textEditorMain::tableAddRecord $textEditorMain::tableNum $tableName
                                lappend ::textEditorMain::xmlData($curWName,bodyText) "$tableName" "window"
                                set xmlData(tableCell,currentTableName) $tableName
                        } elseif {$tag == "cell"} {
                                if {$xmlData(inTable) == 1} {
                                        set xmlData(tableCell,inBody) 1
                                        set xmlData(tableCell,bodyLevel) 0
                                        set xmlData(tableCell,currentAttrs) ""
                                        set xmlData(tableCell,bodyText) {}
                                        set xmlData(tableCell,curCellAttrs) ""
                                        set curWName "tableCell"                        
                                        set position $tableRowNum,$tableColNum
                                        set xmlData(tableCell,curPosition) $position
                                        textEditorMain::searchFree $xmlData(tableCell,currentTableName)
                                        textEditorMain::setTableMap $xmlData(tableCell,currentTableName) $position $param
                                
                                        # important!!
                                        textEditorMain::incrColNum
                                }
                        } 
                } elseif {$xmlData(inHeader)} {
                        if {$tag == "rendition"} {
                                set renditionName [::TEDB::removePresetPrefix [::WordProcess::getAttrValues $param xml:id]]
                                #puts "renditionName: $renditionName - text: $text"
                                ::TEDB::addToPreset $renditionName XML [list  $text]
                        }
                }
        }
        
        
        # end tag of xml
        proc analyzeEndTag {tag} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                if {$curWName == "" && !$xmlData(inHeader)} {return;}
                if {$curWName != ""} {
                        if {$tag=="p" || $tag == "div"} {
                                lappend xmlData($curWName,bodyText) "\n" " "
                        } elseif {$tag == "table"} {
                                set xmlData(inTable) 0
                                set colNum $textEditorMain::tableMap(maxWidth)
                                set rowNum $textEditorMain::tableRowNum
                                set tableName $xmlData(tableCell,currentTableName)
                                
                                if {$colNum == "" || $colNum == 0 || $rowNum == "" || $rowNum == 0 || $tableName == ""} {
                                        puts "Warning: table $tableName has some probleme! colNum $colNum - rowNum $rowNum"
                                        return;
                                }
                                ::textEditorMain::setTableColNum $tableName $colNum
                                ::textEditorMain::setTableRowNum $tableName $rowNum
                                
                                #TODO: RESET
                                # for next table
                                set textEditorMain::tableMap(maxWidth) 0
                                set textEditorMain::tableRowNum 0
                                set textEditorMain::tableColNum 0
                                set xmlData(tableCell,curPosition) ""
                                set textEditorMain::tablePoint 0,0
                #               set textEditorMain::tableTdNum 0
                #               ::textEditorMain::resetTableNum
                                
                        } elseif {$tag=="row"} {
                                set textEditorMain::tableColNum 0
                                # important!!
                #               textEditorMain::incrRowNum
                                textEditorMain::newRow
                        } elseif {$tag=="cell"} {
                                # clear cell
                                set xmlData(tableCell,inBody) 0
                                set xmlData(tableCell,bodyLevel) 0
                                set xmlData(tableCell,currentAttrs) ""
                                # 
                                set tNum [expr $textEditorMain::tableNum - 1]
                                if {[textEditorMain::hasTable $tNum]} {
                                        #update the cell text into namespace data bank
                                        set position $xmlData(tableCell,curPosition)
                                        textEditorMain::cellInsert $xmlData(tableCell,currentTableName) $position $xmlData($curWName,bodyText) $xmlData(tableCell,curCellAttrs)
                                }
                                
                                set xmlData(tableCell,bodyText) {}              
                                set curWName "main"
                        # new teibody  
                        } elseif {$tag == "item"} {
                                # body of text ends with item tag
                                #clear body
                                set xmlData(main,inBody) 0
                                set xmlData(main,bodyLevel) 0
                                set xmlData(main,currentAttrs) ""
                                set curWName "main"
                        }
                        # remove the last level of attribute array
                        if {$xmlData($curWName,inBody) && $xmlData($curWName,currentAttrs)!="" && $tag != "lb" } {
                                set xmlData($curWName,currentAttrs) [::WordProcess::removeLastTagElement $xmlData($curWName,currentAttrs)]
                        }
                        
                        # reduce the xml level
                        if {$xmlData($curWName,inBody)} {
                                incr xmlData($curWName,bodyLevel) -1
                        }
                } elseif {$xmlData(inHeader)} {
                        #TODO:
                }
        }
        
        # set current text widget name.
        proc checkTextName {} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                if {$xmlData(tableCell,inBody)} {
                        set curWName "tableCell"
                } elseif {$xmlData(main,inBody)} {
                        set curWName "main"
                }
        }
        
        
        
        # configurePreset --
        #    
        #
        # Arguments:
        #    text        the text widget to insert
        #        name            name of tag
        #        
        #
        # Result:
        #    None and no Return.
        #       
        proc configurePreset {text name} {
                if {$text == "" || $name == ""} {return;}
                configureTagType $text preset $name
        }
        
        proc configureParagraph {text name} {
                if {$text == "" || $name == ""} {return;}
                configureTagType $text p $name
                if {[info exists presetStyles($name)]} {
                        $text tag lower [list p $name]
                }
        }
        
        proc configureTagType {text tagType name} {
                variable ::TextStyle::presetStyles
                if {[info exists presetStyles($name)]} {
                  set optionValues $presetStyles($name)
                  configureTagByOptionValues $text [list $tagType $name] $optionValues
                } else {
                  putsstderr "Warning: There is no such element $name in array presetStyles"
                }
        }
        
        proc configureTagByFormatString {text tagname formatEntry} {
                set optionValues {}
                foreach optionValueString [split $formatEntry ";"]  {
                          set optionValue [split $optionValueString :]
                          set option [lindex $optionValue 0 ]
                          set value [lindex $optionValue 1 ]
                          lappend optionValues [list $option $value]
                }
                #return $optionValues 
                configureTagByOptionValues $text $tagname $optionValues
        }
        
        
        proc configureTagByOptionValues {text tagname optionValues} {
                #puts "configureTagByOptionValues.configureList: $text $tagname $optionValues"
                # remove old Values
                set configureList [$text tag configure $tagname] 
                #puts "configureTagByOptionValues.configureList: $configureList"
                
                foreach {optionValue} $configureList {
                        #puts "configureTagByOptionValues.optionValue: $optionValue"
                        set option [lindex $optionValue 0]
                        #puts "configureTagByOptionValues.option: $option"
                        $text tag configure $tagname $option {}
                }
                                
                set newfont {}
                set tagList {}
        #FIXME:         element 1 in array TextStyle::tagorfont!
                foreach {optionValue} $optionValues {
                        #puts "addText.optionValue: $optionValue"
                        set option [lindex $optionValue 0]
                        set value [lindex $optionValue 1]
                        
                        if {![info exists option]||$option == ""} {
                                putsstderr "There is a empty option within $optionValues"
                                continue;
                        }
                        if {![info exists TextStyle::tagorfont($option)]} {
                                putsstderr "Warning: There is no such element $option in array TextStyle::tagorfont!"
                                continue;
                        }
                        if {$TextStyle::tagorfont($option) == "font"} {
                                #font
                                set fontOption [list $option $value]
        #                               puts "fontOption: $fontOption"
                                
                                # add the fonts to the font list
                                lappend newfont $fontOption
                                
                        } elseif {$TextStyle::tagorfont($option) == "tag"} {
                                #tag
                                if {[catch {$text tag configure $tagname -$option $value} result]} {
                                                putsstderr "Error: $result"
                                                continue;
                                }
                        }
                }
                
                if {$newfont != {}} {
                        if {[::WordProcess::checkfontexits $newfont] == 0} {
                                font create $newfont -family $::textEditorMain::defaultFont -size $::textEditorMain::defaultSize
                                foreach newoptionvalue $newfont {
                                        set newoption [lindex $newoptionvalue 0]
                                        set newvalue [lindex $newoptionvalue 1]
        #                                       puts "newoptionvalue $newoptionvalue - newoption $newoption - newvalue $newvalue"
                                        if {[catch {font configure $newfont -$newoption $newvalue} result]} {
                                                putsstderr "Error: font configure- $result"
                                                continue;
                                        }
                                }
                        }
                        $text tag configure $tagname -font $newfont
                }
        }
        
        proc getAttr {inputList attrName} {
                if {$inputList == "" || $attrName == ""} {return "";}
                
                foreach item $inputList {
                        if {[string first ":" $item] < 0} {return;}
                        foreach {key value} [split $item ":"] break;
                        if {$attrName eq $key} {return $value}
                }
                return "";
        }
        
        proc checkfontexits {checkfontname} {
                set fontexists 0
                
                if {$checkfontname != ""} {
                        foreach fontname [font names] {
                                #puts "$checkfontname == $fontname"
                                if {$checkfontname == $fontname} {
                                        set fontexists 1
                                        break
                                }
                        }
                }
        #       puts "Font allready defined $fontexists"
                return $fontexists
        }
        
        
        # saveSelToFile --
        #    Save the current selected text to xml file
        # Arguments:
        #    name      Name of the input window
        #        startIdx: start position of content (default: 1.0)
        #        endIdx:   end position of content (default: end)
        # Returns:
        #    None
        #
        proc saveSelToFile {name {filename ""} {startIdx 1.0} {endIdx end}} {
                        if {[catch { set outfile [open $filename "w"] } result] } {tk_messageBox -message $result ;return;}
                #puts $filename
                fconfigure $outfile -encoding utf-8
                set html ""
                set htmlHeader ""
                set htmlContent ""
                
                # get xml body
                set bodyValue [::WordProcess::getSelToXML $name.text "" $startIdx $endIdx]
                
                set teiBody ""
                # body elment
                # new teibody
                append teiBody "<body><list><item n='main'>"
                append teiBody $bodyValue
                append teiBody "</item></list></body>"
                
                set teiHead [::WordProcess::getXMLHeader]
                
                # get html body
                # change xml body to html body content without <body> tag
                set htmlBody [::WordProcess::TEItoHTML $teiBody]
                # new teibody
                set htmlBody [string map {"<body>" "" "</body>" ""} $htmlBody ]
                
               
        #       append htmlContent "\<html\>"
        #       #html head
        #       append htmlContent "\<head\>"
        #       append htmlContent "\<title\>HTML CLIPBOARD TEMP(Tcl/Tk)\</title\>"
        #       append htmlContent "\</head\>"
        #       
        #       #Html
        #       append htmlContent "<body>" 
        #       append htmlContent "<!--StartFragment-->"
                
                set tempHtmlContent ""
                
                set htmlToStartFragValue ""
                
                #HTML
                append htmlToStartFragValue "<html>"
                #html head
                append htmlToStartFragValue "<head>"
                append htmlToStartFragValue "<title>HTML CLIPBOARD TEMP(Tcl/Tk)</title>"
                append htmlToStartFragValue [::WordProcess::changeXMLHeadStyle2HTML $teiHead]
                append htmlToStartFragValue "</head>"
                
                #Html
                append htmlToStartFragValue "<body>" 
                append htmlToStartFragValue "<!--StartFragment-->"
                
                #set startFragment [::TEUtils::posDetermine $htmlContent 0]
                #set startFragment [string length $htmlContent]
                
                set htmlToStartFragLength [expr [string length [encoding convertto utf-8  $htmlToStartFragValue] ] -1 ]
                #puts "htmlToStartFragLength: $htmlToStartFragLength"
                
                append tempHtmlContent $htmlToStartFragValue
                append tempHtmlContent $htmlBody
                #puts "tempHtmlContent-1: $tempHtmlContent"
                set htmltoEndFragLength [expr [string length [encoding convertto utf-8  $tempHtmlContent ] ] - 1 ]
                #puts "htmltoEndFrag: $tempHtmlContent"
                #puts "htmltoEndFragLength: $htmltoEndFragLength"
                
                #set endFragment [::TEUtils::posDetermine $htmlContent 1]
                #set endFragment [string length $htmlContent]
                append tempHtmlContent "<!--EndFragment--></body></html>"
                
                set htmlLength [expr [string length [encoding convertto utf-8 "$tempHtmlContent" ] ] -1 ]
                #puts "htmlLength: $htmlLength vs [string length $tempHtmlContent] "
        
                
                #puts "endFragmentinPosDet: $endFragment"
        
                ################################
                # tempo head for windows
                ################################
                # html content length prediction
                set tempHtmlIdx [::TEUtils::lengthPrediction [encoding convertto utf-8 "$tempHtmlContent" ] ]
                #puts "tempHtmlIdx: $tempHtmlIdx"
                append htmlHeader "Version:1.0\n"
                append htmlHeader "StartHTML:$tempHtmlIdx\n"
                append htmlHeader "EndHTML:$tempHtmlIdx\n"
                append htmlHeader "StartFragment:$tempHtmlIdx\n"
                append htmlHeader "EndFragment:$tempHtmlIdx\n"
                append htmlHeader "SourceURL:file:///$filename\n"
                # htmlHeader for window's clipboard: version: StartHTML ....
                set headerLen [string length $htmlHeader]
                incr headerlen 6
                #puts "headerLen: $headerLen"
                
                
                set startHTML 0
                set endHTML 0
                set startFragment 0
                set endFragment 0
                
                # calculate the right index
                set startHTML [expr $headerLen + 1]
                set endHTML [expr $htmlLength + $headerLen]
                set startFragment [expr $htmlToStartFragLength + $headerLen + 1]
                set endFragment [expr $htmltoEndFragLength + $headerLen + 1]
                #puts "startHTML $startHTML endHTML $endHTML startFragment $startFragment endFragment $endFragment"
                
                set startHTML [::TEUtils::changeIndexForm $startHTML $tempHtmlIdx]
                set endHTML [::TEUtils::changeIndexForm $endHTML $tempHtmlIdx]
                set startFragment [::TEUtils::changeIndexForm $startFragment $tempHtmlIdx]
                set endFragment [::TEUtils::changeIndexForm $endFragment $tempHtmlIdx]
                
                # refresh with real indices
                set htmlHeader ""
                append htmlHeader "Version:1.0\n"
                append htmlHeader "StartHTML:$startHTML\n"
                append htmlHeader "EndHTML:$endHTML\n"
                append htmlHeader "StartFragment:$startFragment\n"
                append htmlHeader "EndFragment:$endFragment\n"
                append htmlHeader "SourceURL:file:///$filename\n"
                
                append html $htmlHeader $tempHtmlContent
                #puts "::WordProcess::saveSelToFile-html: $html"
                
                puts $outfile $html
                # Attention: Close the output Stream please!
                close $outfile
                
                
                global tcl_platform
                if {[string tolower $tcl_platform(platform)] == "windows"} {
                    set htmlFmt [twapi::register_clipboard_format "HTML Format"]
                    #puts $htmlFmt
                    
                    set html [encoding convertto utf-8 $html]
                                        # make sure clipboard clean
                    catch {twapi::empty_clipboard; twapi::close_clipboard}
                    # write clipboard (need open and close)
                    twapi::open_clipboard
                    #puts "-------clipboard opened-------"
                    twapi::empty_clipboard
                    #puts "::WordProcess::saveSelToFile.clipboardHtml: $html"
                    twapi::write_clipboard $htmlFmt $html
                    
                    # this is for normal copy
                    set textValue [$name.text get $startIdx $endIdx]
                    twapi::write_clipboard_text $textValue 
                    #puts "-------clipboard writed-------"
                    twapi::close_clipboard
                    #puts "-------clipboard closed-------"  
                } elseif {[string tolower $tcl_platform(platform)] == "unix" && [string tolower $tcl_platform(os)] == "darwin" } {
										set ccontent "<body>$htmlBody</body>"	
            		    set cpipe [open "| textutil -inputencoding utf8 -stdin -format html -convert rtf -stdout | pbcopy" w ]
            		    puts $cpipe $ccontent
            		    close $cpipe
                } else {
                    clipboard clear 
                    set ccontent "<body>$htmlBody</body>"
                  
                    clipboard append [regsub -all {(<[^<>]+>)|(</[^<>]+>)|(<[^<>]+/>)} $htmlBody {}]
                  
                    set clist ""
                    foreach c [split $ccontent ""] {
                      set cs [scan $c %c]
                      set cs [format %x $cs]
                      #puts "$c - $cs"
                      append clist "0x$cs"
                      append clist " "
                    }
                  
                    append clist "0x0"
                  
                    clipboard append -type text/html $ccontent
                  
                }
        }
        
        
        # SaveContentsToXMLW --
        #    Save the current text to xml file
        # Arguments:
        #    name      Name of the input window
        #    filename  save the text in the file name
        #        dataType  the file saving as xml, html or tex. (XML / HTML / TEX)
        #        startIdx: start position of content (default: 1.0)
        #        endIdx:   end position of content (default: end)
        # Result:
        #    None
        #
        proc SaveContentsToXMLW {name filename dataType {startIdx 1.0} {endIdx end}} {
                if {[catch { set outfile [open $filename "w"] } result] } {tk_messageBox -message $result ;return;}
                fconfigure $outfile -encoding utf-8
        #       set src [$name.text dump -all 1.0 end]
                
                set xmlValue ""
                set teiBody ""
                set teiHead ""
                
                # 1. get the xml value
                append xmlValue "\<\?xml version=\"1.0\" encoding=\"UTF-8\"\?\>"
                append xmlValue "<TEI>"
                
                # body first, then head.
                #body
                set bodyValue [::WordProcess::getXMLBody $name.text "" $startIdx $endIdx]
                #puts "SaveContentsToXML.xmlBodyValue: $bodyValue"
                # new teibody
                append teiBody "<body><list><item n='main'>"
                append teiBody $bodyValue
                append teiBody "</item></list></body>"
                
                #head
                set teiHead [::WordProcess::getXMLHeader]
                
                append xmlValue $teiHead
                append xmlValue $teiBody
                append xmlValue "</TEI>"
                
                #puts "SaveContentsToXMLW.xmlValue $xmlValue"
                # 2. if type is html, then change xml value to html.
                if {[string equal -nocase $dataType "html"]} {
                        set htmlValue ""
                        set htmlHead ""
                        set htmlBody ""
                        append htmlValue "\<html xmlns=\"http://www.w3.org/1999/xhtml\"\>"
                        
                        #head
                        append htmlHead "\<head xmlns=\"http://kompetenzzentrum.uni-trier.de/\"\>"
                        append htmlHead "\<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/\>"
                        append htmlHead "\<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/\>"
                        append htmlHead "\<title\>Text Editor (Tcl/Tk)\</title\>"
                        append htmlHead [::WordProcess::changeXMLHeadStyle2HTML $teiHead]
                        append htmlHead "\</head\>"
                        
                        #body
                        set teiBody [::WordProcess::TEItoHTML $teiBody]
                        append htmlBody "<body>"
                        append htmlBody $teiBody
                        append htmlBody "</body>"
                        
                        append htmlValue $htmlHead
                        append htmlValue $htmlBody
                        
                        # html end
                        append htmlValue "</html>"
                        
                        puts $outfile $htmlValue
                        close $outfile
                        
                } elseif {[string equal -nocase $dataType "tex"]} {
                        set texValue {
\nonstopmode
\documentclass[10pt,a4paper]{article}
                   
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
   
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}                          
                                        
                                 }
                        append texValue [TexPackages]
                        append texValue \n
                        append texValue [::WordProcess::changeXMLHeadStyle2Tex $teiHead]
                        append texValue \n
                        append texValue "\\begin{document}" \n
                        # new teibody 
                        set teiBody "<body><list><item n='main'>"
                        append teiBody $bodyValue
                        append teiBody "</item></list></body>"
                        append texValue [::WordProcess::TEItoTEX $teiBody]
                        append texValue \n "\\end{document}" 
                        puts $outfile $texValue
                        close $outfile
                                
                } elseif {[string equal -nocase $dataType "rtf"]} {
                  
                        set rtfHead [::WordProcess::changeXMLHeadStyle2Rtf $teiHead]
                        # new teibody
                        set teiBody "<body><list><item n='main'>"
                        append teiBody $bodyValue
                        append teiBody "</item></list></body>"
                        set rtfBody [::WordProcess::TEItoRTF $teiBody]
                        set colorTable {}
                        set colorTableString ""
                        if {[info exists ::rtfParser::rtfColorTable]} {
                            foreach colorname [array names ::rtfParser::rtfColorTable] {
                              append colorTable $colorname ";"
                            }
                            set colorTableString "\{\\colortbl;$colorTable\}"
                          
                        }
                        set rtfValue \{\\rtf1
                  
                        append rtfValue  "\{\\fonttbl \{\\f0 Arial;\}\{\\f1 Courier;\}\{\\f2 Times;\} \}"
                        append rtfValue  $colorTableString
                        append rtfValue  $rtfHead
                        append rtfValue  $rtfBody
                  
                        append rtfValue  \}
                    
  
                        puts $outfile $rtfValue
                        close $outfile
                  
                } elseif {[string equal -nocase $dataType "xml"]} {
                        puts $outfile $xmlValue
                        close $outfile
                }
        }
        
        # TEItoHTML --
        #    Change the given xml contents to the html contents with package htmlparse.
        #
        # Arguments:
        #    bodyValue       the given xml contents
        #
        # Result:
        #    Returns the processed html contents.
        proc TEItoHTML {bodyValue} {
                ::TEUtils::refreshStatus
                # clean the html content
                set bodyValue [::TEUtils::cleanWhitespace $bodyValue]
                #puts "TEItoHTML.bodyValue: $bodyValue"
                
                # parse html content 
                # key proc: parseCommend
                htmlparse::parse -cmd ::WordProcess::xmlToHtmlParse $bodyValue
                
                set htmlBody $textEditorMain::xmlData(main,bodyText)
                
                #puts "TEItoHTML.bodyText: $htmlBody"
                
                # clean the xml datas
                ::TEUtils::refreshStatus
                
                return $htmlBody
        }
        
        
        proc changeXMLHeadStyle2HTML {headValue} {
                ::TEUtils::refreshStatus
                
                # clean the database options
                ::TEDB::resetPreset
                
                # parse html content 
                # key proc: parseCommend
                htmlparse::parse -cmd ::WordProcess::xmlToHtmlParse $headValue
                
                set htmlHead $textEditorMain::xmlData(main,headText)
        
                return $htmlHead
        }
        
        
        proc parseXmlToHtml {xmlContent} {
                ::TEUtils::refreshStatus
                # clean the database options
                ::TEDB::resetPreset
                set xmlContent [::TEUtils::cleanWhitespace $xmlContent]
                
                htmlparse::parse -cmd ::WordProcess::xmlToHtmlParse $xmlContent
                
                set htmlHead $textEditorMain::xmlData(main,headText)
                set htmlBody $textEditorMain::xmlData(main,bodyText)
                
                ::TEUtils::refreshStatus
        }
        
        
        ############################################################
        # Texteditor Tex functions
        ############################################################
        
        # TEItoTex --
        #    Change the given xml contents to the html contents with package htmlparse.
        #
        # Arguments:
        #    bodyValue       the given xml contents
        #
        # Result:
        #    Returns the processed html contents.
        proc TEItoTEX {bodyValue {pid 0}} {
                variable ::texParser::tex
                variable ::texParser::current
                set oldcurrent $current
                set current $pid
                refreshTex
                # clean the html content
                set bodyValue [::TEUtils::cleanWhitespace $bodyValue]
                #puts "TEItoHTML.bodyValue: $bodyValue"
                
                
                
                # parse html content 
                # key proc: parseCommend
                htmlparse::parse -cmd ::WordProcess::xmlToTexParse $bodyValue
                
                set texBody $tex($current,Text)
                set texBody [string map {\n\\\\ \n\\hbox\{\}\\\\ } $texBody]
                
                #puts "TEItoHTML.bodyText: $htmlBody"
                
                # clean the xml datas
                refreshTex
                set current $oldcurrent
                
                return $texBody
        }
        
        proc changeXMLHeadStyle2Tex {headValue} {
                variable ::texParser::tex
                variable ::texParser::current
                # clean the database options
                set tex(headText) ""
                set tex(inHead) 0
                set oldcurrent $current
                set current head
                
                # parse html content 
                # key proc: parseCommend
                #puts "changeXMLHeadStyle2Tex.$headValue"
                
                refreshTex
                
                htmlparse::parse -cmd ::WordProcess::xmlHeadToTex $headValue
                
                refreshTex
                
                set current $oldcurrent
                
                set texhead $tex(headText)
        
                set tex(headText) ""
                set tex(inHead) 0
                
                return $texhead
        }
        
        proc refreshTex {} {
                  variable ::texParser::tex
                  variable ::texParser::current
                  if {[info exists current]} {
                          set tex($current,Text) ""
                          set tex($current,currentAttrs) ""
                          set tex($current,stylesopen) 0
                          set tex($current,specialtag) ""
                          set tex($current,specialend) ""
                          set tex($current,firstcell) 1 
                          set tex($current,inTag) 0
                          set tex($current,inRend) 0
                          set tex($current,singleTag) ""
                          set tex($current,centering) "" 
                          set tex($current,raggedright) "" 
                          set tex($current,tkspacing1) "" 
                          set tex($current,tkspacing2) "" 
                          set tex($current,tkspacing3) "" 
                  }
        }
        
        
        # type: tei/html
        proc checkHeadStatusForTex {tag slash type} {
                variable ::texParser::tex
                
                switch -nocase -regexp -matchvar XYZ $type {
                        (tei|xml) {set headTagName teiHeader}
                        (html|xhtml) {set headTagName head}
                        default {set headTagName ""}
                }
                if {$headTagName == ""} {set tex(inHead) 0; return;}
                
                set headTag [string equal -nocase $tag $headTagName]
                set endTag [string equal -nocase $slash "/"]
                
                if {$tex(inHead) == 0 && $headTag == 1 && $endTag == 0} {
                        # go into head
                        # start tag
                        set tex(inHead) 1
                } elseif {$tex(inHead) == 1 && $headTag == 1 && $endTag == 1} {
                        # end tag
                        set tex(inHead) 2
                } elseif {$tex(inHead) == 2 && $headTag == 0} {
                        # out of head
                        set tex(inHead) 0
                }
        }
        
        
        proc xmlHeadToTex {tag slash param text} {
                variable ::texParser::tex
                variable ::texParser::texNewCommand
                variable ::texParser::current
        
                set isTagEnd [string equal $slash "/"]
        
                switch -nocase $tag {
                        rendition {
                                # text must be changed
                                if {$isTagEnd} {
                                        append tex(NewCommandDefinition) "\#1"
                                        #CONSIDER: some other method to ending the tag
                                        while {$tex($current,stylesopen) > 0} {
                                                #if {$tex($current,stylesopen) > 1} {
                                                #       puts "tag $tag  $tex($current,Text)"
                                                #}
                                          
                                          append tex(NewCommandDefinition) "\}"
                        
                                          incr tex($current,stylesopen) -1
                                        }

                                        append tex(NewCommandDefinition) "\}"
                                        set newCommandName $tex(NewCommandName)
                                        if {![info exists texNewCommand($newCommandName)]} {
                                          append tex(headText) $tex(NewCommandDefinition) \n
                                          set texNewCommand($newCommandName) 1
                                        }
                                } else {
                                        set tex(NewCommandName) [styleNametoTexCommand [getAttrValues $param xml:id]]
                                        set tex(NewCommandDefinition) ""
                                        append tex(NewCommandDefinition) "\\newcommand\{"
                                        append tex(NewCommandDefinition) \\ $tex(NewCommandName) \}\[1\]\{
                                        append tex(NewCommandDefinition) [xmlParamToTex $tag rend=[concat [::TEUtils::backToBracket $text]] rend ]
                                }                       
                        }
                }
        }
        
        
        
        proc texCharConvert {text} {
                set text [string map {&apos; \'
                                      &lt; <
                                      &gt; >
                                      &amp; \&
                                } $text ]
                                set indices {}
                                set start 0             
                                while {[regexp -indices {&#x[0-F]+;} $text indices]} {
                                   set s [lindex $indices 0]
                                   set e [lindex $indices 1]
                                   #puts "$s $e of indices $indices"
                                   set temp $text
                                   set text ""
                                   if {$s > 0} {
                                         append text [string range $temp 0 [expr $s - 1]]
                                   }
                                   set unicode [string range $temp [expr $s + 3] [expr $s + 6]] 
                                   #puts "was: $unicode $temp"
                                   if {$unicode == "007B" } {
                                         set unicode \{
                                   } elseif  {$unicode == "007D" } {
                                         set unicode \}
                                   } else {
                                         set unicode [binary format H* $unicode]
                                   }
                                   
                                   set unicode [encoding convertfrom unicode $unicode]
                                   append text $unicode
                                   append text [string range $temp [expr $s + 8] end]
                                   puts " unicode: $unicode $text"
                                   set indices {}
                                }
                                # http://de.wikipedia.org/wiki/Anf%C3%BChrungszeichen
                                set speciallatex {
                                        "&#123;" "\\{"  
                                        "&#125;" "\\}" 
                                        \u2018 \\lq\{\}
                                        \u2019 \\rq\{\}
                                        \u201A \\glq\{\}
                                        \u201B \\grq\{\}
                                        \u201C \\grqq\{\}
                                        \u201D \\rdq\{\}
                                        \u201E \\glqq\{\}
                                        \#      \\#
                                        \{      \\{
                                        \}      \\}
                                        \'      \\'
                                        \%      \\%
                                        \$      \\$
                                        \&      \\&
                                        \\      \\textbackslash\{\}
                                        ^       \\textasciicircum\{\}
                                        _       \\_
                                        ~       \\textasciitilde\{\}
                
                                }
                                
                                set text [string map $speciallatex $text]
                                return $text
        }
        
        
        # xmlToTexParse -- 
        #       change xml to tex contents.
        #       (need refreshstatus, xins array clear)
        #
        # Arguments:
        #        args:  tag slash param text
        #
        # Return:
        #        none
        proc xmlToTexParse {args} {
                variable ::texParser::tex
                variable ::texParser::current
                # useful tags
                foreach {tag slash param text} $args {
                        # make sure all tag name is low case
                        set tag [string tolower $tag]
                        set isTagEnd [string equal $slash "/"]
                        
                        if {$tag == "hmstart"} {continue}
                        if {$tag == "body"} {continue}
                        if {$tag == "list"} {continue}
                        # new teibody
                        if {$tag == "item" && !$isTagEnd && [string first "n='main'" $param] && $text == ""} {continue}
                  
                        set text [texCharConvert $text]

                        xmlToTexParseTexteditorElement $tag $slash $param $text                 
                }
        }
        
        
        proc xmlToTexParseTexteditorElement {tag slash param text} {
                variable ::texParser::tex
                variable ::texParser::current
                
                #puts "=> tag= $tag - slash= $slash - param= $param - text= $text - current $current"
                # end with "/" in the slash-parameter
        
                set isTagEnd [string equal $slash "/"]
                set tex($current,currentAttrs) ""
                set tex(noTextOutput) 0
        
                # end tag and check the current tag
                if {$isTagEnd } {
                        # new teibody
                        if {$tag == "item"} {
                              # end of item element is end of text body
                              foreach lineformat {centering raggedright tkspacing1 tkspacing2 tkspacing3} {
                                  if {$tex($current,$lineformat) != "" } {
                                    
                                      append tex($current,Text) "\\end\{" $tex($current,$lineformat) "\}"
                                     
                                  } 
                                  set tex($current,$lineformat) ""
                              }
                             
                        }
                        incr tex($current,inTag) -1
                        if {$tag == "lb"} {
                           append [string trimright tex($current,Text) \n ] "\\newline\n" 
                        } elseif {($tag== "ref" || $tag == "anchor")  && $tex($current,singleTag) == "b" } {
                          # type b is now closed
                          set tex($current,singleTag) ""
                        } elseif {$tag== "table" | $tag=="row" | $tag == "cell"} {
                
                                if {$tag == "table"} {
                                        append tex($current,Text) "\\end\{tabular\}\n"
                                } elseif {$tag=="row"} {
                                        append tex($current,Text) "\\\\\n"
                                } elseif {$tag == "cell"} {
                                         
                                }
                        } else {
                                # $tag if applicable
                                while {$tex($current,stylesopen) > 0} {                                        
                                        #if {$tex($current,stylesopen) > 1} {
                                        #       puts "tag $tag $tex($current,Text)"
                                        #}
                                  append tex($current,Text) "\}"
                                  incr tex($current,stylesopen) -1
                                }
                                if {$tag == "p"} {
                                  #append tex($current,Text) "\}" 
                                  append tex($current,Text) "\n" 
                                  ::texParser::setinPTag 0
                                }
                                if {$tag == "p" || $tag == "hi" } {
                                  incr tex($current,inRend) -1
                                }
                                if {$tex($current,specialtag) == "ref" || $tex($current,specialtag) == "anchor"} {
                                  append tex($current,Text) $tex($current,specialend)
                                  set tex($current,specialend) ""
                                }
                        }
                        foreach lineformat {centering raggedright tkspacing1 tkspacing2 tkspacing3}  {
                          if {($tag == "lb" || $tag == "p") && $tex($current,inRend) == 0 && $tex($current,$lineformat) != "" } {
                             append tex($current,Text) "\\end\{" $tex($current,$lineformat) "\}"
                             set tex($current,$lineformat) ""
                          }
                        }
                        append tex($current,Text) $text
                } else {
                        incr tex($current,inTag)
                        # new teibody
                        if {$tag == "item" && [string first "n='main'" $param] >=0 } {
                          set tex($current,inTag) 0
                        }
                        if {$tag == "lb"} {return;}
                        # clean the \t\n\r\v\f
                        set param [::TEUtils::cleanWhitespace $param]
                        if {$tag=="p" | $tag=="hi" | $tag == "ref" | $tag == "anchor"} {
                                # <p>, </p>
                                set attrKey "rend"
                                set attrKey2 ""
                                if {$tag == "ref" || $tag == "anchor"} {
                                        if {[string first "target=" $param] >=0} {
                                                set attrKey "target"
                                        } elseif {[string first "ana=" $param] >=0} {
                                                set attrKey "ana"
                                        } elseif {[string first "n=" $param] >=0} {
                                                set attrKey "n"
                                        }
                                        if {[string first "type=" $param] >=0} {
                                                # the html parser parses <anchor/> as <anchor></anchor> we remember this was only a _b_egin tag and next closure has to be skipped
                                                set tex($current,singleTag) b
                                        }
                                } elseif {$tag=="p"} {
                                        #set attrKey ""
                                        if {[string first "rendition=" $param] >= 0} {
                                                set attrKey "rendition"
                                                incr tex($current,inRend) 1
                                        }
                                        # In P Tag set 
                                        ::texParser::setinPTag 1
                                } elseif {$tag == "hi"} {
                                        # more styles in <hi> element
                                        if {[string first "rendition=" $param] >= 0} {
                                                set attrKey "rendition"
                                        }
                                        if {[string first "rend=" $param] >=0 } {
                                                if {$attrKey == "rendition" } { 
                                                   set attrKey "rend"
                                                   set attrKey2 "rendition"
                                                }
                                        }
                                        # has always rend
                                        incr tex($current,inRend) 1
                                }
                                #  attr=rend;(table) rows,cols,role
                                set param [xmlParamToTex $tag $param $attrKey] ;#removed trim
                                if {$attrKey2 != ""} {
                                  append param [xmlParamToTex $tag $param $attrKey2] ;#removed trim
                                  
                                  #puts "xmlToTexParseTexteditorElement $param"
                                }
        
                                #TODO: addParamToArrList
                                # hi rend, table rows cols, row role, cell,
                                set tex($current,currentAttrs) $param
                         
                                
                        } elseif {$tag== "table" | $tag=="row" | $tag == "cell"} {
                                
                                if {$tag == "table"} {
                                        # set table's attribute: rows / columns count
                                        #set rowsAttr [string trim [xmlParamToTex $param rows ]]
                                        set colsAttr [string trim [xmlParamToTex $tag $param cols ]]
                                        incr colsAttr 0
                                        
                                        append tex($current,Text) \\begin\{tabular\}\{
                                        append tex($current,Text) |
                                        for {set i 1} {$i <= $colsAttr} {incr i} {
                                          append tex($current,Text) l
                                          append tex($current,Text) |
        
                                        }
                                        append tex($current,Text) \}\n
                                        
                                        
                                        
                                } elseif {$tag=="row"} {
                                        # role attribute for future
                                        #set roleAttr [string trim [xmlParamToTex $param role ::TextStyle::xmlHtmlMap]]
                                        #set xmlData(main,currentAttrs) $roleAttr
                                        #set tex($current,currentAttrs) ""
                                        #
                                        set tex($current,firstcell) 1
                                } elseif {$tag == "cell"} {
                                        #puts "width: attribut in cell"
                                        #set param [string trim [xmlParamToTex $param style ]]
                                        #set tex($current,currentAttrs) $param
                                        #set tag "td"
                                        if {$tex($current,firstcell) == 0} {
                                          append tex($current,Text) "\&"
                                        }
                                        set tex($current,firstcell) 0
                                }
                        }
                        #puts "@formats $tex($current,currentAttrs)"
                        foreach lineformatkey {centering raggedright tkspacing1 tkspacing2 tkspacing3} {
                          set formats $tex($current,currentAttrs)
                          set lineformat "";
                          set startlineformat [string first "\\$lineformatkey" $formats ]
                          if {$startlineformat == -1} {
                            continue
                          }
                          # get position after "\"
                          incr startlineformat
                          append lineformat ""
                          if {$lineformatkey == "centering"} {
                             append lineformat "center"
                             #puts  "lineformat center "
                          }
                          if {$lineformatkey == "raggedright"} {
                             append lineformat "flushright"
                          }
                          if {$lineformatkey == "tkspacing1" || $lineformatkey == "tkspacing2" || $lineformatkey == "tkspacing3"} {
                             append lineformat "spacing"
                             set endlineformat [string first "\}" $formats [expr $startlineformat + 10 ] ]
                             # 10 = tkspacing1\} 
                             set lineformat [string range $formats $startlineformat [expr $endlineformat - 1] ]
                          }
                                                   
                          #puts "compare $tex($current,$lineformatkey) != $lineformat"
                          if {$tex($current,$lineformatkey) != $lineformat} {
                            
                            if {$tex($current,$lineformatkey) == "" } {
                              
                                append tex($current,Text) "\\begin\{" $lineformat "\}"
                               
                            } 
                            
                            if {$lineformat == "" } {
                                                                       
                                append tex($current,Text) "\\end\{" $lineformat "\}"
                                                                        
                            } 
                            
                          }
                          
                          #puts "textalign $textalign"
                          set tex($current,$lineformatkey) $lineformat
                        }
                        if {$tex($current,currentAttrs) != ""} {
                                append tex($current,Text) $tex($current,currentAttrs)
                        }
                        #append tex($current,Text) \{                  
                        
                        # add the text and current tag to the ::textEditorMain::xmlData(main,bodyText)
#                       if {$text != "" & [string trim $text] != "" }
                        if {$text != ""} {
                          if {!$tex(noTextOutput)} {
                                  #set text [UTF8FullCodes $text 1]       
                                  #set text [mapXMLToHTML $text]
                                  # get the last style values
                                  append tex($current,Text) $text
                          } else {
                                  #append tex($current,Text) "SUPRESSED $text "
                          }
                        }
                        #puts $tex($current,Text)
                }
        }
        
        
        
        
        # xmlParamToTex -- 
        #       change xml attribute to html attribute.
        #       e.g.  rend="overstrike:1;underline:1" -> style='text-decoration: underline overstrike'
        # Arguments:
        #        param:         parameters of xml content (e.g. rend="overstrike:1;underline:1")
        #        attrName:      attribute key word (e.g. rend, rows, cols, role)
        #        mapArr:        look up table for change attributes
        #
        # Return:
        #        Returns the processed html attributes
        proc xmlParamToTex {tag param attrName} {
                global notextEditorstandalone
                variable ::texParser::tex
                variable ::texParser::current
                #puts "xmlParamToTex $tag $param $attrName current: $current"
                if {$param == ""} {return "";}
                
                # xmltohtml attrName rend
                set styleList [getAttrValues $param $attrName]
                if {$styleList == ""} {return ""}
                
                set inputList [split $styleList ";"]
                
                set texStyles ""
                
                if {$attrName == "rend"} {
                        set styles ""
                        foreach style $inputList {
                                #puts $style
                        
                                #e.g. overstrike:1 size:14 foreground:#00ffff
                                if {[catch {regexp {([^:]+):(.+?)} $style match key value} result]} {
                                        putsstderr "Error: $result"
                                        continue;
                                }
                                
                                if {[TEDB::isOutputFilter $key $value]} {
                                  continue;
                                }
                                
                                if {$key == "overstrike"} {
                                        lappend styles "\\sout\{"
                                } elseif {$key == "underline"} {
                                        lappend styles "\\underline\{"
                                } elseif {$key == "slant"} {
                                        lappend styles "\{\\\itshape "
                                } elseif {$key == "weight"} {
                                        lappend styles "\\textbf\{"
                                } elseif { $key == "size" } {
                                        set styleKey "\\fontsize"
                                        # add "px" to the size value
                                        set sizeValue $value
                                        append sizeValue pt
                                        lappend styles "$styleKey\{$sizeValue\}\{$sizeValue\}\{ \\selectfont "
                                } elseif {$key == "foreground"  || $key == "background" } {
                                        if {$key == "foreground"} {
                                           set styleKey "\\textcolor"
                                        }
                                        if {$key == "background"} {
                                           set styleKey "\\colorbox"
                                        }
                                           set colorValue "\{$value\}\{"
                                           # Try RGB
                                           catch {
                                         set colorValueRed [expr 0x[string range $value 1 2]] 
                                         set colorValueGreen [expr 0x[string range $value 3 4]] 
                                         set colorValueBlue [expr 0x[string range $value 5 6]]
                                         set colorValue "\[RGB\]\{$colorValueRed,$colorValueGreen,$colorValueBlue\}\{"
                                           }
                                   lappend styles $styleKey$colorValue
                                } elseif {$key == "offset"} {
                                        set alignValue ""
                                        if {$value == "-4"} {
                                                set alignValue "\\textsubscript\{" 
                                        } elseif {$value == "0"} {
                                                set alignValue ""
                                        } elseif {$value == "4"} {
                                                set alignValue "\\textsuperscript\{"
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set alignValue ""
                                        }
                                        if {$alignValue != ""} {
                                          lappend styles $alignValue
                                        }
                                } elseif {$key == "family"} {
                                        set familyValue ""
                                        if {$value == "Times"} {
                                                set familyValue "\\textsl\{" 
                                        } elseif {$value == "Courier"} {
                                                set familyValue "\\texttt\{"
                                        } elseif {$value == "Helvetica"} {
                                                set familyValue "\\textsf\{"
                                        } elseif {$value == "smallcaps"} {
                                                #not in GUI
                                                set familyValue "\\textsc\{"
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set familyValue "\\textsc\{"
                                        }
                                        lappend styles $familyValue
                                } elseif {$key == "justify"} {
                                        set justifyValue ""
                                        if {$value == "right"} {
                                                set justifyValue "\{\\raggedright\{\}" 
                                        } elseif {$value == "center"} {
                                                set justifyValue "\{\\centering\{\}" 
                                        } elseif {$value == "left"} {
                                                set familyValue "\{\\raggedleft\{\}" 
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set familyValue "\{\\raggedleft " 
                                        }
                                        lappend styles $justifyValue
                                } elseif {[string first "spacing" $key] >=0} {
                                        set distValue [::TEUtils::getValidTexSpacing $value]
                                        set spacingn [string range $key end end]
                                        set spacingn [string map {1 A 2 B 3 C} $spacingn]
                                        set styleKey "\{\\tkspacing${spacingn}\{$distValue\}"
                                        # add "px" to the size value
                                        lappend styles $styleKey
                                } else {
                                        #TODO: justify, bgstipple, fgstipple, borderwidth, width, height
                                }       
                        }
                        
                        #append htmlStyles "style='" $styles "'"
                        append texStyles [join $styles "" ]
                        
                        incr tex($current,stylesopen) [llength $styles]
                        #puts "formats: $tex($current,stylesopen) of $styles"
                        #if {$tex($current,stylesopen) > 1} {
                        #       puts [llength $styles] 
                        #
                        #}
                } elseif {$attrName == "rows" | $attrName == "cols"} {
                        if {![string is integer $styleList]} {
                                putsstderr "$attrName has not a valid integer Number ($styleList)!"
                                return $htmlStyles;
                        }
                        set num $styleList
                        append texStyles $num
                } elseif {$attrName == "role" } {
                        append texStyles $attrName data
                } elseif {$attrName == "target" || $attrName == "ana"} {
                        # link system to html <a href="www.uni-trier.de">
                        set speciallink ""
                        if {[info exists notextEditorstandalone]} {
                                #puts "Execute special link procedure"
                                set speciallink [toTXTspeciallink $styleList $param]
                        }
                        if {$speciallink == "return"} {
                          return;
                        } elseif  {$speciallink != ""} {
                          append texStyles $speciallink
                        } else {
                          append texStyles "\\href\{" $styleList "\}\{"
                          incr tex($current,stylesopen) 1
                          #puts "incr tex($current,stylesopen) $tex($current,stylesopen) 1"
                        }
                        
                } elseif {$attrName == "n"} {
                        # link system to html <a href="www.uni-trier.de">
                        append texStyles "\\label\{" $styleList "\}"
                        #incr tex($current,stylesopen) 0
                } elseif { $attrName == "rendition"} {
                # clean #f
                        if {[string index $styleList 0] == "#"} {
                                set styleList [string range $styleList 1 end]
                        }
                        set styleList [styleNametoTexCommand $styleList]
                        append texStyles "\\$styleList\{"
                        # Xin 28.10.2014
                        #if {$tag != "p"} {incr tex($current,stylesopen) 1}
                        # MB 03.03.2015
                        incr tex($current,stylesopen) 1
                } 
                return $texStyles;
        }
        
        
        
                
        proc styleNametoTexCommand {name} {
                set map {- ""
                        \\_ ""
                        1 One
                        2 Two
                        3 Three
                        4 Four
                        5 Five
                        6 Six
                        7 Seven
                        8 Eight
                        9 Nine
                        0 Zero    
                }

                set name [string map $map $name]
                regsub -all {[^A-Za-z]} $name "" name
                #puts "styleNametoTexCommand->$name"
                return $name
        }

        proc TexPackages {} {
          return {
\usepackage{color}
\usepackage{hyperref}
                                                                                
                                         
\usepackage{setspace}
                                   
\usepackage{fixltx2e}
            

\newcommand{\tkspacingA}[1]{\vspace{#1}}
\newcommand{\tkspacingB}[1]{\vspace{#1}}
\newcommand{\tkspacingC}[1]{\vspace{#1}}
          }
        }
        
                
        ######################
        # Texteditor Tex functions
        ######################
        
        ############################################################
        # Texteditor Rtf functions
        ############################################################
        
        # TEItoRTF --
        #    Change the given xml contents to the html contents with package htmlparse.
        #
        # Arguments:
        #    bodyValue       the given xml contents
        #
        # Result:
        #    Returns the processed html contents.
        proc TEItoRTF {bodyValue {pid 0}} {
                variable ::rtfParser::rtf
                variable ::rtfParser::rtfColorTable
                variable ::rtfParser::current
                set oldcurrent $current
                set current $pid
                refreshRtf
                # clean the html content
                set bodyValue [::TEUtils::cleanWhitespace $bodyValue]
                #puts "TEItoHTML.bodyValue: $bodyValue"
                
                
                
                # parse html content 
                # key proc: parseCommend
                htmlparse::parse -cmd ::WordProcess::xmlToRtfParse $bodyValue
                
                set rtfBody $rtf($current,Text)
                
                #puts "TEItoHTML.bodyText: $htmlBody"
                
                # clean the xml datas
                refreshRtf
                set current $oldcurrent
                
                return $rtfBody
        }
        
        proc changeXMLHeadStyle2Rtf {headValue} {
                variable ::rtfParser::rtf
                variable ::rtfParser::rtfColorTable
                variable ::rtfParser::current
                # clean the database options
                set rtf(headText) ""
                set rtf(inHead) 0
                set oldcurrent $current
                set current head
                
                # parse html content 
                # key proc: parseCommend
                #puts "changeXMLHeadStyle2Tex.$headValue"
                
                refreshRtf
                
                htmlparse::parse -cmd ::WordProcess::xmlHeadToRtf $headValue
                
                set rtfhead "\{\\stylesheet $rtf(headText)\}"
                                                          #no space here, causes Error 1071 
                
                #puts "RTFHEADER $rtfhead"
        
                set rtf(headText) ""
                set rtf(inHead) 0
                
                refreshRtf
                
                set current $oldcurrent
                
                return $rtfhead
        }
        
        proc refreshRtf {} {
                  variable ::rtfParser::rtf
                  variable ::rtfParser::rtfColorTable
                  variable ::rtfParser::current
                  if {[info exists current]} {
                          set rtf($current,Text) ""
                          set rtf($current,currentAttrs) ""
                          set rtf($current,stylesopen) 0
                          set rtf($current,specialtag) ""
                          set rtf($current,specialend) ""
                          set rtf($current,firstcell) 1 
                          set rtf($current,inTag) 0
                          set rtf($current,inRend) 0
                          set rtf($current,singleTag) ""
                          set rtf($current,qr) "" 
                          set rtf($current,qc) "" 
                          set rtf($current,sb) ""
                          set rtf($current,sa) "" 
                          set rtf($current,sl) ""
                  }
        }
        
        
        # type: tei/html
        proc checkHeadStatusRtf {tag slash type} {
                variable ::rtfParser::rtf
                
                switch -nocase -regexp -matchvar XYZ $type {
                        (tei|xml) {set headTagName teiHeader}
                        (html|xhtml) {set headTagName head}
                        default {set headTagName ""}
                }
                if {$headTagName == ""} {set rtf(inHead) 0; return;}
                
                set headTag [string equal -nocase $tag $headTagName]
                set endTag [string equal -nocase $slash "/"]
                
                if {$rtf(inHead) == 0 && $headTag == 1 && $endTag == 0} {
                        # go into head
                        # start tag
                        set rtf(inHead) 1
                } elseif {$rtf(inHead) == 1 && $headTag == 1 && $endTag == 1} {
                        # end tag
                        set rtf(inHead) 2
                } elseif {$rtf(inHead) == 2 && $headTag == 0} {
                        # out of head
                        set rtf(inHead) 0
                }
        }
        
        
        proc xmlHeadToRtf {tag slash param text} {
                variable ::rtfParser::rtf
                variable ::rtfParser::rtfStylesheet
                variable ::rtfParser::current
        
                set isTagEnd [string equal $slash "/"]
        
                switch -nocase $tag {
                        rendition {
                                # text must be changed
                                if {$isTagEnd} {
                                        append rtf(headText) " "
                                        #CONSIDER: some other method to ending the tag
                                        while {$rtf($current,stylesopen) > 0} {
                                                        #if {$rtf($current,stylesopen) > 1} {
                                                        #       puts "tag $tag  $rtf($current,Text)"
                                                        #}
                                                  
                                                        append rtf(headText) "\}"
                                
                                                incr rtf($current,stylesopen) -1
                                               
                                        }
                                        append rtf(headText) $rtf(styleName) ";\}"
                                } else {
                                        set styleName [styleNametoRtf [getAttrValues $param xml:id] ]
                                        set styleFormat [xmlParamToRtf $tag rend=[concat [::TEUtils::backToBracket $text]] rend 1] 
                                        set rtfStylesheet($styleName) $styleFormat
                                        append rtf(headText) \{
                                        #puts "fud:$rtf(styleName)"
                                        append rtf(headText) $styleFormat " "
                                        set rtf(styleName) $styleName
                                }                       
                        }
                }
        }
        
        
        
        proc rtfCharConvert {text} {
                set text [string map {&apos; \'
                                      &lt; <
                                      &gt; >
                                      &amp; \&
                                } $text ]
                                set indices {}
                                set start 0             
                                while {[regexp -indices {&#x[0-F]+;} $text indices]} {
                                   set s [lindex $indices 0]
                                   set e [lindex $indices 1]
                                   #puts "$s $e of indices $indices"
                                   set temp $text
                                   set text ""
                                   if {$s > 0} {
                                         append text [string range $temp 0 [expr $s - 1]]
                                   }
                                   set unicode [string range $temp [expr $s + 3] [expr $s + 6]] 
                                   #puts "was: $unicode $temp"
                                   if {$unicode == "007B" } {
                                         set unicode \{
                                   } elseif  {$unicode == "007D" } {
                                         set unicode \}
                                   } else {
                                         set unicode [binary format H* $unicode]
                                   }
                                   
                                   set unicode [encoding convertfrom unicode $unicode]
                                   append text $unicode
                                   append text [string range $temp [expr $s + 8] end]
                                   #puts " unicode: $unicode $text"
                                   set indices {}
                                }
                                # http://de.wikipedia.org/wiki/Anf%C3%BChrungszeichen
                                #set speciallatex {

                
                                #}
                                
                                #set text [string map $speciallatex $text]
          
                                set text [encoding convertfrom utf-8 $text]

                                set utext ""
                                foreach i [split $text ""] {
                                    scan $i %c c
                                    if {$c<256} {append utext $i} else {append utext \\u $c G }
                                    
                                }
          
                                return $utext
        }
        
        
        # xmlToRtfParse -- 
        #       change xml to html contents.
        #       (need refreshstatus, xins array clear)
        #
        # Arguments:
        #        args:  tag slash param text
        #
        # Return:
        #        none
        proc xmlToRtfParse {args} {
                variable ::rtfParser::rtf
                variable ::rtfParser::current
                # useful tags
                foreach {tag slash param text} $args {
                        # make sure all tag name is low case
                        set tag [string tolower $tag]                       
                        set isTagEnd [string equal $slash "/"]
                        
                        if {$tag == "hmstart"} {continue}
                        if {$tag == "body"} {continue}
                        if {$tag == "list"} {continue}
                        # new teibody
                        if {$tag == "item" && !$isTagEnd && [string first "n='main'" $param] >=0 && $text == ""} {continue}
                        
                        set text [rtfCharConvert $text]

                        xmlToRtfParseTexteditorElement $tag $slash $param $text                 
                }
        }
        
        
        proc xmlToRtfParseTexteditorElement {tag slash param text} {
                variable ::rtfParser::rtf
                variable ::rtfParser::current
                
                #puts "xmlToRtfParseTexteditorElement => tag= $tag - slash= $slash - param= $param - text= $text"
                # end with "/" in the slash-parameter
        
                set isTagEnd [string equal $slash "/"]
                set rtf($current,currentAttrs) ""
                
        
                # end tag and check the current tag
                if {$isTagEnd } {
                        # new teibody
                        if {$tag == "item"} {
                              # end of <item> element is end of body
                              foreach lineformat {qc qr sb sl sa}  {
                                if {$rtf($current,$lineformat) != "" } {
                                  
                                    append rtf($current,Text) "\}" ;#"LLLL"
                                   
                                } 
                                set rtf($current,$lineformat) ""
                              }
                        }
                        incr rtf($current,inTag) -1
                        if {$tag == "lb"} {
                           append [string trimright rtf($current,Text) \n ] "\\line\n" 
                        } elseif {($tag== "ref" || $tag== "anchor") && $rtf($current,singleTag) == "b" } {
                          # type b is now closed
                          set tex($current,singleTag) ""
                        } elseif {$tag== "table" | $tag=="row" | $tag == "cell"} {
                
                                if {$tag == "table"} {
                                       
                                } elseif {$tag=="row"} {
                                        append rtf($current,Text) \\row \n
                                } elseif {$tag == "cell"} {
                                        append rtf($current,Text) \\intbl\\cell \n
                                }
                        } else {
                                # $tag if applicable
                                while {$rtf($current,stylesopen) > 0} {
                                        #if {$rtf($current,stylesopen) > 1} {
                                        #       puts "tag $tag  $rtf($current,Text)"
                                        #}
                                  append rtf($current,Text) "\}" ;#"SSSS"
                                  incr rtf($current,stylesopen) -1
                                }
                                if {$tag == "p"} {
                                  append rtf($current,Text) "\\par" 
                                }
                                if {$tag == "p" || $tag == "hi" } {
                                  incr rtf($current,inRend) -1
                                }
                                if {$rtf($current,specialtag) == "ref" || $rtf($current,specialtag) == "anchor"} {
                                  append rtf($current,Text) $rtf($current,specialend)
                                  set rtf($current,specialend) ""
                                }
                        }
                        foreach lineformat {qc qr sb sl sa}  {
                          if {($tag == "lb" || $tag == "p") && $rtf($current,inRend) == 0 && $rtf($current,$lineformat) != "" } {
                             append rtf($current,Text) "\}" ;#"CCCC"
                             set rtf($current,$lineformat) ""
                          }
                        }
                        append rtf($current,Text) $text
                } else {
                        incr rtf($current,inTag) 
                        if {$tag == "lb"} {return;}
                        # clean the \t\n\r\v\f
                        set param [::TEUtils::cleanWhitespace $param]
                        if {$tag=="p" | $tag=="hi" | $tag == "ref" | $tag == "anchor"} {
                                # <p>, </p>
                                set attrKey "rend"
                                if {$tag == "ref" || $tag == "anchor"} {
                                        if {[string first "target=" $param] >=0} {
                                                set attrKey "target"
                                        } elseif {[string first "ana=" $param] >=0} {
                                                set attrKey "ana"
                                        } elseif {[string first "n=" $param] >=0} {
                                                set attrKey "n"
                                        }
                                        if {[string first "type=" $param] >=0} {
                                                # the html parser parses <anchor/> as <anchor></anchor> we remember this was only a _b_egin tag and next closure has to be skipped
                                                set rtf($current,singleTag) b
                                        }
                                } elseif {$tag=="p"} {
                                        #set attrKey ""
                                        if {[string first "rendition=" $param] >= 0} {
                                                set attrKey "rendition"
                                                incr rtf($current,inRend) 1
                                                
                                        }
                                        append rtf($current,Text) "\{\\pard"
                                        incr rtf($current,stylesopen) 1
                                } elseif {$tag == "hi"} {
                                        # more styles in <hi> element
                                        if {[string first "rend=" $param] >=0 } {
                                                set attrKey "rend"
                                        } elseif {[string first "rendition=" $param] >= 0} {
                                                set attrKey "rendition"
                                        }
                                        # has always rend
                                        incr rtf($current,inRend) 1
                                }
                                #  attr=rend;(table) rows,cols,role
                                set param [xmlParamToRtf $tag $param $attrKey 0] ;#removed trim
        
                                #TODO: addParamToArrList
                                # hi rend, table rows cols, row role, cell,
                                set rtf($current,currentAttrs) $param
                                
                        } elseif {$tag== "table" | $tag=="row" | $tag == "cell"} {
                                
                                if {$tag == "table"} {
                                        # set table's attribute: rows / columns count
                                        #set rowsAttr [string trim [xmlParamToRtf $param rows 0]]
                                        set colsAttr [string trim [xmlParamToRtf $tag $param cols 0]]
                                        incr colsAttr 0
                                  
                                        set rtf(curCols) $colsAttr
                                        
                                        append rtf($current,Text) \n
                                        
                                        
                                        
                                } elseif {$tag=="row"} {
                                        # role attribute for future
                                        #set roleAttr [string trim [xmlParamToRtf $param role ::TextStyle::xmlHtmlMap 0]]
                                        #set xmlData(main,currentAttrs) $roleAttr
                                        #set rtf($current,currentAttrs) ""
                                        #
                                  
                                        append rtf($current,Text) \\trowd\\trgaph144 \n
                                        for {set i 1} {$i <= $rtf(curCols)} {incr i} {
                                          append rtf($current,Text) \\cell x $i 000 \n        
                                        }
                                  
                                } elseif {$tag == "cell"} {
                                        #puts "width: attribut in cell"
                                        #set param [string trim [xmlParamToRtf $param style ]]
                                        #set rtf($current,currentAttrs) $param
                                        #set tag "td"
                                }
                        }
                        foreach lineformatkey {qc qr sb sl sa} {
                          set formats $rtf($current,currentAttrs)
                          set lineformat "";
                          set startlineformat [string first "\\$lineformatkey" $formats ]
                          if {$startlineformat == -1} {continue}
                          #get Position after backslah
                          incr startlineformat
                          append lineformat ""
                          if {$lineformatkey == "qc"} {
                             append lineformat "qc"
                          }
                          if {$lineformatkey == "qr"} {
                             append lineformat "qr"
                          }
                          if {$lineformatkey == "sb" || $lineformatkey == "sl" || $lineformatkey == "sa"} {
                             append lineformat $lineformatkey 
                             set endlineformat [string first " " $formats $startlineformat ]
                             set lineformat [string range $formats $startlineformat $endlineformat ]
                          }
                                                   
                    
                          if {$rtf($current,$lineformatkey) != $lineformat} {
                            
                            if {$rtf($current,$lineformatkey) == "" } {
                              
                                append rtf($current,Text) "\{\\" $lineformat ;#"AAAA"
                               
                            } 
                            
                            if {$lineformat == "" } {
                                                                       
                                append rtf($current,Text) "\}" ;#"AAAA"
                                                                        
                            } 
                            
                          }
                          
                          #puts "textalign $textalign"
                          set rtf($current,$lineformatkey) $lineformat
                        }
                  
                        if {$rtf($current,currentAttrs) != ""} {
                                append rtf($current,Text) $rtf($current,currentAttrs)
                        }
                        #append tex($current,Text) \{
        
                        
                        # add the text and current tag to the ::textEditorMain::xmlData(main,bodyText)
#                       if {$text != "" & [string trim $text] != "" }
                        if {$text != ""} {
                                #set text [UTF8FullCodes $text 1]       
                                #set text [mapXMLToHTML $text]
                                # get the last style values
                                append rtf($current,Text) $text
                        }
                        #puts $xmlData(main,bodyText)
                }
        }
        
        
        
        
        # xmlParamToRtf -- 
        #       change xml attribute to html attribute.
        #       e.g.  rend="overstrike:1;underline:1" -> style='text-decoration: underline overstrike'
        # Arguments:
        #        param:         parameters of xml content (e.g. rend="overstrike:1;underline:1")
        #        attrName:      attribute key word (e.g. rend, rows, cols, role)
        #        mapArr:        look up table for change attributes
        #        head:          for head definition
        #
        # Return:
        #        Returns the processed html attributes
        proc xmlParamToRtf {tag param attrName head} {
                #puts "xmlParamToRtf $tag $param $attrName $head "
                global notextEditorstandalone
                variable ::rtfParser::rtf
                variable ::rtfParser::rtfColorTable
                variable ::rtfParser::rtfStylesheet
                variable ::rtfParser::current
                if {$param == ""} {return "";}
                
                # xmltohtml attrName rend
                set styleList [getAttrValues $param $attrName]
                if {$styleList == ""} {return ""}
                
                set inputList [split $styleList ";"]
                
                set rtfStyles ""
                
                if {$head} {
                  set openstylestring ""
                } else {
                  set openstylestring "\{" 
                }
                
                if {$attrName == "rend"} {
                        set styles ""
                        foreach style $inputList {
                                #puts $style
                        
                                #e.g. overstrike:1 size:14 foreground:#00ffff
                                if {[catch {regexp {([^:]+):(.+?)} $style match key value} result]} {
                                        putsstderr "Error: $result"
                                        continue;
                                }
                          
                                if {[TEDB::isOutputFilter $key $value]} {
                                  continue;
                                }
                                                  
                                if {$key == "overstrike"} {
                                        lappend styles "$openstylestring\\strike "
                                } elseif {$key == "underline"} {
                                        lappend styles "$openstylestring\\ul "
                                } elseif {$key == "slant"} {
                                        lappend styles "$openstylestring\\i "
                                } elseif {$key == "weight"} {
                                        lappend styles "$openstylestring\\b "
                                } elseif { $key == "size" } {
                                        set sizevalue $value
                                        # \fsN Font size in half-points (the default is 24).
                                        catch {set sizevalue [expr $value * 2 ]}
                                        lappend styles "$openstylestring\\fs${sizevalue} "
                                } elseif {$key == "foreground"  || $key == "background" } {
                                        if {$key == "foreground"} {
                                           set styleKey "$openstylestring\\cf"
                                        }
                                        if {$key == "background"} {
                                           set styleKey "$openstylestring\\chcbpat"
                                        }
                                           set colorValue 0
                                           # Try RGB
                                           catch {
                                         set colorValueRed [expr 0x[string range $value 1 2]] 
                                         set colorValueGreen [expr 0x[string range $value 3 4]] 
                                         set colorValueBlue [expr 0x[string range $value 5 6]]
                                         set colorTableValue \\red$colorValueRed\\green$colorValueGreen\\blue$colorValueBlue
                                         if {[info exists rtfColorTable($colorTableValue)]} {
                                           set colorValue $rtfColorTable($colorTableValue) 
                                         } else {
                                           set colorValue [array size rtfColorTable]
                                           incr colorValue
                                           set rtfColorTable($colorTableValue) $colorValue
                                         }
                                           }
                                   lappend styles "$styleKey$colorValue "
                                } elseif {$key == "offset"} {
                                        set alignValue ""
                                        if {$value == "-4"} {
                                                set alignValue "$openstylestring\\sub " 
                                        } elseif {$value == "0"} {
                                                set alignValue ""
                                        } elseif {$value == "4"} {
                                                set alignValue "$openstylestring\\super "
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set alignValue ""
                                        }
                                        if {$alignValue != ""} {
                                          lappend styles $alignValue
                                        }
                                } elseif {$key == "family"} {
                                        set familyValue ""
                                        if {$value == "Times"} {
                                                set familyValue "$openstylestring\\f2 " 
                                        } elseif {$value == "Courier"} {
                                                set familyValue "$openstylestring\\f1 "
                                        } elseif {$value == "Helvetica"} {
                                                set familyValue "$openstylestring\\f0 "
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set familyValue "$openstylestring\\f0 "
                                        }
                                        lappend styles $familyValue
                                } elseif {$key == "justify"} {
                                        set justifyValue ""
                                        if {$value == "right"} {
                                                set justifyValue "$openstylestring\\qr " 
                                        } elseif {$value == "center"} {
                                                set justifyValue "$openstylestring\\qc " 
                                        } elseif {$value == "left"} {
                                                set familyValue "$openstylestring\\ql " 
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set familyValue "$openstylestring\\ql " 
                                        }
                                        lappend styles $justifyValue
                                } elseif {[string first "spacing" $key] >=0} {
                                        set distValue $value
                                        set spacingn [string range $key end end]
                                        set spacingn [string map {1 b 2 l 3 a} $spacingn]
                                        if {$head} {
                                          set styleKey ""
                                        } else {
                                          set styleKey "\{" 
                                        }
                                        append styleKey "\\s${spacingn}${distValue}"
                                        # add "px" to the size value
                                        lappend styles $styleKey
                                } else {
                                        #TODO: justify, bgstipple, fgstipple, borderwidth, width, height
                                }       
                        }
                        
                        #append htmlStyles "style='" $styles "'"
                        append rtfStyles [join $styles "" ]
                        
                        if {$head} {
                          # nothing
                        } else {
                          
                          set llengthstyles [llength $styles]
                          #puts "xmlParamToRtf $styles : $llengthstyles"
                          incr rtf($current,stylesopen) $llengthstyles
                        }
                        
                        #puts "formats: $rtf($current,stylesopen) of $styles"
                        #if {$rtf($current,stylesopen) > 1} {
                        #       puts [llength $styles] 
                        #
                        #}
                } elseif {$attrName == "rows" | $attrName == "cols"} {
                        if {![string is integer $styleList]} {
                                putsstderr "$attrName has not a valid integer Number ($styleList)!"
                                return $htmlStyles;
                        }
                        set num $styleList
                        append rtfStyles $num
                } elseif {$attrName == "role" } {
                        append rtfStyles $attrName data
                } elseif {$attrName == "target" || $attrName == "ana"} {
                        # link system to html <a href="www.uni-trier.de">
                        set speciallink ""
                        if {[info exists notextEditorstandalone]} {
                                #puts "Execute special link procedure"
                                set speciallink [toTXTspeciallink $styleList $param]
                        }
                        if {$speciallink == "return"} {
                          return;
                        } elseif {$speciallink != ""} {
                          append rtfStyles $speciallink
                        } else {
                          append rtfStyles "\{\\field\{\\*\\fldinst \{ HYPERLINK \"$styleList\" \}\}\{\\fldrslt \{"
                          incr rtf($current,stylesopen) 3
                        }
                } elseif {$attrName == "n"} {
                        # link system to html <a href="www.uni-trier.de">
                        append rtfStyles "\{\\bkmkstart  $styleList \}"
                        set rtf($current,specialtag) "ref"
                        set rtf($current,specialend) "\{\\bkmkend $styleList \}"
                        #incr rtf($current,stylesopen) 0
                } elseif { $attrName == "rendition"} {
                # clean #f
                        if {[string index $styleList 0] == "#"} {
                                set styleList [string range $styleList 1 end]
                        }
                        if {!$head} {
                                set rtfStyleRepresentation $rtfStylesheet([styleNametoRtf $styleList])
                                append rtfStyles "\{" $rtfStyleRepresentation " "
                        } else {
                                set rtfStyleRepresentation [styleNametoRtf $styleList]
                                append rtfStyles "\{\\" $rtfStyleRepresentation " "
                        }
                        
                        # Xin 28.10.2014
                        #if {$tag != "p"} {
                          incr rtf($current,stylesopen) 1
                          
                        #}
                       
                } 
                return $rtfStyles;
        }
        
        proc styleNametoRtf {name} {
                set map {- ""
                }
                return [string map $map $name]
        }
        

        
                
        ######################
        # Texteditor Rtf functions
        ######################

        
        
        # getIdxTags --
        #    Get the tag list of text widget in the selected index.
        # Arguments:
        #    text       Name of the text widget
        #        index          the selected index of text
        #        lut            look-up table
        # Result:
        #    Returns the valid tag list.
        #
        proc getIdxTags {text index lut} {
                set tagList [$text tag names $index]
        #       puts $tagList
                return [::WordProcess::getTagPair $tagList $lut]
        }
        
        
        # getTagPair -- 
        #       Analyze and get the valid tags value from the given tag list and look-up table.
        #
        # Arguments:
        #        tagList        the given tag list
        #        LUT            look-up table
        #
        # Return:
        #        Returns the valid tag value (e.g. under example).
        #
        # Example:
        #       Taglist form: sel {justify center} {background yellow} {foreground #548DD4}
        #       ---> 
        #       tag value form: justify:center;background:yellow;foreground:#548DD4
        #
        # Attention:
        #       sel {{weight bold}}
        #       sel {{justify center} {background yellow}}
        #   sel {{justify center} {background yellow} {foreground #548DD4}}
        proc getTagPair {tagList LUT} {
                if {$tagList == ""} {return "";}
                upvar 1 $LUT arrMap
                if {![array exists arrMap]} {error "\"$arrMap\" isn't an array";}
                
                set tagListLen [llength $tagList]
                #puts "tagListLen: $tagListLen"
                
                set result ""
                
                set listIndex 0
                while {$listIndex < $tagListLen} {
                        set tag [lindex $tagList $listIndex]
        #               puts "tag: $tag"
                        set tagLen [llength $tag]
        #               puts "tagLen: $tagLen"
        
                        if {$tagLen == 2} {
                                set firstItem [lindex $tag 0]
                                set lastItem [lindex $tag 1]
                                set firstLen [llength $firstItem]
                                set lastLen [llength $lastItem]
                                
                                if {$firstLen == 1 && $lastLen == 1} {
                                        # right do it.
                                        # valid form with key and value
                                        set key [lindex $tag 0]
                                        set value [lindex $tag 1]
                                        # valid key in the selected look-up table
                                        if {[info exists arrMap($key)]} {
                                                set result [::TEUtils::appendValue $result $key:$value ";"]
                                        }
                                } else {
                                        # need to more analyze
                                        set tempValue [::WordProcess::getTagPair $tag arrMap]
                                        if {$tempValue != ""} {
                                                set result [::TEUtils::appendValue $result $tempValue ";"]
                                        }
                                }
                        } elseif {[string first "\{" $tag] >= 0} {
                                if {[::TEUtils::checkBracketPairs $tag "\{" "\}"]} {
                                        # if there is more list in it
                                        set tempValue [::WordProcess::getTagPair $tag arrMap]
                                        if {$tempValue != ""} {
                                                set result [::TEUtils::appendValue $result $tempValue ";"]
                                        }
                                } else {
                                        puts "Error: there is no more list in the $tag!"
                                }
                                
                        }
                        # important counting
                        incr listIndex;
                }
                return $result
        }
        
        proc setXMLRendAttr {rendValues inputText} {
                if {$rendValues == "" || [string first ":" $rendValues] < 0 } {return $rendValues;}
                
                # get rend/rendition values
                set rendResult [::TEDB::getRenditionValue $rendValues 0]
                set rend [dict get $rendResult rend]            ;# already has prefix FuD-
                set rendition [dict get $rendResult rendition]
                
                # set xml attr form
                set renditionAttr ""
                set rendAttr ""
                if {$rendition ne ""} {
                        set renditionAttr "rendition=\"#$rendition\""
                }
                if {$rend ne ""} {
                        set rendAttr "rend=\"$rend\""
                }
                
                set resultAttr [::TEUtils::appendValue $rendAttr $renditionAttr " "]
                
                return "<hi $resultAttr>[::TEUtils::mapXML $inputText]</hi>"
        }
        
        proc textToXML {nametext {ignoreList ""} {startIdx 1.0} {endIdx end} {oldRend ""}} {
                variable ::TEDB::presetOption
                
                # reset pre-setting options
                ::TEDB::resetPreset
                
                set indexChanged 0
                set linkAttr ""
                set bodyValue ""
                set oldText ""
        
                set oldIndex $startIdx
                set endIndex $endIdx
        #       set oldRend $startIdxTags
                set oldRend $oldRend
                set endIndex $endIdx
                set isParaEnd 0
                #get the end index waiting for xml process
                #puts "endIdx: $endIdx"
                if {$endIdx == "end"} {
                        set endIndex [$nametext index "end - 1 char"]
                } else {
                        set endIndex $endIdx
                }
          
          
                # save HTML form
                foreach {key value index} [$nametext dump -all $startIdx $endIdx] {                        
                        #puts "textToXML.key: $key - value: $value - index: $index"
                        
                        if {$value == "sel" || $key == "mark" || $value == ""} {continue}
                        
                        
                        # Now in TextEditor this isIgnored value is always 0
                        set isIgnored 0
                        foreach ignore $ignoreList {
                                if {[string match $ignore $value]} {set isIgnored 1}            
                        }
                        if {$isIgnored} {continue}
                        
                        # index change or not
                        if {[string equal $index $oldIndex] == 0} {
                                set indexChanged 1
                        } else {
                                set indexChanged 0
                        }
                        
                        # check key value
                        if {$key == "text"} {
                                # textwidget dump additional return     
                                #set value [::TEUtils::mapLinkTextToXml $value]
                                #Xin - Bug fixed: #2701                              
                                if {$isParaEnd} {
                                  if {$value == "\n"} {
                                    set value ""; 
                                  }
                                  set isParaEnd 0;
                                }                              
                                append oldText $value
                        } elseif {$key == "tagon"} {
                                set isParaEnd 0  
                                # there are some oldRends but here has new tagon
                                # or there are some oldRendition.
                                if {[string length $oldRend] > 0 && $indexChanged} {
                                        # xmlif {$isParaEnd == 1 && $value == " \n"} {set value "<space> </space>"; set isParaEnd 0;}
                                        append bodyValue [::WordProcess::setXMLRendAttr $oldRend $oldText]
                                        set oldText ""
                                } else {
                                        # there is no old rend value
                                        # there are some old texts
                                        if {$oldText != ""} {                                               
                                                append bodyValue [::TEUtils::mapXMLAndSpace $oldText]                                               
                                                set oldText ""
                                        }
                                }
                                set valueHead [lindex $value 0]
                                if {$valueHead == "link" || $valueHead == "anchor" } {
                                        set valueAttr [lindex $value 1]
                                        set linkAttr ""
                                        set newKey [expr {$valueHead == "link" ? "target=\"" : "n=\""}]
                                        append linkAttr $newKey [lindex [list [::TEUtils::mapXML $valueAttr]] 0] "\"" ;#"eclipse
                                        append bodyValue "<ref $linkAttr>"
                                } elseif {$valueHead == "p"} {
                                        set vValue [lindex $value 1]
                                        ::TEDB::addToPreset $vValue TKTEXT
                                        set presetOption(hasHead) 1
                                        set pAttr "rendition=\"#"
                                        append pAttr $presetOption(prefix)  $vValue "\""
                                        # preserve space
                                        #if {[string range $value 0 0 ] == " "} {
                                        #  set value "<space> </space>[string range $value 1 end ]"
                                        #} 
                                        if {$vValue == "Paragraph"} {
                                          append bodyValue "<p>"
                                        } else {
                                          append bodyValue "<p $pAttr>"
                                        }
                                        if {$linkAttr != ""} {
                                                append bodyValue "<ref $linkAttr>"
                                        }
                                        # preserve space
                                        #set bodyValue [string map {" <p" "<space> </space><p"} $bodyValue]
                                } else {
                                        
                                        if {$valueHead == "preset"} {
                                                set vValue [lindex $value 1]
                                                ::TEDB::addToPreset $vValue TKTEXT
                                                set presetOption(hasHead) 1
                                        } 
                                        
                                        # add new Rend value
                                        set value [::WordProcess::getRendValue $value]
                                        #puts "tagOnvalue: $value" ;# e.g. preset:Preset1
                                        # clean the rend value
                                        set value [string trim $value]
                                        # avoid null value 
                                        if {![::TEUtils::valueExists $oldRend $value]} {
                                                set oldRend [::TEUtils::appendValue $oldRend $value ";"]
                                        }
                                }
                        } elseif {$key == "tagoff"} {
                                set isLink 0
                                set valueHead [lindex $value 0]
                                if {$valueHead == "link" || $valueHead == "anchor"} {set isLink 1}
                                if {$valueHead == "p"} {set isParaEnd 1}
                                
                                # if index changed then append the entity to the body value.
                                if {$indexChanged} {
                                        #CONSIDER: need test
        #                               if {$endIndex < [$nametext index end]} {
        #                                       #set oldText [string trimright $oldText \n]
        #                               }
                                        if {$oldRend != ""} {
                                                #TODO: NEED A PROC TO BETTER STRUCTURE
                                                #puts "TagOffoldRend: $oldRend"
                                                append bodyValue [::WordProcess::setXMLRendAttr $oldRend $oldText]
                                                set oldText ""
                                        } 
                                }
                                        #FIXME: TEST
                                        # removing last \n because of existing p
                                #puts "CHECK $isParaEnd [string range $oldText end end ] TRIM"
                                if {$isParaEnd && [string range $oldText end end ] == "\n" } {
                                  #puts "TRIM"
                                  set oldText [string range $oldText 0 end-1 ]      
                                  set isParaEnd 0
                                }
                                if {$isLink} {
                                        append bodyValue [::TEUtils::mapXMLAndSpace $oldText]
                                        append bodyValue </ref>
                                        set linkAttr ""
                                } elseif {$valueHead == "p"} {
                                        
                                        append bodyValue [::TEUtils::mapXMLAndSpace $oldText]
                                        
                                        # preserve space
                                        #if {[string range $bodyValue end end ] == " "} {
                                        #   set bodyvalue "[string range $bodyValue 0 end-1 ]<space> </space>"
                                        #}
                                  
                                        if {$linkAttr != ""} {append bodyValue "</ref>"}   
                                        
                                        append bodyValue </p>
                                } else {
                                        append bodyValue [::TEUtils::mapXMLAndSpace $oldText]
                                }
                                set oldText ""
                                
                                
                                if {!$isLink && !($valueHead == "p")} {
                                        set rendValue [::WordProcess::getRendValue $value]
                                        # finally the selected rend value will be removed no matter index changed or not.
                                        set oldRend [::WordProcess::delRendValue $oldRend $rendValue]
                                }
                        } elseif {$key == "window"} {
                                # window and tables
                                if {$oldText != ""} {
                                        if {$oldRend != ""} {
                                                # xml
                                                append bodyValue [::WordProcess::setXMLRendAttr $oldRend $oldText]
                                                
                                        } else {
                                                append bodyValue [::TEUtils::mapXMLAndSpace $oldText]
                                        }
                                        set oldText ""
                                }
                                if {$linkAttr != ""} {
                                        append bodyValue "</ref>"
                                }
                                # value is table name, get the table content from text widget
                                append bodyValue [::WordProcess::tableContent $value]           
                                if {$linkAttr != ""} {
                                        append bodyValue "<ref $linkAttr>"
                                        set linkAttr ""
                                }
                        }
                        # set old index for next step to compare
                        set oldIndex $index
                }
                # last part of text
                if {$oldText != ""} {
                        set oldText [string trimright $oldText \n]
                        if {[string length $oldRend] > 0} {
                                append bodyValue [::WordProcess::setXMLRendAttr $oldRend $oldText]
                        } else {
                                append bodyValue [::TEUtils::mapXMLAndSpace $oldText]
                        }
                }
                #puts "getxmlbody.bodyValue: $bodyValue"
                
                #CONSIDER: NEED CLEAR?
                # reset pre-setting options
                #::TEDB::resetPreset
                
                # preserve space
                #if {[string range $bodyValue 0 0 ] == " "} {
                #    set bodyValue "<space> </space>[string range $bodyValue 1 end ]"
                #} 
                #if {[string range $bodyValue end end ] == " "} {
                #   set bodyValue "[string range $bodyValue 0 end-1 ]<space> </space>"
                #}  
                return $bodyValue
        } 
               
        
        proc getXMLHeader {} {
                variable ::TEDB::presetOption
                # xml head
                set headValue ""
                if {$presetOption(hasHead) == 0} {
                        append headValue "<teiHeader/>"
                } else {
                        append headValue "<teiHeader>"
                        append headValue "<encodingDesc>"
                        append headValue "<tagsDecl>"
                        
                        set xmlHeadStyles [::TEDB::getValueFromPreset XML]      
                        set headRendition ""
                        if {$xmlHeadStyles != ""} {
                                foreach {name value} $xmlHeadStyles {
                                        append headRendition "<rendition xml:id=\"$presetOption(prefix)$name\">"
                                        set value [string map {\{ "" \} ""} [::TEUtils::backToBracket $value]]
                                        append headRendition $value
                                        append headRendition "</rendition>"
                                }
                        }
                        
                        append headValue $headRendition
                        append headValue </tagsDecl>
                        append headValue "</encodingDesc>"
                        append headValue "</teiHeader>"
                }
                #puts "getXMLHeader.headValue: $headValue"
                return $headValue
        }
        
        # getSelToXML --
        #    Change the contents from the selection of text widget to the XML body. 
        # Arguments:
        #    text               Name of the text widget
        #        ignoreList             
        #        startIdx               the start index of selected text
        #        endIdx                 the end index of selected text
        # Result:
        #    Returns the valid xml body content.
        #
        proc getSelToXML {text {ignoreList ""} {startIdx 1.0} {endIdx end}} {
                if { [catch {$text index sel.first}]} {
                        puts "There is no sel!"
                        return "";
                }
                # get selection range with start and end index
                set selrange [$text tag nextrange sel sel.first sel.last]
                #puts $selrange
                set startIdx [lindex $selrange 0]
                set endIdx [lindex $selrange 1]
                
                set startIdxTags [::WordProcess::getIdxTags $text $startIdx TextStyle::tagNames]
                #puts $startIdxTags
                set bodyValue [::WordProcess::textToXML $text "" $startIdx $endIdx $startIdxTags]
                #puts "getSelToXml.bodyValue: $bodyValue"
                        
                return $bodyValue       
        }
        
        # getXMLBody --
        #    Get the contents of text widget and convert to the xml or html form.
        # Arguments:
        #    nametext      Name of the input window
        #        dataType  the file saving as xml or html. (0:XML / 1:HTML)
        #       TODO: ignoreList
        #        ignoreList  ????
        #        startIdx
        #        endIdx
        # Result:
        #    Returns the whole <body> contents.
        #
        proc getXMLBody {nametext {ignoreList ""} {startIdx 1.0} {endIdx end}} {
                return [::WordProcess::textToXML $nametext $ignoreList $startIdx $endIdx]
        }
        
        
        # tableContent --
        #   Get the table content from text widget.
        #
        # Arguments:
        #   
        #
        # Result:
        #   
        #
        proc tableContent {name} {
                if {$name == ""} {return "";}
                set contents ""
                # check tableInfos
                
                #parray ::textEditorMain::tableInfos
                
                if {![info exists ::textEditorMain::tableInfos(textlist,$name)]} {
                        putsstderr "Attention: there is no $name in the tableInfos!";
                        return;
                }
                
                set textlist $textEditorMain::tableInfos(textlist,$name)
                set rowNum $textEditorMain::tableInfos(rowNum,$name)
                set colNum $textEditorMain::tableInfos(colNum,$name)
                
                if {[expr $rowNum*$colNum] != [llength $textlist]} {
                        putsstderr "row/column is not conform with textlist length!
                                                        \nrow: $rowNum - col: $colNum \ntextlist: $textlist"
                }
                
                
                
                append contents "<table rows=\"$rowNum\" cols=\"$colNum\">"
                set index 0
                set curTable ""
                # row,col -> rows : cells
                for {set row 0} {$row < $rowNum} {incr row} {
                        append contents "<row>" 
                        for {set col 0} {$col < $colNum} {incr col} {
                                set colWidth ""
                                set cellText [lindex $textlist $index]
        #                       set parCell [winfo parent $cellText]
                                set curTableW [::TEUtils::getWidgetParent $cellText TablelistWindow]
                                if {$curTable == ""} {
                                        set curTable [::TEUtils::getWidgetParent $cellText Tablelist]
                                        #puts [expr { round ( [$curTable columnwidth $col] / 7) + 2 } ]
                                }
                                if {![winfo exists $curTable]} {
                                        putsstderr "Error: There is no table $curTable!"
                                        return;
                                }       
                                set colWidth [$curTable columncget $col -width]
        
                                if {$colWidth == "" || $colWidth < 2} {set colWidth $textEditorMain::coefficients(defaultCellWidth)}
                                set widthAttr "width:$colWidth"
                                append widthAttr "em"
                                #puts "tableContent_widthAttr: $widthAttr"
                                
                                append contents "<cell style=\"$widthAttr\">"
                                append contents [::WordProcess::getXMLBody $cellText ""]
                                append contents "</cell>"
                                incr index
                        }
                        append contents "</row>"
                }
                append contents "</table>"
                #puts "TableContents: \n$contents"
                return $contents
        }
        
        #extSep: ";" - inSep: ":"
        proc searchKey {inputStr key extSep inSep} {
                if {$inputStr == ""} {return "";}
                set eleList [split $inputStr $extSep]
                set matchs [lsearch -all $eleList $key$inSep*]
                return $matchs
        }
        
        
        # delRendValue --
        #    Remove the selected rend value out of the given rend list.
        # Arguments:
        #        oldRend   the already exists rend list (e.g. sland:italic;weight:bold;size:14) 
        #    rend      the rend value (e.g. slant:italic;weight:bold)
        #
        # Result:
        #    Returns the rest parts of the given rend list.
        #
        proc delRendValue {oldRend rend} {
                if {$oldRend == ""} {
                        putsstderr "There is no exists rend list! It will return a null value."
                        return;
                }
                if {$rend == ""} {
                        puts "the removing rend value is null!"
                        return $oldRend
                }
                set oldList [split $oldRend ";"]
                set rendList [split $rend ";"]
                
                foreach item $rendList {
                        set oldList [::TEUtils::delListElement $oldList $item]
                }
                
                set result ""
                
                set len [llength $oldList]
                if { $len== 1} {
                        set result $oldList
                } elseif {$len > 1} {
                        set tbl [list " " ";"]
                        set result [string map $tbl $oldList]
                }
                return $result
        }
        
        
        # formatRendValue -- 
        #        format the given rend value with ":" and ";"
        #        e.g. fgstipple:questhead;  or size:14;slant:italic;
        #
        # Arguments:
        #        key: the given hi rend value
        #        value: 
        #
        # Returns:
        #        Returns rend value as formated string
        #
        proc formatRendValue {{key ""} {value ""}} {
                if {$key == "" && $value == ""} { return ""}
                set result ""
                append result $key:$value
                return $result
        }
        
        
        
        
        #TODO: update the structure!! "\}"
        # getRendValue -- 
        #        convert tag value to the formated string and save it.
        #
        # Arguments:
        #        rendValue: the given hi rend value
        #
        # Returns:
        #        Returns rend value as formated string. (e.g. slant:italic;weight:bod)
        #
        # Attention:
        #        Must be with process ::WordProcess::formatRendValue {}
        #
        proc getRendValue {rendValue} {
                set rendLength [llength $rendValue]
        #       puts "@rendlength: $rendLength"
                if {[catch {llength $rendValue}] } {
                        puts $hallo "Error: Bugs in the rendValue: $rendValue"
                        flush $hallo
                        return
                } 
                # result string
                set result ""
                # if rendValue is a validated list
                if {[llength $rendValue] > 0} {
                        
                        # font values has braces ({, }) and can more items and subItems in a list.
                        # tag values has no braces ({, }) and only 2 items in a list.
                        # e.g.
                        # 1.0 tagon {family {@Arial Unicode MS}} {size 14} {slant italic} ;# fonts
                        # 1.0 tagon justify center      ;# tag
                        
                        
                        # whether rendValue has the brace or not.
                        if {[string last "\}" $rendValue]        > 0} {
                                # fonts
                                foreach item $rendValue {
                                        # item must be a list with 2 subItems.
                                        # Error must be catch and skip the Error!
                                        if {[llength $item] != 2 
                                                || [catch {lindex $item 0} result1] 
                                                || [catch {lindex $item 1} result2] } {
                                                putsstderr "Error: Bugs in the item: $item of rendValue: $rendValue"
                                                continue                
                                        }
                                        set key [lindex $item 0]
                                        set value [lindex $item 1]
                                        # e.g. family {@Arial Unicode MS} 
                                        if {$key == "family"} {
                                                set replaceMap [list "\{" "" "\}" ""]
                                                set value [string map $replaceMap $value]
                                        }
                                        set formatValue [::WordProcess::formatRendValue $key $value]
                                        set result [::TEUtils::appendValue $result $formatValue ";"]
                                }
                        } else {
                                # tag
                                # There is only one list with 2 items!
                                if {[llength $rendValue] != 2 
                                        || [catch {lindex $rendValue 0} result1] 
                                        || [catch {lindex $rendValue 1} result2] } {
                                        putsstderr "Error: Bugs in the rendValue: $rendValue"
                                        return  
                                }
                                set key [lindex $rendValue 0]
                                set value [lindex $rendValue 1]
                                set result [::WordProcess::formatRendValue $key $value]
                        }
                }
                # clear result  
                #puts "TagResult: $result"
                return $result
        }
        
        # SaveContents --
        #    Save the contents of the edit window to the given file
        # Arguments:
        #    name        Name of the edit window
        #    filename    Name of the file to save the contents to
        # Result:
        #    None
        #
        proc SaveContents {name filename} {
                        if {[catch { set outfile [open $filename "w"] } result] } {tk_messageBox -message $result ;return;}
                fconfigure $outfile -encoding utf-8
                # Get the contents and analyse it
                set src [$name.text dump -all 1.0 end]
                puts $outfile $src
                close $outfile
        }
        
        proc cleanFragment {content {startIdx 0}} {
                if {$content == ""} {return;}
                # 25.09.2014
                #set pattern {\s*(<!--StartFragment--><span ([^<>]*)>|</span><!--EndFragment-->)\s*}
                set pattern {\s*(<!--StartFragment-->|<!--EndFragment-->)\s*}
                
                regsub -all $pattern $content "" content
                #puts "cleanFragment_content: $content"
                return $content
        }
        
        # getAttrValues -- 
        #        Get the attribute's value
        #
        # Arguments:
        #        inputStr:      the given string 
        #        attrType:      the selected attribute name (e.g. style, class...)
        #        startIdx:      start position for searching (default: 0)
        #
        # Returns:
        #        Returns the attribute value. 
        #
        proc getAttrValues {inputStr attrType {startIdx 0}} {
                if {$inputStr == "" | $attrType==""} {return "";}
                array set result {}
        
                #TODO: test new pattern 
                set pattern {([^=]+)\s*=('([^'<>]+)'|\"([^\"<>]+)\"|\s*([^\"'<>\s]+))}
                
                set validKeyList {rend rendition role rows colspan rowspan cols style target ana href name n xml:id class}
                
                foreach {all key allValue sQuotValue dQuotValue nValue} [regexp -all -inline -start $startIdx $pattern $inputStr] {
                        #puts "all: $all - key: $key - allValue: $allValue - sQuotValue: $sQuotValue - dQuotValue: $dQuotValue - nValue: $nValue"
                        set value [expr {$sQuotValue ne "" ? $sQuotValue : [expr {$dQuotValue ne "" ? $dQuotValue : $nValue}]}]
                        set key [string trim $key]
                        set value [string trim $value]
                        set value [::TEUtils::mapHTMLToText $value];
                        if {$key == ""} {continue}
                        if {[lsearch -exact $validKeyList $key] >= 0} {
                                set result($key) $value
                        }
                }
                if {![info exists result($attrType)]} {
                        #puts "Attention:There is no valid value for attrType!"
                        return "";
                }
                return $result($attrType);
        }
        
        
        # get all valid attribute from the xml element
        proc getValidAttrsList {inputStr {startIdx 0}} {
                if {$inputStr == ""} {return "";}
                set resultList {}
                #TODO: test new pattern 
                set pattern {([^=]+)\s*=('([^'=<>]+)'|\"([^\"=<>]+)\"|\s*([^\"'=<>\s]+))}
                foreach {all key allValue sQuotValue dQuotValue nValue} [regexp -all -inline -start $startIdx $pattern $inputStr] {
                        #puts "all: $all - key: $key - allValue: $allValue - sQuotValue: $sQuotValue - dQuotValue: $dQuotValue - nValue: $nValue"
                        set value [expr {$sQuotValue ne "" ? $sQuotValue : [expr {$dQuotValue ne "" ? $dQuotValue : $nValue}]}]
                        set key [string trim $key]
                        set value [string trim $value]
                        if {$key == ""} {continue}
                        lappend resultList [list $key $value]
                }
                return $resultList
        }
        
        
        # attrList with form: {{colspan 3} {rowspan 2} {style {font-size:13;background:red black}} }
        # type: rowCol, cellStyle, style 
        proc getTargetAttrValue {attrList keyList type} {
                if {$attrList == "" || $keyList == ""} {return "";}
                set result ""
        
                foreach item $attrList {
                        set k [lindex $item 0]
                        set v [lindex $item 1]
                        if {[lsearch -exact $keyList $k]>=0} {
                                if {$type == "rowCol"} {
                                        set result [::TEUtils::appendValue $result $k:$v ";"]
                                } elseif {$type == "style"} {
                                        return $v
                                }
                        }
                }
                return $result
        }
        
        
        # getValidStyleValue -- 
        #        Clean the param to get the attribute's value with mapping and change the HTML to XML TAG.
        #
        # Arguments:
        #        param:         the given parameters (e.g. class=MoNomal style='font-size:14.0pt;mso-bidi-font-size:11.0pt') 
        #        attrName:      the selected attribute name (e.g. style, class...)
        #        startIdx:      start position for searching (default: 0)
        #
        # Returns:
        #        Returns the attribute value. 
        #
        proc getValidStyleValue {param attrName mapArr} {
                variable ::TEDB::presetOption
                variable ::TEDB::htmlFilter
                
                if {$param == "" || $attrName == ""} {return "";}
                upvar 1 $mapArr arrM
                if {![array exists arrM]} {
                   error "\"$mapArr\" isn't an array"
                }
                set result ""
                #puts "param: $param - attrName: $attrName"
                if {$attrName=="style"} {
                        set styleValues [::WordProcess::getAttrValues $param style]
                } elseif {$attrName == "href" | $attrName == "name"} {
        #               set quot [expr {[string first "\"" $param] >= 0 ? "\"" : ""}]
                        
                        set styleValue [::TEUtils::mapLinkTextToXml [::WordProcess::getAttrValues $param $attrName]]
                        return $arrM($attrName):$styleValue
                } elseif {[string first "rowColSpanHtml" $attrName] >=0} {
                        # for rowspan, colspan of td th
                        #TODO: need be combinated
                        set styles ""
                        set colRowAttr ""
                        
                        #FIXME FOREACH wrong!
                        
                        set param [::WordProcess::getValidAttrsList $param]
                        
                        # cols, rows attributes
                        set colRowAttr [::WordProcess::getTargetAttrValue $param "colspan rowspan" rowCol]
                        if {$colRowAttr != ""} {
                                set result [::TEUtils::appendValue $result $colRowAttr ";"]
                        }
                        
                        #styles
                        set styles [::WordProcess::getTargetAttrValue $param "style" style]
                        if {$styles != ""} {
                                set styles [::WordProcess::getValidCellStyle $styles] 
                                if {$styles != ""} {
                                        set result [::TEUtils::appendValue $result $styles ";"]
                                }
                        }
                        
                        return $result
                } elseif {[string first "rowColSpanTei" $attrName] >=0} {
                        set styles ""
                        set colRowAttr ""
                        set result ""
                        return $result
                } elseif {[string equal -nocase $attrName "styleClass"]} {
                        # get the possible class attribute values
                        append  pattern {class=[\'\"]?} $presetOption(prefix) {[^\'\"\s]*['\"\s]?} ;#"eclipse
                        
                        set fudClassMatch ""
                        # get the last one
                        foreach {fudClassMatch} [regexp -all -inline $pattern $param] {}
                        if {![string equal $fudClassMatch ""]} {
                                set tempValue [::WordProcess::getAttrValues $fudClassMatch class]
                                set classValue "preset:$tempValue"
                                set result [::TEUtils::appendValue $result $classValue ";"]
                        }
                        
                        # style values
                        set styleValues [::WordProcess::getAttrValues $param style]
                } elseif {[string equal -nocase $attrName "headToXML"]} {
                        set styleValues $param
                }
                
                if {$styleValues == ""} {return $result}
                set inputList [split $styleValues ";"]
                
                foreach style $inputList {
                        # not valid element form
                        if {[string first ":" $style] <=0} {
                                #putsstderr "Warning: style $style contains no valid element!"
                                #puts "style: $style"
                                continue;
                        }
                        
        #               if {[string first ":" $style]<0} {continue;}
                        set keyValue [split $style ":"]
                        set key [string trim [lindex $keyValue 0]]
                        set value [string trim [lindex $keyValue 1]]
                        
                        set filterKeyValue 0
                        if {[info exists htmlFilter($key)]} {
                                if {$htmlFilter($key) == {}} {
                                        set filterKeyValue 1
                                } else {
                                        if {$value == $htmlFilter($key) }  {
                                                set filterKeyValue 1
                                        }
                                }
                        }
                        
                        if {!$filterKeyValue && [info exists arrM($key)]} {
                                # change HTML to TEI tag
        
                                if {$key == "text-decoration"} {
                                        if {[lsearch -exact $value "underline"] >= 0} {
                                                set result [::TEUtils::appendValue $result $arrM(u) ";"]
                                        }
                                        if {[lsearch -exact $value "line-through"] >= 0} {
                                                set result [::TEUtils::appendValue $result $arrM(s) ";"]
                                        }
                                } elseif {$key == "font-weight"} {
                                        if {$value == "bold"} {
                                                set result [::TEUtils::appendValue $result $arrM(b) ";"]
                                        }
                                } elseif {$key == "font-style"} {
                                        if {$value == "italic" | $value== "oblique"} {
                                                set result [::TEUtils::appendValue $result $arrM(i) ";"]
                                        }
                                } elseif {$key == "vertical-align"} {
                                        set subsup [split $arrM($key) ";"]
                                        if {$value == "sub"} {  
                                                set result [::TEUtils::appendValue $result [lindex $subsup 0] ";"]
                                        } elseif {$value=="super"} {
                                                set result [::TEUtils::appendValue $result [lindex $subsup 1] ";"]
                                        }
                                } elseif {$key == "background-color"} {
                                        set result [::TEUtils::appendValue $result background:$value ";"]
                                } elseif {[regexp {^(cols|rows)+} $key]} {
                                        continue;
                                } else {
                                        set styleKey $arrM($key)
                                        set style $styleKey:$value
                                        set result [::TEUtils::appendValue $result $style ";"]
                                }
                        }
                } 
                #puts "getValidStyleValue_result: $result"
                return $result;
        } 
        
        
        proc getValidCellStyle {cellAttrs} {
                variable ::TextStyle::validCellStyles           ;# valid list
                if {$cellAttrs == ""} {return "";}
                set inputList [split $cellAttrs ";"]
                set result ""
                foreach style $inputList {
                        if {[string first ":" $style] <=0} {
                                continue;
                        }
                        set keyValue [split $style ":"]
                        set key [string trim [lindex $keyValue 0]]
                        set value [string trim [lindex $keyValue 1]]
                        
                        if {[lsearch -exact $validCellStyles $key] >= 0 } {
                                set result [::TEUtils::appendValue $result $key:$value ";"]
                        }
                }
                return $result
        }
        
        # removeLastTagElement -- 
        #        Remove the last element of given current tag list.
        #
        # Arguments:
        #        currentAttrss: the given current tag list
        #
        # Returns:
        #        Returns the new tag list
        #
        proc removeLastTagElement {currentAttrs} {
                if {$currentAttrs == ""} {
                        return ""
                } else {
                        return [lreplace $currentAttrs end end]
                }
        }
        
        
        
        # addParamToArrList -- 
        #       Add the new last tag into the current attribute list
        #
        # Arguments:
        #        arrayList:     the current attribute values list
        #        param:                 the new input attribute value
        #        ctName:                the current tag Name
        #        level:                 the XML level
        #
        # Return:
        #        Returns the last current attributes
        #
        proc addParamToArrList  {arrayList param ctName level} {
                # check the currentAttrs
                #puts "addParamToArrList: param: $param - level: $level"
                if {$arrayList == ""} {
                        set last ""
                } else {
                        # last one of current attribute list
                        set last [lindex $arrayList end]
                }
                
                # set different name of last element 
                set lastName $ctName$level
                
                # set new namespace array
                set lastName [::xins::setNewArray $lastName]
                if {[array exists $last]} {
                        # copy the last array to the current array
                        ::TEUtils::copyArr $last $lastName
                } 
                
                # param is empty so copy the last one
                if {$param == ""} {
                        if {$last!=""} {
                                return [lappend arrayList $last]
                        } else {
                                return [lappend arrayList $lastName]
                        }
                }
                
                # param style list
                set paramList [split $param ";"]
                set pattern {([^:]*):(.*?)}
                # check each param in the param list to compare the last element of current tags
                foreach sp $paramList {
                        # not valid element form
                        if {[string first ":" $sp] <=0} {
                                #putsstderr "Warning: styles contains no valid element!\n item: $sp"
                                continue;
                        }
                        set match ""
                        set spKey ""
                        set spValue ""
                        # for link
                        regexp $pattern $sp match spKey spValue
                        ::TEUtils::setMapValue $lastName $spKey $spValue
                }
                # add to the last
                # 2715
                lappend arrayList $lastName
        #       parray $lastName
                        
#               set arrayList [linsert $arrayList 0 $lastName]
                #puts $arrayList
                return $arrayList
        }
        
        
        
        # getStyleMaps -- 
        #       Change the array map to the style string. 
        #
        # Arguments:
        #        map:           array with styles
        #        type:          normal, link or tableCell (0: normal | 1:link | 2: tableCell)
        # Return:
        #        Returns style string
        #
        proc getStyleMaps {map type} {
                
                upvar 1 $map arrM
                if {![array exist arrM]} {
                        #puts "Warning: There is no Array in map $map"
                        return "";      
                } 
                set result ""
                
                set linkKeys {n name href target ana}
                set numberList {width height border size font-size word-spacing letter-spacing 
                                                lmargin1 lmargin2 rmargin spacing1 spacing2 spacing3}
                foreach {key value} [array get arrM] {
                        # pt--> px
                        if  {[lsearch -exact $numberList $key]>= 0} {
                                if {[string first "pt" $value] > 0 || [string first "px" $value] > 0 || [string first "em" $value] > 0} {
                                        if {$key == "width"} {
                                                set value [::WordProcess::setWidthToEM $value]
                                        } else {
                                                set value [::TEUtils::getNumber $value]
                                        }
                                        #set value [string range $value 0 [expr [string length $value] -5]]     
                                }
                        }
                        # font modification
                        if {$key == "family"} {
                                set value [::TEUtils::fontMap $value]
                                if {$value == {}} {continue;}
                        } 
                        
                        if {$type == 1} {
                                if {[lsearch -exact $linkKeys $key] >= 0} {
                                        set result [concat $result $key:$value]
                                }
                        } elseif {$type == 2} {
                                if {[regexp {^(cols|rows|width|height)+} $key]} {
                                        set result [concat $result $key:$value]
                                } 
                        } else {
                                if {[lsearch -exact $linkKeys $key] < 0 && ![regexp {^(cols|rows)+} $key]} {
                                        set result [concat $result $key:$value]
                                }
                        }
                }
              #Bug: 2715
                return $result
        }
        
        
        proc setWidthToEM input {
                if {$input == ""} {return;}
                set value [::TEUtils::getNumber $input]
        
                if {[string first "px" $input ] >= 0 || [string first "pt" $input] > 0} {
                        set value [expr { round( $value / 7 )}]
                        #puts "setWithToEM_widthValue: $value"
                } elseif {$value == $input} {
                        # no suffix --> default width
                        set value $textEditorMain::coefficients(defaultCellWidth)
                }
                
                return $value
        } 
        
        
        # parseHtmltoXml -- 
        #       Saving the html content with xml form according the input arguments (tag, slash, parameters, text content)  
        #
        # Arguments:
        #        args:  {tag slash param text}
        #        
        # Return:
        #        None
        # Attention:
        #        set new save form of bodytext: 1. text 2. AttrKey:currentAttrs 3.Element name(e.g. hi, /hi)
        proc parseHtmltoXml {args} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                variable ::TEDB::presetOption
                
                # useful tags
                foreach {tag slash param text} $args {
                        # make sure all tag name is low case
                        set tag [string tolower $tag]
                        
                        set isTagEnd [string equal $slash "/"]
                        
                        ::WordProcess::checkHeadStatus $tag $slash html
                        
                        if {$presetOption(inHead) != 0} {
                                htmlHeadToXML $tag $slash $param $text
                        }
                        
                        
                        if {$tag == "body" && !$isTagEnd} {
                                set xmlData(main,inBody) 1
                                set xmlData(main,bodyLevel) 0
                                set xmlData(main,currentAttrs) ""
                                set xmlData(main,bodyText) {}
                                set curWName "main"
                        }
                        
                        ::WordProcess::checkTextName
                        
                        if {$curWName == ""} {continue}
                        
                        #CONSIDER: THIS IS TEMPORary solution of changing the particular Tag
                        if {[string equal -nocase "strong" $tag]} {
                                set tag "b"
                        } 
        
                        #puts "tag: $tag - slash: $slash - param: $param - text: $text"
                        if {$isTagEnd} {
                                analyzeHtmlEndTag $tag
                        } else {
                                analyzeHtmlStartTag $tag $param $text
                        }
                        if {$isTagEnd && ($tag == "p" || $tag =="div"  || $tag =="br") } {
                          set text [::TEUtils::cleanWhitespaceW $text ] 
                          set text [string trimleft $text]
                        }
                        set text [::TEUtils::mapHTMLspace $text]
                        getHtmlText $tag $text $isTagEnd
                }
        #       if {$curWName==""} {return;}
        #       puts "main,bodyText: $textEditorMain::xmlData($curWName,bodyText)"
        }
        
        
        # bodytext -> 1. text 2. attrKey:currentAttrs 3.Element name(e.g. hi, /hi)
        proc getHtmlText {tag text isTagEnd} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                if {$curWName==""} {return;}
                # add the text and current tag to the ::textEditorMain::xmlData(curWName,bodyText)
                
                #if {$isTagEnd} {set tag "/$tag"}
                
                set curTag $xmlData($curWName,tagName)
                
                if {$isTagEnd} {
                        set curAttrs ""
                } else {
                        # get the last style values
                        if {$tag == "table" | $tag == "tr"} {
                                set curAttrs ""
                        } elseif {$tag == "td" | $tag == "th"} {
                                set curAttrs  [::WordProcess::getStyleMaps [lindex $xmlData($curWName,currentAttrs) end] 2]
                        } elseif {$tag == "a"} {
                                set curAttrs [::WordProcess::getStyleMaps [lindex $xmlData($curWName,currentAttrs) end] 1]
                        } else {
                                set curAttrs [::WordProcess::getStyleMaps [lindex $xmlData($curWName,currentAttrs) end] 0]
                        } 
                }
                set curText ""
                
        #       if {$text != "" & [string trim $text] != "" } 
                if {$text != "" } {
                        set curText [::TEUtils::UTF8FullCodes $text 1]  
                        set curText [::TEUtils::mapHTMLToText $curText]
                } 
                set text [::TEUtils::cleanWhitespaceW $text]
                #puts "getHtmlText.text $text"
                #lappend xmlData($curWName,bodyText) $text [::WordProcess::getStyleMaps [lindex $xmlData($curWName,currentAttrs) end] 0 $isTagEnd] $tag
                #if {[string trim $curAttrs] == "" && $curText == "" && [string trim $curTag] == ""} {return;}
                # clean the empty tag. 
                if {$curText == "" && [string trim $curTag] == ""} {return;}
                # important: {text attrs tag} as a group
                lappend xmlData($curWName,bodyText) [list $text $curAttrs $curTag]
                #puts $xmlData($curWName,bodyText)
        }
        
        # save it for xml form: bodytext -> 1. text 2. attrKey:currentAttrs 3.tag name(e.g. hi, /hi)
        proc analyzeHtmlStartTag {tag param text} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                variable ::TEDB::noParagraph
                if {$curWName==""} {return;}
                # add the xml level
                incr xmlData($curWName,bodyLevel)
                # clean the \t\n\r\v\f
                #set param [::TEUtils::cleanWhitespace $param]
                
                if {[string match -nocase {h[0-9]}  $tag] || $tag == "p" || $tag == "div" || $tag == "br" 
                        || $tag=="span" || $tag=="a" || $tag == "caption"} {
                        #TODO: # url tag has independent tag in html, xml as table tag.
                        if { $tag=="br"} {
                                #lappend xmlData($curWName,bodyText) [list " " "lb" "lb"]
                                set xmlData($curWName,tagName) "lb/"
                        }
                                
                        set attrKey "style"
                        if {$tag == "a"} {
                                if {[string first "name=" $param]>=0} {
                                        set attrKey "name"
                                } elseif {[string first "href=" $param]>=0} {
                                        set attrKey "href"
                                }
                        }
                                                #
                        if {$xmlData(hasHeadStyle) == 1 && $attrKey != "href" && $attrKey != "name" } {set attrKey "styleClass"}
                
                        set param [string trim [::WordProcess::getValidStyleValue $param $attrKey ::TextStyle::htmlXmlMap]]
        
                        
                        #NTEST:
                        if {[string match -nocase {h[0-9]} $tag] || $tag == "p"} {
#                               set newParam "p:Paragraph"
#                               if {[string match -nocase {h[0-9]}  $tag]} {
#                                       set newParam "p:[string map {h headline} $tag]"
#                               }
                                #puts "newParam: $newParam"
                                
                                set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                         $xmlData($curWName,currentAttrs) " " $xmlData($curWName,xmlArrName) \
                                         $xmlData($curWName,bodyLevel)]
                                
                                set xmlData($curWName,tagName) $tag
                                if {$noParagraph} {
                                         set xmlData($curWName,tagName) $TextStyle::htmlXmlMap(span)
                                }                               
                        } else {
                                set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                         $xmlData($curWName,currentAttrs) $param $xmlData($curWName,xmlArrName) \
                                         $xmlData($curWName,bodyLevel)]
                                set xmlData($curWName,tagName) $TextStyle::htmlXmlMap($tag)
                        }
        
                } elseif {$tag=="b" || $tag=="i" || $tag=="u" || $tag=="s" 
                        || $tag=="sub" ||$tag=="sup" } {
                        set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                                         $xmlData($curWName,currentAttrs) $TextStyle::htmlXmlMap($tag) \
                                                         $xmlData($curWName,xmlArrName) $xmlData($curWName,bodyLevel)]
                                
                        set xmlData($curWName,tagName) $TextStyle::htmlXmlMap(span)
        
                } elseif {$tag=="table" | $tag=="tr"| $tag=="td" | $tag == "th"} {
                        # table row cell
                        #Attention: Null in currentAttrs
                        set xmlData($curWName,currentAttrs) ""
                        
                        if {$tag == "table"} {
                                # normal table
                                if {$xmlData(inTable) == 0} {
                                        textEditorMain::resetTableNum
                                        set xmlData(inTable) 1
                                        set xmlData(tableCell,embeddedTable) 0
                                } else {
                                        # table in table cell
                                        #set xmlData(tableCell,embeddedTable) 1
                                        incr xmlData(tableCell,embeddedTable)
                                }
                        } elseif {$tag  == "tr"} {
                                if {$xmlData(tableCell,embeddedTable) == 0} {
                                        #textEditorMain::incrRowNum
                                }
                        } elseif {$tag == "td" | $tag == "th"} {
                                if {$xmlData(tableCell,embeddedTable) == 0} {
                                        # get rowspan/colspan attribute of td, th
                                        # get style-width attribute
                                        set attrName "rowColSpanHtml style"
                                        set param [string trim [::WordProcess::getValidStyleValue $param $attrName ::TextStyle::htmlXmlMap]]
                                        set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                                         $xmlData($curWName,currentAttrs) $param \
                                                         $xmlData($curWName,xmlArrName) $xmlData($curWName,bodyLevel)]
                                }
                        }
                        
                        if {$xmlData(tableCell,embeddedTable) == 0} {
                                set xmlData($curWName,tagName) $TextStyle::htmlXmlMap($tag)
                        }
                } else {
                        set xmlData($curWName,currentAttrs) [::WordProcess::addParamToArrList \
                                         $xmlData($curWName,currentAttrs) "" $xmlData($curWName,xmlArrName)\
                                         $xmlData($curWName,bodyLevel)]
                        set xmlData($curWName,tagName) ""
                }
        }
        
        # save it for xml form: bodytext -> 1. text 2. attrKey:currentAttrs 3.Element name(e.g. hi, /hi)
        proc analyzeHtmlEndTag {tag} {
                variable ::textEditorMain::xmlData
                variable ::textEditorMain::curWName
                variable ::TEDB::noParagraph
                if {$curWName==""} {return;}
                if {$tag == "body"} {
                        set xmlData(main,inBody) 0
                        set xmlData(main,bodyLevel) 0
                        set curWName ""
                        return;
                }
                # tag in htmlXmlMap is valid 
                if {[info exists TextStyle::htmlXmlMap($tag)]} {
                        if {$TextStyle::htmlXmlMap($tag) == "lb"} {
                                set xmlData($curWName,tagName) ""
                        } elseif {$tag=="b" || $tag=="i" || $tag=="u" 
                                || $tag=="s" || $tag=="sub" || $tag=="sup" } {
                                # b,i,u,s,sub,sup has no independent tag name, it can use only <hi>.
                                # this form is from MS-Word.
                                set xmlData($curWName,tagName) "/hi"
                        } else {
                                # paragraph, headline, justify end with tag "p"
                                set xmlData($curWName,tagName) "/$TextStyle::htmlXmlMap($tag)"
                                if {[string match -nocase {h[0-9]} $tag] || $tag == "p"} {
                                        if {$noParagraph} {
                                                 set xmlData($curWName,tagName) "/hi"
                                                 lappend xmlData($curWName,bodyText) [list "" "lb" "lb"]
                                        }
                                }
                        }
                } else {
                        # not valid tag name
                        #set xmlData($curWName,currentAttrs) ""
                        set xmlData($curWName,tagName) ""
                }
                
                # attribute's content
                if {$xmlData($curWName,currentAttrs) != ""} {
                        # end tag then delete the latest tag value
                        set xmlData($curWName,currentAttrs) [::WordProcess::removeLastTagElement $xmlData($curWName,currentAttrs)] 
                }
                
                # table end
                if {$tag=="table" || $tag=="tr"|| $tag=="td" || $tag == "th"} {
                        # table row cell
                        # Attention: Null in currentAttrs
                        set xmlData($curWName,currentAttrs) ""
        
                        #table in table
                        if {$xmlData(tableCell,embeddedTable)>0} {
                                set xmlData($curWName,tagName) ""
                        }
                        if {$tag == "table" && $xmlData(tableCell,embeddedTable)>0} {
                                incr xmlData(tableCell,embeddedTable) -1
                        } elseif {$tag == "table" && !$xmlData(tableCell,embeddedTable)} {
                                set xmlData(inTable) 0
                        }
                } 
                
                # reduce the xml level
                incr xmlData($curWName,bodyLevel) -1
        }
        
        
        
        
        
        
        # xmlToHtmlParse -- 
        #       change xml to html contents.
        #       (need refreshstatus, xins array clear)
        #
        # Arguments:
        #        args:  tag slash param text
        #
        # Return:
        #        none
        proc xmlToHtmlParse {args} {
                variable ::textEditorMain::xmlData
                variable ::TEDB::presetOption
        
                
                # useful tags
                foreach {tag slash param text} $args {
                        #puts "xmlToHtmlParse.tag: $tag - slash: $slash - param: $param - text: $text"
                        # make sure all tag name is low case
                        set tag [string tolower $tag]
                        
                        ::WordProcess::checkHeadStatus $tag $slash xml
                        
                        if {$presetOption(inHead) != 0} {
                                xmlHeadToHTML $tag $slash $param $text
                        }
                        # new teibody
                        if {$tag == "item" && [string first "n='main'" $param] >=0 && $slash != "/"} {
                                # in <body><list><item n='main'>...</item></list></body>
                                set xmlData(main,inBody) 1
                        # new teibody        
                        } elseif {$tag == "item" && $slash == "/"} {
                                # out of item element
                                set xmlData(main,inBody) 0
                                set xmlData(main,bodyLevel) 0
                                foreach lineformatkey {text-align padding-top padding-bottom} {
                                  if {$xmlData(main,$lineformatkey) != "" } {
                                  
                                      append xmlData(main,bodyText) "</div>"
                                   
                                  } 
                                set xmlData(main,$lineformatkey) ""
                                }  
                                
                        }
                        # must in body tag
                        if {!$xmlData(main,inBody)} {continue}
                        
                        if {$xmlData(main,inBody)} {
                                #puts "=> tag= $tag - slash= $slash - param= $param - text= $text"
                                # end with "/" in the slash-parameter
                                set isTagEnd [string equal $slash "/"]
                                set xmlData(main,currentAttrs) ""
                                
                                # end tag and check the current tag
                                if {$isTagEnd} {
                                        incr xmlData(main,inTag) -1
                                        if {[info exists TextStyle::xmlHtmlMap($tag)]} {
                                                set tag $TextStyle::xmlHtmlMap($tag)
                                        } 
                                        if {$tag == "lb"} {
                                                append xmlData(main,bodyText) "<br/>"
                                        } elseif {$tag == "space"} { 
                                                append xmlData(main,bodyText) "</kbd>"
                                        } else {
                                                if {$xmlData(paragraphTag) != "" && $tag == "p"} {set tag $xmlData(paragraphTag); set xmlData(paragraphTag) ""}
                                                append xmlData(main,bodyText) "</$tag>"
                                        }
                                        if {$tag == "p" || $tag == "hi" } {
                                          incr xmlData(main,inRend) -1
                                        }
                                        foreach lineformatkey {text-align padding-top padding-bottom} {
                                          if {($tag == "lb" || $tag == "p") && $xmlData(main,inRend) == 0 && $xmlData(main,$lineformatkey) != "" } {
                                             append xmlData(main,bodyText) "</div>"
                                             set xmlData(main,$lineformatkey) ""
                                          }
                                        }
                                } else {
                                        incr xmlData(main,inTag)
                                        #if {$tag == "body"} {continue;}
                                        if {$tag == "lb"} {continue;}
                                        # clean the \t\n\r\v\f
                                        set param [::TEUtils::cleanWhitespace $param]
                                        if {$tag=="p" | $tag == "div" | $tag=="hi" | $tag == "ref" | $tag == "anchor"} {
                                                # <p>, </p>
                                                #TODO: set current Tag array clean
                                                set resParam ""
                                                set styleKeyList ""                                           
        
                                                # take the keys
                                                if {[string equal -nocase $tag "ref"] || [string equal -nocase $tag "anchor"]} {
                                                        # only one style in <ref> <anchor>
                                                        if {[string first "target=" $param] >=0} {
                                                                set styleKeyList "target"
                                                        } elseif {[string first "ana=" $param] >=0} {
                                                                set styleKeyList "ana"
                                                        } elseif {[string first "n=" $param] >=0} {
                                                                set styleKeyList "n"
                                                        }
                                                } elseif {[string equal -nocase $tag "hi"]} {
                                                        # more styles in <hi> element
                                                        if {[string first "rend=" $param] >=0 } {
                                                                lappend styleKeyList "rend"
                                                        } elseif {[string first "rendition=" $param] >= 0} {
                                                                lappend styleKeyList "rendition"
                                                        }
                                                        # has always rend
                                                        incr xmlData(main,inRend) 1
                                                } 
                                                
                                                
                                                if {[string equal -nocase $tag "p"]} {
                                                        # 1. paragraph
                                                        #set tag "p"
                                                        
                                                        # 2.Headline1-6
                                                        set resParam [::TEUtils::appendValue $resParam [string trim \
                                                                                [::WordProcess::xmlParamToHtml $param rendition ::TextStyle::xmlHtmlMap]] " "]
                                                        
                                                        
                                                        switch -nocase -glob -- $resParam {
                                                                *paragraph* {set tag p}
                                                                *headline* {
                                                                        regexp -nocase {(\d)+} $resParam match
                                                                        if {[info exists match]} {set tag H$match}
                                                                        set xmlData(paragraphTag) $tag
                                                                }
                                                        }
                                                        # has rend
                                                        if {$resParam != {}} {
                                                          incr xmlData(main,inRend) 1
                                                        }
                                                        
                                                } else {
                                                        #  attr=rend;(table) rows,cols,role
                                                        # get all
                                                        foreach key $styleKeyList {
                                                                set resParam [::TEUtils::appendValue $resParam [string trim \
                                                                                 [::WordProcess::xmlParamToHtml $param $key ::TextStyle::xmlHtmlMap]] " "]
                                                        }
                                                                        
                                                        # hi rend, table rows cols, row role, cell,
                                                        set xmlData(main,currentAttrs) $resParam
                                                        if {[info exists TextStyle::xmlHtmlMap($tag)]} {
                                                                set tag $TextStyle::xmlHtmlMap($tag)
                                                        } 
                                                }
                                                
                                                
                                        } elseif {$tag== "table" | $tag=="row" | $tag == "cell"} {
                                                
                                                if {$tag == "table"} {
                                                        # set table's attribute: rows / columns count
                                                        set rowsAttr [string trim [::WordProcess::xmlParamToHtml $param rows ::TextStyle::xmlHtmlMap]]
                                                        set colsAttr [string trim [::WordProcess::xmlParamToHtml $param cols ::TextStyle::xmlHtmlMap]]
                                                        
                                                        set xmlData(main,currentAttrs) [list $rowsAttr $colsAttr]
                                                } elseif {$tag=="row"} {
                                                        # role attribute for future
                                                        #set roleAttr [string trim [::WordProcess::xmlParamToHtml $param role ::TextStyle::xmlHtmlMap]]
                                                        #set xmlData(main,currentAttrs) $roleAttr
                                                        set xmlData(main,currentAttrs) ""
                                                        set tag "tr"
                                                } elseif {$tag == "cell"} {
                                                        #puts "width: attribut in cell"
                                                        set param [string trim [::WordProcess::xmlParamToHtml $param style ::TextStyle::xmlHtmlMap]]
                                                        set xmlData(main,currentAttrs) $param
                                                        set tag "td"
                                                }
                                        } elseif {$tag == "space"} {
                                            set tag "kbd style=\"white-space:pre\""
                                        }
 
                                          
                                        #puts "xmlData(main,currentAttrs) $xmlData(main,currentAttrs)"
                                        
                                        foreach lineformatkey {text-align padding-top padding-bottom} {
                                          set formats $xmlData(main,currentAttrs)
                                          set lineformat "";   
                                          set startlineformat [string first "$lineformatkey:" $formats ]
                                          if {$startlineformat == -1} {continue}
                                          set endlineformat [string first ";" $formats $startlineformat ] 
                                          if {$endlineformat == -1} {set endlineformat "end-1"};
                                          set lineformat [string range $formats $startlineformat $endlineformat ]
                                          
                                          if {$xmlData(main,$lineformatkey) != $lineformat} {
                                            
                                            if {$xmlData(main,$lineformatkey) == "" } {
                                                
                                                append xmlData(main,bodyText) "<div style=\"" $lineformat "\" >"
                                               
                                            } 
                                            
                                            if {$lineformat == "" } {
                                                                                       
                                                append xmlData(main,bodyText) "</div>"
                                                                                        
                                            } 
                                            
                                          }
                                          
                                          #puts "textalign $textalign"
                                          set xmlData(main,$lineformatkey) $lineformat
                                          
                                        }
                                                                         
                                          
                                        if {$xmlData(main,currentAttrs) == ""} {
                                                # new teibody
                                                if {![string equal -nocase $tag "item"] && ![string equal -nocase $tag "list"] && ![string equal -nocase $tag "body"] } {
                                                        append xmlData(main,bodyText) "<$tag>"
                                                }
                                        } else {                                                
                                          
                                                append xmlData(main,bodyText) "<$tag $xmlData(main,currentAttrs)>"
                                        }
                                }
                                
                                # add the text and current tag to the ::textEditorMain::xmlData(main,bodyText)
                                #TODO: TEST:
        #                       if {$text != "" & [string trim $text] != "" }
                                if {$text != ""} {
                                        #set text [::TEUtils::UTF8FullCodes $text 1]    
                                        #set text [::TEUtils::mapXMLToHTML $text]
                                        # get the last style values
                                        append xmlData(main,bodyText) $text
                                }
                                #puts $xmlData(main,bodyText)
                        }
                }
        }
        
        
        # type: tei/html
        proc checkHeadStatus {tag slash type} {
                variable ::TEDB::presetOption
                
                switch -nocase -regexp -matchvar XYZ $type {
                        (tei|xml) {set headTagName teiHeader}
                        (html|xhtml) {set headTagName head}
                        default {set headTagName ""}
                }
                if {$headTagName == ""} {set presetOption(inHead) 0; return;}
                
                set headTag [string equal -nocase $tag $headTagName]
                set endTag [string equal -nocase $slash "/"]
                
                if {$presetOption(inHead) == 0 && $headTag == 1 && $endTag == 0} {
                        # go into head
                        # start tag
                        set presetOption(inHead) 1
                } elseif {$presetOption(inHead) == 1 && $headTag == 1 && $endTag == 1} {
                        # end tag
                        set presetOption(inHead) 2
                } elseif {$presetOption(inHead) == 2 && $headTag == 0} {
                        # out of head
                        set presetOption(inHead) 0
                }
        }
        
        #TODO:
        proc xmlHeadToHTML {tag slash param text} {
                variable ::textEditorMain::xmlData
                variable ::TEDB::presetOption
        
                set isTagEnd [string equal $slash "/"]
                #FIXME: STYLE start tag!
                switch -nocase $tag {
                        
                        tagsDecl {
                                append xmlData(main,headText) [expr {$isTagEnd \
                                        ? "</style>" : "<style type=\"text/css\">"}]
                        }
                        
                        rendition {
                                # text must be changed
                                append xmlData(main,headText) [expr {$isTagEnd ? "" : \
                                        ".[::WordProcess::getAttrValues $param xml:id] \{[::WordProcess::getAttrValues [::WordProcess::xmlParamToHtml rend=[concat [::TEUtils::backToBracket $text]] rend ::TextStyle::xmlHtmlMap] style];\} "}]
                        }
                }
                #puts "xmlData(main,headText): $xmlData(main,headText)"
        }
        
        proc htmlHeadToXML {tag slash param text} {
                variable ::textEditorMain::xmlData
                variable ::TEDB::presetOption
                
                set isTagEnd [string equal $slash "/"]
        
                # style -> tagsDecl
                if {[string equal -nocase $tag "style"]} {
                        if {$isTagEnd} {
                                append xmlData(main,headText) "</tagsDecl></encodingDesc>"
                        } else {
                                append xmlData(main,headText) "<encodingDesc><tagsDecl>"
                                
                                if {$text != ""} {
                                        set nameList ""
                                        set text2 [string trim $text]
                                        set text2 [cleanHtmlHead $text2]
                                        set text2 [::TEUtils::backToBracket $text2]
        
                                        #puts "text2: $text2"
                                        dict for {name value} $text2 {
                                                if {[string first $presetOption(prefix) $name] >= 0} {
                                                        #puts "name: $name\nvalue: $value"
                                                        if {[string first "." $name] == 0} {set name [string range $name 1 end]}
                                                        lappend nameList $name
                                                        # valid fud preset options
                                                        append xmlData(main,headText) "<rendition xml:id=\"$name\">"
                                                        set value [string map {\{ "" \} ""} [::TEUtils::backToBracket $value]]
                                                        set param [string trim [::WordProcess::getValidStyleValue $value headToXML ::TextStyle::htmlXmlMap]]
                                                        if {$name == "${presetOption(prefix)}Paragraph" && $param == "" } {set param $::TextStyle::presetStylesPara}
                                                        append xmlData(main,headText) $param
                                                        append xmlData(main,headText) "</rendition>"
                                                        set xmlData(hasHeadStyle) 1
                                                }
                                        }
                                        # get all the paragraph style without whole html text check
                                        foreach {pName pType} [array get ::TextStyle::presetType] {
                                                set fudPName $TEDB::presetOption(prefix)$pName
                                                if {$pType == "p" && [expr {$fudPName ni $nameList}]} {
                                                        append xmlData(main,headText) "<rendition xml:id=\"$fudPName\">"
                                                        set rValue $TextStyle::presetStyles($pName)
                                                        set rValue [string map {"\{" "" "\}" ""} $rValue]
                                                        set param ""
                                                        dict for {k v} $rValue {
                                                                set temp $k:$v
                                                                set param [::TEUtils::appendValue $param $temp ";"]
                                                        }
                                                        append xmlData(main,headText) $param
                                                        append xmlData(main,headText) "</rendition>"
                                                }
                                        }
                                }
                        }
                } elseif {[string equal -nocase $tag "head"]} {
                        if {$isTagEnd} {
                                append xmlData(main,headText) "</teiHeader>"
                        } else {
                                append xmlData(main,headText) "<teiHeader>"
                        }
                }
                
        }
        
        proc cleanHtmlHead {headContent} {
                # 1. /* ... */
                # 2. <!--...-->
                set pattern {\s*<!--[^-->].*?-->\s*|\s*/\*[^\*/].*?\*/\s*}
                regsub -all $pattern $headContent " " headContent
                #puts $headContent
                return $headContent
        }
        
        
        
        # xmlParamToHtml -- 
        #       change xml attribute to html attribute.
        #       e.g.  rend="overstrike:1;underline:1" -> style='text-decoration: underline overstrike'
        # Arguments:
        #        param:         parameters of xml content (e.g. rend="overstrike:1;underline:1")
        #        attrName:      attribute key word (e.g. rend, rows, cols, role)
        #        mapArr:        look up table for change attributes
        #
        # Return:
        #        Returns the processed html attributes
        proc xmlParamToHtml {param attrName mapArr} {
                if {$param == ""} {return "";}
                upvar 1 $mapArr arrM
                if {![array exists arrM]} {
                   error "\"$mapArr\" isn't an array"
                }
                
                # xmltohtml attrName rend
                set styleList [::WordProcess::getAttrValues $param $attrName]
                #puts "xmlParamToHtml-styleList: $styleList"
                if {$styleList == ""} {return ""}
                
                set inputList [split $styleList ";"]
                
                set htmlStyles ""
                
                if {$attrName == "rend" || $attrName == "style"} {
                        set styles ""
                        foreach style $inputList {
                                #e.g. overstrike:1 size:14 foreground:#00ffff
                                set match ""
                                set key ""
                                set value ""
                                if {[catch {regexp {([^:]+):(.+?)} $style match key value} result]} {
                                        putsstderr "Error: $result"
                                        if {$key == "" || $value == ""} {
                                                putsstderr "Warning: There a empty value in the Style: $style!";
                                        } 
                                        continue;
                                }
                                
                                #puts "style: $style - key: $key - value: $value"
                                if {![info exists arrM($key) ] } {puts "There is wrong value in proc xmlParaToHtml: $key!";continue;}
                                set styleKey $arrM($key) 
                                
                                if {$key == "overstrike"} {
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey "line-through" 1]
                                } elseif {$key == "underline"} {
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey "underline" 1]
                                } elseif {$key == "slant"} {
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey "italic" 0]
                                } elseif {$key == "weight"} {
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey "bold" 0]
                                } elseif {$key == "relief" 
                                        || $key == "family" 
                                        || $key == "background" 
                                        || $key == "foreground"
                                        || $key == "justify"
                                } {
                                        # same value
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey $value 0]
                                } elseif {$key == "borderwidth" 
                                        || $key == "size" || $key == "width" || $key == "height" || $key == "padding-top" || $key == "line-height" || $key == "padding-bottom"} {
                                        # add "px" to the size value
                                        set sizeValue $value
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey $sizeValue 0]
                                } elseif {$key == "offset"} {
                                        set alignValue ""
                                        if {$value == "-4"} {
                                                set alignValue "sub" 
                                        } elseif {$value == "0"} {
                                                set alignValue "baseline"
                                        } elseif {$value == "4"} {
                                                set alignValue "super"
                                        } else {
                                                putsstderr "There is Error in the value of $key!: Value - $value !"
                                                # default
                                                set alignValue "baseline"
                                        }
                                        set styles [::WordProcess::addNewStyleW $styles $styleKey $alignValue 0]
                                } else {
                                        #TODO: justify, bgstipple, fgstipple
                                }       
                        }
                        # Attention: xml to html -> Apostroph '
                        append htmlStyles "style='" $styles "'"
                } elseif {$attrName == "rows" | $attrName == "cols"} {
                        if {![string is integer $styleList]} {
                                putsstderr "$attrName has not a valid integer Number ($styleList)!"
                                return $htmlStyles;
                        }
                        set num $styleList
                        # Attention: xml to html -> Apostroph '
                        append htmlStyles $attrName "='" $num "'"
                } elseif {$attrName == "role" } {
                        append htmlStyles $attrName "='" "data" "'"
                } elseif {$attrName == "target" || $attrName == "n" || $attrName == "ana"} {
                        # link system to html <a href="www.uni-trier.de">
                        append htmlStyles $arrM($attrName) "=\"" $styleList "\""
                } elseif { $attrName == "rendition"} {
                        # clean #f
                        if {[string index $styleList 0] == "#"} {
                                set styleList [string range $styleList 1 end]
                        }
                        append htmlStyles $arrM($attrName) "=\"" $styleList "\""
                } 
                #puts "htmlStyles: $htmlStyles"
                return $htmlStyles;
        }
        
        
        # addNewStyleW -- 
        #       Add the new style to the styleList.
        # Arguments:
        #       styleList:              the input style list (e.g. font-weight:bold;font-style:italic)
        #       styleKey:               stlye key (e.g. font-weight)
        #       styleValue:     style value (e.g. bold)
        #       repeate:                value repeatable or not (e.g. text-decoration can repeatable)
        #
        # Return:
        #        Returns the new style list.
        proc addNewStyleW {styleList styleKey styleValue repeatStyle} {
                if {$styleKey == ""} {puts "It does not input new style value"; return $styleList}
                if {$styleValue == ""} {puts "There is a empty styleValue with $styleKey!"; return $styleList}
                # set new style for input styleKey and styleValue
                set newStyle ""
                set listChanged 0
                # there is no style key in the list or list is empty
                if {$styleList == "" || [::WordProcess::searchKey $styleList $styleKey ";" ":"] == ""} {
                        # add to the first place
                        set styleList [::TEUtils::appendValue $styleList $styleKey:$styleValue ";"]
                } else {
                        # eleList: e.g. "background-color:red color:black font-weight:bold"
                        set eleList [split $styleList ";"]
                        set inSep ":"
                        set matches [lsearch -all $eleList $styleKey$inSep*]
                        # There are more than one styleKey in the styleList.
                        if {[llength $matches] > 1} {
                                putsstderr "Error: There are duplicated element in the $styleList!";
                                return $styleList
                        } elseif {[llength $matches] == 1} {
                                set keyVal ""
                                # found a same styleKey in the styleList.
                                set sameKeyStyle [split [lindex $eleList $matches] $inSep]
                                set sameKeyValue [lindex $sameKeyStyle 1]
                                
                                if {$repeatStyle} {
                                        # There is no same value, add styleValue to oldValue
                                        append sameKeyValue " $styleValue"
                                        # key - value pair
                                        set keyVal $styleKey:$sameKeyValue
                                } else {
                                        # not allow repeate!(e.g. font-style:italic)
                                        set keyVal $styleKey:$styleValue
                                }
                                
                                # refresh the eleList with found index
                                lset eleList $matches $keyVal
                        } else {
                                puts "no match!"
                        }
                        # join <-> split
                        set styleList [join $eleList ";"]
                }
                return $styleList
        }
        
        #****f* textEditor/WordProcess::setParagraphStyle
        # NAME
        #  setParagraphStyle
        # DESCRIPTION
        #  paragraph the selected text with the spacing3=10. 
        # AUTHOR
        #  Xin Zhou
        # CREATION DATE 
        #  15.07.2014
        # SYNOPSIS
        proc setParagraphStyles {text type} {
                # PARAMETERS
                #       *type: the given paragraph's type (Paragraph/Headline/Justify). eg: Headline1, headline2, Justify:center...
                #       *operation: add or remove
                # RETURN VALUE 
                #       None
                # NOTES
                if {$text == "" || $type == ""} {return;}
                #2. new tag "p"
                #make sure first character is upper
                #TODO: stabil
                ::WordProcess::ChangeStyleW $text p [string totitle $type] 0 standard
                
                #4. form: <p> </p> with spacing3=10
        }
        # USES
        #  Variables:
        #       * ::searchTE::lastTextWidget
        #****
        
        #unused
        proc selLine {curText} {
          set selLine [getSelLine $curText]
          set startParaPos [lindex $selLine 0]
          set endParaPos [lindex $selLine 1]

          #2.2 change old seleted tag to the new one
          $curText tag remove sel 1.0 end
          $curText tag add sel $startParaPos $endParaPos
        }
        #unused
        proc getSelLine {curText} {
          #2.1 get new text range
          set selRang [$curText tag ranges sel]
          if {$selRang == ""} {
                  #puts "no sel"
                  set startParaPos [$curText index "insert linestart"] 
                  set endParaPos [$curText index "insert lineend"]
          } else {
                  #puts "sel Rang: $selRang"
                  set startParaPos [$curText index "sel.first linestart"] 
                  set endParaPos [$curText index "sel.last lineend"]
                  #puts "startParaPos: $startParaPos - endParaPos: $endParaPos"
          }
  
          return [list $startParaPos $endParaPos ]
        }
        

        # headline: h1, h2, h3, h4, h5, h6
        proc setHeadlineStyle {headline} {
                # 1. paragraph style
                
        }
        
        # justify: center, left, right
        proc setJustifyStyle {justify} {
                # 1. paragraph style
                
        }
        
        
        
        #End ::WordProcess::
}



#separator --
#       set a separator to the widget.
#
#Arguments:
#       path: the position of separator
#
#Returns:
#       a line as separator.
#
#Side effects
#       Nothing
#
proc separator {path} {
   return [ttk::frame $path -relief groove -borderwidth 2 -width 2 -height 2]
}


# for package
proc SaveContentsToXMLP {text filename dataType {startIdx 1.0} {endIdx end}} {
        if {[catch { set outfile [open $filename "w"] } result] } {tk_messageBox -message $result ;return;}
        fconfigure $outfile -encoding utf-8
#       set src [$text dump -all 1.0 end]
        
        set xmlValue ""
        set teiBody ""
        set teiHead ""
        
        # 1. get the xml value
        append xmlValue "\<\?xml version=\"1.0\" encoding=\"UTF-8\"\?\>"
        append xmlValue "<TEI>"
        
        # body first, then head.
        #body
        set bodyValue [::WordProcess::getXMLBody $text "" $startIdx $endIdx]
        #puts "SaveContentsToXML.xmlBodyValue: $bodyValue"
        # new teibody
        append teiBody "<body><list><item n='main'>"
        append teiBody $bodyValue
        append teiBody "</item></list></body>"
        
        #head
        set teiHead [::WordProcess::getXMLHeader]
        
        append xmlValue $teiHead
        append xmlValue $teiBody
        append xmlValue "</TEI>"
        
        # 2. if type is html, then change xml value to html.
        if {[string equal -nocase $dataType "html"]} {
                set htmlValue ""
                set htmlHead ""
                set htmlBody ""
                append htmlValue "\<html xmlns=\"http://www.w3.org/1999/xhtml\"\>"
                
                #head
                append htmlHead "\<head xmlns=\"http://kompetenzzentrum.uni-trier.de/\"\>"
                append htmlHead "\<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/\>"
                append htmlHead "\<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/\>"
                append htmlHead "\<title\>Text Editor (Tcl/Tk)\</title\>"
                append htmlHead [::WordProcess::changeXMLHeadStyle2HTML $teiHead]
                append htmlHead "\</head\>"
                
                #body
                set teiBody [::WordProcess::TEItoHTML $teiBody]
                append htmlBody "<body>"
                append htmlBody $teiBody
                append htmlBody "</body>"
                
                append htmlValue $htmlHead
                append htmlValue $htmlBody
                
                # html end
                append htmlValue "</html>"
                
                puts $outfile $htmlValue
                close $outfile
                
        } elseif {[string equal -nocase $dataType "xml"]} {
                puts $outfile $xmlValue
                close $outfile
        }
}
        
                                
                                
                                
                                
                                
                                
                                
                                
                                

# for test
proc timeMeasure {procName} {
        set first [lindex $procName 0]
        set startZeit [clock clicks -milliseconds]
        eval $procName
        set endZeit [clock clicks -milliseconds]
        set sekunden [expr ($endZeit - $startZeit)/1000.0]
        puts [format "($first): %.3f sec." $sekunden]
}

proc assert condition {
        if {![uplevel 1 expr $condition]} {
                return -code error "assertion failed: $condition"
        }
}
                                
                                

namespace eval textEditor {
        namespace import ::TEUtils::*
        namespace import ::searchTE::*
        namespace import ::combo::*
        namespace import ::SpecialButton::*
        namespace import ::TEDB::*
        namespace import ::textEditorMain::*
        namespace import ::TextStyle::*
        namespace import ::TEGUI::*
        namespace import ::WordProcess::*
        namespace import ::images::*
        
        namespace export *
        #CONSIDER: ENSEMBLE CREATE IN DOCUMENTATION

                variable textModiMsg 0
        
        proc showMainEditor {} {
                initTextEditor
                showMain
        }
        
        proc setModi {value} {
                        set ::textEditor::textModiMsg $value; 
        }
        proc getModi {} {
                        return $::textEditor::textModiMsg;
        }
        
  
        # the IndexInsert functions check if an manual insertion was made by the user textIndexInsertPress 1 or not 0
        proc setIndexInsert {t} {
                       set ::textEditor::textIndexInsert [$t index insert];                        
         }
        proc getIndexInsert {} {
                       return $::textEditor::textIndexInsert;
        }
  
        proc setIndexInsertPress {bool} {
                       set ::textEditor::textIndexInsertPress $bool;                        
        }
        proc getIndexInsertPress {} {
                       return $::textEditor::textIndexInsertPress;
        }
        
        proc xmlToHtml {inputXmlFile outputHtmlFile} {
                # input file
                set infile [open $inputXmlFile "r"]
                fconfigure $infile -encoding binary
                
                if {$inputXmlFile == "" || ![file exists $inputXmlFile] 
                        || $outputHtmlFile == ""
                } {return "";}
                
                while {![eof $infile]} {
                        # CONSIDER: there are no ram limit?
                        set xmlContents [read $infile]
                }
                # close input file
                close $infile
                
                if {$xmlContents != ""} {
                        # output file
                        set outfile [open $outputHtmlFile "w"]
                        #puts $filename
                        fconfigure $outfile -encoding utf-8
                                
                        set contents [::TEUtils::UTF8FullCodes $xmlContents 1]
                        
                        ::TEUtils::refreshStatus
                        #clean the database options
                        ::TEDB::resetPreset
                        
                        set htmlValue ""
                        set htmlHead ""
                        set htmlBody ""
                        set headValue ""
                        set bodyValue ""
                        
                        
                        set xmlContent [::TEUtils::cleanWhitespace $xmlContents]
                        
                        htmlparse::parse -cmd ::WordProcess::xmlToHtmlParse $xmlContents
                        
                        set htmlHead $textEditorMain::xmlData(main,headText)
                        set htmlBody $textEditorMain::xmlData(main,bodyText)
                        
                        ::TEUtils::refreshStatus
                        #########Analyze done#############
                        
                        ###########Writing################
                        append headValue "\<html xmlns=\"http://www.w3.org/1999/xhtml\"\>"
                        
                        #head
                        append headValue "\<head xmlns=\"http://kompetenzzentrum.uni-trier.de/\"\>"
                        append headValue "\<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/\>"
                        append headValue "\<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"/\>"
                        append headValue "\<title\>Text Editor (Tcl/Tk)\</title\>"
                        append headValue $htmlHead
                        append headValue "\</head\>"
                        
                        #body
                        append bodyValue "<body>"
                        append bodyValue $htmlBody
                        append bodyValue "</body>"
                        
                        append htmlValue $headValue
                        append htmlValue $bodyValue
                        
                        # html end
                        append htmlValue "</html>"
                        
                        puts $outfile $htmlValue
                        # close output file
                        close $outfile
                }
                
                
        }
        
        proc htmlToXml {inputHtmlFile outputXmlFile} {
                initTextEditor
                # input file
                set infile [open $inputHtmlFile "r"]
                fconfigure $infile -encoding binary
                
                if {$inputHtmlFile == "" || ![file exists $inputHtmlFile] 
                        || $outputXmlFile == ""
                } {return "";}
                
                while {![eof $infile]} {
                        # CONSIDER: there are no ram limit?
                        set htmlContents [read $infile]
                }
                
                # close input file
                close $infile
                
                if {$htmlContents != ""} {
                        # output file
                        set outfile [open $outputXmlFile "w"]
                        #puts $filename
                        fconfigure $outfile -encoding utf-8
                        
                        set xmlValue ""
                        set xmlContents [::WordProcess::parseHtmlToXmlContents $htmlContents]
                        # 1. get the xml value
                        append xmlValue "\<\?xml version=\"1.0\" encoding=\"UTF-8\"\?\>"
                        append xmlValue "<TEI>"
                        append xmlValue $xmlContents
                        append xmlValue "</TEI>"
                        
                        puts $outfile $xmlValue
                        # close output file
                        close $outfile
                }
        }
        
        proc loadXmlToText {inputXmlFile text {insertIdx end}} {
                initTextEditor
                # input file
                set infile [open $inputXmlFile "r"]
                fconfigure $infile -encoding binary
                if {$inputXmlFile == "" || ![file exists $inputXmlFile] || ![winfo exists $text]} {return "";}
                
                while {![eof $infile]} {
                        # CONSIDER: there are no ram limit?
                        set xmlContents [read $infile]
                }
                # close input file
                close $infile
                
                if {$xmlContents != ""} {
                        ::TEUtils::refreshStatus
                        set ::textEditorMain::editorText $text
                        # parse html content 
                        # key proc: parseCommend
                        htmlparse::parse -cmd ::WordProcess::parseXmlToText $xmlContents
                        
                        # update tags and fonts to the textEditor       
                        ::TEGUI::addText $text $textEditorMain::xmlData(main,bodyText) $insertIdx
                        
                        ::TEUtils::refreshStatus
                }
        }

        proc loadHtmlToText {inputHtmlFile text {insertIdx end}} {
                initTextEditor
                # input file
                set infile [open $inputHtmlFile "r"]
                fconfigure $infile -encoding binary
                
                if {$inputHtmlFile == "" || ![file exists $inputHtmlFile] 
                        || [winfo exists $text] == ""
                } {return "";}
                
                while {![eof $infile]} {
                        # CONSIDER: there are no ram limit?
                        set htmlContents [read $infile]
                }
                
                # close input file
                close $infile
                
                if {$htmlContents != ""} {
                        set contents [::WordProcess::parseHtmlToXmlContents $htmlContents]
                        # parse xml to text
                        
                        ::TEUtils::refreshStatus
                        set ::textEditorMain::editorText $text
                        # parse html content 
                        # key proc: parseCommend
                        htmlparse::parse -cmd ::WordProcess::parseXmlToText $contents
                        
                        # update tags and fonts to the textEditor       
                        ::TEGUI::addText $text $textEditorMain::xmlData(main,bodyText) $insertIdx
                        ::TEUtils::refreshStatus
                }
        }
        
        proc saveTextToXml {text outputXmlFile} {
                if {![winfo exists $text]} {return;}
                SaveContentsToXMLP $text $outputXmlFile xml
        }
        
        proc saveTextToHtml {text outputHtmlFile} {
                if {![winfo exists $text]} {return;}
                SaveContentsToXMLP $text $outputHtmlFile html
        }
        
        
}

proc putsstderr {report} {
        # puts $report
}
         
# main --
#    Main code
#
if {![info exists notextEditorstandalone]} {
  #puts "standalone"
  ::TEUtils::initTextEditor
  ::textEditor::showMainEditor
}
